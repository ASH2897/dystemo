/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package sentimentclassification;

import java.util.List;
import java.util.Map;

import functionality.UtilCollections;

import sentimentclassification.PolarityCategories.Polarity;

public abstract class SentimentClassifier {
	
	public abstract Map<Polarity, Double> findPolarityWeights(String text) throws Exception;
	
	public Polarity findPolarityFromWeights(Map<Polarity, Double> polarityWeights)
	{
		// return the highest weight or if several - Neutral
		List<Polarity> maxWeightPolarities = UtilCollections.findKeysWithMaxValues(polarityWeights);
		
		if (maxWeightPolarities.size() == 1)
			return maxWeightPolarities.get(0);
		else
			return Polarity.Neutral;
	}
	

	public abstract void printToTextFile(String filename);
	
	public abstract List<String> getFeatureNames();
	
	public abstract int getFeatureNumber();
	public abstract void clear();
	
	
}
