/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification;

import java.util.HashMap;
import java.util.Map;

import parameters.EmotionRecognitionParameters;
import parameters.StringParameters;
import utility.Pair;
import classification.definitions.WeightedClassifierInterface;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import classification.detectors.NonOverlapTermDetector;
import classification.detectors.NonOverlapTermDetectorWithNegations;
import classification.detectors.TermDetectorWithNegations;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class ClassifierLoader {

	private static String getLexiconName(String filePath)
	{
		String resName = filePath;
		int dirInd = filePath.lastIndexOf("/");
		if (dirInd != -1)
			resName = filePath.substring(dirInd + 1);
		try {
			return resName.replace(StringParameters.lexiconExtension, "");
		}
		catch (Exception e) {
			return resName;
		}
	}
	
	/**
	 * This will create the standard weighted classification from the lexicon file with GEW categories
	 * @param lexiconFilename. The lexicon file should contain the data with weights, i.e. each line is <emotionterm>\t<emotion1Weight>\t<emotion2Weight>\t...\t<emotionXXWeight> (following the ids of emotions in the categoriesData, e.g. for GEW: the 20 emotions + No emotion + Other emotion)
	 * @param applyNonOverlap
	 * @return the weightedClassification
	 * @throws Exception 
	 */
	public static WeightedClassifierLexiconBased getLexiconBasedWeightedClassification(String lexiconFilename, CategoryDataType catDataType, boolean applyNonOverlap) throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.loadFromTextFile(lexiconFilename, catDataType, applyNonOverlap);
		lexicon.name = getLexiconName(lexiconFilename);
		updateForDroppingNegations(lexicon);
		return new WeightedClassifierLexiconBased(lexicon);
	}
	
	public static WeightedClassifierLexiconBased getLexiconBasedWeightedClassificationSimple(String lexiconFilename, CategoryDataType catDataType, boolean applyNonOverlap) throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.loadFromTextFile(lexiconFilename, catDataType, applyNonOverlap);
		lexicon.name = getLexiconName(lexiconFilename);
		return new WeightedClassifierLexiconBased(lexicon);
	}

	/**
	 * This will create the standard weighted classification from the lexicon file with GEW categories
	 * @param lexiconFilename. The lexicon file should contain the data with categories assignment, each term may occur several times; each line is <emotionterm>\t<categoryId>
	 * @param applyNonOverlap
	 * @return the weightedClassification
	 * @throws Exception 
	 */
	public static WeightedClassifierLexiconBased getLexiconBasedWeightedClassificationFromCategoryFile(String lexiconFilename, CategoryDataType catDataType, boolean applyNonOverlap) throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.readFromFileWithCategories(lexiconFilename, catDataType, applyNonOverlap);
		lexicon.name = getLexiconName(lexiconFilename);
		updateForDroppingNegations(lexicon);
		return new WeightedClassifierLexiconBased(lexicon);
	}
	
	/**
	 * Joints several lexicons into one with specified weights, and creates the result Classification
	 * @param lexiconFilenames
	 * @param applyNonOverlap
	 * @return
	 * @throws Exception
	 */
	public static WeightedClassifierInterface jointLexiconsBasedWeightedClassification(Map<String,Pair<Double,LexiconFileType>> lexiconFilenames, CategoryDataType catDataType, boolean applyNonOverlap) throws Exception
	{
		//List<WeightedEmotionLexicon> lexicons = new ArrayList<WeightedEmotionLexicon>();
		WeightedEmotionLexicon resLexicon = new WeightedEmotionLexicon();
		Map<String,double[]> resLexData = new HashMap<String,double[]>();
		Map<String, Double> termMultiplier = new HashMap<String, Double>();
		for (Map.Entry<String, Pair<Double,LexiconFileType>> lexiconEntry : lexiconFilenames.entrySet())
		{
			WeightedEmotionLexicon lexicon;
			Pair<Double,LexiconFileType> lexParams = lexiconEntry.getValue();
			LexiconFileType lexType = lexParams.second;
			
			if (lexType == LexiconFileType.Weights)
				lexicon = WeightedEmotionLexicon.loadFromTextFile(lexiconEntry.getKey(), catDataType, applyNonOverlap);
			else if (lexType == LexiconFileType.Categories)
				lexicon = WeightedEmotionLexicon.readFromFileWithCategories(lexiconEntry.getKey(), catDataType, applyNonOverlap);
			else
				lexicon = null;
			
			Map<String,double[]> lexData = lexicon.getAllTermsData();
			
			double multiplierLex = lexParams.first;
			for (Map.Entry<String, double[]> lexDataEntry: lexData.entrySet())
			{
				double[] newWeights = UtilArrays.multiplyArray(lexDataEntry.getValue(), multiplierLex);
				UtilCollections.sumArrayValueToMap(resLexData, lexDataEntry.getKey(), newWeights);
				UtilCollections.incrementIntValueInMap(termMultiplier, lexDataEntry.getKey(), multiplierLex);
			}
		}
		//normalize weight distribution
		for (Map.Entry<String, double[]> lexDataEntry: resLexData.entrySet())
		{
			UtilArrays.multiplyArrayInternal(lexDataEntry.getValue(), 1/termMultiplier.get(lexDataEntry.getKey()));
			//UtilArrays.normalizeArrayInternal(lexDataEntry.getValue());
		}
		
		resLexicon.createSimpleFromGivenData(resLexData, CategoryProcessor.getCategoriesData(catDataType), applyNonOverlap);
		resLexicon.name = "JointLexicon";
		updateForDroppingNegations(resLexicon);
		return new WeightedClassifierLexiconBased(resLexicon);
	}
	
	/**
	 * Sets the negation in classifier. Will work only if EmotionRecognitionParameters.dropNegatedTermsInDetector is set to true.
	 * @param lexicon
	 */
	public static void updateForDroppingNegations(WeightedEmotionLexicon lexicon)
	{
		if (EmotionRecognitionParameters.dropNegatedTermsInDetector)
		{
			if (lexicon.tDetector instanceof NonOverlapTermDetector)
				lexicon.tDetector = new NonOverlapTermDetectorWithNegations(lexicon.tDetector);
			else
				lexicon.tDetector = new TermDetectorWithNegations(lexicon.tDetector);
		}
	}
	
	public enum LexiconFileType {Weights, Categories};
}
