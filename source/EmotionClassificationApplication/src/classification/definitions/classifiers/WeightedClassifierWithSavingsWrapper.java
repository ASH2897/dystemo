/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import utility.Pair;

import linguistic.UtilText;

import classification.definitions.WeightedClassifierInterface;
import classification.detectors.DetectedFeatureOccurrenceDetails;
import data.categories.ICategoriesData;
import data.documents.Tweet;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class WeightedClassifierWithSavingsWrapper implements WeightedClassifierInterface
{
	WeightedClassifierLexiconBased internalClassifier;
	
	public WeightedClassifierWithSavingsWrapper(WeightedClassifierLexiconBased classifier)
	{
		this.internalClassifier = classifier;
		internalClassifier.lexicon.tDetector.savePositions = true; 
		internalClassifier.lexicon.tDetector.saveTokenList = true;
	}

	@Override
	public double[] findCategoryWeights(String text, boolean preprocessText)
			throws Exception {
		return internalClassifier.findCategoryWeights(text, preprocessText);
	}

	@Override
	public double[] findCategoryWeights(String text) throws Exception {
		return internalClassifier.findCategoryWeights(text);
	}

	@Override
	public double[] findCategoryWeights(Tweet tweet, boolean preprocessText)
			throws Exception {
		return internalClassifier.findCategoryWeights(tweet, preprocessText);
	}

	@Override
	public double[] findCategoryWeights(Tweet tweet) throws Exception {
		return internalClassifier.findCategoryWeights(tweet);
	}

	@Override
	public ICategoriesData getCategoriesData() {
		return internalClassifier.getCategoriesData();
	}

	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		return internalClassifier.getDominantCategories(weights);
	}
	
	public List<DetectedFeatureOccurrenceDetails> getLastUsedFeaturesInfo()
	{
		Map<String, List<int[]>> lastPositions = internalClassifier.lexicon.tDetector.getLastPositionsOfFoundTerms();
		List<String> lastTokensList = internalClassifier.lexicon.tDetector.getLastTokenStream();
		
		List<DetectedFeatureOccurrenceDetails> featureOccurrences = new ArrayList<DetectedFeatureOccurrenceDetails>();
		for (String term : lastPositions.keySet())
		{
			for (int[] termPos : lastPositions.get(term))
			{
				DetectedFeatureOccurrenceDetails featDets = new DetectedFeatureOccurrenceDetails();
				featDets.term = term;
				if (internalClassifier.lexicon.tDetector.usePatterns)
				{
					featDets.occurrenceInstance = UtilCollections.join(
							UtilArrays.subList(lastTokensList, termPos[0], termPos[1])
							, " ");
				}
				// check if it is negated 
				String prevToken = (termPos[0] - 1 >= 0)?lastTokensList.get(termPos[0] - 1):"";
				featDets.isNegated = UtilText.isNegationWord(prevToken.toLowerCase());
				
				// check if it is in the end
				featDets.isEnding = (termPos[1] == (lastTokensList.size() - 1));
				featDets.startInd = termPos[0];
				featDets.isIntensified = UtilText.isIntensifierWord(prevToken.toLowerCase());
				featDets.isDiminished = UtilText.isDiminisherWord(prevToken.toLowerCase());
				featureOccurrences.add(featDets);
			}
		}
		return featureOccurrences;
	}
	
	
	public Pair<List<String>, Map<String, List<int[]>>> getLastFoundPos()
	{
		return new Pair (internalClassifier.lexicon.tDetector.getLastTokenStream(), internalClassifier.lexicon.tDetector.getLastPositionsOfFoundTerms());
	}

	public double[] getWeightsOfFeature(String term)
	{
		return internalClassifier.lexicon.getWeightForTerm(term);
	}
}
