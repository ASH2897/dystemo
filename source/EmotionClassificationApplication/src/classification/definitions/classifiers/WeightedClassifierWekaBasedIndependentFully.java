/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import linguistic.TermDetectionParameters;

import classification.ClassificationUtil;
import classification.definitions.BinaryClassifier;
import classification.definitions.WeightedClassifier;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithNegations;
import classification.featurerepresentation.WekaAttributesSet;
import data.categories.CategoryProcessor.CategoryDataType;


import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

/**
 * This is the classifier to apply separate binary classifier to each category.
 * The difference from ClassifierWekaBasedIndependent is that in this class the feature representation for each category can be different (saved and applied separately)
 * 
 */
public class WeightedClassifierWekaBasedIndependentFully extends AbstractWeightedClassifierWekaBased implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8196778898165740622L;
	protected List<weka.classifiers.Classifier> wekaClassifiersForCategories;
	protected List<WekaAttributesSet> attributesSets;
	protected List<Instances> correspondingBinaryProblems;
	
	public WeightedClassifierWekaBasedIndependentFully(List<weka.classifiers.Classifier> wekaClassifiersForCategories, 
			List<WekaAttributesSet> attributesSets, List<Instances> correspondingBinaryProblems)
	{
		this.wekaClassifiersForCategories = wekaClassifiersForCategories;
		this.attributesSets = attributesSets;
		this.correspondingBinaryProblems = correspondingBinaryProblems;
		
		emptyInstancePerCategory = new ArrayList<Instance>();
		for (WekaAttributesSet attrSet : attributesSets)
		{
			if (attrSet == null)
			{
				emptyInstancePerCategory.add(null);
				continue;
			}
			double[] curDefValues = new double[attrSet.size()];
			Arrays.fill(curDefValues, 0.0);
			
			Instance curEmptyInstance = new SparseInstance(1.0, curDefValues);
			
			emptyInstancePerCategory.add(curEmptyInstance); 
		}
	}
	
	public WeightedClassifierWekaBasedIndependentFully(WeightedClassifierWekaBasedIndependentFully initial)
	{
		this(initial.wekaClassifiersForCategories, initial.attributesSets, initial.correspondingBinaryProblems);
	}
	
	protected List<Instance> emptyInstancePerCategory = null;
	
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText) throws Exception 
	{

		double[] resWeights = new double[this.getCategoriesData().getCategoryNum()];
		
		for (int catInd = 0; catInd < wekaClassifiersForCategories.size(); ++catInd)
		{
			weka.classifiers.Classifier curCategoryClassifier = wekaClassifiersForCategories.get(catInd);
			if (curCategoryClassifier == null)
				continue;
			
			Set<Integer> foundAttributes = attributesSets.get(catInd).findAttributesInText(text, toPreprocessText);

			Instance inst = new SparseInstance(emptyInstancePerCategory.get(catInd));
			inst.setWeight(1.0);
			inst.setDataset(correspondingBinaryProblems.get(catInd));
			
			for (Integer attrIndex : foundAttributes)
				inst.setValue(attrIndex, 1.0);
			resWeights[catInd] = findSpecificClassWeight(curCategoryClassifier, inst);
		}
		
		return resWeights;
	}
	

	@Override
	public void printToTextFile(String filename) {
		
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
			        new FileOutputStream(filename));
			oos.writeObject(this);	
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<String> getFeatureNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		return ClassificationUtil.getMaximalCategoriesIds(weights, this.emotionCategoriesType);
	}
	
	@Override
	public WeightedClassifier clone() {
		WeightedClassifier resClassifier = new WeightedClassifierWekaBasedIndependentFully(wekaClassifiersForCategories, attributesSets, correspondingBinaryProblems);
		resClassifier.defaultPreprocessing = this.defaultPreprocessing;
		return resClassifier;
	}
	
	@Override
	public int getFeatureNumber() {
		
		return 0;
	}
	
	@Override
	public void clear()
	{
		for (int catInd = 0; catInd < wekaClassifiersForCategories.size(); ++catInd)
		{
			wekaClassifiersForCategories.set(catInd, null);
		}
		
		wekaClassifiersForCategories.clear();
		attributesSets.clear();
		correspondingBinaryProblems.clear();
	}

	public static WeightedClassifierWekaBasedIndependentFully  readFromBinaryFile(String filename) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object classifier = ois.readObject();	
		ois.close();
		WeightedClassifierWekaBasedIndependentFully  resClassifier = (WeightedClassifierWekaBasedIndependentFully)classifier;
		
		return resClassifier;
	}
	

	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			for (int catInd = 0; catInd <  attributesSets.size(); ++catInd)
			{
				WekaAttributesSet attrSet = attributesSets.get(catInd);
				if (attrSet == null)
					continue;
				
				attrSet.tDetector = TermDetectorUtil.updateTermDetectorForParameters(termDetectionParameters,  attrSet.tDetector);
			}
		}
		
	}
}
