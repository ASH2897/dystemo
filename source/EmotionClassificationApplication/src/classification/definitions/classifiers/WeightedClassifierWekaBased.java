/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import linguistic.TermDetectionParameters;

import classification.ClassificationUtil;
import classification.definitions.WeightedClassifier;
import classification.detectors.NonOverlapTermDetector;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithNegations;
import classification.detectors.TermDetectorWithProcessing;
import classification.featurerepresentation.WekaAttributesSet;
import data.categories.CategoryProcessor.CategoryDataType;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

/**
 * This is a weka-based classifier wrapper for emotion recognitin.
 * 
 *	IMPORTANT: 	when no used features are found in the text, No Emotion will be returned instead of the majority class as returned usually by weka classifier 
 *
 */
public class WeightedClassifierWekaBased extends WeightedClassifier implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 538821265177040128L;
	weka.classifiers.Classifier wekaCategoryClassifier;
	WekaAttributesSet attributesSet;
	Instances correspondingCategoryProblem;

	public WeightedClassifierWekaBased(weka.classifiers.Classifier wekaCategoryClassifier, 
			WekaAttributesSet attributesSet, Instances correspondingCategoryProblem)
	{
		this.wekaCategoryClassifier = wekaCategoryClassifier;
		this.attributesSet = attributesSet;
		this.correspondingCategoryProblem = correspondingCategoryProblem;
		
		double[] defaultAttValues = new double[attributesSet.size()];
		Arrays.fill(defaultAttValues, 0.0);
		emptyInstance = new SparseInstance(1.0, defaultAttValues);
	}
	
	@Override
	public double[] findCategoryWeights(String text) throws Exception {
		return findCategoryWeights(text, defaultPreprocessing);
	}

	Instance emptyInstance = null;
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText)
			throws Exception {
		
		Set<Integer> foundAttributes = attributesSet.findAttributesInText(text, toPreprocessText);
		
		double[] resWeights;
		if (foundAttributes == null || foundAttributes.size() == 0)
		{
			// return No emotion weights
			resWeights = ClassificationUtil.getNeutralWeightAllocation(emotionCategoriesType.toString());
		}
		else
		{
			Instance inst = new SparseInstance(emptyInstance);
			inst.setWeight(1.0);
			inst.setDataset(correspondingCategoryProblem);
			
			for (Integer attrIndex : foundAttributes)
				inst.setValue(attrIndex, 1.0);
			
			resWeights = ClassificationUtil.createNewWeightsForEmotionCategories(emotionCategoriesType);
			
			double[] distr = wekaCategoryClassifier.distributionForInstance(inst);
			
			double classInd = wekaCategoryClassifier.classifyInstance(inst);
			
			System.arraycopy(distr, 0, resWeights, 0, distr.length);
		}
		return resWeights;
		
		
	}

	@Override
	public void printToTextFile(String filename) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
			        new FileOutputStream(filename));
			oos.writeObject(this);	
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<String> getFeatureNames() {
		return new ArrayList<String>(attributesSet.getFeatureSet());
	}

	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		return ClassificationUtil.getMaximalCategoriesIds(weights, emotionCategoriesType);
	}

	@Override
	public WeightedClassifier clone() {
		WeightedClassifier resClassifier = new WeightedClassifierWekaBased(wekaCategoryClassifier, attributesSet, correspondingCategoryProblem);
		resClassifier.defaultPreprocessing = this.defaultPreprocessing;
		return resClassifier;
	}
	
	@Override
	public int getFeatureNumber() {
		return attributesSet.size();
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}

	public static WeightedClassifierWekaBased readFromBinaryFile(String filename) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object classifier = ois.readObject();	
		ois.close();
		WeightedClassifierWekaBased resClassifier = (WeightedClassifierWekaBased)classifier;
		
		return resClassifier;
	}

	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			attributesSet.tDetector = TermDetectorUtil.updateTermDetectorForParameters(termDetectionParameters,  attributesSet.tDetector);
		}
		
	}
	
}
