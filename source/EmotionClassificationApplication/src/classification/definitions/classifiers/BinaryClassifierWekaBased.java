/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import linguistic.TermDetectionParameters;

import classification.definitions.BinaryClassifier;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithNegations;
import classification.featurerepresentation.SyntacticFeaturesSet;
import classification.featurerepresentation.WekaAttributesSet;
import classification.featurerepresentation.WekaAttributesSetWithSyntFeatures;

import data.documents.Tweet;
import data.documents.TweetFeaturesExtended;
import functionality.UtilArrays;

import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class BinaryClassifierWekaBased extends BinaryClassifier implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3128029221174318935L;

	weka.classifiers.Classifier wekaCategoryClassifier;
	WekaAttributesSet attributesSet;
	Instances correspondingProblem;

	public BinaryClassifierWekaBased(weka.classifiers.Classifier wekaCategoryClassifier, 
			WekaAttributesSet attributesSet, Instances correspondingCategoryProblem)
	{
		this.wekaCategoryClassifier = wekaCategoryClassifier;
		this.attributesSet = attributesSet;
		this.correspondingProblem = correspondingCategoryProblem;
		
		defaultAttValues = new double[attributesSet.size()];
		Arrays.fill(defaultAttValues, 0.0);
	}
	
	double[] defaultAttValues = null;
	
	@Override
	/**
	 * This method is used if no other (syntactic) features were included
	 */
	public int findClassIndex(String text, boolean preprocessText)
			throws Exception 
	{
		Set<Integer> foundAttributes = attributesSet.findAttributesInText(text, preprocessText); 
		Instance inst = new SparseInstance(1.0, defaultAttValues);
		inst.setDataset(correspondingProblem);
		
		for (Integer attrIndex : foundAttributes)
			inst.setValue(attrIndex, 1.0);
		
		double classInd = wekaCategoryClassifier.classifyInstance(inst);
		
		if (classInd > 0)
			return 1;
		else
			return -1;
	}

	@Override
	/**
	 * This method is used if no other (syntactic) features were included
	 */
	public int findClassIndex(String text) throws Exception {
		return findClassIndex(text, defaultPreprocessing);
	}
	
	
	
	/**
	 * This method is used if other (syntactic) features are included
	 */
	public int findClassIndex(String text, boolean preprocessText, Map<Integer,Integer> otherFeatures)
			throws Exception 
	{
		Set<Integer> foundAttributes = attributesSet.findAttributesInText(text, preprocessText); 
		
		Instance inst = new SparseInstance(1.0, defaultAttValues);
		inst.setDataset(correspondingProblem);
		
		for (Integer attrIndex : foundAttributes)
			inst.setValue(attrIndex, 1.0);
		
		if (attributesSet instanceof WekaAttributesSetWithSyntFeatures)
		{	
			Map<Integer,Integer> syntFeatures = ((WekaAttributesSetWithSyntFeatures)attributesSet).updateSyntFeatureIndexes(otherFeatures);
			for (Map.Entry<Integer, Integer> feat : syntFeatures.entrySet())
				inst.setValue(feat.getKey(), feat.getValue());
		}
		
		double classInd = wekaCategoryClassifier.classifyInstance(inst);
		
		if (classInd > 0)
			return 1;
		else
			return -1;
	}
	
	/** Returns the index of the found class (1 or -1) based on the tweet. It will be either preprocessed first or not depending on the 'preprocessText' parameter. */
	@Override
	public int findClassIndex(Tweet tweet, boolean preprocessText) throws Exception
	{
		if (tweet instanceof TweetFeaturesExtended)
			return findClassIndex(tweet.text, preprocessText, SyntacticFeaturesSet.parseFeatureValues((TweetFeaturesExtended)tweet));
		else
			return findClassIndex(tweet.text, preprocessText);
	}

	@Override
	public void printToTextFile(String filename) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
			        new FileOutputStream(filename));
			oos.writeObject(this);	
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static BinaryClassifierWekaBased readFromTextFile(String filename) throws Exception {
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object classifier = ois.readObject();	
		ois.close();
		BinaryClassifierWekaBased resClassifier = (BinaryClassifierWekaBased)classifier;
		
		return resClassifier;
	}

	@Override
	public void clear() {
		wekaCategoryClassifier = null;
		attributesSet = null;
		correspondingProblem = null;
	}

	@Override
	public void printToFileFeatureDescription(String filename) {
		
		if (wekaCategoryClassifier instanceof NaiveBayesMultinomial)
		{
			
			NaiveBayesMultinomial nbClassifier = (NaiveBayesMultinomial)wekaCategoryClassifier;
			try {
				PrintWriter pw = new PrintWriter(filename);
				pw.println(nbClassifier.toString());
				/*
				 * pw.println("feature\tclass0\tclass1");
				
				for (String feature : attributesSet.getFeatureSet())
				{
					double[] featureDistr = this.getGeneralFeatureDistribution(feature);
					pw.println(feature + "\t" + UtilArrays.join(featureDistr, "\t"));
				}*/
				pw.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/*
	public double[] getGeneralFeatureDistribution(String featureText) throws Exception
	{
		Integer featureInd = attributesSet.getAttributeIndexes().get(featureText);
		if (featureInd == null)
			return null;
		Instance inst = new SparseInstance(1.0, defaultAttValues);
		inst.setDataset(correspondingProblem);
		
		inst.setValue(featureInd, 1.0);
		
		double[] distr = wekaCategoryClassifier.distributionForInstance(inst);
		return distr;
	}*/
	
	Object featuresDistrInfoSaved;
	
	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			attributesSet.tDetector = TermDetectorUtil.updateTermDetectorForParameters(termDetectionParameters,  attributesSet.tDetector);
		}
		
	}
}
