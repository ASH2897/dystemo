/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.util.HashMap;
import java.util.Map;

import classification.definitions.lexicons.WeightedEmotionLexiconWithIntensityModel;
import classification.definitions.lexicons.WeightedEmotionLexicon;

import processing.TweetPreprocessing;
import weka.classifiers.Classifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import functionality.UtilArrays;

/**
 * This classifier in addition applies the model of strength/intensity to simple weighted classifier.
 *
 */
public class WeightedClassifierLexiconBasedWithIntensityModel extends WeightedClassifierLexiconBased{

	public WeightedClassifierLexiconBasedWithIntensityModel(WeightedEmotionLexicon lexicon) {
		super((lexicon instanceof WeightedEmotionLexiconWithIntensityModel)?lexicon:new WeightedEmotionLexiconWithIntensityModel(lexicon));
		if (!(lexicon instanceof WeightedEmotionLexiconWithIntensityModel))
			System.out.println("Warning: You created complex classification with wrong lexicon! It was adjusted accordingly");
	}
	
	public WeightedClassifierLexiconBasedWithIntensityModel(WeightedClassifierLexiconBased classifier)
	{
		super((classifier.lexicon instanceof WeightedEmotionLexiconWithIntensityModel)?classifier.lexicon:new WeightedEmotionLexiconWithIntensityModel(classifier.lexicon));
		if (!(lexicon instanceof WeightedEmotionLexiconWithIntensityModel))
			System.out.println("Warning: You created complex classification with wrong lexicon! It was adjusted accordingly");
	}
	
	public double[] findCategoryWeights(String text) throws Exception
	{
		double[] categoryWeights = new double[lexicon.getCategoriesToUseNum()];
		WeightedEmotionLexiconWithIntensityModel complexLexicon = (WeightedEmotionLexiconWithIntensityModel)lexicon;
		Map<String, Double> foundTerms = complexLexicon.findEmotionalTermsWithCoefInText(text);
		Map<String, Integer> occurrences = complexLexicon.getLastOccurrences();
		
		double sumEmoStrength = 0.0;
		int sumOccurrences = 0;
		
		for (String term : foundTerms.keySet())
		{
			if (complexLexicon.containsTerm(term))
			{
				categoryWeights = UtilArrays.sumTwoArrays(categoryWeights, 
														  UtilArrays.multiplyArray(
																  		complexLexicon.getWeightForTerm(term), 
																		foundTerms.get(term) * complexLexicon.getStrengthForTerm(term)));
				sumEmoStrength += foundTerms.get(term) * complexLexicon.getStrengthForTerm(term);
				sumOccurrences += occurrences.get(term);
			}
		}
		
		strengthData = new StrengthAttributes();
		strengthData.sumEmoOccurrences = sumOccurrences;
		strengthData.sumEmoStrength = sumEmoStrength;
		strengthData.wordsWithRepeatedLetters = TweetPreprocessing.getNumberOfTimesRepeatedCharacters(text);
		strengthData.punctuationNumber = TweetPreprocessing.getNumberOfPunctuationSigns(text, "?!");
		strengthData.tabSpelledNumber = TweetPreprocessing.getNumberOfTabWords(text);
		
		return UtilArrays.normalizeArray(categoryWeights);
	}
	
	public Map<String, double[]> findCategoryWeightsReasons(String text) throws Exception
	{
		Map<String, double[]> resfoundTerms = new HashMap<String, double[]>();
		
		Map<String, Double> foundTerms = ((WeightedEmotionLexiconWithIntensityModel)lexicon).findEmotionalTermsWithCoefInText(text);
		for (String term : foundTerms.keySet())
		{
			if (lexicon.containsTerm(term))
				resfoundTerms.put(term, UtilArrays.multiplyArray(
											lexicon.getWeightForTerm(term), 
											foundTerms.get(term)));
		}
		return resfoundTerms;
	}
	
	public static class StrengthAttributes
	{
		double sumEmoStrength;
		int sumEmoOccurrences;
		int punctuationNumber;
		int tabSpelledNumber;
		int wordsWithRepeatedLetters;
		
	}
	StrengthAttributes strengthData;
	
	public StrengthAttributes getStrengthData()
	{
		return strengthData;
	}
	
	Classifier wekaStrengthClassifier;
	Instances wekaStrengthTrainData;
	
	public void setStrengthClassifier(Classifier wekaStrengthClassifier, Instances wekaStrengthTrainData )
	{
		this.wekaStrengthClassifier = wekaStrengthClassifier;
		this.wekaStrengthTrainData = wekaStrengthTrainData;
	}
	
	public int computeStrengthLabel() throws Exception
	{
		//TODO: use saved StrengthAttributes for strength classifier
		if (wekaStrengthClassifier == null)
			return 2;
		
		if (strengthData.sumEmoOccurrences == 0)
			return 0;
		
		// else need to compute the strength by ML classifier
		// construct instance:
		Instance testInst = constructInstanceForStrengthDetection(wekaStrengthTrainData, strengthData, 0);
		testInst.setDataset(wekaStrengthTrainData);
		
		double wekaOutput = wekaStrengthClassifier.classifyInstance(testInst);
		
		return (int) Math.round(wekaOutput);
	}

	
	public static Instance constructInstanceForStrengthDetection(Instances wekaProblem, StrengthAttributes strengthFeatures, double averageStrength)
	{
		Instance instance = new DenseInstance(6);
		instance.setDataset(wekaProblem);
		
		instance.setValue(wekaProblem.attribute(0), Integer.toString(getClosestStrengthLabel(averageStrength)));
		instance.setValue(wekaProblem.attribute(1),  strengthFeatures.sumEmoStrength);
		instance.setValue(wekaProblem.attribute(2),  new Integer(strengthFeatures.sumEmoOccurrences).doubleValue());
		instance.setValue(wekaProblem.attribute(3),  new Integer(strengthFeatures.punctuationNumber).doubleValue());
		instance.setValue(wekaProblem.attribute(4),	 new Integer( strengthFeatures.tabSpelledNumber).doubleValue());
		instance.setValue(wekaProblem.attribute(5),  new Integer(strengthFeatures.wordsWithRepeatedLetters).doubleValue());
		
		return instance;
	}
	
	public static int getClosestStrengthLabel(double strengthValue)
	{
		if (strengthValue >= 3.0)
			return 3;
		if (strengthValue <= 0.0)
			return 0;
		if (Math.floor(strengthValue) - strengthValue == 0.5)
			return (int) Math.floor(strengthValue);
		else
			return (int) Math.round(strengthValue);
	}
	
	
	@Override
	public String toString()
	{
		return "ComplexWeightedClassifier-" + lexicon.name;
	}
}
