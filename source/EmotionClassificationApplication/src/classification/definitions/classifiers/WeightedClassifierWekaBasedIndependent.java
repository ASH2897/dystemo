/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import linguistic.TermDetectionParameters;

import classification.ClassificationUtil;
import classification.definitions.WeightedClassifier;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithNegations;
import classification.featurerepresentation.WekaAttributesSet;
import data.categories.CategoryProcessor.CategoryDataType;



import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

/**
 * This is the classifier to apply separate binary classifier to each category.
 * Important note: in this implementation, feature representation is shared between the categories (all the same)
 */
public class WeightedClassifierWekaBasedIndependent extends AbstractWeightedClassifierWekaBased implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8196778898165740622L;
	List<weka.classifiers.Classifier> wekaClassifiersForCategories;
	WekaAttributesSet attributesSet;
	Instances correspondingBinaryProblem;
	
	public WeightedClassifierWekaBasedIndependent(List<weka.classifiers.Classifier> wekaClassifiersForCategories, 
			WekaAttributesSet attributesSet, Instances correspondingBinaryProblem)
	{
		this.wekaClassifiersForCategories = wekaClassifiersForCategories;
		this.attributesSet = attributesSet;
		this.correspondingBinaryProblem = correspondingBinaryProblem;
		
		double[] defaultAttValues = new double[attributesSet.size()];
		Arrays.fill(defaultAttValues, 0.0);
		emptyInstance = new SparseInstance(1.0, defaultAttValues);
		
	}
	
	public WeightedClassifierWekaBasedIndependent(WeightedClassifierWekaBasedIndependent initial)
	{
		this(initial.wekaClassifiersForCategories, initial.attributesSet, initial.correspondingBinaryProblem);
	}
	
	/** returns the weights of emotions. the text should be already preprocessed*/
	@Override
	public double[] findCategoryWeights(String text) throws Exception 
	{
		return findCategoryWeights(text, defaultPreprocessing);
	}
	
	Instance emptyInstance = null;
	
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText) throws Exception 
	{
		Set<Integer> foundAttributes = attributesSet.findAttributesInText(text, toPreprocessText);
		
		Instance inst = new SparseInstance(emptyInstance);
		inst.setWeight(1.0);
		inst.setDataset(correspondingBinaryProblem);
		
		for (Integer attrIndex : foundAttributes)
			inst.setValue(attrIndex, 1.0);
		
		double[] resWeights = new double[this.getCategoriesData().getCategoryNum()];
		
		for (int catInd = 0; catInd < wekaClassifiersForCategories.size(); ++catInd)
		{
			resWeights[catInd] = findSpecificClassWeight( wekaClassifiersForCategories.get(catInd), inst);
		}
		
		return resWeights;
	}
	

	@Override
	public void printToTextFile(String filename) {
		
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
			        new FileOutputStream(filename));
			oos.writeObject(this);	
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<String> getFeatureNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		return ClassificationUtil.getMaximalCategoriesIds(weights, this.emotionCategoriesType);
	}
	
	@Override
	public WeightedClassifier clone() {
		WeightedClassifier resClassifier = new WeightedClassifierWekaBasedIndependent(wekaClassifiersForCategories, attributesSet, correspondingBinaryProblem);
		resClassifier.defaultPreprocessing = this.defaultPreprocessing;
		return resClassifier;
	}
	
	@Override
	public int getFeatureNumber() {
		
		return 0;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			attributesSet.tDetector = TermDetectorUtil.updateTermDetectorForParameters(termDetectionParameters,  attributesSet.tDetector);
		}
		
	}
	
	public static WeightedClassifierWekaBasedIndependent readFromBinaryFile(String filename) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object classifier = ois.readObject();	
		ois.close();
		WeightedClassifierWekaBasedIndependent resClassifier = (WeightedClassifierWekaBasedIndependent)classifier;
		
		return resClassifier;
	}

}
