/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.util.Map;

import linguistic.TermDetectionParameters;

import classification.definitions.BinaryClassifier;
import classification.detectors.TermDetectorWithNegations;

import functionality.UtilCollections;

/**
 * Not fully functional class. 
 * It is used only to store intermediate results for now. */
public class BinaryClassifierLexiconBased extends BinaryClassifier {

	public Map<String, Double> lexicon;
	
	public BinaryClassifierLexiconBased(Map<String, Double> lexicon)
	{
		this.lexicon = lexicon;
	}
	
	@Override
	public int findClassIndex(String text, boolean preprocessText)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int findClassIndex(String text) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void printToTextFile(String filename) {
		try
		{
			UtilCollections.printToFileMap(lexicon, filename);
		}
		catch (Exception e)
		{
			System.err.println("Not able to write a lexicon file!");
		}
	}

	public static BinaryClassifierLexiconBased readFromTextFile(String filename) {
	
		try
		{
			Map<String, Double> lexicon = UtilCollections.readMapDoubleFromFile(filename);
			return new BinaryClassifierLexiconBased(lexicon);
		}
		catch (Exception e)
		{
			System.err.println("Not able to read a lexicon from a file!");
		}
		return null;
	}

	@Override
	public void clear() {
		lexicon.clear();
		lexicon = null;
	}

	@Override
	public void printToFileFeatureDescription(String filename) {
		printToTextFile(filename);
	}

	@Override
	public void setupTermDetectionParameters(
			TermDetectionParameters termDetectionParameters) {
		try {
			throw new Exception("Setting term detection parameters is impossible for this non-real classifier");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	


}
