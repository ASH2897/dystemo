/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions;

import java.util.List;

import data.categories.ICategoriesData;
import data.documents.Tweet;


public interface WeightedClassifierInterface {
	
	/** Returns the non-normalized weights of emotion categories based on the text. It will be either preprocessed first or not depending on the 'preprocessText' parameter. */
	public double[] findCategoryWeights(String text, boolean preprocessText) throws Exception;
	
	/** Returns the non-normalized weights of emotion categories based on the text. */
	public double[] findCategoryWeights(String text) throws Exception;
	
	/** Returns the non-normalized weights of emotion categories based on the text. It will be either preprocessed first or not depending on the 'preprocessText' parameter. */
	public double[] findCategoryWeights(Tweet tweet, boolean preprocessText) throws Exception;
	
	/** Returns the non-normalized weights of emotion categories based on the text. */
	public double[] findCategoryWeights(Tweet tweet) throws Exception;
	
	/** Returns the emotion categories used by the classifier */
	public ICategoriesData getCategoriesData();
	
	/** Returns the identifiers of the dominant categories */
	public List<Integer> getDominantCategories(double[] weights);
	
}
