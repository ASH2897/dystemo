/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions;

import linguistic.TermDetectionParameters;
import data.documents.Tweet;


public abstract class BinaryClassifier implements BinaryClassifierInterface {

	public boolean defaultPreprocessing = false;
	private String classifierName;

	/** Returns the index of the found class (1 or -1) based on the text. It will be either preprocessed first or not depending on the 'preprocessText' parameter. */
	public abstract int findClassIndex(String text, boolean preprocessText) throws Exception;
	
	/** Returns the index of the found class (1 or -1) based on the text. */
	public abstract int findClassIndex(String text) throws Exception;
	
	/** Returns the index of the found class (1 or -1) based on the tweet. It will be either preprocessed first or not depending on the 'preprocessText' parameter. */
	public int findClassIndex(Tweet tweet, boolean preprocessText) throws Exception
	{
		return findClassIndex(tweet.text, preprocessText);
	}
	
	/** Returns the index of the found class (1 or -1) based on the tweet. */
	public int findClassIndex(Tweet tweet) throws Exception
	{
		return findClassIndex(tweet, defaultPreprocessing);
	}
	
	public abstract void printToTextFile(String filename);

	public abstract void clear();
	
	public abstract void printToFileFeatureDescription(String filename);
	
	public abstract void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters);
	
}
