/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.lexicons;

import java.io.FileReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.TweetPreprocessing;
import au.com.bytecode.opencsv.CSVReader;
import classification.detectors.NonOverlapTermDetector;
import classification.detectors.TermDetectorWithProcessing;
import classification.detectors.TermDetectorWithModifiers;
import data.categories.CategoriesDataGEW;
import data.categories.CategoriesDataOLD;
import data.categories.CategoriesMappingOLDtoGEW;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import functionality.UtilCollections;
import functionality.UtilFiles;


public class WeightedEmotionLexiconWithIntensityModel extends WeightedEmotionLexicon implements Serializable{
	
	
	private static final long serialVersionUID = -3641958314619950212L;
	TermDetectorWithModifiers coefTermDetector;
	
	public WeightedEmotionLexiconWithIntensityModel()
	{
		
	}
	public WeightedEmotionLexiconWithIntensityModel(WeightedEmotionLexicon weightedLexicon)
	{
		this.lexiconData = weightedLexicon.lexiconData;
		this.categoriesData = weightedLexicon.categoriesData;
		this.params = weightedLexicon.params;
		this.tDetector = weightedLexicon.tDetector;
		this.name = weightedLexicon.name;
		updateTermDetector();
		updateTermInfoToComplex();
	}
	
	private void updateTermDetector()
	{
		coefTermDetector = new TermDetectorWithModifiers(tDetector);
	}
	
	private void updateTermInfoToComplex()
	{
		Map<String, TermInfo> lexiconDataNew = new HashMap<String, TermInfo>();
		for (String term: lexiconData.keySet())
		{
			lexiconDataNew.put(term, new ComplexTermInfo(lexiconData.get(term)));
		}
		lexiconData = lexiconDataNew;
	}
	
	static class ComplexTermInfo extends WeightedEmotionLexicon.TermInfo implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -4634544854123729089L;
		double strength = 1;
		
		ComplexTermInfo (TermInfo termInfo)
		{
			this.categoryWeights = termInfo.categoryWeights;
		}
	}
	
	@Override
	public void loadDouble(Map<String, double[]> termsData, boolean toPreprocess, ICategoriesData categoriesData)
	{
		super.loadDouble(termsData, toPreprocess, categoriesData);
		updateTermDetector();
		updateTermInfoToComplex();
	}
	
	@Override
	public void load(Map<String, int[]> termsData, boolean toPreprocess, ICategoriesData categoriesData)
	{
		super.load(termsData, toPreprocess, categoriesData);
		updateTermDetector();
		updateTermInfoToComplex();
	}
	
	public void loadStrengths(Map<String, Double> termsStrengthData, boolean toPreprocess, ICategoriesData categoriesData)
	{
		for (Map.Entry<String, Double> termData : termsStrengthData.entrySet())
		{
			String term = termData.getKey();
			if (toPreprocess)
				term = TweetPreprocessing.preprocessText(term);
			ComplexTermInfo termInfo = (ComplexTermInfo)lexiconData.get(term);
			if (termInfo != null)
				termInfo.strength = termData.getValue();
		}
	}
	
	
	public static WeightedEmotionLexiconWithIntensityModel loadFromBinaryFile(String filename) throws Exception
	{
		return (WeightedEmotionLexiconWithIntensityModel) UtilFiles.readSerializableDataFromFile(filename);
	}
	
	public static WeightedEmotionLexiconWithIntensityModel loadFromBinaryFile(String filename, boolean useNonOverlap) throws Exception
	{
		Object lexicon = UtilFiles.readSerializableDataFromFile(filename);
		
		WeightedEmotionLexiconWithIntensityModel weLex = null;
		try
		{
			weLex = (WeightedEmotionLexiconWithIntensityModel)lexicon;
		}
		catch(Exception e)
		{
			weLex = new WeightedEmotionLexiconWithIntensityModel ((WeightedEmotionLexicon)lexicon);
		}
		
		weLex.params.useNonOverlapDetector = useNonOverlap;
		if (useNonOverlap)
			weLex.tDetector = new NonOverlapTermDetector(weLex.tDetector);
		else
			weLex.tDetector = new TermDetectorWithProcessing(weLex.tDetector);
		weLex.coefTermDetector = new TermDetectorWithModifiers (weLex.tDetector);
		
		return weLex;
	}
	
	public static WeightedEmotionLexiconWithIntensityModel readFromFileWithEmptyWeights(String filename, String categoryDataType) throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.readFromFileWithEmptyWeights(filename, categoryDataType);
		return new WeightedEmotionLexiconWithIntensityModel(lexicon);
	}
	
	public static WeightedEmotionLexiconWithIntensityModel readFromFileWithWeights(String filename, String categoryDataType) throws Exception
	{
		 WeightedEmotionLexiconWithIntensityModel lexicon = new WeightedEmotionLexiconWithIntensityModel();
		 Map<String, double[]> termsData = UtilCollections.readMapDoubleArrayFromFile(filename);
		 ICategoriesData catData = CategoryProcessor.getCategoriesDataByName(categoryDataType);
		
		lexicon.loadDouble(termsData, false, catData);
		
		lexicon.tDetector.usePatterns = usePatternsBasedOnFilename(filename);
		
		 return lexicon;
	}
	
	public static WeightedEmotionLexiconWithIntensityModel readFromFileWithCategories(String filename, String categoryDataType) throws Exception
	{
		 WeightedEmotionLexiconWithIntensityModel lexicon = new WeightedEmotionLexiconWithIntensityModel();
		 List<String> dataLines = UtilFiles.getContentLines(filename);
			
		 Map<String, int[]> termsData = new HashMap<String, int[]>();
		 ICategoriesData catData = CategoryProcessor.getCategoriesDataByName(categoryDataType);
			
			
		 for (String line : dataLines)
		 {
			 String[] parsedLine = line.split("\t");
			 String term = parsedLine[0].toLowerCase();
			 int[] categoryWeights = new int[catData.categoriesToUse().length];
			 int catId = Integer.parseInt(parsedLine[1]);
				
			 if (catId != catData.contextCategory()) // we will not load the terms which don't have a strict emotion category
			 {
				categoryWeights[catData.getCategoryIndexById(catId)] = 1;
				UtilCollections.sumArrayValueToMap(termsData, term, categoryWeights);
			 }
		 }
		 
		lexicon.load(termsData, false, catData);
		
		lexicon.tDetector.usePatterns = usePatternsBasedOnFilename(filename);
		 return lexicon;
	}
	
	
	public static WeightedEmotionLexiconWithIntensityModel readFromFileWithCategoriesOldToGew(String filename) throws Exception
	{
		 WeightedEmotionLexiconWithIntensityModel lexicon = new WeightedEmotionLexiconWithIntensityModel();
		 CSVReader reader = new CSVReader(new FileReader( filename),  '\t');
		 Map<String, int[]> termsData = new HashMap<String, int[]>();
		 ICategoriesData catData = new CategoriesDataOLD();
		 
		String [] nextLine;
		reader.readNext(); //miss the first line
		while ((nextLine = reader.readNext()) != null) 
		{
			String term = nextLine[0].toLowerCase();
			int[] categoryWeights = new int[catData.categoriesToUse().length];
			int catInd = catData.getCategoryIndexById(Integer.parseInt(nextLine[1]));
			categoryWeights[catInd] = 1;
			termsData.put(term, categoryWeights); 
		}
		reader.close();
			
		catData = new CategoriesDataGEW();	
		
		Map<String, double[]> termsDataGEW = new HashMap<String, double[]>();
		
		// transform to GEW categories
		for (Map.Entry<String, int[]> phraseEntry : termsData.entrySet())
		{								
			double[] weights = CategoriesMappingOLDtoGEW.getGEWWeightsForOldDistr(phraseEntry.getValue());
			termsDataGEW.put(phraseEntry.getKey(), weights);
		}
			
		lexicon.loadDouble(termsDataGEW, false, catData);
		
		lexicon.tDetector.usePatterns = usePatternsBasedOnFilename(filename);
		
		 return lexicon;
	}
	
	public static WeightedEmotionLexiconWithIntensityModel loadFromTextFile(String filename, CategoryDataType catDataType) throws Exception
	{
		return new WeightedEmotionLexiconWithIntensityModel(WeightedEmotionLexicon.loadFromTextFile(filename, catDataType));
	}

	public static WeightedEmotionLexiconWithIntensityModel loadFromTextFile(String filename, CategoryDataType catDataType, boolean useNonOverlap) throws Exception
	{
		return new WeightedEmotionLexiconWithIntensityModel(WeightedEmotionLexicon.loadFromTextFile(filename, catDataType, useNonOverlap));
	}

	
	public Map<String, Double> findEmotionalTermsWithCoefInText(String text)
	{
		return coefTermDetector.findEmotionalTermsWithCoefInText(text);
	}
	
	
	public double getStrengthForTerm(String term)
	{
		return ((ComplexTermInfo)lexiconData.get(term)).strength;
	}
	
	public Map<String, Integer> getLastOccurrences()
	{
		return coefTermDetector.getLastOccurrences();
	}
}

