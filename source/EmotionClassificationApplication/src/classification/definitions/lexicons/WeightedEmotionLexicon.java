/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.lexicons;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import processing.TweetPreprocessing;
import au.com.bytecode.opencsv.CSVReader;
import classification.detectors.NonOverlapTermDetector;
import classification.detectors.TermDetectorWithProcessing;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import functionality.UtilArrays;
import functionality.UtilCollections;
import functionality.UtilFiles;


public class WeightedEmotionLexicon implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 839980973726948590L;
	public String name = "";

	static class TermInfo implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -4634544854123729089L;
		double[] categoryWeights;
		
		public TermInfo () {}
		
		
		public TermInfo (double[] weights)
		{
			categoryWeights = weights;
		}
	}
	
	public Map<String, TermInfo> lexiconData;
	public TermDetectorWithProcessing tDetector;
	
	public WeightedEmotionLexicon()
	{}
	
	public WeightedEmotionLexicon(WeightedEmotionLexicon lexPrototype)
	{
		this.lexiconData = lexPrototype.lexiconData;
		this.tDetector = lexPrototype.tDetector;
		this.params = lexPrototype.params;
		this.categoriesData = lexPrototype.categoriesData;
	}
	
	public static class LexiconParams implements Serializable
	{
		
		private static final long serialVersionUID = -5586486969646484320L;
		boolean wasPreprocessed = false;
		boolean useNonOverlapDetector = true;
		
		public boolean normalize = true;
	}
	
	public LexiconParams params = new LexiconParams();
	
	public ICategoriesData categoriesData;
	
	public int getCategoriesToUseNum()
	{
		return categoriesData.categoriesToUse().length;
	}
	
	public int getUsedCategoryIdByIndex(int catIndex)
	{
		return categoriesData.categoriesToUse()[catIndex];
	}
	
	public Set<String> allTerms()
	{
		return lexiconData.keySet();
	}
	
	public double[] getWeightForTerm(String term)
	{
		if (containsTerm(term))
			return lexiconData.get(term).categoryWeights;
		else
			return null;
	}
	
	public boolean containsTerm(String term)
	{
		return lexiconData.containsKey(term);
	}
	
	/**
	 * This method generates each time the map! Call it only once per lexicon.
	 * @return
	 */
	public Map<String, double[]> allTermsData() {
		Map<String, double[]> termsData = new HashMap<String, double[]> ();
		for (Map.Entry<String, TermInfo> termEntry : lexiconData.entrySet()) {
			termsData.put(termEntry.getKey(), termEntry.getValue().categoryWeights);
		}
		
		return termsData;
	}
	
	/**
	 * By default simple detector without taking into account overlaps is used
	 * @param termsData
	 * @param categoriesData
	 */
	public void createSimpleFromGivenData(Map<String, double[]> termsData, ICategoriesData categoriesData)
	{
		this.categoriesData = categoriesData;
		lexiconData = new HashMap<String, TermInfo>();
		tDetector = new TermDetectorWithProcessing(termsData.keySet(), false);
		
		for (Map.Entry<String, double[]> termData : termsData.entrySet())
		{
			TermInfo termInfo = new TermInfo();
			termInfo.categoryWeights = termData.getValue();
			String term = termData.getKey();
			lexiconData.put(term, termInfo);
		}
	}
	
	public void createSimpleFromGivenData(Map<String, double[]> termsData, ICategoriesData categoriesData, boolean useNonOverlap)
	{
		this.categoriesData = categoriesData;
		lexiconData = new HashMap<String, TermInfo>();
		
		if (useNonOverlap)
			tDetector = new NonOverlapTermDetector(termsData.keySet(), false);
		else
			tDetector = new TermDetectorWithProcessing(termsData.keySet(), false);
		
		
		for (Map.Entry<String, double[]> termData : termsData.entrySet())
		{
			TermInfo termInfo = new TermInfo();
			termInfo.categoryWeights = termData.getValue();
			String term = termData.getKey();
			lexiconData.put(term, termInfo);
		}
	}
	
	public void loadDouble(Map<String, double[]> termsData, boolean toPreprocess, ICategoriesData categoriesData)
	{
		this.categoriesData = categoriesData;
		params.wasPreprocessed = toPreprocess;
		lexiconData = new HashMap<String, TermInfo>();
		if (params.useNonOverlapDetector)
			tDetector = new NonOverlapTermDetector(termsData.keySet(), toPreprocess);
		else
			tDetector = new TermDetectorWithProcessing(termsData.keySet(), toPreprocess);
		for (Map.Entry<String, double[]> termData : termsData.entrySet())
		{
			TermInfo termInfo = new TermInfo();
			double[] categories = termData.getValue();
			double[] extractedCategories = new double[getCategoriesToUseNum()];
			int[] categoriesToUse = categoriesData.categoriesToUse();
			for (int ind = 0; ind < categoriesToUse.length; ++ind)
			{
				int mappedInd = categoriesData.getCategoryIndexById(categoriesToUse[ind]);
				if (mappedInd < categories.length)
					extractedCategories[ind] = categories[mappedInd];
			}
			double[] categoryWeights;
			if (params.normalize)
				categoryWeights = UtilArrays.normalizeArray(extractedCategories);
			else
				categoryWeights = extractedCategories;
			termInfo.categoryWeights = categoryWeights;
			String term = termData.getKey();
			if (toPreprocess)
				term = TweetPreprocessing.preprocessText(term);
			lexiconData.put(term, termInfo);
		}
	}
	
	public void load(Map<String, int[]> termsData, boolean toPreprocess, ICategoriesData categoriesData)
	{
		this.categoriesData = categoriesData;
		params.wasPreprocessed = toPreprocess;
		lexiconData = new HashMap<String, TermInfo>();
		
		if (params.useNonOverlapDetector)
			tDetector = new NonOverlapTermDetector(termsData.keySet(), toPreprocess);
		else
			tDetector = new TermDetectorWithProcessing(termsData.keySet(), toPreprocess);
		
		for (Map.Entry<String, int[]> termData : termsData.entrySet())
		{
			TermInfo termInfo = new TermInfo();
			int[] categories = termData.getValue();
			int[] categoriesToUse = categoriesData.categoriesToUse();
			int[] extractedCategories = new int[categoriesToUse.length];
			for (int ind = 0; ind < categoriesToUse.length; ++ind)
			{
				int mappedInd = categoriesData.getCategoryIndexById(categoriesToUse[ind]);
				if (mappedInd < categories.length)
					extractedCategories[ind] = categories[mappedInd];
			}
			double[] categoryWeights = UtilArrays.normalizeArray(extractedCategories);
			termInfo.categoryWeights = categoryWeights;
			String term = termData.getKey();
			if (toPreprocess)
				term = TweetPreprocessing.preprocessText(term);
			lexiconData.put(term, termInfo);
		}
	}
	
	public void saveToBinaryFile(String filename) throws IOException
	{
		ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(filename));
		oos.writeObject(this);	
		oos.flush();
		oos.close();
	}
	
	public void saveToTextFile(String filename) throws IOException
	{
		Map<String, double[]> termsData = new HashMap<String, double[]>();
		for (Map.Entry<String, TermInfo> termData : lexiconData.entrySet())
		{
			termsData.put(termData.getKey(), termData.getValue().categoryWeights);
		}
		
		String caption = "term\t" + UtilCollections.join(categoriesData.getCategoriesEnumeration(), "\t");
		UtilFiles.writeAdditionalDataToFileFromNewLine(filename, caption);
		UtilCollections.printToFileMapObjectToDoubleArray(termsData, filename);
	}
	
	public static WeightedEmotionLexicon loadFromTextFileCatDiff(String filename, CategoryDataType catDataType, boolean contextCategoryPresent, boolean useNonOverlap) throws Exception
	{
		WeightedEmotionLexicon weLex = loadFromTextFileCatDiff(filename, catDataType, contextCategoryPresent);
		weLex.params.useNonOverlapDetector = useNonOverlap;
		if (useNonOverlap)
			weLex.tDetector = new NonOverlapTermDetector(weLex.tDetector);
		else
			weLex.tDetector = new TermDetectorWithProcessing(weLex.tDetector);
		
		return weLex;
	}
	
	public static WeightedEmotionLexicon loadFromTextFileCatDiff(String filename, CategoryDataType catDataType, boolean contextCategoryPresent) throws Exception
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		List<String[]> lexiconColl = UtilFiles.getParsedLines(filename, '\t', 0);
		WeightedEmotionLexicon lexicon = new WeightedEmotionLexicon();
		lexicon.lexiconData = new HashMap<String, TermInfo>();
		lexicon.categoriesData = categoriesData;
		lexicon.name = filename;
		int catNum = lexicon.getCategoriesToUseNum();
		if (!contextCategoryPresent && UtilArrays.findFirstWithLinearSearch(lexicon.categoriesData.categoriesToUse(), categoriesData.contextCategory()) > 0)
			catNum--;
		
		for (String[] termData : lexiconColl)
		{
			String term = termData[0];
			if (termData.length - 1 < catNum)
				System.out.println("Warning: Number of categories in the file doesn't correspond to the specified category set");
			double[] weights = new double[catNum];
			for (int i = 0; i < Math.min(catNum, termData.length - 1); ++i)
			{
				weights[i] = Double.parseDouble(termData[i + 1]);
			}
			lexicon.lexiconData.put(term, new TermInfo(weights));
		}
		
		if (lexicon.params.useNonOverlapDetector)
			lexicon.tDetector = new NonOverlapTermDetector(lexicon.lexiconData.keySet(), false);
		else
			lexicon.tDetector = new TermDetectorWithProcessing(lexicon.lexiconData.keySet(), false);
		
		return lexicon;
	}
	
	public static WeightedEmotionLexicon loadFromTextFile(String filename, CategoryDataType catDataType) throws Exception
	{
		return loadFromTextFileCatDiff(filename, catDataType, true);
	}
	
	public static WeightedEmotionLexicon loadFromTextFile(String filename, CategoryDataType catDataType, boolean useNonOverlap) throws Exception
	{
		WeightedEmotionLexicon weLex = loadFromTextFile(filename, catDataType);
		weLex.params.useNonOverlapDetector = useNonOverlap;
		if (useNonOverlap)
			weLex.tDetector = new NonOverlapTermDetector(weLex.tDetector);
		else
			weLex.tDetector = new TermDetectorWithProcessing(weLex.tDetector);
		
		return weLex;
	}
	
	public static WeightedEmotionLexicon loadFromBinaryFile(String filename) throws Exception
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object lexicon = ois.readObject();	
		ois.close();
		return (WeightedEmotionLexicon)lexicon;
	}
	
	public static WeightedEmotionLexicon loadFromBinaryFile(String filename, boolean useNonOverlap) throws Exception
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object lexicon = ois.readObject();	
		ois.close();
		WeightedEmotionLexicon weLex = (WeightedEmotionLexicon)lexicon;
		weLex.params.useNonOverlapDetector = useNonOverlap;
		if (useNonOverlap)
			weLex.tDetector = new NonOverlapTermDetector(weLex.tDetector);
		else
			weLex.tDetector = new TermDetectorWithProcessing(weLex.tDetector);
		
		return weLex;
	}
	
	public static WeightedEmotionLexicon readFromFileWithEmptyWeights(String filename, String categoryDataType) throws Exception
	{
		 WeightedEmotionLexicon lexicon = new WeightedEmotionLexicon();
		 CSVReader reader = new CSVReader(new FileReader( filename),  '\t');
		 Map<String, int[]> termsData = new HashMap<String, int[]>();
		 ICategoriesData catData = CategoryProcessor.getCategoriesDataByName(categoryDataType);
		String [] nextLine;
		reader.readNext(); //miss the first line
		while ((nextLine = reader.readNext()) != null) 
		{
			String term = nextLine[0].toLowerCase();
			termsData.put(term, new int[catData.categoriesToUse().length]); 
		}
			reader.close();
		lexicon.load(termsData, false, catData);
		 return lexicon;
	}
	
	/** finds terms in the text and returns found terms with number of occurrences */
	public Map<String, Integer> findEmotionalTermsInText(String text)
	{
		return tDetector.findEmotionalTermsInText(text);
	}
	
	/** finds terms in the text and returns found terms with number of occurrences */
	public Map<String, Integer> findEmotionalTermsInText(String text, boolean preprocessText)
	{
		return tDetector.findEmotionalTermsInText(text, preprocessText);
	}
	
	/** returns the map of terms with the weights of corresponding category*/
	public Map<String, Double> getCategorySlice(int categoryId)
	{
		int categoryIndex = Arrays.binarySearch(categoriesData.categoriesToUse(), categoryId); // should always present!
		Map<String, Double> resultCategorySlice = new HashMap<String, Double>();
		
		for (String term : lexiconData.keySet())
		{
			TermInfo termInfo = lexiconData.get(term);
			resultCategorySlice.put(term, termInfo.categoryWeights[categoryIndex]);
		}
		return resultCategorySlice;
	}
	
	
	public static void replaceEmoticonsInLexicon(String lexiconFilename, CategoryDataType catDataType, String resultFilename) throws Exception
	{
		WeightedEmotionLexicon givenLexicon = WeightedEmotionLexicon.loadFromTextFileCatDiff(lexiconFilename, catDataType, false, false);
		Map<String,String> replacements = new HashMap<String,String>();
		for (String term : givenLexicon.allTerms())
		{
			String termEmot = TweetPreprocessing.replaceEmoticons(term.toLowerCase()).toLowerCase();
			if (!term.equals(termEmot))
			{
				replacements.put(term, termEmot);
			}
		}
		for(Map.Entry<String, String> repl : replacements.entrySet())
		{
			TermInfo savedTI = givenLexicon.lexiconData.get(repl.getKey());
			givenLexicon.lexiconData.put(repl.getValue(), savedTI);
			givenLexicon.lexiconData.remove(repl.getKey());
		}
		givenLexicon.saveToTextFile(resultFilename);
	}
	
	/**
	 * Reads the weighted emotion lexicon from the data with categories.
	 * Each term can appear with several categories. 
	 * @param filename
	 * @param categoryDataType
	 * @return
	 * @throws Exception
	 */
	public static WeightedEmotionLexicon readFromFileWithCategories(String filename, String categoryDataType) throws Exception
	{
		WeightedEmotionLexicon lexicon = new WeightedEmotionLexicon();
		 List<String> dataLines = UtilFiles.getContentLines(filename);
			
		 Map<String, int[]> termsData = new HashMap<String, int[]>();
		 ICategoriesData catData = CategoryProcessor.getCategoriesDataByName(categoryDataType);
			
			
		 for (String line : dataLines)
		 {
			 String[] parsedLine = line.split("\t");
			 String term = parsedLine[0].toLowerCase();
			 int[] categoryWeights = new int[catData.categoriesToUse().length];
			 int catId = Integer.parseInt(parsedLine[1]);
				
			 if (catId != catData.contextCategory()) // we will not load the terms which don't have a strict emotion category
			 {
				categoryWeights[catData.getCategoryIndexById(catId)] = 1;
				UtilCollections.sumArrayValueToMap(termsData, term, categoryWeights);
			 }
		 }
		 
		lexicon.load(termsData, false, catData);
		
		lexicon.tDetector.usePatterns = usePatternsBasedOnFilename(filename);
		
		 return lexicon;
	}
	
	/** This adapts to Scherer(GALC) lexicon **/
	static boolean usePatternsBasedOnFilename(String filename)
	{
		boolean usePatterns = false;
		if (filename.contains("Schrerer")||filename.contains("GALC"))
			usePatterns = true;
		return usePatterns;
	}
	
	
	public void removeTerms(Collection<String> termsToRemove)
	{
		UtilCollections.removeAllKeysFromMap(lexiconData, termsToRemove);
		tDetector.deleteTerms(termsToRemove);
	}
	
	/** multiplies weights of given terms to the coefficients*/
	public void reweightTerms(Set<String> termsToReweight, double coef)
	{
		for (String term : termsToReweight)
		{
			if (lexiconData.containsKey(term))
			{
				TermInfo curTermInfo = lexiconData.get(term);
				curTermInfo.categoryWeights = UtilArrays.multiplyArray(curTermInfo.categoryWeights, coef);
			}
		}
	}
	
	/** multiplies weights of given terms to the coefficients*/
	public void reweightTerms(String patternTermToReweight, double coef)
	{
		for (String term : lexiconData.keySet())
		{
			if (term.matches(patternTermToReweight))
			{
				TermInfo curTermInfo = lexiconData.get(term);
				curTermInfo.categoryWeights = UtilArrays.multiplyArray(curTermInfo.categoryWeights, coef);
			}
		}
	}
	
	/**
	 * Updates the weights of the term, but only if it was already in the lexicon
	 * @param term
	 * @param newWeights
	 */
	public void updateTermWeight(String term, double[] newWeights)
	{
		if (lexiconData.containsKey(term))
		{
			TermInfo tInfo = lexiconData.get(term);
			tInfo.categoryWeights = newWeights;
		}		
	}
	
	/** the terms should be in the same format! Only those that were not yet in the lexicon will be added */
	public void addTerms(Map<String, double[]> termsData)
	{
		addTermsToLexiconData(termsData);
		addTermsToTermDetector(termsData.keySet());
	}
	
	/** the terms should be in the same format! Only those that were not yet in the lexicon will be added */
	private void addTermsToLexiconData(Map<String, double[]> termsData)
	{
		for (Map.Entry<String, double[]> termData : termsData.entrySet())
		{
			String term = termData.getKey();
			if (lexiconData.containsKey(term))
				continue;
			TermInfo termInfo = new TermInfo();
			termInfo.categoryWeights = termData.getValue();
			lexiconData.put(term, termInfo);
		}
		
	}
	
	private void addTermsToTermDetector(Collection<String> terms)
	{
		tDetector.addTerms(terms);
	}
	
	public Map<String, double[]> getAllTermsData()
	{
		Map<String, double[]> termsData = new HashMap<String, double[]>();
		for (Map.Entry<String, TermInfo> termData : lexiconData.entrySet())
		{
			termsData.put(termData.getKey(), termData.getValue().categoryWeights);
		}
		return termsData;
	}
	
	public TermDetectorWithProcessing getInternalTermDetector()
	{
		return tDetector;
	}
	
	public void clear()
	{
		tDetector = null;
		lexiconData.clear();
	}
	
	public static WeightedEmotionLexicon readFromFileWithCategories(String filename, CategoryDataType catDataType, boolean useNonOverlap) throws Exception
	{
		WeightedEmotionLexicon weLex = readFromFileWithCategories(filename, catDataType);
		weLex.params.useNonOverlapDetector = useNonOverlap;
		if (useNonOverlap)
			weLex.tDetector = new NonOverlapTermDetector(weLex.tDetector);
		else
			weLex.tDetector = new TermDetectorWithProcessing(weLex.tDetector);
		
		return weLex;
	}
	
	/**
	 * @param catLexicon
	 * @param catDataTypeName
	 * @return
	 * @throws Exception
	 */
	public static WeightedEmotionLexicon readFromGivenCategoryLexicon(CategoryLexicon catLexicon, String catDataTypeName) throws Exception
	{
		
		WeightedEmotionLexicon lexicon = new WeightedEmotionLexicon();
			
		 Map<String, int[]> termsData = new HashMap<String, int[]>();
		 
		 Map<Integer, Set<String>> lexiconCategoryMap = catLexicon.mapCategoryToTerms();
		 
		 ICategoriesData catData =  CategoryProcessor.getCategoriesDataByName(catDataTypeName);
		
		
		for (Map.Entry<Integer, Set<String>> catEntry : lexiconCategoryMap.entrySet())
		{
			 int[] categoryWeights = new int[catData.categoriesToUse().length];
			 int catId = catEntry.getKey();
			 if (catId != catData.contextCategory()) // we will not load the terms which don't have a strict emotion category
			 {
				 categoryWeights[catData.getCategoryIndexById(catId)] = 1;
				
				for (String term : catEntry.getValue())
				{
					UtilCollections.sumArrayValueToMap(termsData, term, categoryWeights);
				}
			 }
		}
		 
		lexicon.load(termsData, false, catData);
		
		lexicon.tDetector.usePatterns = false; //TODO: check it somehow
		
		 return lexicon;
	}
	
	public static WeightedEmotionLexicon readFromFileWithCategories(String filename, CategoryDataType catDataType) throws Exception
	{
		return readFromFileWithCategories(filename, catDataType.toString());
	}
	
	/** 
	 * Changes the total weight sum of each term to the specified weight. If the new weight is not given, the weight of the term remains the same
	 * @param newWeightsForTerms
	 * @param lexicon
	 */
	public static void setWeightsToTerms (Map<String, Double> newWeightsForTerms, WeightedEmotionLexicon lexicon)
	{
		
		for (String term : newWeightsForTerms.keySet())
		{
			double newSumWeight = newWeightsForTerms.get(term);
			if (lexicon.containsTerm(term))
			{
				double[] termWeights = lexicon.getWeightForTerm(term);
				UtilArrays.normalizeArrayInternal(termWeights);
				UtilArrays.multiplyArrayInternal(termWeights, newSumWeight);
				lexicon.updateTermWeight(term, termWeights);
			}
		}
	}
	
	public void NormalizeWeightsInLexicon() {
		for (Map.Entry<String, TermInfo> termEntry : lexiconData.entrySet()) {
			UtilArrays.normalizeArrayInternal(termEntry.getValue().categoryWeights);
		}
	}
}

