/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.lexicons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;
import functionality.UtilCollections;

/** Lexicon where each term can correspond to one or several categories
 * (one term to multiple categories)*/
public class CategoryLexiconUnrestricted extends CategoryLexicon {
	
	Map<String, Set<Integer>> mapTermToCategoryUnrestricted;
	
	public Map<String, Set<Integer>> getMapTermToCategories()
	{
		return mapTermToCategoryUnrestricted;
	}
	
	@Override
	public Map<String, Integer> getMapTermToCategory() throws Exception
	{
		throw new Exception("It is not possible to obtain the map to category map for unrestricted lexicon");
	}
	
	@Override
	public Set<String> getAllLexiconTerms()
	{
		return mapTermToCategoryUnrestricted.keySet();
	}
	
	/** Lexicon where each term can correspond to one or several categories
	 * Initialization from the file with the saved map data in the format term\tcategory (if a term belongs to several categories, there will be several lines*/
	public CategoryLexiconUnrestricted (String mapFile) throws Exception
	{
		this(mapFile, true);
	}
	
	/** Lexicon where each term can correspond to one or several categories
	 * Initialization from the file with the saved map data in the format term\tcategory (if a term belongs to several categories, there will be several lines*/
	public CategoryLexiconUnrestricted (String mapFile, boolean toLowerCase) throws Exception
	{
		super();

		mapCategoryToTerms = null;
		
		mapTermToCategoryUnrestricted = new HashMap<String, Set<Integer>>();
		BufferedReader input =  new BufferedReader(new InputStreamReader(new FileInputStream(mapFile), "UTF-8"));
		
		String [] nextLine;
		String line;   
		while (( line = input.readLine()) != null)
		 {
			nextLine = line.split("\t");
			
			if (nextLine[0].startsWith("//"))
				continue;
			String term = nextLine[0];
			if (toLowerCase)
				term = term.toLowerCase();
			UtilCollections.appendNewObjectToValueInMap(mapTermToCategoryUnrestricted, term, Integer.parseInt(nextLine[1]));
		}
		input.close();
		createMapCategoryToTerms();
	}
	
	/** Lexicon where each term can correspond to one or several categories*/
	public CategoryLexiconUnrestricted (Map<Integer, Set<String>> mapCategoryToTerms) throws Exception
	{
		this.mapCategoryToTerms = mapCategoryToTerms;
		mapTermToCategoryUnrestricted = new HashMap<String, Set<Integer>>();
		for (Integer category : mapCategoryToTerms.keySet())
		{
			Set<String> categoryTerms = mapCategoryToTerms.get(category);
			for (String term : categoryTerms)
			{
				UtilCollections.appendNewObjectToValueInMap(mapTermToCategoryUnrestricted, term, category);
			}
		}
	}
	
	@Override
	public void createMapCategoryToTerms()
	{
		mapCategoryToTerms = new HashMap<Integer, Set<String>>();
		for (Map.Entry<String, Set<Integer>> termEntry : mapTermToCategoryUnrestricted.entrySet())
		{
			for (Integer catId : termEntry.getValue())
				UtilCollections.appendNewObjectToValueInMap(mapCategoryToTerms, catId, termEntry.getKey());
		}
	}
	
	public Map<Integer, Set<String>> getMapCategoryToTerms()
	{
		if (mapCategoryToTerms == null)
			createMapCategoryToTerms();
		return mapCategoryToTerms;
	}
	
	@Override
	public void printFileInCategoryToLineFormat(String filename) throws Exception
	{
		if (mapCategoryToTerms == null)
			createMapCategoryToTerms();
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<Integer, Set<String>> catEntry : mapCategoryToTerms.entrySet())
		{
			pw.println(catEntry.getKey() + " " + UtilCollections.join(catEntry.getValue(), " "));
		}	
		pw.close();
	}
	
	@Override
	public void saveToMapFile(String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		UtilCollections.printToFileMapObjectToSetWithRepeatedFirstLines(this.mapTermToCategoryUnrestricted, filename);
	}
	
	/** It will print categories upto 100*/
	@Override
	public void saveToMapFileInCategoryOrder(String filename) throws Exception
	{
		if (mapCategoryToTerms == null)
			createMapCategoryToTerms();
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (int catId = 1; catId < 101; ++catId)
		{
			if (mapCategoryToTerms.containsKey(catId))
			{
				for (String term : mapCategoryToTerms.get(catId))
				{
					pw.println(term + "\t" + catId);
				}
			}
			
		}
		pw.close();
	}
	
	public static void rewriteInCatToLineFormat(String initialMapFile, String resFilename) throws Exception
	{
		CategoryLexiconUnrestricted cl = new CategoryLexiconUnrestricted(initialMapFile);
		cl.printFileInCategoryToLineFormat(resFilename);
	}
	
	public static CategoryLexiconUnrestricted readCategoryLexiconFromFile(String filename) throws Exception
	{
		return new CategoryLexiconUnrestricted (filename);
	}
	
	public static CategoryLexiconUnrestricted readCategoryLexiconFromFile(String filename, boolean toLowerCase) throws Exception
	{
		return new CategoryLexiconUnrestricted (filename, toLowerCase);
	}

}
