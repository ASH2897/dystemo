/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.featurerepresentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import weka.core.Attribute;

import classification.detectors.TermDetectorWithProcessing;

public class WekaAttributesSet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6053948135156100801L;

	public TermDetectorWithProcessing tDetector;

	protected Map<String, Integer> attributeIndexes; // indexes start from 0
	LinkedHashMap<String,Attribute> savedAttributeMap;
	
	public WekaAttributesSet (LinkedHashMap<String,Attribute> savedAttributeMap)
	{
		this.savedAttributeMap = savedAttributeMap;
		setAttributeIndexes(new HashMap<String, Integer>());
		int ind = 0;
		for (Map.Entry<String,Attribute> attribute : savedAttributeMap.entrySet())
			getAttributeIndexes().put(attribute.getKey(), ind++);
		tDetector = new TermDetectorWithProcessing(savedAttributeMap.keySet(), false);
	}
	
	public Set<Integer> findAttributesInText(String text, boolean toPreprocess)
	{
		Set<String> foundTerms = tDetector.findEmotionalTermsInText(text, toPreprocess).keySet();
		Set<Integer> foundIndexes = new HashSet<Integer>();
		for (String term: foundTerms)
		{
			Integer curIndex = getAttributeIndexes().get(term);
			if (curIndex != null)
				foundIndexes.add(curIndex);
		}
		return foundIndexes;
	}
	
	public Set<Integer> findAttributesInText(String text)
	{
		Set<String> foundTerms = tDetector.findEmotionalTermsInText(text).keySet();
		Set<Integer> foundIndexes = new HashSet<Integer>();
		for (String term: foundTerms)
		{
			foundIndexes.add(getAttributeIndexes().get(term));
		}
		return foundIndexes;
	}
	
	public int size()
	{
		return getAttributeIndexes().size();
	}

	public Map<String, Integer> getAttributeIndexes() {
		return attributeIndexes;
	}

	public void setAttributeIndexes(Map<String, Integer> attributeIndexes) {
		this.attributeIndexes = attributeIndexes;
	}
	
	public Set<String> getFeatureSet()
	{
		return savedAttributeMap.keySet();
	}
	
	public Map<String,Attribute> getAttributeMap()
	{
		return savedAttributeMap;
	}
	
	public Attribute getAttributeByName(String term)
	{
		return savedAttributeMap.get(term);
	}
}
