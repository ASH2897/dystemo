/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import processing.TweetPreprocessing;

/**
 * Searches for the terms occurrences using java patterns. (Not optimal)
 *
 */
public class SimpleTermDetector extends TermDetectorWithProcessing {

	private static final long serialVersionUID = 7809527942789947269L;
	Pattern searchPattern = null;

	public SimpleTermDetector(Collection<String> terms, boolean toPreprocess)
	{
		super();
		this.terms = new HashSet<String>(terms);
		createRootNode();
		
		StringBuilder patternStr = new StringBuilder();
		for (String term : terms)
		{
			if (toPreprocess)
				term = TweetPreprocessing.preprocessText(term);
			String[] tokens = term.trim().split(" +");
			addNewTermInPrefixNode(rootPrefixNode, tokens, 0); // for consistency of inheritance
			
			patternStr.append("|(" + term+")");
			
		}
		searchPattern = Pattern.compile(patternStr.toString().substring(1));
	}
	
	/** finds terms in the text and returns found terms with number of occurrences */
	@Override
	public Map<String, Integer> findEmotionalTermsInText(String text)
	{
		String evalText = text.toLowerCase();
		Map<String, Integer> returnMap = new HashMap<String, Integer>();
		
		Matcher m = searchPattern.matcher(evalText);
		
		if (m.find())
		{
			returnMap.put(m.group(), 1);
		}

		return returnMap;
	}
}
