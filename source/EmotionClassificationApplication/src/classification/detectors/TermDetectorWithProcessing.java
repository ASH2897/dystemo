/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import linguistic.TermDetector;
import linguistic.UtilText;
import processing.TweetPreprocessing;
import functionality.UtilCollections;

public class TermDetectorWithProcessing extends TermDetector implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -297055663080417738L;

	/**
	 * This will create the detector with preprocessed terms (i.e. terms first preprocessed before adding into detector)
	 * @param terms
	 */
	public TermDetectorWithProcessing(Collection<String> terms)
	{
		// do preprocessing first
		this(terms,true);
	}
	
	public TermDetectorWithProcessing()
	{
		super();
	}
	
	public TermDetectorWithProcessing(Collection<String> terms, boolean toPreprocess)
	{
		super();
		if (!toPreprocess)
			initializeOnTerms(terms);
		else
		{
			Set<String> newTerms = new HashSet<String>();
			for (String term : terms)
				newTerms.add(TweetPreprocessing.preprocessText(term));
			initializeOnTerms(newTerms);
		}
		
	}
	
	public TermDetectorWithProcessing(TermDetector tdetector) 
	{
		super(tdetector);
	}
		
	 
	/** finds terms in the text and returns found terms with number of occurrences */
	public Map<String, Integer> findEmotionalTermsInText(String text)
	{
		List<String> tokens = TweetPreprocessing.tokenizeWithoutLemmatization(text);
		return findEmotionalTermsForTokens(tokens);
	}
	
	public Map<String, Integer> findEmotionalTermsInText(String text, boolean toPreprocess)
	{
		
		List<String> tokens = (toPreprocess)?
								TweetPreprocessing.tokenizeWithoutLemmatization(text):
								TweetPreprocessing.tokenizeWithoutPreprocessing(text);
		return  findEmotionalTermsForTokens(tokens);
	}
	
	protected Map<String,Integer> findEmotionalTermsForTokens(List<String> tokens)
	{
		return findTermsForTokens(tokens);
	}
	
}
