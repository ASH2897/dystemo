/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import linguistic.UtilText;
import functionality.UtilFiles;

public class TermDetectorWithModifiers extends TermDetectorWithProcessing  implements Serializable {
	
	private static final long serialVersionUID = -5969936692982428592L;

	TermDetectorWithProcessing tdetector;
	
	public TermDetectorWithModifiers(Collection<String> terms, boolean toPreprocess) {
		super(terms, toPreprocess);
		tdetector = this;
		this.tdetector.savePositions = true;
		this.tdetector.saveTokenList = true;
	}
	
	public TermDetectorWithModifiers(TermDetectorWithProcessing tdetector) {
		super(tdetector);
		this.tdetector = tdetector;
		this.tdetector.savePositions = true;
		this.tdetector.saveTokenList = true;
	}
	
	static
	{
		//load the modifiers files
		try {
			amplyfiers = new HashSet<String>(UtilText.getAllPlusWords());
			downtoners = new HashSet<String>(UtilText.getAllMinusWords());
			negations = new HashSet<String>(UtilText.getAllNegationWords());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	static Set<String> amplyfiers;
	static Set<String> downtoners;
	static Set<String> negations;
    
	double amplifyCoef = 1.0; //default value
	public double downCoef = 1.0; //default value
	private static double negCoef = 0.0;
	
	/** finds terms in the text and returns found terms with number of occurrences */
	
	public Map<String, Double> findEmotionalTermsWithCoefInText(String text)
	{
		occurrences = tdetector.findEmotionalTermsInText(text);
		List<String> tokens = tdetector.getLastTokenStream();
		Map<String, List<int[]>> foundPositions = tdetector.getLastPositionsOfFoundTerms();
		
		Map<String, Double> foundCoeff = new HashMap<String, Double>();
		for (String term : foundPositions.keySet())
		{
			double termCoef = 0.0;
			List<int[]> positions = foundPositions.get(term);
			for (int[] position : positions)
			{
				String prevToken = (position[0] - 1 >= 0)?tokens.get(position[0] - 1):"";
				if (amplyfiers.contains(prevToken))
					termCoef += amplifyCoef;
				else if (downtoners.contains(prevToken))
					termCoef += downCoef;
				else if (negations.contains(prevToken) || prevToken.endsWith("n't") || prevToken.endsWith("n`t"))
					termCoef += negCoef;
				else
					termCoef += 1.0;
			}
			if (termCoef != 0.0) // we will output also the terms with negative weights when negation coefficient is negative
				foundCoeff.put(term, termCoef);
		}
		
		return foundCoeff;
	}
	
	Map<String, Integer> occurrences;
	
	public Map<String, Integer> getLastOccurrences()
	{
		return occurrences;
	}

	public static void setNegCoef(double negCoef) {
		TermDetectorWithModifiers.negCoef = negCoef;
	}

	public boolean containsTerm(String term) {
		return tdetector.containsTerm(term);
	}
	
	
	@Override
	public void deleteTerms(Collection<String> termsToRemove)
	{
		tdetector.deleteTerms(termsToRemove);
		super.deleteTerms(termsToRemove);
	}
}
