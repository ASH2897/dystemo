/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import processing.TweetPreprocessing;
import functionality.UtilCollections;

/** 
 * Searches for the given terms by going though all substrings. Doesn't ensure that a found term occurs as a separate word. 
 * This method is suitable for substring search. (Not optimal)
 *
 */
public class SearchTermDetector extends TermDetectorWithProcessing {

	private static final long serialVersionUID = 7809527942789947269L;
	
	

	public SearchTermDetector(Collection<String> terms, boolean toPreprocess)
	{
		super();
		this.terms = new HashSet<String>(terms);
		createRootNode();
		
		for (String term : terms)
		{
			if (toPreprocess)
				term = TweetPreprocessing.preprocessText(term);
			String[] tokens = term.trim().split(" +");
			addNewTermInPrefixNode(rootPrefixNode, tokens, 0); // for consistency in inheritance			
		}
	}
	
	/** finds terms in the text and returns found terms with number of occurrences */
	@Override
	public Map<String, Integer> findEmotionalTermsInText(String text)
	{
		String evalText = text.toLowerCase();
		Map<String, Integer> returnMap = new HashMap<String, Integer>();
		
		if (evalText.length() * evalText.length() < terms.size() )
		{
			for (int ind1 = 0; ind1 < evalText.length() - 1; ++ind1)
			{
				for (int ind2 = ind1 + 1; ind2 < evalText.length(); ++ind2)
				{
					if (terms.contains(evalText.substring(ind1, ind2)))
						UtilCollections.incrementIntValueInMap(returnMap, evalText.substring(ind1, ind2));
				}
				
			}
		}
		else
		{
			for (String term : terms)
				if (evalText.contains(term))
					UtilCollections.incrementIntValueInMap(returnMap, term);
		}
		
		return returnMap;
	}
}
