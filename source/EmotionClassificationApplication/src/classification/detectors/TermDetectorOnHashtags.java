/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import processing.TweetPreprocessing;

/**
 * Searches the given terms appeared only as hashtags in the text. The terms to search should not include the hashtag symbol (#). 
 *
 */
public class TermDetectorOnHashtags extends TermDetectorWithProcessing implements Serializable
{
	
	private static final long serialVersionUID = -8293502739324677061L;

	/**
	 * The terms to search should not include the hashtag symbol (#).
	 * @param terms
	 * @param toPreprocess
	 */
	public TermDetectorOnHashtags(Collection<String> terms, boolean toPreprocess)
	{
		super(terms, toPreprocess);
	}

	
	public TermDetectorOnHashtags(TermDetectorWithProcessing tdetector) 
	{
		super(tdetector);
	}
	
	@Override
	public Map<String, Integer> findEmotionalTermsInText(String text)
	{
		List<String> foundTags = TweetPreprocessing.getHashTags(text);
		
		foundTags.retainAll(terms); // it is enough as hashtag can be only unigram
		
		Map<String, Integer> result = new HashMap<String,Integer>();
		for (String term : foundTags)
		{
			result.put(term, 1);
		}
		return result;
	}
	
	public String replaceFoundTerms(String text, String replacement)
	{
		String resultText = text.toLowerCase();
		
		Set<String> foundTags = findEmotionalTermsInText(resultText).keySet();
		
		for (String foundTag : foundTags)
		{
			resultText = resultText.replaceAll("#" + foundTag+"(?=(\\b|\\s|\u00A0|$))", "");
		}
		return resultText;
	}
}
