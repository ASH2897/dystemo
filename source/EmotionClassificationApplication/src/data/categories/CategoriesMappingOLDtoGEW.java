/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import functionality.UtilArrays;
import functionality.UtilFiles;

public class CategoriesMappingOLDtoGEW {
	
	static ICategoriesData GewCategories = new CategoriesDataGEW();
	static ICategoriesData OldCategories = new CategoriesDataOLD();

	static Map<Integer, double[]> categoryWeightsMap;
	static
	{
		categoryWeightsMap = new HashMap<Integer, double[]>();
		
		int oldCatNum = OldCategories.getCategoryNum();
		int gewCatNum = GewCategories.getCategoryNum();
		/*for (int i = 0; i < oldCatNum; ++i)
			categoryWeightsMap.put(i, new double[gewCatNum]);*/
		
		try {
			String fileWithMapping = CategoriesDataGEW.class.getClassLoader().getResource("emotionModels/mappingCatsOLDtoGEW.txt").toURI().getPath();
			List<String[]> mapLines = UtilFiles.getParsedLinesWithCSV(fileWithMapping, '\t', 1);
			for (String[] catLine : mapLines)
			{
				int oldCatId = Integer.parseInt(catLine[0]);
				int[] catDistr = new int[gewCatNum];
				for (int gewCatId = 0; gewCatId < gewCatNum; ++gewCatId)
				{
					if (!catLine[gewCatId + 1].equals(""))
						catDistr[gewCatId] = Integer.parseInt(catLine[gewCatId + 1]);
				}
				
				categoryWeightsMap.put(oldCatId, UtilArrays.normalizeArray(catDistr));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static double[] getGEWWeightsForOldCategory (int oldCategoryId)
	{
		return categoryWeightsMap.get(oldCategoryId);
	}
	
	public static double[] getGEWWeightsForOldDistr(int[] oldCatDistr) throws Exception
	{
		double[] resultWeights = new double[GewCategories.getCategoryNum()];
		
		for (int i = 0; i < oldCatDistr.length; ++i)
		{
			double[] gewWeights = getGEWWeightsForOldCategory(i + 1);
			if (gewWeights != null)
			{
				gewWeights = UtilArrays.multiplyArray(gewWeights, oldCatDistr[i]);
				resultWeights = UtilArrays.sumTwoArrays(resultWeights, gewWeights);
			}
		}
		
		return UtilArrays.normalizeArray(resultWeights);
	}
}
