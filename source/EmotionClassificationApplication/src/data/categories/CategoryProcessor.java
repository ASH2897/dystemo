/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.util.HashMap;
import java.util.Map;


/**
 * This class is for managing the connection to the specific emotion categories sets
 * @author Erian
 *
 */
public class CategoryProcessor {

	/**
	 * GEW = 20 categories from Geneva Emotion Wheel, version 2.0; also includes Neutral and Context-Dependent (or Other) category. In total uses 22 emotion categories.
	 * 
	 * OLD = 12 categories adapted for Sports' events analysis; also includes Neutral, Context-dependent, NoCategory, Skipped, PositiveOther, NegativeOther to deal with uncertanities in labeling. In total uses 18 categories.
	 */
	public enum CategoryDataType {OLD, GEW};
	
	static Map<CategoryDataType, ICategoriesData> categoriesData;
	
	static
	{
		categoriesData = new HashMap<CategoryDataType, ICategoriesData> ();
		categoriesData.put(CategoryDataType.OLD, new CategoriesDataOLD());
		categoriesData.put(CategoryDataType.GEW, new CategoriesDataGEW());
	}
	
	public static String getCategoryName(int catId, CategoryDataType catDataType)
	{
		return categoriesData.get(catDataType).getCategoryName(catId);
	}
	
	public static int getCategoryPolarity(int category,  CategoryDataType catDataType)
	{
		return categoriesData.get(catDataType).getCategoryPolarity(category);
	}
	
	public static int neutralCategory(CategoryDataType catDataType)
	{
		return categoriesData.get(catDataType).neutralCategory();
	}
	
	public static int contextCategory(CategoryDataType catDataType)
	{
		return categoriesData.get(catDataType).contextCategory();
	}
	
	public static double getCategorySimilarity(CategoryDataType catDataType, int cat1, int cat2)
	{
		return categoriesData.get(catDataType).getCategorySimilarity(cat1, cat2);
	}
	
	public static ICategoriesData getCategoriesData(CategoryDataType catDataType)
	{
		return categoriesData.get(catDataType);
	}
	
	public static CategoryDataType getCategoryDataTypeByString(String categoryDataName)
	{
		if (categoryDataName.equals("GEW"))
			return CategoryDataType.GEW;
		else if (categoryDataName.equals("OLD"))
			return CategoryDataType.OLD;
		return null;
	}
	
	public static ICategoriesData getCategoriesDataByName(String categoryDataName)
	{
		return categoriesData.get(getCategoryDataTypeByString(categoryDataName));
	}
	
	public static int[] getNonDominantEmotions(CategoryDataType catDataType) {
		return getCategoriesData(catDataType).getNonDominantEmotions();
	}
	
}
