/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

public class CategoryPair {

	// the categories are ordered
			int categoryId1;
			int categoryId2;
			
			// makes sure that the categories are ordered
			public CategoryPair(int categoryId1, int categoryId2)
			{
				this (categoryId1, categoryId2, true);
			}

			public CategoryPair(int categoryId1, int categoryId2, boolean oneOrder)
			{
				if (oneOrder)
					if (categoryId1 > categoryId2)
					{
						this.categoryId1 = categoryId2;
						this.categoryId2 = categoryId1;
					}
					else
					{
						this.categoryId1 = categoryId1;
						this.categoryId2 = categoryId2;
					}
				else
				{
					this.categoryId1 = categoryId1;
					this.categoryId2 = categoryId2;
				}
			}
			
			@Override
			public boolean equals(Object obj)
			{
				if (obj instanceof CategoryPair)
				{
					CategoryPair objTr = (CategoryPair) obj;
					return (this.categoryId1 == objTr.categoryId1) && (this.categoryId2 == objTr.categoryId2);
				}
				return false;
			}
			
			@Override
			public int hashCode()
			{
				return this.toString().hashCode();
			}
			
			
			public String toString()
			{
				return categoryId1 + "\t" + categoryId2;
			}
}
