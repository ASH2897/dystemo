/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import functionality.UtilCollections;

public class PolarityUtil {

	public static String getPolarityName(int polarityLabel)
	{
		switch(polarityLabel)
		{
			case 0: return "Neutral";
			case 1: return "Positive";
			case -1: return "Negative";
			case -2: return "Context-Dependent";
			case -3: return "NotAgreed";
		}
		return null;
	}
	
	
	public static int disambiguatePolarity(List<Integer> polarityLabels)
	{
		Map<Integer, Integer> catFreq = UtilCollections.computeFrequencies(polarityLabels);
		int maxFreq = 0;
		for (Integer label : catFreq.keySet())
		{
			int curFreq = catFreq.get(label);
			if (curFreq > maxFreq)
				maxFreq = curFreq;
		}
		
		List<Integer> maxCatsIndexes = new ArrayList<Integer>();
		for (Integer label : catFreq.keySet())
		{
			if (catFreq.get(label) == maxFreq)
				maxCatsIndexes.add(label);
		}
		
		if (maxCatsIndexes.size() == 1)
			return maxCatsIndexes.get(0);
		else
			return -3;
	}

}
