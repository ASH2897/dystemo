/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.categories.CategoryProcessor.CategoryDataType;
import au.com.bytecode.opencsv.CSVReader;
import functionality.UtilArrays;
import functionality.UtilCollections;

/**
 * The categories data corresponding to 20 emotion categories from  Geneva Emotion Wheel (GEW), version 2.0.
 * Please, refer to http://www.affective-sciences.org/en/gew/ for more information and references.
 * 
 */
public class CategoriesDataGEW implements ICategoriesData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4565419595369525199L;


	static
	{
		try {
			readGEWCategoriesNames();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static final int categoryNum = 22;
	
	static Map<Integer, String> categoriesNames;
	
	public static void readGEWCategoriesNames() throws IOException
	{
		categoriesNames = new HashMap<Integer,String>();
		String fileWithCats = CategoriesDataGEW.class.getClassLoader().getResource("emotionModels/GEWCategories.txt").getPath();
		fileWithCats = fileWithCats.replace("%20", " ");
		CSVReader reader = new CSVReader(new FileReader(fileWithCats),  '\t');
		String [] nextLine;
		 while ((nextLine = reader.readNext()) != null) {
			 String catName = nextLine[0];
			 String catIndexStr = nextLine[1];
			 categoriesNames.put(Integer.parseInt(catIndexStr), catName);
		 }
		 reader.close();
		 categoriesNames.put(-3, "NotAgreed");
	}
	
	
	public String getCategoryName(int catId) {
		return categoriesNames.get(catId);
	}

	public String getPolarityName(int polarityLabel) {
		switch(polarityLabel)
		{
			case 0: return "Neutral";
			case 1: return "Positive";
			case -1: return "Negative";
			case -2: return "Context-Dependent";
			case -3: return "NotAgreed";
		}
		return null;
	}

	public double getPolaritySimilarity(int cat1, int cat2) {
		if (cat1 == cat2)
			return 1.0;
		int catMin = Math.min(cat1, cat2);
		if (catMin == -2)
			return 0.5;
		return 0.0;
	}

	public double getCategorySimilarity(int cat1, int cat2) {
		if (cat1 == cat2)
			return 1.0;
		else if (getCategoryPolarity(cat1) != getCategoryPolarity(cat2))
			return 0.0;
		else
		{
			return (10 - Math.abs(cat1 - cat2))/10.0;
		}
	}

	public int getMostGeneralCategory(List<Integer> categories) {
		return -1;
	}

	public boolean hasSamePolarity(List<Integer> categories) {
		int firstPolarity = getCategoryPolarity(categories.get(0));
		boolean allSamePolarity = true;
			for (Integer cat : categories)
			{
				if (getCategoryPolarity(cat) != firstPolarity)
				{
					allSamePolarity = false;
					break;
				}
			}
		return allSamePolarity;
	}

	
	public int getCategoryPolarity(int categoryId) {
		if (categoryId >= 1 && categoryId <= 10)
			return Polarity.POSITIVE;
		if (categoryId >= 11 && categoryId <= 20)
			return Polarity.NEGATIVE;
		if (categoryId == 21)
			return Polarity.NEUTRAL;
		if (categoryId == 22)
			return -2;
		return -3;
	}

	@Override
	public int getCategoryNum() {
		return categoryNum;
	}


	@Override
	public int neutralCategory() {
		return 21;
	}


	@Override
	public int contextCategory() {
		return 22;
	}
	
	@Override
	public int notAgreedCategory() {
		return -3;
	}


	private final int[] categoriesToUse = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22}; // if changed - need to change methods getCategoryIndexBy... as well
	
	@Override
	public final int[] categoriesToUse() {
		return categoriesToUse;
	}
	
	@Override
	public boolean isAmbiguous(int[] categoryData) {
		int posValue = 0;
		int negValue = 0;
		int neutralValue = categoryData[20];
		for (int i = 0; i < 10; ++i)
			posValue += categoryData[i];
		for (int i = 10; i < 20; ++i)
			negValue += categoryData[i];
		int maxValue = Math.max(posValue, Math.max(negValue, neutralValue));
		int sumValue = posValue + negValue + neutralValue;
		double score = 1.0 * (2 * maxValue - sumValue) / (sumValue);
		return score <= 0.5;
	}


	@Override
	/**
	 * Computes if the current data is ambiguous -> TODO: ideally should put into CategoryUtil
	 */
	public boolean isAmbiguous(double[] categoryData) {
		return isAmbiguous(categoryData, 0.5);
	}
	
	@Override
	public boolean isAmbiguous(double[] categoryData, double ambLevel) {
		double posValue = 0;
		double negValue = 0;
		double neutralValue = categoryData[20];
		for (int i = 0; i < 10; ++i)
			posValue += categoryData[i];
		for (int i = 10; i < 20; ++i)
			negValue += categoryData[i];
		double maxValue = Math.max(posValue, Math.max(negValue, neutralValue));
		double sumValue = posValue + negValue + neutralValue;
		double score = 1.0 * (2 * maxValue - sumValue) / (sumValue);
		return score < ambLevel;
	}



	@Override
	public String getCategoryNameByIndex(int catIndex) {
		
		return getCategoryName(categoriesToUse()[catIndex]);
	}


	@Override
	public int getCategoryQuadrant(int category) {
		if (category >= 1 && category <= 5)
			return Quadrant.POSITIVE_HIGHCONTROL;
		if (category >= 6 && category <= 10)
			return Quadrant.POSITIVE_LOWCONTROL;
		if (category >= 11 && category <= 15)
			return Quadrant.NEGATIVE_LOWCONTROL;
		if (category >= 16 && category <= 20)
			return Quadrant.NEGATIVE_HIGHCONTROL;
		if (category == 21)
			return Quadrant.NEUTRAL;
		if (category == 22)
			return Quadrant.CONTEXTDEPENDENT; // -2
		return Quadrant.UNDEFINED; // -3
	}


	@Override
	public CategoryDataType getCategoriesType() {
		return CategoryDataType.GEW;
	}


	@Override
	public List<String> getCategoriesEnumeration() {
		List<String> catList = new ArrayList<String>();
		for (int catId : categoriesToUse())
		{
			catList.add(categoriesNames.get(catId));
		}
		
		return catList;
	}


	@Override
	public int getCategoryIndexByName(String categoryName) {
		List<String> categoryEnumeration = getCategoriesEnumeration();
		if (categoryName.equals("Neutral"))
			categoryName = "No Emotion";
		return categoryEnumeration.indexOf(categoryName);
	}
	
	@Override
	public int getCategoryId(int categoryIndex)
	{
		if (categoryIndex < categoriesToUse.length && categoryIndex >= 0)
			return categoriesToUse[categoryIndex];
		else
			return -1;
	}


	@Override
	public int getPolarityIndex(int polarityLabel) {
		switch(polarityLabel)
		{
			case 0: return 0;
			case 1: return 1;
			case -1: return 2;
			case -2: return 3;
			case -3: return 4;
		}
		return -1;
	}


	@Override
	public int getCategoryIndexById(int id) {
		return id - 1;
	}


	@Override
	public boolean isEmotionalCategory(int categoryId) {
		return (categoryId <= 20);
	}

	private final int[] emotionalCategories = UtilArrays.subArray(categoriesToUse, 0, 19);
	

	@Override
	public int[] emotionalCategoriesToUse() {
		return emotionalCategories;
	}
	
	private int[] nonDominantEmotions;
	
	@Override
	public int[] getNonDominantEmotions() {
		if (nonDominantEmotions == null) {
			nonDominantEmotions = new int[]{2,5,7,8,11,15,18,19};
		}
		return nonDominantEmotions;
	}
	
}
