/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.documents;

public class TweetFeaturesExtended extends Tweet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1640161772985915719L;

	public TweetFeaturesExtended() {};
	public TweetFeaturesExtended(Tweet origTweet)
	{
		this.tweetId = origTweet.tweetId;
		this.text = origTweet.text;
		this.userName = origTweet.userName;
	}
	
	public TweetFeaturesExtended(Tweet origTweet, String featureLine)
	{
		this.tweetId = origTweet.tweetId;
		this.text = origTweet.text;
		this.userName = origTweet.userName;
		String[] features = featureLine.split(separator);
		this.length = Integer.parseInt(features[0]);
		this.isEmoticonPresent =  (Integer.parseInt(features[1]) > 0);
		this.numberOfExclMarks =  Integer.parseInt(features[2]);
		this.numberOfQuestionMarks =  Integer.parseInt(features[3]);
		this.numberOfTabWords =  Integer.parseInt(features[4]);
		this.numberOfAlongations =  Integer.parseInt(features[5]);
	}
	
	public int length;
	public boolean isEmoticonPresent;
	public int numberOfExclMarks;
	public int numberOfQuestionMarks;
	public int numberOfTabWords;
	public int numberOfAlongations; // number of times the characters are repeated in order
	
	public boolean equals(Object obj)
	{
		if (obj instanceof Tweet)
		{
			return ((Tweet)obj).tweetId == this.tweetId;
		}
		return false;
	}
	
	static String separator = ";";
	public String getFeaturesString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(length + separator);
		sb.append((isEmoticonPresent?1:0) + separator);
		sb.append(numberOfExclMarks + separator);
		sb.append(numberOfQuestionMarks + separator);
		sb.append(numberOfTabWords + separator);
		sb.append(numberOfAlongations + separator);
		return sb.toString();
	}
	
	public boolean endPosition;
	public int categoryID;
	public boolean severalHashTags;
}
