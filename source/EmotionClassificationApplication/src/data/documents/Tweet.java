/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.documents;

import java.io.Serializable;

public class Tweet implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4226679432895679860L;
	public int tweetId;
	public String text;
	public String userName;

	
	public boolean equals(Object obj)
	{
		if (obj instanceof Tweet)
		{
			return ((Tweet)obj).tweetId == this.tweetId;
		}
		return false;
	}
	
	@Override
	public String toString()
	{
		return "id: " + tweetId+ "\ttext: \"" + text + "\"\tuser: "+ userName;
	}

	@Override
	public int hashCode() {
		return ((Integer)tweetId).hashCode();
	}
}
