/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package processing;

public class TweetPatterns {
	static final String linkRegEx = "https?://[\\S&&[^#@\"]]+";
	static final String usernameRegEx = "@[\\S&&[^#@]]+";
	static final String hashtagRegEx = "#[[^\\p{Punct}\\s]_]+";
	static final String retweetPattern = "(?<=(\\s|\u00A0|^))[rR][tT](?=(\\s|\u00A0|$|@))"; // includes the conditions on leading and closing symbols
	
}
