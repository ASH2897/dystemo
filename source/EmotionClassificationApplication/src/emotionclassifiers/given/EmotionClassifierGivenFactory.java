/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
    
    ------------------------------------
    
    Note that the given classifiers are provided for exemplary and research purposes. 
    Please refer to LEXICONS_LICENSING.txt file to read the terms of use for the provided lexicon-based classifiers.
    
*/

package emotionclassifiers.given;

import classification.definitions.classifiers.WeightedClassifierLexiconBasedWithIntensityModel;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.WeightedClassifierWekaBased;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import data.categories.CategoryProcessor.CategoryDataType;


public class EmotionClassifierGivenFactory {
	
	public static WeightedClassifierLexiconBased getOlympLexClassifier() throws Exception
	{
		return new WeightedClassifierLexiconBased(new OlympLexEmotionLexicon());
	}
	
	public static WeightedClassifierLexiconBased getOlympLexClassifier(boolean preprocessed) throws Exception
	{
		boolean oldPreproc = OlympLexEmotionLexicon.toUsePreprocessedVersion;
		OlympLexEmotionLexicon.toUsePreprocessedVersion = preprocessed;
		WeightedClassifierLexiconBased olympLexClassifier = new WeightedClassifierLexiconBased(new OlympLexEmotionLexicon());
		OlympLexEmotionLexicon.toUsePreprocessedVersion = oldPreproc;
		return olympLexClassifier;
	}
	
	public static WeightedClassifierLexiconBased getGALCClassifier() throws Exception
	{
		return GALCClassifierLoader.getGALCClassifier();
	}
	
	public static WeightedClassifierLexiconBased getGALCRevisedInstancesClassifier() throws Exception
	{
		return GALCClassifierLoader.getGALCRevisedInstancesClassifier();
	}
	
	
	public static WeightedClassifierLexiconBasedWithIntensityModel getComplexOlympLexClassifier() throws Exception
	{
		return new WeightedClassifierLexiconBasedWithIntensityModel(new OlympLexEmotionLexicon());
	}
	
	public static WeightedClassifierLexiconBasedWithIntensityModel getComplexGALCClassifier() throws Exception
	{
		return new WeightedClassifierLexiconBasedWithIntensityModel(getGALCClassifier());
	}

	/**
	 * The PMI-based classifier built over 500000 tweets with emotional hashtags for 20 GEW2.0 emotion categories 
	 * @return
	 * @throws Exception
	 */
	public static WeightedClassifierLexiconBased getPMIHashClassifier() throws Exception
	{
		return new WeightedClassifierLexiconBased(
				WeightedEmotionLexicon.loadFromTextFile(
						EmotionClassifierGivenFactory.class.getClassLoader().getResource("givenLexicons/PMI-Hash.txt").toURI().getPath(), 
						CategoryDataType.GEW, false));
	}
	
}
