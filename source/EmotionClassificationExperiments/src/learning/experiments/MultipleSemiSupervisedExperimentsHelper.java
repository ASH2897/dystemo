/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import semisupervisedlearning.parameters.SSLFrameworkParameters;
import experimentsavers.ExperimentsResultsSaver;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.experiments.MultipleLearningExperimentsHelper.AllLearningParametersEnumerationList;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.ClassifierApplicationParameters.ClassifierApplicationParamsEnumerationList;
import learning.parameters.ClassifierLearnerParams.ClassifierLearnerParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.DataNgramExtractionParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.FeatureExtractionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.FeatureSelectionParamsEnumerationList;

public class MultipleSemiSupervisedExperimentsHelper {
	
	
	public static void runOverParamList(
			AllSemiSupervisedLearningParametersEnumerationList allParamsEnumList, String[] testDataNames) throws Exception
	{
		List<AllSemiSupervisedLearningParameters> diffLearningParams = getAllLearningParametersEnumerationFromGivenInput(allParamsEnumList);
		runOverMultipleParametersOnMultipleTestsWithMultipleApplicationParams(diffLearningParams, testDataNames, 0);
	
	}
	
	public static void runOverParamList(
			AllSemiSupervisedLearningParametersEnumerationList allParamsEnumList, String[] testDataNames, int startInd) throws Exception
	{
		List<AllSemiSupervisedLearningParameters> diffLearningParams = getAllLearningParametersEnumerationFromGivenInput(allParamsEnumList);
		runOverMultipleParametersOnMultipleTestsWithMultipleApplicationParams(diffLearningParams, testDataNames, startInd);
	
	}
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * The built classifiers will be saved on the disk in the folder for classifiers output (specified by OneSemiSupervisedLearningProcess printing folder and file names).
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTestsWithSaving(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames) throws Exception
	{
		runOverMultipleParametersOnMultipleTestsWithSaving(allDifferentParams, testDataNames, 0);
	}
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * The built classifiers will be saved on the disk in the folder for classifiers output (specified by OneSemiSupervisedLearningProcess printing folder and file names).
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTestsWithSaving(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames, int paramStartInd) throws Exception
	{
		OneSemiSupervisedLearningProcessHelper.toPrintResultClassifier = true;
		OneSemiSupervisedLearningProcessHelper.toPrintWithClassifiersRunNum = true;
		
		int paramInd = -1;
		for (AllSemiSupervisedLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; Date:" + (new Date()).toString());
			
			OneSemiSupervisedLearningProcessHelper.runOverSeveralGivenTests(fullLearningParams, testDataNames);
		}
	}
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTests(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames) throws Exception
	{
		runOverMultipleParametersOnMultipleTests(allDifferentParams, testDataNames, 0);
	}
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTests(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames, int paramStartInd) throws Exception
	{
		OneSemiSupervisedLearningProcessHelper.toPrintResultClassifier = false;
		int paramInd = -1;
		for (AllSemiSupervisedLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; Date:" + (new Date()).toString());
			
			OneSemiSupervisedLearningProcessHelper.runOverSeveralGivenTests(fullLearningParams, testDataNames);
		}
	}
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTestsWithMultipleApplicationParams(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames) throws Exception
	{
		runOverMultipleParametersOnMultipleTestsWithMultipleApplicationParams(
				allDifferentParams, testDataNames, 0);
	}
	
	
	/**
	 * Run multiple semi-supervised learning experiments with the list of specified parameters. 
	 * The evaluation is performed over several specified test data (given as names in the default index).
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams The list of different parameters for training classifiers.
	 * @param testDataNames The given list of test data to perform evaluation of resultant classifiers.
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersOnMultipleTestsWithMultipleApplicationParams(
			List<AllSemiSupervisedLearningParameters> allDifferentParams, String[] testDataNames, int paramStartInd) throws Exception
	{
		OneSemiSupervisedLearningProcessHelper.toPrintResultClassifier = false; 
		int paramEndInd = Integer.MAX_VALUE;
		int paramInd = -1;
		for (AllSemiSupervisedLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd; 
			if (paramInd < paramStartInd || paramInd >= paramEndInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; Date:" + (new Date()).toString());
			
			OneSemiSupervisedLearningProcessHelper.runOverSeveralGivenTests(fullLearningParams, testDataNames, lastResultClassifierApplicationParametersOptions.get(fullLearningParams.learnerName));
		}
	}
	
	/** PARAMETERS SPECIFICATIONS AND PARSING PROCEDURE **/
	
	public static class  AllSemiSupervisedLearningParametersEnumerationList extends AllLearningParametersEnumerationList
	{
		public String[] initialClassifiersNames;
		public ClassifierApplicationParameters[] initialClassifierApplicationParameters;
		public String[] unlabeledDataNames;
		public boolean useCacheOnAnnotation;
		
		public boolean ignoreInitialAnnotationInNeutralTweets;
	}
	
	static Map<LearnerName, List<ClassifierApplicationParameters>> lastResultClassifierApplicationParametersOptions;
	
	public static List<AllSemiSupervisedLearningParameters> getAllLearningParametersEnumerationFromGivenInput(
			AllSemiSupervisedLearningParametersEnumerationList allParamsEnumList
			)
	{
		return getAllSemiSupervisedLearningParametersEnumerationFromGivenInput(
				allParamsEnumList.experimentsName, allParamsEnumList.trainDataName, allParamsEnumList.useCache, allParamsEnumList.usedFeatureType, allParamsEnumList.featureExtractionParamsEnumerationList,
				allParamsEnumList.usedFeatureSelectionTypes, allParamsEnumList.featureSelectionParamsEnumerationListMap, allParamsEnumList.learnerNames, allParamsEnumList.classifierLearnersParametersEnumeration,
				allParamsEnumList.initialClassifiersNames, allParamsEnumList.unlabeledDataNames, allParamsEnumList.useCacheOnAnnotation,
				allParamsEnumList.initialClassifierApplicationParameters,
				allParamsEnumList.learnersApplicationParametersEnumeration, allParamsEnumList.ignoreInitialAnnotationInNeutralTweets
				
				);
	}
	
	public static List<AllSemiSupervisedLearningParameters> getAllSemiSupervisedLearningParametersEnumerationFromGivenInput(
			String experimentName,
			String trainDataName, boolean useCache,
			FeatureType usedFeatureType, FeatureExtractionParamsEnumerationList featureExtractionParamsEnumerationList,
			List<SelectionType[]> usedFeatureSelectionTypes,
			Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap,
			LearnerName[] learnerNames, Map<LearnerName, ClassifierLearnerParamsEnumerationList> classifierLearnersParametersEnumeration,
			String[] initialClassifiersNames, String[] unlabeledDataNames, boolean useCacheOnAnnotation,
			ClassifierApplicationParameters[] initialClassifierApplicationParameters,
			Map<LearnerName, ClassifierApplicationParamsEnumerationList> classifierApplicationParamsEnumeration,
			boolean ignoreInitialAnnotationInNeutralTweets
			)
	{
		ExperimentsResultsSaver.currentTestName = experimentName;
		
		List<AllSemiSupervisedLearningParameters> allParametersToRun = new ArrayList<AllSemiSupervisedLearningParameters>();
		lastResultClassifierApplicationParametersOptions = new HashMap<LearnerName, List<ClassifierApplicationParameters>>();
		
		
		List<FeatureExtractionParameters> diffFeatureExtractionParams = MultipleLearningExperimentsHelper.getFeatureExtractionParametersEnumeration(useCache, usedFeatureType, featureExtractionParamsEnumerationList);
		List<FeatureSelectionParameters> diffFeatureSelectionParams = MultipleLearningExperimentsHelper.getFeatureSelectionParametersEnumeration(usedFeatureSelectionTypes, featureSelectionParamsEnumerationListMap);
		Map<LearnerName, List<ClassifierLearnerParams>> mapOfDiffClassifierLearnerParams = new HashMap<LearnerName, List<ClassifierLearnerParams>>();
		
		for (Map.Entry<LearnerName, ClassifierLearnerParamsEnumerationList> learnerEnumn : classifierLearnersParametersEnumeration.entrySet())
		{
			List<ClassifierApplicationParameters> curLearnerApplicOptions =  MultipleLearningExperimentsHelper.getApplicationParametersEnumeration(classifierApplicationParamsEnumeration.get(learnerEnumn.getKey()));
			lastResultClassifierApplicationParametersOptions.put(learnerEnumn.getKey(), curLearnerApplicOptions );
			List<ClassifierLearnerParams> curLearnerParamOptions = MultipleLearningExperimentsHelper.getLearnerParamsEnumeration(learnerEnumn.getKey(), learnerEnumn.getValue(), curLearnerApplicOptions.get(0));
			mapOfDiffClassifierLearnerParams.put(learnerEnumn.getKey(), curLearnerParamOptions);
		}
		
		for (String unlabeledDataName : unlabeledDataNames)
		for (String initialClassifierName : initialClassifiersNames)
		for (FeatureExtractionParameters featureExtractionParameters : diffFeatureExtractionParams)
		for (LearnerName learnerName : learnerNames)
		for (FeatureSelectionParameters featureSelectionParameters : diffFeatureSelectionParams)
		for (ClassifierApplicationParameters  initApplicationParams : initialClassifierApplicationParameters)
		for (ClassifierLearnerParams classifierParams : mapOfDiffClassifierLearnerParams.get(learnerName))
		{
			ClassifierLearnerParams  newLearnerParams  = MultipleLearningExperimentsHelper.makeTermDetectionParametersConsistent(classifierParams, featureExtractionParameters);
		
			// avoiding unnecessary initial refinement variation
			if (learnerName != LearnerName.WeightedBalancedVoting && learnerName != LearnerName.WeightedVoting)
			{
				// to include other classifiers for which there might be a change due to initial refinement (depending whether the weights or labels are taken)
				if (initialClassifierName.equals("GALC-R-Inst"))
				{
					if (newLearnerParams.multiLabelInitialRefinementParams != null)
						continue;
				}
				else if (initialClassifierName.equals("OlympLex") || initialClassifierName.equals("OlympLex-Prep") || initialClassifierName.equals("PMI-Hash"))
				{
					if (newLearnerParams.multiLabelInitialRefinementParams == null)
						continue;
				}
			}
			
			AllSemiSupervisedLearningParameters fullLearningParams = new AllSemiSupervisedLearningParameters();
			fullLearningParams.featureExtractionParams = featureExtractionParameters;
			fullLearningParams.featureSelectionParams = featureSelectionParameters;
			fullLearningParams.learnerName = learnerName;
			fullLearningParams.learnerParams = newLearnerParams;
			fullLearningParams.trainDataName = trainDataName;
			fullLearningParams.sslParameters = new SSLFrameworkParameters();
			fullLearningParams.sslParameters.initialClassifierApplicationParams =  initApplicationParams;
			fullLearningParams.sslParameters.unlabeledDataName = unlabeledDataName;
			fullLearningParams.sslParameters.initialClassifierName = initialClassifierName;
			fullLearningParams.sslParameters.ignoreInitialAnnotationInNeutralTweets = ignoreInitialAnnotationInNeutralTweets;
			
			allParametersToRun.add(fullLearningParams);
		}
		
		return allParametersToRun;
	}
	
	public static void updateNgramsOptions(DataNgramExtractionParamsEnumerationList dataNgramExtractionParamsList, int[] newNgramsMaximums)
	{
		dataNgramExtractionParamsList.diffMaxNgramLengthList = newNgramsMaximums;
	}
	
	public static void updateThresholdOptionsInBinaryFeatureSelectionSpecifics(Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap, 
			SelectionType selType, double[] thresholds)
	{
		BinarySelectionParamsEnumerationList binSelOptions = (BinarySelectionParamsEnumerationList)featureSelectionParamsEnumerationListMap.get(selType);
		binSelOptions.thresholdsOptions = thresholds;
	}
	
	public static void updateInitialOutputSettings(LearnerName[] learnerNames, 
			MultiLabelRefineParams[] initialMultiLabelOptions, 
			Map<LearnerName, ClassifierLearnerParamsEnumerationList> classifierLearnersParametersEnumeration)
	{
		for (LearnerName learnerName : learnerNames)
		{
			 ClassifierLearnerParamsEnumerationList paramList =  classifierLearnersParametersEnumeration.get(learnerName);
			 paramList.initialMultiLabelOptions = initialMultiLabelOptions;
		}
	}
	
}
