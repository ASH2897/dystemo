/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.Date;
import java.util.List;
import java.util.Map;

import semisupervisedlearning.SemiSupervisedLearner;
import semisupervisedlearning.SemiSupervisedLearnerFactory;
import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import utility.Pair;
import utils.UtilCategoryLabelFormatter;
import learners.definitions.ClassifierLearner;
import learners.given.ClassifierLearnerFactory;
import learning.evaluation.ClassifierEvaluation;
import learning.parameters.AllLearningParameters;
import learning.parameters.ClassifierApplicationParameters;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classifiers.initialization.GivenFeaturesIndex;
import classifiers.initialization.UsedClassifierFactory;
import data.CompositeData;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.categories.CategoryProcessor;
import data.documents.Tweet;
import data.loading.DataSetFormatter;
import experimentsavers.ExperimentsResultsSaver;


public class OneSemiSupervisedLearningProcessHelper {

	public static String folderToPrintClassifiers = "output/learnedClassifiers/";
	public static String nameForPrintClassifiers = "trained-classifier-ssl";
	public static boolean toPrintResultClassifier = true;
	public static boolean toPrintWithClassifiersRunNum = false;
	
	
	/**
	 * Builds the classifier using the specified learning parameters.
	 * Note that we assume that provided unlabeled data can be either TweetData or CompositeData types. 
	 * In the former case, only the unlabeled data would be used.
	 * In the latter case, both unlabeled data and pseudo-neutral data would be used (if given). 
	 * @param fullLearningParams
	 * @return
	 * @throws Exception
	 */
	private static WeightedClassifier buildClassifierWithSSL(AllSemiSupervisedLearningParameters fullLearningParams) throws Exception
	{
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
		
		AllLearningParameters.currentSetup = fullLearningParams;
		
		// initialization
		WeightedClassifier initialClassifier = UsedClassifierFactory.getClassifierByName(
				fullLearningParams.sslParameters.initialClassifierName, 
				fullLearningParams.sslParameters.initialClassifierApplicationParams);
		
		List<String> initialFeaturesList = null;
		
		if (fullLearningParams.featureExtractionParams.givenFeatureListsNames != null)
			initialFeaturesList = GivenFeaturesIndex.getGivenFeatureList(fullLearningParams.featureExtractionParams.givenFeatureListsNames);// has to be initialized in any case 
		else if (fullLearningParams.sslParameters.preserveTermsFromInitialClassifier) {
			fullLearningParams.featureExtractionParams.useGivenIndicatorsAlongWithDiscovered = true;
			initialFeaturesList = initialClassifier.getFeatureNames();
		}
		
		ClassifierLearner classifierLearner = ClassifierLearnerFactory.getLearner(fullLearningParams, initialFeaturesList);
		
		CompositeData compositeTweetData = null;
		
		if (fullLearningParams.sslParameters.unlabeledDataName != null)
			compositeTweetData = DataRepositoryIndex.getCompositeDataByName(fullLearningParams.sslParameters.unlabeledDataName);
		
		Pair<List<Tweet>, Map<Integer, double[]>> givenLabeledTweetData = null;
		if (fullLearningParams.trainDataName != null)
		{
			// initialize the labeled data
			LabeledData labelData = DataRepositoryIndex.getLabeledDataByName(fullLearningParams.trainDataName);
			Map<Integer, String> labeledTweetData = labelData.textData;
			Map<Integer, List<Integer>> categoryAssignment = labelData.emotionLabels;
			List<Tweet> labeledTweets = DataSetFormatter.getTweetsFromStrings(labeledTweetData);
			
			Map<Integer, double[]> weightedAssignment = UtilCategoryLabelFormatter.transformStrictAnnotationsIntoWeighted(
					CategoryProcessor.getCategoriesData(classifierLearner.getEmotionCategoriesType()), categoryAssignment);
			
			givenLabeledTweetData = new Pair(labeledTweets, weightedAssignment);
		}
		
		System.out.println(new Date().toString() + ": Ready for learning (data loaded)");
		SemiSupervisedLearner sslLearner = SemiSupervisedLearnerFactory.initializeSemiSupervisedLearner(
				fullLearningParams, initialClassifier, classifierLearner, 
				compositeTweetData == null ? null : compositeTweetData.unlabeledData.tweets, 
				compositeTweetData == null ? null : compositeTweetData.pseudoNeutralData.tweets, 
				givenLabeledTweetData);
		
		// learning part
		WeightedClassifier resClassifier = sslLearner.learn();
		if (toPrintResultClassifier)
		{
			Date printStart = new Date();
			resClassifier.printToTextFile(folderToPrintClassifiers + nameForPrintClassifiers + (toPrintWithClassifiersRunNum?ExperimentsResultsSaver.currentRunNum:"") + ".txt");
			System.out.println("Classifier is printed out. It took " + ((new Date().getTime() - printStart.getTime())/60000.0) + " minutes");
		}
		lastClassifierLearner = classifierLearner;
		return resClassifier;
	}
	
	static ClassifierLearner lastClassifierLearner;

	/**
	 * This function learns the classifier according to the specified parameters and tests in across several test data from data index. 
	 */
	public static void runOverSeveralGivenTests(
			AllSemiSupervisedLearningParameters fullLearningParams,  
			String[] testDataNames) throws Exception
	{
		WeightedClassifier resClassifier =  buildClassifierWithSSL(fullLearningParams);
		
		for (String testDataName : testDataNames)
		{
			System.out.println("Evaluate over test data:"  + testDataName);
			Date evalStart = new Date();
			EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataName);
			System.out.println("Classifier evaluation took " + ((new Date().getTime() - evalStart.getTime())/60000.0) + " minutes");
			System.out.println(evalResult.toString());
			
			ExperimentsResultsSaver.saveEvaluationResults(testDataName, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
			if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
				ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataName, true, true);
		}
	}
	
	/**
	 * This function learns the classifier and tests in across several test data from data index. 
	 * However, it also varies the different application parameters of the classifier, without requiring to re-learn it.
	 */
	public static void runOverSeveralGivenTests(
			AllSemiSupervisedLearningParameters fullLearningParams,  
			String[] testDataNames, 
			List<ClassifierApplicationParameters> classifierApplicationOptions) throws Exception
	{
		WeightedClassifier resClassifier =  buildClassifierWithSSL(fullLearningParams);
		
		for (String testDataName : testDataNames)
		{
			System.out.println("Evaluate over test data:"  + testDataName);
			Date evalStart = new Date();
			EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataName);
			System.out.println("Classifier evaluation took " + ((new Date().getTime() - evalStart.getTime())/60000.0) + " minutes");
			System.out.println(evalResult.toString());
			
			ExperimentsResultsSaver.saveEvaluationResults(testDataName, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
			if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
				ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataName, true, true);
		}
		
		if (classifierApplicationOptions != null)
		{
			// evaluating other application parameters directly
			for (int i = 1; i < classifierApplicationOptions.size(); ++i)
			{
				ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
				ClassifierApplicationParameters newApplicationParams = classifierApplicationOptions.get(i);
				newApplicationParams.parametersOfTermDetectionForClassifier = fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection;
				resClassifier = lastClassifierLearner.updateOutputClassifierForApplicationParameters(resClassifier, newApplicationParams);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = newApplicationParams; // so that the parameters would be printed correctly
				
				for (String testDataName : testDataNames)
				{
					System.out.println("Evaluate over test data:"  + testDataName);
					Date evalStart = new Date();
					EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataName);
					System.out.println("Classifier evaluation took " + ((new Date().getTime() - evalStart.getTime())/60000.0) + " minutes");
					System.out.println(evalResult.toString());
					
					ExperimentsResultsSaver.saveEvaluationResults(testDataName, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
				
					if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
						ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataName, true, true);
				}
				
			}
		}
	}
	
	/**
	 * This function builds the classifier using the specified learning parameters and evaluates its performance over the given test data from data index.
	 */
	public static void runOverGivenTest(AllSemiSupervisedLearningParameters fullLearningParams,  String testDataName) throws Exception
	{
		WeightedClassifier resClassifier =  buildClassifierWithSSL(fullLearningParams);
		
		System.out.println("Evaluate over test data:"  + testDataName);
		
		
		Date evalStart = new Date();
		EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataName);
		System.out.println("Classifier evaluation took " + ((new Date().getTime() - evalStart.getTime())/60000.0) + " minutes");
		System.out.println(evalResult.toString());
		
		ExperimentsResultsSaver.saveEvaluationResults(testDataName, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
		// if to save the detailed evaluation
		if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
			ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataName, true, true);
	}
}
