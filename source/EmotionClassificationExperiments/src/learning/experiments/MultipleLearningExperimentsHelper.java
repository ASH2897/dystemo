/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;
import utility.Pair;
import experimentsavers.ExperimentsResultsSaver;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learners.given.ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams;
import learners.given.ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams;
import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierApplicationParameters.ClassifierApplicationParamsEnumerationList;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.ClassifierLearnerParams.ClassifierLearnerParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.FeatureExtractionParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.GivenFeaturesExtractionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.ThresholdOnlySelectionParamsEnumerationList;
import learning.parameters.IParametersEnumerationList;
import learning.parameters.IndependentLearnerParameters;
import learning.parameters.IndependentLearnerParameters.IndependentLearnerParamsEnumerationList;
import learning.parameters.AllLearningParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.RebalancingParameters;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.DataNgramExtractionParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.DataNgramExtractionParamsEnumerationList.ExtraParamCombinationForNgramExtraction;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.FeatureSelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList.ExtraParamCombinationForBinaryFeatureSelection;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import linguistic.TermDetectionParameters;


public class MultipleLearningExperimentsHelper {

	static Map<LearnerName, List<ClassifierApplicationParameters>> lastResultClassifierApplicationParametersOptions;
	
	public static void runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(
			AllLearningParametersEnumerationList allParamsEnumList, String testDataName) throws Exception {
		List<AllLearningParameters> diffLearningParams = getAllLearningParametersEnumerationFromGivenInput(allParamsEnumList);
		runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(diffLearningParams, testDataName);
	}
	
	public static void runOverMultipleParamsDevelopmentAndTestSplit(
			AllLearningParametersEnumerationList allParamsEnumList, double splitPercentage) throws Exception
	{
		List<AllLearningParameters> diffLearningParams = getAllLearningParametersEnumerationFromGivenInput(allParamsEnumList);
		runOverMultipleParametersDevelopmentAndTestSplit(diffLearningParams, splitPercentage);
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The evaluation is performed over the specified test data (given as name in the default index).
	 * @param allDifferentParams
	 * @param testDataName
	 * @throws Exception
	 */
	public static void runOverMultipleParametersLearnAndTest(
			List<AllLearningParameters> allDifferentParams, String testDataName) throws Exception
	{
		runOverMultipleParametersLearnAndTest(allDifferentParams, testDataName, 0);
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The evaluation is performed over the specified test data (given as name in the default index).
	 * @param allDifferentParams
	 * @param testDataName
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersLearnAndTest(
			List<AllLearningParameters> allDifferentParams, String testDataName, int paramStartInd) throws Exception
	{
		int paramInd = -1;
		for (AllLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; date:" + (new Date()).toString());
			OneLearningProcessHelper.runOverParametersWithGivenTrainTest(fullLearningParams, testDataName);
		}
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The evaluation is performed over the specified test data (given as name in the default index).
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams
	 * @param testDataName
	 * @throws Exception
	 */
	public static void runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(
			List<AllLearningParameters> allDifferentParams, String testDataName) throws Exception
	{
		runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(allDifferentParams, testDataName, 0);
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The evaluation is performed over the specified test data (given as name in the default index).
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams
	 * @param testDataName
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(
			List<AllLearningParameters> allDifferentParams, String testDataName, int paramStartInd) throws Exception
	{
		int paramInd = -1;
		for (AllLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; date:" + (new Date()).toString());
			OneLearningProcessHelper.runOverParametersWithGivenTrainTest(fullLearningParams, testDataName, lastResultClassifierApplicationParametersOptions.get(fullLearningParams.learnerName));
		}
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The provided training data is split into development and test set according to the split percentage. 
	 * The classifiers are trained over the development set and evaluated over the test split. 
	 * @param allDifferentParams
	 * @param splitPercentage The percentage of tweets to put into the development set. Note that while the split is random, it will be fixed across all the experiment runs. 
	 * @throws Exception
	 */
	public static void runOverMultipleParametersDevelopmentAndTestSplit(
			List<AllLearningParameters> allDifferentParams, double splitPercentage) throws Exception
	{
		runOverMultipleParametersDevelopmentAndTestSplit(allDifferentParams, splitPercentage, 0);
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The provided training data is split into development and test set according to the split percentage. 
	 * The classifiers are trained over the development set and evaluated over the test split. 
	 * @param allDifferentParams
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @param splitPercentage The percentage of tweets to put into the development set. Note that while the split is random, it will be fixed across all the experiment runs. 
	 * @throws Exception
	 */
	public static void runOverMultipleParametersDevelopmentAndTestSplit(
			List<AllLearningParameters> allDifferentParams, double splitPercentage, int paramStartInd) throws Exception
	{
		int paramInd = -1;
		for (AllLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; date:" + (new Date()).toString());
			OneLearningProcessHelper.runOverParametersWithDevelopmentTestSplit(fullLearningParams, splitPercentage, false);
		}
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The provided training data is split into development and test set according to the split percentage. 
	 * The classifiers are trained over the development set and evaluated over the test split. 
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams
	 * @param splitPercentage The percentage of tweets to put into the development set. Note that while the split is random, it will be fixed across all the experiment runs.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersDevelopmentAndTestSplitWithMultipleApplicationParameters(
			List<AllLearningParameters> allDifferentParams, double splitPercentage) throws Exception
	{
		runOverMultipleParametersDevelopmentAndTestSplitWithMultipleApplicationParameters(
				allDifferentParams, splitPercentage, 0);
	}
	
	/**
	 * Run multiple learning experiments with the list of specified learning parameters. 
	 * The provided training data is split into development and test set according to the split percentage. 
	 * The classifiers are trained over the development set and evaluated over the test split. 
	 * This run of experiments allows to avoid retraining of the classifiers by directly varying their application parameters without retraining.
	 * @param allDifferentParams
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @param splitPercentage The percentage of tweets to put into the development set. Note that while the split is random, it will be fixed across all the experiment runs.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersDevelopmentAndTestSplitWithMultipleApplicationParameters(
			List<AllLearningParameters> allDifferentParams, double splitPercentage, int paramStartInd) throws Exception
	{
		int paramInd = -1;
		for (AllLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; date:" + (new Date()).toString());
			OneLearningProcessHelper.runOverParametersWithDevelopmentTestSplit(fullLearningParams,  
					lastResultClassifierApplicationParametersOptions.get(fullLearningParams.learnerName), splitPercentage, false);
		}
	}
	
	/**
	 * Run multiple cross-validation learning experiments with the list of specified learning parameters. 
	 * The cross-validation is performed over the specified training data.
	 * @param allDifferentParams
	 * @param foldsNumber The number of folds for cross-validation. Note that while the data split is random, it will be fixed across all the experiment runs.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersInCrossValidation(List<AllLearningParameters> allDifferentParams, int foldsNumber) throws Exception
	{
		runOverMultipleParametersInCrossValidation(allDifferentParams, foldsNumber, 0);
		
	}
	
	/**
	 * Run multiple cross-validation learning experiments with the list of specified learning parameters. 
	 * The cross-validation is performed over the specified training data.
	 * @param allDifferentParams
	 * @param foldsNumber The number of folds for cross-validation. Note that while the data split is random, it will be fixed across all the experiment runs.
	 * @param paramStartInd The experiments will start from the parameters with index paramStartInd in the list.
	 * @throws Exception
	 */
	public static void runOverMultipleParametersInCrossValidation(
			List<AllLearningParameters> allDifferentParams, int foldsNumber, int paramStartInd) throws Exception
	{
		int paramInd = -1;
		for (AllLearningParameters fullLearningParams : allDifferentParams)
		{
			++paramInd;
			if (paramInd < paramStartInd)
				continue;
			System.out.println("Parameters index = " + paramInd  + "; date:" + (new Date()).toString());
			OneLearningProcessHelper.runOverParametersWithCrossValidation(fullLearningParams, foldsNumber, false);
		}
	}
	
	
	/** PARAMETERS SPECIFICATIONS AND PARSING PROCEDURE **/
	
	public static class AllLearningParametersEnumerationList implements IParametersEnumerationList
	{
		public String experimentsName;
		public String trainDataName;
		public boolean useCache;
		public FeatureType usedFeatureType;
		public FeatureExtractionParamsEnumerationList featureExtractionParamsEnumerationList;
		public List<SelectionType[]> usedFeatureSelectionTypes;
		public Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap;
		public LearnerName[] learnerNames;
		public Map<LearnerName, ClassifierLearnerParamsEnumerationList> classifierLearnersParametersEnumeration;
		public Map<LearnerName, ClassifierApplicationParamsEnumerationList> learnersApplicationParametersEnumeration;
	}
	
	
	static List<FeatureExtractionParameters> getFeatureExtractionParametersEnumeration(boolean useCache, FeatureType usedFeatureType, FeatureExtractionParamsEnumerationList featureExtractionParamsEnumerationList)
	{
		List<FeatureExtractionParameters> foundParametersOptions = new ArrayList<FeatureExtractionParameters>();
		
		FeatureType[] usedFeatureTypes = new FeatureType[1];
		usedFeatureTypes[0] = usedFeatureType;
		
		
		if (usedFeatureType == FeatureType.Ngrams)
		{
			// if to extract ngrams
			DataNgramExtractionParamsEnumerationList curExtractionParamsEnumerationList = (DataNgramExtractionParamsEnumerationList) featureExtractionParamsEnumerationList;
			
			for (int minNOcc : curExtractionParamsEnumerationList.diffTermOccNumList)
				for (int maxN : curExtractionParamsEnumerationList.diffMaxNgramLengthList)
					for (ExtraParamCombinationForNgramExtraction otherParamCombo : curExtractionParamsEnumerationList.extraParamCombinations)
					{
						FeatureExtractionParameters curFeatureExtractionParams = new FeatureExtractionParameters();
						curFeatureExtractionParams.useCache = useCache;
						curFeatureExtractionParams.featureTypes = usedFeatureTypes;
						
						curFeatureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
						curFeatureExtractionParams.dataNgramExtractionParams.maxN = maxN;
						curFeatureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = minNOcc;
						curFeatureExtractionParams.dataNgramExtractionParams.removeStopWords = otherParamCombo.removeStopWords;
						curFeatureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = otherParamCombo.termDetectionParameters;
						curFeatureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = otherParamCombo.applySevereNonOverlap;	
						foundParametersOptions.add(curFeatureExtractionParams);
					}
			
		}
		else if (usedFeatureType == FeatureType.GivenList)
		{
			GivenFeaturesExtractionParamsEnumerationList curExtractionParamsEnumerationList = 
					(GivenFeaturesExtractionParamsEnumerationList) featureExtractionParamsEnumerationList;
			
			for (String givenFeatureListName : curExtractionParamsEnumerationList.givenFeatureNamesList) {
				FeatureExtractionParameters curFeatureExtractionParams = new FeatureExtractionParameters();
				curFeatureExtractionParams.useCache = useCache;
				curFeatureExtractionParams.featureTypes = usedFeatureTypes;
				
				curFeatureExtractionParams.givenFeatureListsNames = givenFeatureListName;
				foundParametersOptions.add(curFeatureExtractionParams);
			}
		}
		
		return foundParametersOptions;
	}
	
	static List<BinarySelectionParams> getBinaryFeatureSelectionParametersEnumeration(
			SelectionType usedBinaryFeatureSelectionType,
			BinarySelectionParamsEnumerationList binarySelectionParamsEnumerationList
		)
	{
		List<BinarySelectionParams> binarySelectionParameters = new ArrayList<BinarySelectionParams>();
		
		for (FeatureSelectionAlgOption featSelAlg : binarySelectionParamsEnumerationList.selectionAlgsOptions)
		{
			if (featSelAlg == FeatureSelectionAlgOption.No)
			{
				BinarySelectionParams curBinaryParams = new BinarySelectionParams();
				curBinaryParams.selectionAlg = featSelAlg;
				binarySelectionParameters.add(curBinaryParams);
			}
			else
			{
				for (double threshold : binarySelectionParamsEnumerationList.thresholdsOptions)
					for (ExtraParamCombinationForBinaryFeatureSelection extraOption : binarySelectionParamsEnumerationList.extraOptions)
					{

						BinarySelectionParams curBinaryParams = new BinarySelectionParams();
						curBinaryParams.selectionAlg = featSelAlg;
						curBinaryParams.scoreThreshold = threshold;
						extraOption.setParameters(curBinaryParams);
						binarySelectionParameters.add(curBinaryParams);
					}
			}
		}
		return binarySelectionParameters;
	}
	
	static List<FeatureSelectionParameters> getFeatureSelectionParametersEnumeration(
			List<SelectionType[]> usedFeatureSelectionTypes,
			Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap
		)
	{
		List<FeatureSelectionParameters> foundParametersOptions = new ArrayList<FeatureSelectionParameters>();
	
		for (SelectionType[] selectionTypeCombo : usedFeatureSelectionTypes)
		{
			List<FeatureSelectionParameters> curSelectionOptions = new ArrayList<FeatureSelectionParameters>();
			
			FeatureSelectionParameters curFeatureSelectionParams = new FeatureSelectionParameters();
			curFeatureSelectionParams.selectionTypesToApply = selectionTypeCombo;
			
			curSelectionOptions.add(curFeatureSelectionParams);
		
			// will consider that its at most 2 selection types at the same time
			for (SelectionType usedFeatureSelectionType : selectionTypeCombo)
			{
				FeatureSelectionParamsEnumerationList featureSelectionParamsEnumerationList = featureSelectionParamsEnumerationListMap.get(usedFeatureSelectionType);
				List<FeatureSelectionParameters> newFeatureSelectionOptions = new ArrayList<FeatureSelectionParameters>();
				
				if (usedFeatureSelectionType == SelectionType.OnPolarity || usedFeatureSelectionType == SelectionType.OnEmotionality)
				{
					List<BinarySelectionParams> binarySelectionParametersList = getBinaryFeatureSelectionParametersEnumeration(usedFeatureSelectionType, (BinarySelectionParamsEnumerationList)featureSelectionParamsEnumerationList);
					for (FeatureSelectionParameters internalFeatureSelectionParams :  curSelectionOptions)
					{
						for (BinarySelectionParams binarySelectionParams : binarySelectionParametersList)
						{
							FeatureSelectionParameters newFeatureSelectionParams = internalFeatureSelectionParams.clone();
							if (usedFeatureSelectionType == SelectionType.OnPolarity)
							{
								newFeatureSelectionParams.polaritySelectionParams = binarySelectionParams;
							}
							else if  (usedFeatureSelectionType == SelectionType.OnEmotionality)
							{
								newFeatureSelectionParams.emotionalitySelectionParams = binarySelectionParams;
							}
							newFeatureSelectionOptions.add(newFeatureSelectionParams);
						}
					}
				}
				else if (usedFeatureSelectionType == SelectionType.OnOccurrence)
				{
					for (int minOccThreshold : ((ThresholdOnlySelectionParamsEnumerationList)featureSelectionParamsEnumerationList).intThresholdsOptions)
					{
						for (FeatureSelectionParameters internalFeatureSelectionParams :  curSelectionOptions)
						{
							FeatureSelectionParameters newFeatureSelectionParams = internalFeatureSelectionParams.clone();
							newFeatureSelectionParams.minOccurrenceThreshold = minOccThreshold;
							newFeatureSelectionOptions.add(newFeatureSelectionParams);
						}
					}
				}
				
				curSelectionOptions = newFeatureSelectionOptions;
			}
			
			foundParametersOptions.addAll(curSelectionOptions);
		}
		
		//remove if several times no feature selection
		boolean wasAlreadyNoFeatureSelection = false;
		for (int i = 0; i < foundParametersOptions.size(); ++i)
		{
			FeatureSelectionParameters curSelectionParams = foundParametersOptions.get(i);
			boolean isNoSelection = false;
			if (curSelectionParams.selectionTypesToApply.length == 0)
				isNoSelection = true;
			else
			{
				isNoSelection = (curSelectionParams.polaritySelectionParams == null || curSelectionParams.polaritySelectionParams.selectionAlg == FeatureSelectionAlgOption.No) 
						&& (curSelectionParams.emotionalitySelectionParams == null || curSelectionParams.emotionalitySelectionParams.selectionAlg == FeatureSelectionAlgOption.No);
			}
			if (isNoSelection)
			{
				if (!wasAlreadyNoFeatureSelection)
					wasAlreadyNoFeatureSelection = true;
				else
				{
					// need to remove i-th
					foundParametersOptions.remove(i);
					i--;
				}
			}
				
		}
		
		return foundParametersOptions;
	}
	
	public static List<ClassifierApplicationParameters> getApplicationParametersEnumeration(ClassifierApplicationParamsEnumerationList classifierApplicationParamsEnumerationList)
	{
		List<ClassifierApplicationParameters> classifierApplicationOptions = new ArrayList<ClassifierApplicationParameters>();
		
		for (HierarchyApplyOption hierarchyOption : classifierApplicationParamsEnumerationList.hierarchyOptions)
		for (MultiLabelRefineParams outputRefineParams : classifierApplicationParamsEnumerationList.outputMultiLabelOptions)
		for (ProbabilisticOutputTreatment probabOutputTreatment :  classifierApplicationParamsEnumerationList.probabilisticOutputTreatmentOptions)	
			
		{
			for (TermDetectionParameters termDetectionParameters : classifierApplicationParamsEnumerationList.termDetectionOptions)
			{
				ClassifierApplicationParameters classifierApplicParams = new ClassifierApplicationParameters();
				classifierApplicParams.hierarchyOption = hierarchyOption;
				classifierApplicParams.multiLabelOutputRefinementParams = outputRefineParams;
				classifierApplicParams.parametersOfTermDetectionForClassifier = termDetectionParameters;
				classifierApplicParams.probabilisticBinaryOutputTreatmentParams = probabOutputTreatment;
				classifierApplicationOptions.add(classifierApplicParams);
			}
		}
		
		
		return classifierApplicationOptions ;
	}
	
	public static List<ClassifierLearnerParams> getLearnerParamsEnumeration(LearnerName learnerName,
			ClassifierLearnerParamsEnumerationList classifierLearnerParamsEnumerationList, ClassifierApplicationParameters classifierApplicationParams) 
	{
		List<ClassifierLearnerParams> foundParametersOptions = new ArrayList<ClassifierLearnerParams>();
		
		for (RebalancingParameters rebalancingOption : classifierLearnerParamsEnumerationList.rebalancingOptions)
			for (MultiLabelRefineParams initialMultiLabelP : classifierLearnerParamsEnumerationList.initialMultiLabelOptions)
				for (String specificLearnerP : classifierLearnerParamsEnumerationList.extraParameters)
					
				{
					if (learnerName.name().startsWith("CategoryWeka"))
					{
							ClassifierLearnerParams curLearnerParams = new WekaCategoryClassifierLearnerParams(learnerName);
							
							curLearnerParams.multiLabelInitialRefinementParams = initialMultiLabelP;
							curLearnerParams.outputClassifierApplicationParameters = classifierApplicationParams;
							curLearnerParams.specificLearnerParams =  specificLearnerP;
							curLearnerParams.rebalancingParameters = rebalancingOption;
							
							foundParametersOptions.add(curLearnerParams);
					}
					else if (learnerName.name().startsWith("Independent"))//learnerName == LearnerName.IndependentPMI)
					{
						IndependentLearnerParamsEnumerationList indepParamsList = (IndependentLearnerParamsEnumerationList)classifierLearnerParamsEnumerationList;
						for (Pair<CloseCategoryUseType, CloseCategoryCalculation> closeCategoryTreatment : indepParamsList.closeCategoryTreatmentOptions)
						{
							for (String extraBinaryParams : indepParamsList.binaryLearnerParametersEnumerationList.differentAdditionalBinaryLearnerOptions)
							//for (ProbabilisticOutputTreatment probabOutputTreatment :  indepParamsList.binaryLearnerParametersEnumerationList.probabilisticOutputTreatmentOptions)	
							for (FeatureSelectionAlgOption binFeatSelAlg : indepParamsList.binaryLearnerParametersEnumerationList.binarySelectionParamsEnumerationList.selectionAlgsOptions)
							{
								if (binFeatSelAlg == FeatureSelectionAlgOption.No)
								{
									IndependentLearnerParameters curLearnerParams  = new IndependentLearnerParameters(learnerName);
									curLearnerParams.closeCategoryUseType = closeCategoryTreatment.first;
									curLearnerParams.closeCategoryCalculation =   closeCategoryTreatment.second;
									curLearnerParams.binaryLearnerParameters.binaryLexiconParams = extraBinaryParams;
									curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
									//curLearnerParams.binaryLearnerParameters.probabilisticOutputTreatmentParams =  probabOutputTreatment;
									
									curLearnerParams.useCacheForIndependentRepresentation = indepParamsList.useCacheForIndependentRepresentation;
									
									curLearnerParams.multiLabelInitialRefinementParams = initialMultiLabelP;
									curLearnerParams.outputClassifierApplicationParameters = classifierApplicationParams;
									curLearnerParams.specificLearnerParams = specificLearnerP;
									curLearnerParams.rebalancingParameters = rebalancingOption;
									curLearnerParams.useDependentClassifierForOutput = indepParamsList.useDependentClassifierForOutput;
									foundParametersOptions.add(curLearnerParams);
								}
								else
								{
									for (double threshold : indepParamsList.binaryLearnerParametersEnumerationList.binarySelectionParamsEnumerationList.thresholdsOptions)
										for (ExtraParamCombinationForBinaryFeatureSelection extraOption : indepParamsList.binaryLearnerParametersEnumerationList.binarySelectionParamsEnumerationList.extraOptions)
										{
											IndependentLearnerParameters curLearnerParams  = new IndependentLearnerParameters(learnerName);
											curLearnerParams.closeCategoryUseType = closeCategoryTreatment.first;
											curLearnerParams.closeCategoryCalculation =   closeCategoryTreatment.second;
											curLearnerParams.binaryLearnerParameters.binaryLexiconParams = extraBinaryParams;
											//curLearnerParams.binaryLearnerParameters.probabilisticOutputTreatmentParams =  probabOutputTreatment;
											
											curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  binFeatSelAlg;

											curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams.scoreThreshold = threshold;
											extraOption.setParameters(curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams);
											
											curLearnerParams.useCacheForIndependentRepresentation = indepParamsList.useCacheForIndependentRepresentation;
											
											curLearnerParams.multiLabelInitialRefinementParams = initialMultiLabelP;
											curLearnerParams.outputClassifierApplicationParameters = classifierApplicationParams;
											curLearnerParams.specificLearnerParams = specificLearnerP;
											curLearnerParams.rebalancingParameters = rebalancingOption;
											curLearnerParams.useDependentClassifierForOutput = indepParamsList.useDependentClassifierForOutput;
											foundParametersOptions.add(curLearnerParams);
										}
								}
							}
						}
						
					}
					else if (learnerName  == LearnerName.WeightedBalancedVoting)
					{
						
						ClassifierLearnerParams curLearnerParams = new WeightedBalancedVotingLearnerParams(rebalancingOption, false, false);
						curLearnerParams.multiLabelInitialRefinementParams = initialMultiLabelP;
						curLearnerParams.outputClassifierApplicationParameters = classifierApplicationParams;
						curLearnerParams.specificLearnerParams =  specificLearnerP;
						
						foundParametersOptions.add(curLearnerParams);
					}
					else if (learnerName == LearnerName.WeightedVoting)
					{
						ClassifierLearnerParams curLearnerParams = new WeightedVotingLearnerParams(false, false);
						curLearnerParams.multiLabelInitialRefinementParams = initialMultiLabelP;
						curLearnerParams.outputClassifierApplicationParameters = classifierApplicationParams;
						curLearnerParams.specificLearnerParams =  specificLearnerP;
						curLearnerParams.rebalancingParameters = rebalancingOption;
						
						foundParametersOptions.add(curLearnerParams);
					}
					else
					{
						System.err.println("The process of setting parameters for the chosen learner " + learnerName.name() + " is not yet implemented.");
					}
				}
	
		
		
		
		
		
		return foundParametersOptions;
	}
	
	
	public static List<AllLearningParameters> getAllLearningParametersEnumerationFromGivenInput(
			AllLearningParametersEnumerationList allParamsEnumList
			)
	{
		return getAllLearningParametersEnumerationFromGivenInput(
				allParamsEnumList.experimentsName, allParamsEnumList.trainDataName, allParamsEnumList.useCache, allParamsEnumList.usedFeatureType, allParamsEnumList.featureExtractionParamsEnumerationList,
				allParamsEnumList.usedFeatureSelectionTypes, allParamsEnumList.featureSelectionParamsEnumerationListMap, allParamsEnumList.learnerNames, allParamsEnumList.classifierLearnersParametersEnumeration,
				allParamsEnumList.learnersApplicationParametersEnumeration
				);
	}
	
	static ClassifierLearnerParams makeTermDetectionParametersConsistent(ClassifierLearnerParams learnerParams, FeatureExtractionParameters featExtractionParams)
	{
		ClassifierLearnerParams newLearnerParams = learnerParams.clone();
		newLearnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = featExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection;
		return newLearnerParams;
	}
	
	
	public static List<AllLearningParameters> getAllLearningParametersEnumerationFromGivenInput(
			String experimentName,
			String trainDataName, boolean useCache,
			FeatureType usedFeatureTypes, FeatureExtractionParamsEnumerationList featureExtractionParamsEnumerationList,
			List<SelectionType[]> usedFeatureSelectionTypes,
			Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap,
			LearnerName[] learnerNames, Map<LearnerName, ClassifierLearnerParamsEnumerationList> classifierLearnersParametersEnumeration,
			Map<LearnerName, ClassifierApplicationParamsEnumerationList> classifierApplicationParamsEnumeration
			)
	{
		ExperimentsResultsSaver.currentTestName = experimentName;
		
		List<AllLearningParameters> allParametersToRun = new ArrayList<AllLearningParameters>();
		lastResultClassifierApplicationParametersOptions = new HashMap<LearnerName, List<ClassifierApplicationParameters>>();
		
		List<FeatureExtractionParameters> diffFeatureExtractionParams = getFeatureExtractionParametersEnumeration(useCache, usedFeatureTypes, featureExtractionParamsEnumerationList);
		List<FeatureSelectionParameters> diffFeatureSelectionParams = getFeatureSelectionParametersEnumeration(usedFeatureSelectionTypes, featureSelectionParamsEnumerationListMap);
		Map<LearnerName, List<ClassifierLearnerParams>> mapOfDiffClassifierLearnerParams = new HashMap<LearnerName, List<ClassifierLearnerParams>>();
		
		for (Map.Entry<LearnerName, ClassifierLearnerParamsEnumerationList> learnerEnumn : classifierLearnersParametersEnumeration.entrySet())
		{
			List<ClassifierApplicationParameters> curLearnerApplicOptions = getApplicationParametersEnumeration(classifierApplicationParamsEnumeration.get(learnerEnumn.getKey()));
			lastResultClassifierApplicationParametersOptions.put(learnerEnumn.getKey(), curLearnerApplicOptions );
			List<ClassifierLearnerParams> curLearnerParamOptions = getLearnerParamsEnumeration(learnerEnumn.getKey(), learnerEnumn.getValue(), curLearnerApplicOptions.get(0));
			mapOfDiffClassifierLearnerParams.put(learnerEnumn.getKey(), curLearnerParamOptions);
		}
		
		
		for (FeatureExtractionParameters featureExtractionParameters : diffFeatureExtractionParams)
			for (LearnerName learnerName : learnerNames)
				for (FeatureSelectionParameters featureSelectionParameters : diffFeatureSelectionParams)
					for (ClassifierLearnerParams classifierParams : mapOfDiffClassifierLearnerParams.get(learnerName))
		{
		
			ClassifierLearnerParams  newLearnerParams  = makeTermDetectionParametersConsistent(classifierParams, featureExtractionParameters);
			
			AllLearningParameters fullLearningParams = new AllLearningParameters();
			fullLearningParams.featureExtractionParams = featureExtractionParameters;
			fullLearningParams.featureSelectionParams = featureSelectionParameters;
			fullLearningParams.learnerName = learnerName;
			fullLearningParams.learnerParams = newLearnerParams;
			fullLearningParams.trainDataName = trainDataName.toString();	
			
			allParametersToRun.add(fullLearningParams);
		}
		
		return allParametersToRun;
	}
	
}
