/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import utility.Pair;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classification.evaluation.ComplexEvaluation;
import classification.evaluation.EvaluationUtil;
import classifiers.initialization.GivenFeaturesIndex;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.documents.Tweet;
import data.loading.DataSetFormatter;
import experimentsavers.ExperimentsResultsSaver;
import learners.definitions.ClassifierLearner;
import learners.given.ClassifierLearnerFactory;
import learning.evaluation.ClassifierEvaluation;
import learning.parameters.AllLearningParameters;
import learning.parameters.ClassifierApplicationParameters;

/**
 * The class to run one learning / evaluation process of a classifier
 * 
 */
public class OneLearningProcessHelper {
	
	public static String folderToPrintClassifiers = "output/learnedClassifiers/";
	public static String nameForPrintClassifiers = "trained-classifier";
	public static boolean toPrintResultClassifier = true;
	public static boolean toPrintWithClassifiersRunNum = false;
	
	
	
	public static void runOverParametersWithDevelopmentTestSplit(AllLearningParameters fullLearningParams) throws Exception
	{
		runOverParametersWithDevelopmentTestSplit(fullLearningParams, null);
	}
	
	public static void runOverParametersWithDevelopmentTestSplit(AllLearningParameters fullLearningParams, 
			double splitPercentage, boolean useFullRandom) throws Exception
	{
		runOverParametersWithDevelopmentTestSplit(fullLearningParams, null, splitPercentage, useFullRandom);
	}
	
	
	/**
	 * Takes the specified parameters and train data (given by fullLearningParams.trainDataName) to learn the classifier over the 80% of the train data and test on the remaining part.
	 * 
	 * Uses 0.8 as the default split percentage and doesn't change the random seed for split between runs.
	 * @param fullLearningParams
	 * @param classifierApplicationOptions
	 * @throws Exception
	 */
	public static void runOverParametersWithDevelopmentTestSplit(
			AllLearningParameters fullLearningParams, List<ClassifierApplicationParameters> classifierApplicationOptions) throws Exception
	{
		double splitPercentage = 0.8;
		boolean useFullRandom = false; 
		
		runOverParametersWithDevelopmentTestSplit(fullLearningParams, classifierApplicationOptions, splitPercentage, useFullRandom);
	}
	
	/**
	 * Takes the specified parameters and train data (given by fullLearningParams.trainDataName) to learn the classifier over the splitPercentage of the train data and test on the remaining part.
	 * @param fullLearningParams
	 * @param classifierApplicationOptions
	 * @param splitPercentage
	 * @param useFullRandom If true - the split of data will differ from one run to another. If false - will stay fixed to ensure the comparable evaluation.
	 * @throws Exception
	 */
	public static void runOverParametersWithDevelopmentTestSplit(
			AllLearningParameters fullLearningParams, 
			List<ClassifierApplicationParameters> classifierApplicationOptions,
			double splitPercentage, boolean useFullRandom) throws Exception
	{
		Date start = new Date();
		
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		AllLearningParameters.currentSetup = fullLearningParams;
		
		System.out.println("Test + train on development with " + splitPercentage + " split");
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
		
		
		// getting dqta
		LabeledData fullLabelData = DataRepositoryIndex.getLabeledDataByName(fullLearningParams.trainDataName);
		Map<Integer, String> tweetData = fullLabelData.textData;
		Map<Integer, List<Integer>> categoryAssignment = fullLabelData.emotionLabels;
		List<Tweet> tweets = DataSetFormatter.getTweetsFromStrings(tweetData);
		
		
		// development/test split
		Pair<List<Integer>, List<Integer>> splitIndexes = EvaluationUtil.selectIndexesSubpartRandomly(tweets.size(), splitPercentage, useFullRandom);
		List<Tweet> trainTweets = EvaluationUtil.applyIdsList(tweets, splitIndexes.first);
		List<Tweet> testTweets = EvaluationUtil.applyIdsList(tweets, splitIndexes.second);
		Map<Integer, List<Integer>> trainCategoryAssignment = EvaluationUtil.findSelectedAssignmentSubset(categoryAssignment, trainTweets);
		Map<Integer, List<Integer>> testCategoryAssignment = EvaluationUtil.findSelectedAssignmentSubset(categoryAssignment, testTweets);
			
		runOverGivenParametersAndData(fullLearningParams, 
				trainTweets, trainCategoryAssignment, 
				testTweets, testCategoryAssignment, 
				fullLearningParams.trainDataName+"_DevTest_" + splitPercentage, classifierApplicationOptions);
		

		Date finish = new Date();
		System.out.println("It took " + (finish.getTime() - start.getTime())/60000.0 + " minutes");
	}
	
	/**
	 * Takes the specified parameters and train data (given by fullLearningParams.trainDataName) to learn/test the classifier via cross-validation. 
	 * 
	 * Uses 10 as the default number of folds in cross-validation and doesn't change the random seed for split between runs.
	 * @param fullLearningParams
	 * @throws Exception
	 */
	public static void runOverParametersWithCrossValidation(AllLearningParameters fullLearningParams) throws Exception {
		int foldsNumber = 10;
		boolean useFullRandom = false; 
		runOverParametersWithCrossValidation(fullLearningParams, foldsNumber, useFullRandom);
	}
			

	/**
	 * Takes the specified parameters and train data (given by fullLearningParams.trainDataName) to learn/test the classifier via cross-validation. 
	 * 
	 * @param fullLearningParams
	 * @param foldsNumber The number of folds in cross-validation
	 * @param useFullRandom If true - the split of data will differ from one run to another. If false - will stay fixed to ensure the comparable evaluation.
	 * @throws Exception
	 */
	public static void runOverParametersWithCrossValidation(AllLearningParameters fullLearningParams, 
			int foldsNumber, boolean useFullRandom) throws Exception
	{
		Date start = new Date();
		
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		AllLearningParameters.currentSetup = fullLearningParams;
		
		System.out.println("Cross-validation with " + foldsNumber + " folds");
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
		
		
		// getting dqta
		LabeledData fullLabelData = DataRepositoryIndex.getLabeledDataByName(fullLearningParams.trainDataName);
		Map<Integer, String> tweetData = fullLabelData.textData;
		Map<Integer, List<Integer>> categoryAssignment = fullLabelData.emotionLabels;
		List<Tweet> tweets = DataSetFormatter.getTweetsFromStrings(tweetData);
			
		runOverGivenParametersAndDataWithCrossValidation(fullLearningParams, tweets, categoryAssignment, foldsNumber, "cross-validation-" +foldsNumber);

		Date finish = new Date();
		System.out.println("It took " + (finish.getTime() - start.getTime())/60000.0 + " minutes");
	}
		

	public static void runOverParametersWithGivenTrainTest(
			AllLearningParameters fullLearningParams, String testDataNameInIndex
			) throws Exception
	{
		// getting train dqta
		LabeledData trainLabelData = DataRepositoryIndex.getLabeledDataByName(fullLearningParams.trainDataName);
	
		runOverParametersWithGivenTrainTest(fullLearningParams, trainLabelData, testDataNameInIndex, null);
		
	}
	
	public static void runOverParametersWithGivenTrainTest(
			AllLearningParameters fullLearningParams, String testDataNameInIndex, 
			List<ClassifierApplicationParameters> classifierApplicationOptions
			) throws Exception
	{
		// getting train dqta
		LabeledData trainLabelData = DataRepositoryIndex.getLabeledDataByName(fullLearningParams.trainDataName);
	
		runOverParametersWithGivenTrainTest(fullLearningParams, trainLabelData, testDataNameInIndex, classifierApplicationOptions);
		
	}
	
	public static void runOverParametersWithGivenTrainTest(
			AllLearningParameters fullLearningParams, LabeledData trainLabelData, 
			String testDataNameInIndex, List<ClassifierApplicationParameters> classifierApplicationOptions
			) throws Exception
	{
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		AllLearningParameters.currentSetup = fullLearningParams;
		
		System.out.println("Test + train on given");
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
			
		
		// getting train dqta
		Map<Integer, String> trainTweetData = trainLabelData.textData;
		Map<Integer, List<Integer>> trainCategoryAssignment = trainLabelData.emotionLabels;
		List<Tweet> trainTweets = DataSetFormatter.getTweetsFromStrings(trainTweetData);
		
		
		// getting test data
		LabeledData testLabelData = DataRepositoryIndex.getLabeledDataByName(testDataNameInIndex);
		Map<Integer, String> testTweetData = testLabelData.textData;
		Map<Integer, List<Integer>> testCategoryAssignment = testLabelData.emotionLabels;
		List<Tweet> testTweets = DataSetFormatter.getTweetsFromStrings(testTweetData);			
		
		runOverGivenParametersAndData(fullLearningParams, 
				trainTweets, trainCategoryAssignment, 
				testTweets, testCategoryAssignment, 
				testDataNameInIndex, classifierApplicationOptions);
		
	}
	
	
	private static void runOverGivenParametersAndData(AllLearningParameters fullLearningParams,
			List<Tweet> trainTweets, Map<Integer, List<Integer>> trainCategoryAssignment, 
			List<Tweet> testTweets, Map<Integer, List<Integer>> testCategoryAssignment, String testDataNameInIndex,
			List<ClassifierApplicationParameters> classifierApplicationOptions) throws Exception
	{
		
		List<String> initialFeaturesList = null;
		if (fullLearningParams.featureExtractionParams.givenFeatureListsNames != null)
			initialFeaturesList = GivenFeaturesIndex.getGivenFeatureList(fullLearningParams.featureExtractionParams.givenFeatureListsNames);// has to be initialized in any case 
		
		// learning part
		ClassifierLearner classifierLearner = ClassifierLearnerFactory.getLearner(fullLearningParams, initialFeaturesList);
		classifierLearner.initialize(trainTweets);
		
		WeightedClassifier resClassifier = classifierLearner.learnClassifierOnStrictLabels(trainCategoryAssignment);
		if (toPrintResultClassifier)
		{
			Date printStart = new Date();
			resClassifier.printToTextFile(folderToPrintClassifiers + nameForPrintClassifiers + (toPrintWithClassifiersRunNum?ExperimentsResultsSaver.currentRunNum:"")+ ".txt");
			System.out.println("Classifier is printed out. It took " + ((new Date().getTime() - printStart.getTime())/60000.0) + " minutes");
		}
		
		Date evalStart = new Date();
		EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifier(resClassifier, testTweets,  testCategoryAssignment);
		System.out.println("Classifier evaluation took " + ((new Date().getTime() - evalStart.getTime())/60000.0) + " minutes");
		System.out.println(evalResult.toString());
		
		ExperimentsResultsSaver.saveEvaluationResults(testDataNameInIndex, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
		// if to save the detailed evaluation
		if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
			ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataNameInIndex, true, true);

		
		if (classifierApplicationOptions != null)
		{
			// evaluating other application parameters directly
			for (int i = 1; i < classifierApplicationOptions.size(); ++i)
			{
				ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
				ClassifierApplicationParameters newApplicationParams = classifierApplicationOptions.get(i);
				if (newApplicationParams.parametersOfTermDetectionForClassifier == null)
					newApplicationParams.parametersOfTermDetectionForClassifier = fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection;
				classifierLearner.updateOutputClassifierForApplicationParameters(resClassifier, newApplicationParams);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = newApplicationParams; // so that the parameters would be printed correctly
				
				evalResult = ClassifierEvaluation.evaluateClassifier(resClassifier, testTweets,  testCategoryAssignment);
				ExperimentsResultsSaver.saveEvaluationResults(testDataNameInIndex, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
	
				// if to save the detailed evaluation
				if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
					ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataNameInIndex, true, true);
			}
		}
	}
	
	
	
	private static void runOverGivenParametersAndDataWithCrossValidation(AllLearningParameters fullLearningParams,
			List<Tweet> allTweets, Map<Integer, List<Integer>> allCategoryAssignment, int nFolds, String testDataNameForEvaluationPrint) throws Exception
	{
		
		List<String> initialFeaturesList = null;
		if (fullLearningParams.featureExtractionParams.givenFeatureListsNames != null)
			initialFeaturesList = GivenFeaturesIndex.getGivenFeatureList(fullLearningParams.featureExtractionParams.givenFeatureListsNames);// has to be initialized in any case 
		
		fullLearningParams.featureExtractionParams.useCache = false; // because doesn't work properly with cross-validation
		
		// learning part
		ClassifierLearner classifierLearner = ClassifierLearnerFactory.getLearner(fullLearningParams, initialFeaturesList);
		// Get the folds
		Vector<List<Integer>> foldIdsVector = EvaluationUtil.separateIndexesRandomlyForCrossValidation(allTweets.size(), nFolds);
		
		EvaluationResult sumResult = new EvaluationResult(classifierLearner.getEmotionCategoriesType());
		// For each fold
		for (int i = 0; i < nFolds; i++)
		{
			System.out.print("fold "+(i+1)+";");
			List<Tweet> trainData = EvaluationUtil.getTrainDataForFold(allTweets, foldIdsVector, i);
			List<Tweet> testData = EvaluationUtil.getTestDataForFold(allTweets, foldIdsVector, i);
		
			classifierLearner.initialize(trainData);
			
			Map<Integer, List<Integer>> curFoldTrainCategoryAssignment = 
					EvaluationUtil.findSelectedAssignmentSubset(allCategoryAssignment, trainData);
				
			WeightedClassifier foldClassifier = classifierLearner.learnClassifierOnStrictLabels(curFoldTrainCategoryAssignment);	
			foldClassifier.defaultPreprocessing = false;
			
			foldClassifier.printToTextFile("output/tmpClassifier.txt");
			
			EvaluationResult foldResult = ComplexEvaluation.getEvaluationSumsOnTweets(allCategoryAssignment, testData, foldClassifier); 
			//UtilFiles.writeSerializableDataToFile(foldResult, "output/evaluationResults/curResData/res-fold-" + i +".bin");
			sumResult.plus(foldResult);
			sumResult.plusConfusionMatrix(foldResult);
		}
		
		sumResult.normalize();
		ExperimentsResultsSaver.saveEvaluationResults(testDataNameForEvaluationPrint, fullLearningParams, sumResult, 0);

	}
	
}
