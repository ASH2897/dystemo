/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.List;
import java.util.Map;

import learning.parameters.AllParametersToEvaluateGiven;
import learning.parameters.ClassifierApplicationParameters;
import linguistic.TermDetectionParameters;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classification.extraclassifiers.RandomClassifier;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import data.documents.Tweet;
import data.loading.DataSetFormatter;
import experimentsavers.ExperimentsResultsSaver;
import functionality.UtilArrays;

public class RandomExperimentsHelper {

	/**
	 * Run evaluation of a random classifier over the given test file.
	 * The random classifier decides independently whether or not each emotion is present, 
	 * with probability defined by the emotion distribution in the given test dataset. 
	 * Performance scores were averaged over the specified number of runs.
	 * @param runNums The number of random runs where performance is recorded.
	 * @param testDataNameInIndex
	 * @param experimentName The name of the experiment.
	 * @param catDataType The type of emotion categories data.
	 * @throws Exception
	 */
	public static void run(int runNums, String testDataNameInIndex, String experimentName, CategoryDataType catDataType) throws Exception
	{
		ExperimentsResultsSaver.currentTestName = experimentName;
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		// creating prior based on the distribution on labels in the test data
		LabeledData testLabelData = DataRepositoryIndex.getLabeledDataByName(testDataNameInIndex);
		Map<Integer, String> testTweetData = testLabelData.textData;
		Map<Integer, List<Integer>> testCategoryAssignment = testLabelData.emotionLabels;
		List<Tweet> testTweets = DataSetFormatter.getTweetsFromStrings(testTweetData);			
		
		double[] prior = new double[categoriesData.getCategoryNum()];
		
		for (List<Integer> labels : testCategoryAssignment .values())
		{
			for (Integer label : labels)
				prior[label - 1]++;
		}
		UtilArrays.normalizeArrayInternal(prior);
		
		EvaluationResult totalResult = new EvaluationResult(catDataType);
		WeightedClassifier randClassifier;
		EvaluationResult runResult ;
		for (int runInd = 0; runInd < runNums; ++runInd)
		{
			 randClassifier = new RandomClassifier(prior);
			 runResult = ComplexEvaluation.getEvaluationSumsOnTweets(testCategoryAssignment , testTweets, randClassifier ); 
			 totalResult.plus(runResult);
		}
		totalResult.confusionMatrix = new double[categoriesData.getCategoryNum()][categoriesData.getCategoryNum()]; 
		totalResult.normalize();
		
		
		AllParametersToEvaluateGiven randParams = new AllParametersToEvaluateGiven();
		
		randParams.evaluatedClassifierName = "Random-" + runNums;
		randParams.classifierApplicationParams = new ClassifierApplicationParameters();
		randParams.classifierApplicationParams.parametersOfTermDetectionForClassifier = new TermDetectionParameters();
		
		
		ExperimentsResultsSaver.saveEvaluationResults(testDataNameInIndex, randParams, totalResult, 0);
		
	}
}
