/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
    
    ------------------------------------
    
    Note that the given classifiers are provided for exemplary and research purposes. 
    Please refer to LEXICONS_LICENSING.txt file to read the terms of use for the provided lexicon-based classifiers.

*/

package classifiers.initialization;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import learning.parameters.ClassifierApplicationParameters;
import linguistic.TermDetectionParameters;
import classification.ClassifierLoader;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.WeightedClassifierWekaBased;
import classification.definitions.classifiers.WeightedClassifierWekaBasedIndependent;
import classification.definitions.classifiers.WeightedClassifierWekaBasedIndependentFully;
import classification.definitions.classifiers.wrappers.ClassifierWekaBasedIndependentFullyWithProbabilities;
import classification.definitions.classifiers.wrappers.ClassifierWekaBasedIndependentWithProbabilities;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classifiers.initialization.UsedClassifierFactory.ClassifierLoadInfo.ClassifierType;
import data.categories.CategoryProcessor.CategoryDataType;
import emotionclassifiers.given.EmotionClassifierGivenFactory;

public class UsedClassifierFactory {
	
	public static String[] standardUsedClassifiers = {"GALC-R-Inst", "OlympLex-Prep", "PMI-Hash"};
	
	private static WeightedClassifierLexiconBased getBasicOlympLexClassifier() throws Exception
	{
		WeightedClassifierLexiconBased resClassifier = EmotionClassifierGivenFactory.getOlympLexClassifier(false);
		resClassifier.setName("OlympLex");
		System.out.println("The number of terms in the lexicon: " + resClassifier.getFeatureNumber());
		return resClassifier;
	}
	
	private static WeightedClassifierLexiconBased getPreprocessedOlympLexClassifier() throws Exception
	{
		WeightedClassifierLexiconBased resClassifier = EmotionClassifierGivenFactory.getOlympLexClassifier(true);
		resClassifier.setName("OlympLex-Prep");
		System.out.println("The number of terms in the lexicon: " + resClassifier.getFeatureNumber());
		return resClassifier;
	}
	
	private static WeightedClassifierLexiconBased getOlympLexClassifier(String lexName) throws Exception
	{
		if (lexName.contains("-Prep"))
			return getPreprocessedOlympLexClassifier();
		else
			return getBasicOlympLexClassifier();
	}

	
	private static WeightedClassifierLexiconBased getGALCRevisedInstanceClassifier() throws Exception
	{
		WeightedClassifierLexiconBased resClassifier = EmotionClassifierGivenFactory.getGALCRevisedInstancesClassifier();
		resClassifier.setName("GALC-R-Inst");
		return resClassifier;
	}
	
	private static WeightedClassifierLexiconBased getGALCClassifier() throws Exception
	{
		
		WeightedClassifierLexiconBased resClassifier = EmotionClassifierGivenFactory.getGALCClassifier();
		resClassifier.setName("GALC");
		return resClassifier;
		
	}
	
	public static WeightedClassifier getClassifierByName (String classifierName, TermDetectionParameters termDetectionParameters) throws Exception
	{
		WeightedClassifier resClassifier = getClassifierByNameSimpleDetector(classifierName);
		
		if (termDetectionParameters.requiresChangeInDetection())
			resClassifier.setupTermDetectionParameters(termDetectionParameters);
		
		return resClassifier;
	}
	
	/**
	 * Returnds the stored classifier based on its name. The resultant classifier will have the specified parameters for classifier application.
	 * Available names: 
	 * - GALC
	 * - GALC-R-Inst
	 * - OlympLex
	 * - OlympLex-Prep
	 * - PMI-Hash
	 * 
	 * + Those that were added via addNewClassifierInformation function
	 * 
	 * @param classifierName
	 * @param classifierApplicationParameters
	 * @return
	 * @throws Exception
	 */
	public static WeightedClassifier getClassifierByName (String classifierName, ClassifierApplicationParameters classifierApplicationParameters) throws Exception
	{
		WeightedClassifier resClassifier = getClassifierByNameSimpleDetector(classifierName);
		
		resClassifier = updateWeightedClassifierForApplicationParameters(resClassifier, classifierApplicationParameters);
		
		return resClassifier;
	}
	
	/** creates the default classifiers, without any complex weighting, term detection treatment, or other parameters for classifier application
	 * It will create the classifier with simple non-overlap detector (or default one for those classifiers)
	 * Available names: 
	 * - GALC
	 * - GALC-R-Inst
	 * - OlympLex
	 * - OlympLex-Prep
	 * - PMI-Hash
	 * 
	 * + Those that were added via addNewClassifierInformation function
	 * */
	public static WeightedClassifier getClassifierByNameSimpleDetector (String classifierName) throws Exception
	{
		WeightedClassifier resClassifier = getClassifierByNameSimpleDetectorInternal(classifierName);
		resClassifier.setName(classifierName);
		return resClassifier;
	}
	
	/**
	 * Returns a list of available classifier names, which can be accessed by calling 
	 * UsedClassifierFactory.getClassifierByName or getClassifierByNameSimpleDetector
	 * @return
	 */
	public static List<String> getListOfAvailableClassifierNames() {
		List<String> availableClassifierNames = new ArrayList<String>();
		availableClassifierNames.add("GALC");
		availableClassifierNames.add("GALC-R-Inst");
		availableClassifierNames.add("OlympLex");
		availableClassifierNames.add("OlympLex-Prep");
		availableClassifierNames.add("PMI-Hash");
		
		availableClassifierNames.addAll(otherClassifiersInFilesMap.keySet());
		return availableClassifierNames;
	}
	
	/**
	 * Add a new classifier link into UsedClassifierFactory.
	 * @param classifierName
	 * @param classifierType Can be 'Lexicon' or 'Weka'. In the first case the classifier file stores the lexicon in text format, in the second case it stores the binary of the classifier.
	 * @param classifierFilename The absolute path to the stored classifier file.
	 * @param catDataType Has to be set for Lexicon-based classifiers
	 */
	public static void addNewClassifierInformation(
			String classifierName, String classifierType, String classifierFilename,
			CategoryDataType catDataType) {
		ClassifierLoadInfo classifierInfo = new ClassifierLoadInfo();
		classifierInfo.classifierName = classifierName;
		classifierInfo.classifierType = ClassifierType.valueOf(classifierType);
		classifierInfo.classifierFileName = classifierFilename;
		classifierInfo.catDataType = catDataType;
		otherClassifiersInFilesMap.put(classifierName, classifierInfo);
	}
	
	
	/**
	 * Caution! This function can not extract the additional parameters of classifier application, 
	 * as they are not saved in the files.
	 * @param classifierName
	 * @param classifierFilename
	 * @return
	 * @throws Exception
	 */
	private static WeightedClassifier getBuiltWekaBasedClassifierFromFileSimple(
			String classifierFilename) throws Exception
	{
		WeightedClassifier resClassifier = null;
		try
		{
			resClassifier = WeightedClassifierWekaBased.readFromBinaryFile(classifierFilename);
		}
		catch (ClassCastException e1)
		{
			try
			{
				resClassifier = ClassifierWekaBasedIndependentFullyWithProbabilities.readFromBinaryFile(classifierFilename);
			}
			catch (ClassCastException e2)
			{
				try
				{
					resClassifier = ClassifierWekaBasedIndependentWithProbabilities.readFromBinaryFile(classifierFilename);
				}
				catch (ClassCastException e3)
				{
					try
					{
						resClassifier = WeightedClassifierWekaBasedIndependentFully.readFromBinaryFile(classifierFilename);
					}
					catch (ClassCastException e4)
					{
						resClassifier = WeightedClassifierWekaBasedIndependent.readFromBinaryFile(classifierFilename);
					}
				}
			}
		}
		
		return resClassifier;
	}
	
	
	/**
	 * Caution! This function can not extract the additional parameters of classifier application, 
	 * as they are not saved in the files.
	 * @param classifierName
	 * @param classifierFilename
	 * @return
	 * @throws Exception
	 */
	public static WeightedClassifier getBuiltWekaBasedClassifierFromFile (
			String classifierName, 
			String classifierFilename, 
			ClassifierApplicationParameters classifierApplicationParams) throws Exception
	{
		WeightedClassifier resClassifier = getBuiltWekaBasedClassifierFromFileSimple(classifierFilename);
		resClassifier.setName(classifierName);
		
		resClassifier = updateWeightedClassifierForApplicationParameters(resClassifier, classifierApplicationParams);
		return resClassifier;
	}
	
	/**
	 * This function might change the initial classifier!
	 * @param initClassifier
	 * @param classifierApplicationParams
	 * @return
	 */
	private static WeightedClassifier updateWeightedClassifierForApplicationParameters(
			WeightedClassifier initClassifier, ClassifierApplicationParameters classifierApplicationParams) {
		WeightedClassifier resClassifier = initClassifier;
		
		if (classifierApplicationParams.multiLabelOutputRefinementParams != null)
		{
			resClassifier = new WeightedClassifierWrapperWithApplicationParams(resClassifier);
			((WeightedClassifierWrapperWithApplicationParams)resClassifier).curApplicationParams.alphaCut = 
					classifierApplicationParams.multiLabelOutputRefinementParams.alphaCut;
		}
		
		if (classifierApplicationParams.parametersOfTermDetectionForClassifier.requiresChangeInDetection())
			resClassifier.setupTermDetectionParameters(classifierApplicationParams.parametersOfTermDetectionForClassifier);
		
		return resClassifier;
	}
	
	/**
	 *  * Caution! This function can not extract the additional parameters of classifier application from the file, as this information is not stored there! 
	 *  That is why separate classifierApplicationParams should be set.
	 * @param classifierName
	 * @param classifierFilename
	 * @param termDetectionParameters
	 * @return
	 * @throws Exception
	 */
	public static WeightedClassifier getBuiltLexiconClassifierFromFile(
			String classifierName, String classifierFilename, CategoryDataType catDataType,
			ClassifierApplicationParameters classifierApplicationParams) throws Exception
	{
		WeightedClassifier resClassifier = 
				ClassifierLoader.getLexiconBasedWeightedClassificationSimple(classifierFilename, catDataType, false);
		resClassifier.setName(classifierName);
		resClassifier = updateWeightedClassifierForApplicationParameters(resClassifier, classifierApplicationParams);
		
		return resClassifier;
	}
	
	
	
	private static WeightedClassifier getClassifierByNameSimpleDetectorInternal (String classifierName) throws Exception
	{
		if (classifierName.equals("GALC"))
			return getGALCClassifier();
		else if (classifierName.equals("GALC-R-Inst"))
			return getGALCRevisedInstanceClassifier();
		else if (classifierName.startsWith("OlympLex"))
			return getOlympLexClassifier(classifierName);
		else if (classifierName.equals("PMI-Hash"))
			return getPMIHashClassifier();
		else if (otherClassifiersInFilesMap.containsKey(classifierName)) 
			return getClassifierFromOtherListSimpleDetector(classifierName);
		else
			throw new Exception("The name of classifier doesn't exist: " + classifierName);
	}
	
	private static WeightedClassifier getPMIHashClassifier() throws Exception {
		WeightedClassifierLexiconBased resClassifier = EmotionClassifierGivenFactory.getPMIHashClassifier();
		resClassifier.setName("PMI-Hash");
		return resClassifier;
	}
	
	static HashMap<String, ClassifierLoadInfo> otherClassifiersInFilesMap;
	
	static {
		otherClassifiersInFilesMap = new HashMap<String, ClassifierLoadInfo>();
	}

	
	private static WeightedClassifier getClassifierFromOtherListSimpleDetector(String otherClassifierName) throws Exception {
		ClassifierLoadInfo classifierInfo = otherClassifiersInFilesMap.get(otherClassifierName);
		return getClassifierByInfoSimpleDetector(classifierInfo);
	}
	
	private static WeightedClassifier getClassifierByInfoSimpleDetector(ClassifierLoadInfo classifierInfo) throws Exception {
		
		WeightedClassifier resClassifier = null;
		if (classifierInfo.classifierType == ClassifierType.Lexicon) {
			resClassifier = ClassifierLoader.getLexiconBasedWeightedClassificationSimple(
					classifierInfo.classifierFileName, classifierInfo.catDataType, false);
		} else if (classifierInfo.classifierType == ClassifierType.Weka) {
			resClassifier = getBuiltWekaBasedClassifierFromFileSimple(classifierInfo.classifierFileName);
		}
		
		return resClassifier;
	}
	
	static class ClassifierLoadInfo {
		String classifierName;
		String classifierFileName; // should be the absolute path
		ClassifierType classifierType;
		CategoryDataType catDataType; // required for lexicon
		
		public enum ClassifierType {Lexicon, Weka};
	}
	
}
