/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.processing;

import java.util.List;
import java.util.Map;

import classification.definitions.WeightedClassifier;
import classifiers.initialization.UsedClassifierFactory;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.TweetData;
import data.loading.DataSetFormatter;
import learning.parameters.ClassifierApplicationParameters;
import linguistic.TermDetectionParameters;

public class RunStatisticsComputation {

	public static String outputFolder = "output/stats/";
	
	/** 
	 * Outputs the category distribution found after applying the given classifier  into the file with the default name:
	 * outputFolder + "categoryDistr_of_"+evaluatedClassifierName + "_over_" + dataNameToApplyClassifier +".txt" ;
	 * @param evaluatedClassifierName
	 * @param applicationParams
	 * @param dataNameToApplyClassifier
	 * @throws Exception
	 */
	public static void runToComputeCategoryDistributionOfClassifier(
			String evaluatedClassifierName, 
			ClassifierApplicationParameters applicationParams,
			String dataNameToApplyClassifier
			) throws Exception
	{
		String defaultFileName =  outputFolder + "categoryDistr_of_"+evaluatedClassifierName + "_over_" + dataNameToApplyClassifier +".txt" ; 
		runToComputeCategoryDistributionOfClassifier(evaluatedClassifierName, applicationParams, dataNameToApplyClassifier, 
				 defaultFileName );
	}
	
	/**
	 * Output the category distribution found after applying the given classifier
	 * @param evaluatedClassifierName One classifier name available from UsedClassifierFactory.getClassifierByName
	 * @param applicationParams
	 * @param dataNameToApplyClassifier
	 * @throws Exception
	 */
	public static void runToComputeCategoryDistributionOfClassifier(
			String evaluatedClassifierName, 
			ClassifierApplicationParameters applicationParams,
			String dataNameToApplyClassifier,
			String statFileName
			) throws Exception
	{
		TweetData tweetDataToApply = 
				DataRepositoryIndex.getTweetDataByName(dataNameToApplyClassifier);
		
		// classifier initialization
		WeightedClassifier evaluatedClassifier = UsedClassifierFactory.getClassifierByName(
				evaluatedClassifierName,
				applicationParams.parametersOfTermDetectionForClassifier);
	
		DataStatistics.computeCategoryDistributionAsDetectedByClassifier(statFileName, tweetDataToApply.tweets, evaluatedClassifier, true);
	}
	
	/**
	 * Computes per-category term occurrences for a given list of features. 
	 * The categories are assigned to the unlabeled data based on the output of the given classifier.
	 * @param featuresList
	 * @param classifierToApply One classifier name available from UsedClassifierFactory.getClassifierByName
	 * @param applicationParams
	 * @param dataNameToApplyClassifier
	 * @param statFileName
	 * @throws Exception
	 */
	public static void runToGetTermAppearencesOverUnlabeledDataWithInitialClassifier(
			List<String> featuresList, 
			String classifierToApply, ClassifierApplicationParameters applicationParams,
			String dataNameToApplyClassifier, 
			boolean toExcludeTweetsWithMultipleFeatures, 
			TermDetectionParameters termDetectionParameters, int minFeatureOccurNum,
			String statFileName) throws Exception
	{
		
		// classifier initialization
		WeightedClassifier initialClassifier = UsedClassifierFactory.getClassifierByName(
						classifierToApply, 
						applicationParams.parametersOfTermDetectionForClassifier);
			
		TweetData tweetDataToApply = 
				DataRepositoryIndex.getTweetDataByName(dataNameToApplyClassifier);
		
		
		Map<Integer, List<Integer>> categoryAssignment = DataStatistics.getCategoryAssignmentByClassifier(
				tweetDataToApply.tweets, initialClassifier, false);
		Map<Integer, String> textData = DataSetFormatter.getStringFromTweets(tweetDataToApply.tweets);
		
		DataStatistics.computeTermPerCategoryAppearence(statFileName, 
				new LabeledData(categoryAssignment, textData), initialClassifier.getCategoriesData().getCategoriesType(),
				featuresList,
				toExcludeTweetsWithMultipleFeatures, termDetectionParameters, minFeatureOccurNum);
		
	}
		
}
