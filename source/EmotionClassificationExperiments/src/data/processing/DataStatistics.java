/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.processing;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utils.UtilCategoryLabelFormatter;
import utils.UtilLearner;
import learners.featureextraction.TweetRepresentation;
import learners.given.ClassifierLearnerWeightedVoting;
import linguistic.TermDetectionParameters;
import classification.definitions.WeightedClassifier;
import data.LabeledData;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.documents.Tweet;
import data.loading.DataFilesFormatReader;
import data.loading.DataSetFormatter;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class DataStatistics {
	
	
	public static Map<Integer, List<Integer>> getCategoryAssignmentByClassifier(List<? extends Tweet> tweetData, WeightedClassifier classifier, boolean includeNeutral) throws Exception
	{
		Map<Integer, List<Integer>> categoryAssignment = new HashMap<Integer,List<Integer>>();
		classifier.returnNoEmotionWhenNoFeaturesFound = includeNeutral;
		double[] curWeights;
		List<Integer> foundCategories;
		for (Tweet tweet : tweetData)
		{
			curWeights = classifier.findCategoryWeights(tweet);
			foundCategories = classifier.getDominantCategories(curWeights);
			if (foundCategories.size() > 0)
				categoryAssignment.put(tweet.tweetId, foundCategories);
		}
		
		return categoryAssignment;
	}
	
	/**
	 * Computes the distribution of the dominant emotion categories returned by the classifier. 
	 * By default, the neutral emotion is not returned when no features are found. To require the classifier to return it, set includeNeutral to true.
	 * @param statFilename
	 * @param tweetData
	 * @param classifier
	 * @param includeNeutral
	 * @throws Exception
	 */
	public static void computeCategoryDistributionAsDetectedByClassifier(String statFilename, List<? extends Tweet> tweetData, WeightedClassifier classifier, boolean includeNeutral) throws Exception
	{
		Map<Integer,Integer> countOfFoundCategories = new HashMap<Integer,Integer>();
		double[] curWeights;
		List<Integer> foundCategories;
		classifier.returnNoEmotionWhenNoFeaturesFound = includeNeutral;
		for (Tweet tweet : tweetData)
		{
			curWeights = classifier.findCategoryWeights(tweet);
			foundCategories = classifier.getDominantCategories(curWeights);
			for (Integer foundCategory : foundCategories)
			{
				UtilCollections.incrementIntValueInMap(countOfFoundCategories, foundCategory);
			}
		}
		
		countOfFoundCategories = UtilCollections.sortMapByKeyAndReturnNewOne(countOfFoundCategories);
		
		UtilCollections.printToFileMap(countOfFoundCategories, statFilename);
	}
	
	/**
	 * Print statistics on the occurrence of the given terms within the provided tweets (using their category assignment).
	 * @param statFilename Path to the file to output the statistics results
	 * @param labeledData
	 * @param catDataType
	 * @param givenFeatures
	 * @param toExcludeTweetsWithMultipleFeatures Whether to consider tweets where several given feature are present.
	 * @param termDetectionParameters
	 * @param minFeatureOccurNum
	 * @throws Exception
	 */
	public static void computeTermPerCategoryAppearence(String statFilename, 
			LabeledData labeledData, CategoryDataType catDataType,
			List<String> givenFeatures, 
			boolean toExcludeTweetsWithMultipleFeatures, 
			TermDetectionParameters termDetectionParameters, int minFeatureOccurNum) throws Exception
	{
		
		List<Tweet> tweetData = DataSetFormatter.getTweetsFromStrings(labeledData.textData);
		
		Map<Integer,List<String>> tweetRepresentation = TweetRepresentation.getTokenNgramRepresentationFromFixedIndicators (
				tweetData, givenFeatures, termDetectionParameters, minFeatureOccurNum );
		
		if (toExcludeTweetsWithMultipleFeatures)
		{
			Set<Integer> keysToRemove = new HashSet<Integer>();
			for (Map.Entry<Integer, List<String>> reprEntry: tweetRepresentation.entrySet() )
			{
				if (reprEntry.getValue().size() > 1)
					keysToRemove.add(reprEntry.getKey());
			}
			UtilCollections.removeAllKeysFromMap(tweetRepresentation, keysToRemove);
		}
		
		
		Map<Integer, double[]> weightedAssignment = UtilCategoryLabelFormatter.transformStrictAnnotationsIntoWeighted(
				CategoryProcessor.getCategoriesData(catDataType), labeledData.emotionLabels);
		
		// compute the distribution of each term
		Map<String, double[]> resultTermEmotionWeights = new HashMap<String, double[]>();
		ClassifierLearnerWeightedVoting.computeInternalFeatureRepresentation(weightedAssignment, tweetRepresentation, resultTermEmotionWeights);
		
		// compute the occurrence of each term
		Map<String,Integer> termOccurrence = UtilLearner.computeTermOccurrence(tweetRepresentation);
		
		PrintWriter pw = new PrintWriter(statFilename);
		
		for (Map.Entry<String, double[]> termEntry : resultTermEmotionWeights.entrySet())
		{
			pw.print(termEntry.getKey() + "\t");
			pw.print(termOccurrence.get(termEntry.getKey()) + "\t");
			pw.print(UtilArrays.join(termEntry.getValue(), "\t"));
			pw.println();
		}
		pw.close();
		
	}
	
	/**
	 * Prints the distribution of the emotion categories as saved in the saved detailed output of the classifier
	 * @param algResultsOutputFile The file with the detailed output
	 * @throws IOException
	 */
	public static void computeEmotionDistributionFromClassifierOutput(String algResultsOutputFile, CategoryDataType catDataType) throws IOException
	{
		Map<Integer, List<Integer>> labels = DataFilesFormatReader.getLabelDataFromFileOutput(algResultsOutputFile);
		
		Map<Integer, Integer> categoryCount = new HashMap<Integer, Integer>();
		
		// adapt to count the neutral category
		for (List<Integer> categories : labels.values())
		{
			if (categories.contains(CategoryProcessor.neutralCategory(catDataType)) || categories.size() == 0)
				UtilCollections.incrementIntValueInMap(categoryCount, CategoryProcessor.neutralCategory(catDataType));
			else
				for (Integer cat : categories)
				{
					UtilCollections.incrementIntValueInMap(categoryCount, cat);
				}
		}
		
		categoryCount = UtilCollections.sortMapByKeyAndReturnNewOne(categoryCount);
		
		for (Map.Entry<Integer, Integer> catD : categoryCount.entrySet())
		{
			System.out.println(catD.getKey() + "\t" + catD.getValue());
		}
	}
}
