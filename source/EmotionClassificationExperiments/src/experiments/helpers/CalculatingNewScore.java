/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import utility.Triple;
import classification.evaluation.ComplexEvaluation.CategoryEvaluationResult;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import experimentsavers.EvaluationSaving;

public class CalculatingNewScore {

	/**
	 * Computes a new score of macro F-0.5 for all evaluation results.
	 * A new field `emotionf05Score` should be added to evaluation results table before calling this function.
	 * @throws Exception
	 */
	public static void calculateF05ScoreForAllExperimentResults(CategoryDataType catDataType) throws Exception
	{
		// 1, get ids of all exp results in full table
		String query = "select RunId, RunComp, TestFileName  from  " + EvaluationSaving.evaluationResultTablename + " where emotionf05Score is null";
		
		Connection conn = EvaluationSaving.GetDatabaseConnectionForEvaluationSaving();  
		
		Statement st =  conn.createStatement();
		
		List<Triple<Integer,String,String>> foundIds = new ArrayList<Triple<Integer,String,String>>();
		 ResultSet rs = st.executeQuery(query);
		  while (rs.next()) 
		    {
			  foundIds.add(new Triple(rs.getInt("RunId"), rs.getString(2), rs.getString(3)));
		    }
		  
		 ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		  
		// 2. for each run id get the per-category results
		 
		for (Triple<Integer,String,String> runIdComp : foundIds)
		{
		  String queryCat = "select categoryId, objectsGivenNum, `precision`, `recall` from " + EvaluationSaving.evaluationCategoryResultTablename + 
		  		" where RunId = ? and RunComp = ? and TestFileName = ? and CategoryType=\"Category\"";
		  PreparedStatement prep = conn.prepareStatement(queryCat);
		  prep.setInt(1, runIdComp.first);
		  prep.setString(2, runIdComp.second);
		  prep.setString(3, runIdComp.third);
		 
		  rs = prep.executeQuery();
		
		  
		  List<CategoryEvaluationResult> curCatResults = new ArrayList<CategoryEvaluationResult>();
		  while (rs.next()) 
		  {
			  int catId = rs.getInt("categoryId");
			  if (!categoriesData.isEmotionalCategory(catId))
				  continue;
			  CategoryEvaluationResult catResult = new  CategoryEvaluationResult();
			  catResult.objectsGivenNum = rs.getInt("objectsGivenNum");
			  catResult.precision = rs.getDouble("precision");
			  catResult.recall = rs.getDouble("recall");
			  curCatResults.add(catResult);
		  }
		  rs.close();
		  prep.close();
		  
		  // 3. calculate the new score
		  double newScore = computeMacroF05scoreForClassArray(curCatResults, 10.0);
		  
		  // 4. save the new score in the database
		  String updateQuery = "update  " + EvaluationSaving.evaluationResultTablename + " set emotionf05Score = ? where RunId = ? and RunComp = ? and TestFileName = ?";
		  prep = conn.prepareStatement(updateQuery);
		  prep.setDouble(1, newScore);
		  prep.setInt(2, runIdComp.first);
		  prep.setString(3, runIdComp.second);
		  prep.setString(4, runIdComp.third);
		  prep.execute();
		  prep.close();
		}
	}
	
	private static double computeMacroF05scoreForClassArray (List<? extends CategoryEvaluationResult> classesData, double maxWeight)
	{
		double finalScore = 0.0;
		int foundClassNum = 0;
		for (CategoryEvaluationResult classResult : classesData)
		{
			if (classResult.objectsGivenNum <= maxWeight)
				continue;
			foundClassNum++;
			try
			{
				double score = (1.25 * classResult.precision * classResult.recall)/ (0.25 * classResult.precision + classResult.recall);
				finalScore += Double.isNaN(score)?0.0:score;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
		}
		return finalScore / (1.0 * foundClassNum);
	}
	
	
	/**
	 * Add the computed microF1, micro-Precision, and micro-Recall scores where they are not computed.
	 * @throws Exception
	 */
	public static void calculateMicroF1ScoresForAllExperimentResults(CategoryDataType catDataType) throws Exception
	{
		// 1, get ids of all exp results in full table
		String query = "select RunId, RunComp, TestFileName from  " + EvaluationSaving.evaluationResultTablename + " where emotionMicrof1Score is null";
		
		Connection conn = EvaluationSaving.GetDatabaseConnectionForEvaluationSaving(); 
		
		Statement st =  conn.createStatement();
		
		List<Triple<Integer,String,String>> foundIds = new ArrayList<Triple<Integer,String,String>>();
		 ResultSet rs = st.executeQuery(query);
		  while (rs.next()) 
		    {
			  foundIds.add(new Triple(rs.getInt("RunId"), rs.getString(2), rs.getString(3)));
		    }
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
			
		// 2. for each run id get the per-category results
		 
		for (Triple<Integer,String,String> runIdComp : foundIds)
		{
		  String queryCat = "select categoryId, objectsGivenNum, objectsFoundNum, objectsCorrectNum, `precision`, `recall` "
		  		+ "from " + EvaluationSaving.evaluationCategoryResultTablename + " where "
		  		+ "RunId = ? and RunComp = ? and TestFileName = ? and CategoryType=\"Category\"";
		  PreparedStatement prep = conn.prepareStatement(queryCat);
		  prep.setInt(1, runIdComp.first);
		  prep.setString(2, runIdComp.second);
		  prep.setString(3, runIdComp.third);
		  
		  rs = prep.executeQuery();
		
		  
		  List<CategoryEvaluationResult> curCatResults = new ArrayList< CategoryEvaluationResult>();
		  while (rs.next()) 
		  {
			  int catId = rs.getInt("categoryId");
			  if (!categoriesData.isEmotionalCategory(catId))
				  continue;
			  CategoryEvaluationResult catResult = new  CategoryEvaluationResult();
			  catResult.objectsGivenNum = rs.getInt("objectsGivenNum");
			  catResult.precision = rs.getDouble("precision");
			  catResult.recall = rs.getDouble("recall");
			  catResult.objectsFoundNum = rs.getInt("objectsFoundNum");
			  catResult.objectsCorrectNum = rs.getInt("objectsCorrectNum");
				 
			  curCatResults.add(catResult);
		  }
		  rs.close();
		  prep.close();
		  
		  // 3. calculate the new score
		  Triple<Double, Double, Double> newScores = computeMicroScoresForClassArray (curCatResults);
		  
		  // 4. save the new score in the database
		  String updateQuery = "update " + EvaluationSaving.evaluationResultTablename + " set emotionMicrof1Score = ?, emotionMicroPrecision = ?, emotionMicroRecall = ? where RunId = ? and RunComp = ? and TestFileName=? ";
		  prep = conn.prepareStatement(updateQuery);
		  if (newScores.first != null)
			  prep.setDouble(1, newScores.first);
		  else
			  prep.setNull(1, Types.DOUBLE);
		  if (newScores.second != null)
			  prep.setDouble(2, newScores.second);
		  else
			  prep.setNull(2, Types.DOUBLE);
		  if (newScores.third != null)		
			  prep.setDouble(3, newScores.third);
		  else
			  prep.setNull(3, Types.DOUBLE);
		  prep.setInt(4, runIdComp.first);
		  prep.setString(5, runIdComp.second);
		  prep.setString(6, runIdComp.third);
		  
		  prep.execute();
		  prep.close();
		}
	}
	
	
	
	private static Triple<Double, Double, Double> computeMicroScoresForClassArray (List<? extends CategoryEvaluationResult> classesData)
	{
		if (classesData.size() == 0)
			return new Triple (null, null, null);
		
		int allCorrectNum = 0;
		int allFoundNum = 0;
		int allGivenNum = 0;
		for (CategoryEvaluationResult classResult : classesData)
		{
			 allGivenNum += classResult.objectsGivenNum;
			 allFoundNum += classResult.objectsFoundNum;
			 allCorrectNum += classResult.objectsCorrectNum;
		}
		
		
		Double microPrecision = 1.0 * allCorrectNum / (1.0 * allFoundNum);
		Double microRecall = 1.0 * allCorrectNum / (1.0 * allGivenNum);
		Double microF1Score =  (2 * microPrecision * microRecall)/ (microPrecision + microRecall);
		
		microF1Score = Double.isNaN(microF1Score)?0.0:microF1Score;
		microPrecision = Double.isNaN(microPrecision)?null:microPrecision;
		microRecall = Double.isNaN(microRecall)?null:microRecall;
		
		return new Triple<Double, Double, Double>(microF1Score, microPrecision, microRecall);
	}
	
	/**
	 * This calculates for all results the geometric product between macro_F1, A, and micro_F1.
	 * The new field `geomGenProd` should be added before to the evaluation results.
	 * @throws Exception
	 */
	public static void calculateGeneralizingScoreForAllExperimentResults() throws Exception
	{
		// 1, get ids of all exp results in full table
		String query = "select RunId, RunComp, TestFileName, emotionMacroF1Score as `macroF1Score`, " +
				"emotionAccuracy as `accuracy`, emotionMicroF1Score as `microF1Score` " +
				"from " + EvaluationSaving.evaluationResultTablename + " where `geomGenProd` is null";
		
		Connection conn = EvaluationSaving.GetDatabaseConnectionForEvaluationSaving();  
		
		Statement st = conn.createStatement();
		
		PreparedStatement prep = conn.prepareStatement(
				"update " + EvaluationSaving.evaluationResultTablename + " set `geomGenProd` = ? where RunId = ? and RunComp = ? and TestFileName = ?");
	  
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) 
		{
			int runId = rs.getInt("RunId");
			String runComp = rs.getString("RunComp");
			String testName = rs.getString("TestFileName");
			
			double[] scores = new double[3];
			scores[0] = rs.getDouble("macroF1Score");
			scores[1] = rs.getDouble("accuracy");
			scores[2] = rs.getDouble("microF1Score");
			
			double newScore = 1.0;
			
			for (int i = 0; i < scores.length; ++i) {
				newScore *= (Double.isNaN(scores[i]) || scores[i] == 0.0) ? 0.001 : scores[i];
			}
			newScore = Math.pow(newScore, 1 / (1.0 * scores.length)); // geometric mean between macroF1Score, microF1Score, and Accuracy
			
			prep.setDouble(1, newScore);
			prep.setInt(2, runId);
			prep.setString(3, runComp);
			prep.setString(4, testName);
	    	prep.addBatch();
		}
		
		rs.close();
		st.close();
		
		conn.setAutoCommit(false);
		prep.executeBatch();
		conn.setAutoCommit(true);
		prep.close();
		
		conn.close();
	}

}
