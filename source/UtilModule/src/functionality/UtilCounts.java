/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UtilCounts {

	public static Set<String> findLowOccurrencesInMap(Map<String, Integer> occurTermMap, int frequencyThreshold)
	{
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Integer> termEntry : occurTermMap.entrySet())
		{
			if (termEntry.getValue() < frequencyThreshold)
				termsToRemove.add(termEntry.getKey());
		}
		return termsToRemove;
	}
	
}
