/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class UtilDates {

	
	private static SimpleDateFormat  mySQLdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2012-08-11 00:43:24
	
	private static SimpleDateFormat fullDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy"); //Fri Nov 09 00:05:32 CET 2012
	
	
	/**
	 * Time zone is BST for London
	 * @param date
	 * @param timeZone
	 * @return
	 */
	public static String transformToSQLFormat(Date date)
	{
		return  mySQLdateFormat.format(date);
	}
	
	public static void setTimeZone (String timeZone)
	{
		mySQLdateFormat.setTimeZone(TimeZone.getTimeZone( timeZone));
		fullDateFormat.setTimeZone(TimeZone.getTimeZone( timeZone));
	}

	/**
	 * Time zone is BST for London.
	 * MySql format: yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @param timeZone
	 * @return
	 * @throws ParseException 
	 */
	public static Date parseDateFromMySQLString(String dateString) throws ParseException
	{
		Date datetime = mySQLdateFormat.parse(dateString);
		return datetime;
	}
	
	/**
	 * Time zone is BST for London
	 * @param date
	 * @param timeZone
	 * @return
	 * @throws ParseException 
	 */
	public static Date parseDateFromFullString(String dateString) throws ParseException
	{
		Date datetime = fullDateFormat.parse(dateString);
		return datetime;
	}
	
	
	public static Date parseDateFromString(String dateString, String dateFormat) throws ParseException
	{
		SimpleDateFormat dateFormatInst = new SimpleDateFormat(dateFormat);
		Date datetime = dateFormatInst.parse(dateString);
		return datetime;
	}
	
	/**
	 * Compares the two dates. 
	 * Returns <0 if date1 < date2 (where greater means later);
	 * Returns 0 if they are the same
	 * Returns >0 if date1 > date2
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int compareDates(Date date1, Date date2)
	{
		return date1.compareTo(date2);
	}
	
	public static Date addSomeTimeToDate (Date initDate, long addValueInMilliseconds)
	{
		Date newDate = new Date();
		newDate.setTime(initDate.getTime() + addValueInMilliseconds);
		return newDate;
	}
}
