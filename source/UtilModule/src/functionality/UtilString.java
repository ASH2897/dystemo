/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import utility.Pair;

public class UtilString {
	
	/**
	 * Merges the tokens into one string. The separator is white space.
	 * @param tokens
	 * @return
	 */
	public static String generateStringFromStringList(List<String> tokens)
	{
		StringBuilder sb = new StringBuilder();
		for (String token : tokens)
		{
			sb.append(token);
			sb.append(" ");
		}
		if (sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	/**
	 * Merges the tokens into one string by calling token.toString() method. The separator is white space.
	 * @param tokens
	 * @return
	 */
	public static<T extends Object> String generateStringFromList(List<T> tokens)
	{
		if (tokens == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (T token : tokens)
		{
			sb.append(token.toString());
			sb.append(" ");
		}
		if (sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}
	
	/**
	 * Outputs the data array as a string, with elements separated with a tab (\t).
	 * @param data
	 * @return
	 */
	public static String generateStringFromArray(String[] data) {
		StringBuffer out = new StringBuffer();
		for (int x=0;x<data.length;x++){
			   out.append(data[x]);
			   out.append("\t");
		}
		return out.toString();
	}
	
	public static List<Integer> getIntegerListFromString(String data, String separator) throws NumberFormatException
	{
		if (data == null)
			return null;
		String[] splitData = data.split(Pattern.quote(separator));
		List<Integer> resultInts = new ArrayList<Integer>();
		for (String token : splitData)
		{
			int curInt = Integer.parseInt(token.trim());
			resultInts.add(curInt);
		}     
		return resultInts;
	}
	
	/** Returns all the tokens separated via white spaces (or similar chars) 
	 * */
	public static List<String> getAllSeparatedNgrams(String data, int ngramLength)
	{
		List<String> allNgrams = new ArrayList<String>();
		List<String> tokens = getAllSeparatedTokens(data);
		allNgrams.addAll(tokens);
		if (ngramLength > 1)
		{
			Map<String, Integer> ngramPrefixes = new HashMap<String, Integer>(); // will contain only those prefixes which still allow continuation
			for (int i = 0; i < tokens.size(); i++)
			{
				String token = tokens.get(i);
				Map<String, Integer> newPrefixes = new HashMap<String, Integer>();
				for (String prefix : ngramPrefixes.keySet())
				{
					String newNgram = prefix + " " + token;
					allNgrams.add(newNgram);
					
					int curPrefixLength = ngramPrefixes.get(prefix);
					if (curPrefixLength + 1 < ngramLength)
					{
						newPrefixes.put(newNgram, curPrefixLength + 1);
						
					}
				}
				newPrefixes.put(token, 1);
				
				ngramPrefixes=newPrefixes;
			}
		}
		return allNgrams;
	}
	
	/** Returns all the tokens separated via white spaces (or similar chars) 
	 * */
	public static List<String> getAllSeparatedTokens(String data)
	{
		return splitTextRegex(data, "\\s+");
	}
	
	public static List<String> splitTextStrict(String data, String separator) throws NumberFormatException
	{
		if (data == null)
			return null;
		String[] splitData = data.split(Pattern.quote(separator), -1);
		List<String> result = Arrays.asList(splitData);
		//result.removeAll(droppableStringElements);
		return result;
	}
	
	public static List<String> splitTextRegex(String data, String separator) 
	{
		if (data == null)
			return null;
		String[] splitData = data.split(separator, -1);
		List<String> result = Arrays.asList(splitData);
		
		return result;
	}
	
	/** remove all empty and null strings */
	public static List<String> refineStringList(List<String> list) throws NumberFormatException
	{
		List<String> result = new ArrayList<String>(list);
		for (int ind = result.size() - 1; ind >= 0; --ind)
		{
			String phrase = result.get(ind);
			if (phrase == null || phrase.length() == 0)
				result.remove(ind);
		}
		return result;
	}
	
	
	static public int getWordNumber(String text)
	{
		if (text == null)
			return 0;
		//TODO: delete all punctuation signs?
		return text.trim().split("\\s+").length;
	}
	
	
	static public boolean checkAllUpperCase(String text)
	{
		return text.equals(text.toUpperCase());
	}

	public static boolean nullOrEmpty(String text) {
		return (text == null || text.length() == 0);
	}
	
	/**
	 * Parses integer from the string and returns specific code (number) in case of an error
	 * @param number
	 * @param errorNumber The code to return in case of an error
	 * @return
	 */
	static public int parseInt(String number, int errorNumber)
	{
		int res;
		try
		{
			 res = Integer.parseInt(number);
		}
		catch (Exception e)
		{
			res = errorNumber;
		}
		return res;
	}
	
	
	/**
	 * Parses the text into two parts using the separator. The split is based on the first occurrence of separator. 
	 * If no separator is found, it throws exception!
	 * @param text
	 * @param separator
	 * @return
	 * @throws Exception 
	 */
	public static Pair<String,String> parseInPair(String text, String separator) throws Exception
	{
		int index = text.indexOf(separator);
		if (index < 0)
			throw new Exception("No separator found!");
		
		String first = text.substring(0, index);
		String second = text.substring(index + separator.length());
		
		return new Pair<String,String>(first, second);
	}
	
	public static Pair<String,String> parseInPairForLastOccurrence(String text, String separator) throws Exception
	{
		int index = text.lastIndexOf(separator);
		if (index < 0)
			throw new Exception("No separator found!");
		
		String first = text.substring(0, index);
		String second = text.substring(index + separator.length());
		
		return new Pair<String,String>(first, second);
	}
	
	/**
	 * Converts the input stream to string format assuming it has "UTF-8" encoding
	 * @param input
	 * @return
	 * @throws IOException 
	 */
	public static String convertInputStreamToString(InputStream input) throws IOException
	{
		StringWriter writer = new StringWriter();
		IOUtils.copy(input, writer, "UTF-8");
		String theString = writer.toString();
		return theString;
	}
	
	
	/**
	 * Splits each data line into the entries based on a given separator. The first element to save in the output list is under index 'firstLine'.
	 * @param dataLines
	 * @param separator
	 * @param firstLine
	 * @return
	 * @throws IOException
	 */
	static public List<String[]> parseTable (List<String> dataLines, String separator, int firstLine) throws IOException
	{
		
		List<String[]> resultLines = new ArrayList<String[]>();
		 
		 for (int lineIndex = firstLine; lineIndex < dataLines.size(); ++lineIndex)
		 {
			 String line = dataLines.get(lineIndex);
			 String[] parsedLine = line.split(separator, -1);//-1 in order to include the trailing inclusions (empty ones)
			 resultLines.add(parsedLine);
		 }
		return resultLines;
	}

	/**
	 * This function finds the entry substring in the text and then extracts the full word in which it appeared (separated by white spaces or text limits). 
	 * !!! Text is supposed to be preprocessed -> the separation between words are white spaces.
	 * @param text
	 * @param entry
	 * @return
	 */
	static public String extendPresentEntryToWord(String text, String entry)
	{
		if (!text.contains(entry))
			return null;
		
		int startInd = text.indexOf(entry);
		int endInd = startInd + entry.length();
		
		while (startInd >= 0 && text.charAt(startInd) != ' ')
			--startInd;
		
		while (endInd < text.length() && text.charAt(endInd) != ' ')
			++endInd;
		
		return text.substring(startInd + 1, endInd);
		
	}
	
	
	/**
	 * Returns the found instance of the first group in the pattern; null if nothing is matching
	 * @param regexPattern
	 * @param text
	 * @return
	 */
	public static String getPatternFirstGroupEntry(String regexPattern, String text)
	{
		
		Matcher m = Pattern.compile(regexPattern).matcher(text);
		
		while(m.find())
		{
			String x = new String(m.group(1));
			return x;
		}
		
		return null;
	}
	
	/**
	 * Returns the found instance of the first group in the pattern; null if nothing is matching
	 * @param regexPattern
	 * @param text
	 * @return
	 */
	public static String getPatternFirstGroupEntry(String regexPattern, String text, int searchStartInd)
	{
		
		Matcher m = Pattern.compile(regexPattern).matcher(text);
		
		while(m.find(searchStartInd))
		{
			String x = new String(m.group(1));
			return x;
		}
		
		return null;
	}
	
	public static String escapeSpecialSymbols(String str)
	{
		return StringEscapeUtils.escapeJava(str);
	}
	
	public static String unescapeSpecialSymbols(String str)
	{
		return StringEscapeUtils.unescapeJava(str);
	}
	
	public static String getPatternEntry(String regexPattern, String text)
	{
		
		Matcher m = Pattern.compile(regexPattern).matcher(text);
		
		while(m.find())
		{
			String x = new String(m.group(1));
			return x;
		}
		
		return null;
	}

	/**
	 * Prints the given double number in shorter format.
	 * @param number
	 * @return
	 */
	public static String formatNumberInShort(double number) {
		return String.format("%.5f", number);
	}
}
