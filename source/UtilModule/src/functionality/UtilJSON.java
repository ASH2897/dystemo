/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UtilJSON {

	public static<K,V extends Object> String getJSONStringForMap (Map<K,V> data)
	{
		JSONObject dataJSON = new JSONObject(data);
		return dataJSON.toString();
	}
	
	public static void getMapFromString(String stringJSON)
	{
		
	}
	
	public static JSONObject getJSONFromString (String stringJSON) throws JSONException
	{
		return new JSONObject(stringJSON);
	}
	
	public static JSONArray getJSONArrayFromString (String stringJSON) throws JSONException
	{
		return new JSONArray(stringJSON);
	}
}
