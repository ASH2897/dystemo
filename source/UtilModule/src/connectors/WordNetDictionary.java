/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package connectors;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import utility.Pair;

/** This class is unfinished!*/
public class WordNetDictionary {
	
	static Log log = LogFactory.getLog(WordNetDictionary.class);

	static String fullLexiconFilePath = null;
	
	static
	{
		URL fullLexiconURL = WordNetDictionary.class.getClassLoader().getResource("lexicons/SentiWordNet3.0/SentiWordNet_3.0.0_20100908.txt");
		try {
			fullLexiconFilePath = fullLexiconURL.toURI().getPath();
			LoadSynsets();
		} catch (Exception e) {
			log.error("Problem with loading SentiWordNet", e);
		}
		
	}
	
	static Set<String> allTerms;
	static Set<String> allPosTerms;
	
	static Set<Pair<Set<String>, String>> synsetsWithWordsAndPOSTag;
	
	
	public static boolean containsTermWithPos(String term)
	{
		return allPosTerms.contains(term.replace(' ', '_'));
	}
	
	public static boolean containsTerm(String term)
	{
		return allTerms.contains(term.replace(' ', '_'));
	}
	
	static String[] allPosTags = {"a", "n", "v", "r"};
	
	public static Set<String> getPossiblePosTagsForTerm(String term) {
		Set<String> possibleTags = new HashSet<String>();
		for (String posTag : allPosTags) {
			if (allPosTerms.contains(term + "/" + posTag))
				possibleTags.add(posTag);
		}
		return possibleTags;
	}
	
	
	public static void LoadSynsets() throws NumberFormatException, IOException
	{
		 synsetsWithWordsAndPOSTag = new LinkedHashSet<Pair<Set<String>, String>>();
		 allTerms = new HashSet<String>();
		 allPosTerms = new HashSet<String>();
		 
		 BufferedReader fr = new BufferedReader(new InputStreamReader(new FileInputStream(fullLexiconFilePath), "UTF-8"));
			String line = null; 
			while (( line = fr.readLine()) != null){
				
				if (line.length() == 0 || line.startsWith("#"))
					continue;
				
				StringTokenizer st = new StringTokenizer(line, "\t");
				if (st.countTokens() < 6)
					continue;
				String posTag =  st.nextToken().trim();
				st.nextToken();
				st.nextToken();
				st.nextToken();
				
				Set<String> wordsInSynset = new LinkedHashSet<String>();
				String allWords = st.nextToken().trim();
				StringTokenizer wordst = new StringTokenizer(allWords, " ");
				
				while (wordst.hasMoreTokens())
				{
					String word = wordst.nextToken().trim();
					word = word.substring(0, word.indexOf('#'));
					wordsInSynset.add(word);
					
					allTerms.add(word);
					allPosTerms.add(word+"/" + posTag);
				}
				synsetsWithWordsAndPOSTag.add(new Pair<Set<String>,String>(wordsInSynset, posTag));
			}	
			fr.close();
	}
}
