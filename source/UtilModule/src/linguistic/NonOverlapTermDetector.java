/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import utility.Pair;
import functionality.UtilCollections;

public class NonOverlapTermDetector extends TermDetector implements Serializable {
	
	
	public NonOverlapTermDetector(Collection<String> terms) {
		super(terms);
	}
	
	public NonOverlapTermDetector(TermDetector tdetector) {
		super(tdetector);
	}


	private static final long serialVersionUID = -5013063535665938252L;
	
	/** finds terms in the text and returns found terms with number of occurrences */
	@Override
	public Map<String, Integer> findTermsInText(String text)
	{
		Map<String, Integer> foundOccur = super.findTermsInText(text);
		Map<String, List<int[]>> foundPositions = super.getLastPositionsOfFoundTerms();
		
		// sort in order of length
		List<Pair<String,Integer>> termLengths = new ArrayList<Pair<String,Integer>>();
		for (String term :  foundOccur.keySet())
		{
			termLengths.add(new Pair(term, term.length()));
		}
		UtilCollections.sortPairsBySecondElementIncreasing(termLengths);
		
		//we should remove all the phrases appeared (fully contained) in longer phrase
		for (int ind = termLengths.size() - 1; ind >=0; --ind)
		{
			String term = termLengths.get(ind).first;
			
			for (int ind2 = ind + 1; ind2 < termLengths.size(); ++ind2)
			{
				String checkTerm = termLengths.get(ind2).first;
						
				if (checkTerm.matches(".*(?=(\\s|^|\\A|\\b))"+Pattern.quote(term)+"(?=(\\s|$|\\z|\\b)).*"))
				{
					// checkTerm contains fully our term
					
					
					//we should also update the positions for remaining
					if (super.savePositions)
					{
						List<int[]> outsideIntervals = foundPositions.get(checkTerm);
						List<int[]> insideIntervals = foundPositions.get(term);
						int k1 = outsideIntervals.size() - 1, k2 = insideIntervals.size() - 1;
						while (k1 >= 0 && k2 >= 0)
						{
							int[] insideInterval = insideIntervals.get(k2);
							int[] outsideInterval =  outsideIntervals.get(k1);
							boolean nesting = checkIntervalNesting(insideInterval, outsideInterval);
							if (nesting)
							{
								//should remove
								insideIntervals.remove(k2);
								k2--;
							}
							else if (insideInterval[1] <= outsideInterval[0])
								k1--;
							else
								k2--;
						}
						
						// update the occurrence values
						foundOccur.put(term, insideIntervals.size()); 
					}
					else
					{
						UtilCollections.incrementIntValueInMap(foundOccur, term, -foundOccur.get(checkTerm));
					}
					if (foundOccur.get(term) <= 0)
					{
						//delete term if no more occurences found
						foundOccur.remove(term);
						termLengths.remove(ind);
						//we should also update the positions
						if (super.savePositions)
							foundPositions.remove(term);
						break;
					}
					//TODO: test if the numbers don't correspond
					if (foundOccur.get(term) != foundPositions.get(term).size())
					{
						int h = 0;
						h++;
					}
				}
			}
		}
		
		return foundOccur;
	}
	
	private boolean checkIntervalNesting(int[] insideInterval, int[] outsideInterval)
	{
		if ((insideInterval[0] >= outsideInterval[0]) && (insideInterval[1] <= outsideInterval[1]))
			return true;
		return false;
	}
	
	/** finds terms in the text and returns found terms with number of occurrences */
	public static Map<String, Integer> findTermsInText(String text, List<String> possibleTerms)
	{
		NonOverlapTermDetector tDet = new NonOverlapTermDetector(possibleTerms);
		return tDet.findTermsInText(text);
	}
}
