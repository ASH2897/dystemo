/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import functionality.UtilCollections;
import linguistic.TermDetector.PrefixNode;

public class SubstringDetector extends TermDetector {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 70254201724089447L;
	public int skipAllowenceBetweenSubstringTokens = 2;
	public boolean treatSeparators = true;
	
	private Set<String> nounWords;
	private Set<String> verbWords;
	private Set<String> adjWords;
	
	public SubstringDetector(Collection<String> termsToDetect) {
		super(termsToDetect);
		initializeLists(null, null, null);
	}
	
	public void initializeLists(Set<String> nounWords, Set<String> verbWords, Set<String> adjWords) {
		if (nounWords == null) 
			this.nounWords = new HashSet<String>();
		else
			this.nounWords = nounWords;
		if (verbWords == null) 
			this.verbWords = new HashSet<String>();
		else
			this.verbWords = verbWords;
		if (adjWords == null) 
			this.adjWords = new HashSet<String>();
		else
			this.adjWords = adjWords;
	}

	private HashMap<String, Integer> termsFoundWithCurTokenStart;  // with the number of occurrences
	
	@Override	
	// This almost repeat the function from super, except that it also stores which terms were found
	protected void continueTokenIfPresent(String curToken, PrefixNode startingNode, Map<String, Integer> foundOccur, int tokenInd, List<String> tokens)
	{
		// get the node child of the startingNode whose continuation label corresponds to the curToken
		PrefixNode nextNode = findNextPrefixNode(startingNode, curToken);
		if (nextNode == null)
			return; // nothing was found
		if (nextNode.isEnding())
		{
			// need to add a current found token in the found list
			//unigram found
			UtilCollections.incrementIntValueInMap(foundOccur, nextNode.prefix);
			if (savePositions) {
				UtilCollections.appendNewObjectToListInMap(lastPositions, nextNode.prefix, new int[]{tokenInd - nextNode.depth + 1, tokenInd});
				UtilCollections.incrementIntValueInMap(termsFoundWithCurTokenStart, nextNode.prefix);
			}
			if (stopOnFirst)
				return;
		}
		if (nextNode.hasNonEndingChildren())
			checkIfTokenContinuationIsPresent(nextNode, foundOccur, tokenInd + 1, tokens);
	}


	protected void checkIfTokenContinuationIsPresentSimple(PrefixNode startingNode, 
			Map<String, Integer> foundOccur, int tokenInd, List<String> tokens) {
		String searchedToken = tokens.get(tokenInd);
		continueTokenIfPresent(searchedToken, startingNode, foundOccur, tokenInd, tokens);
		if (ignoreHahstagSymbols && searchedToken.startsWith("#"))
		{
			continueTokenIfPresent(searchedToken.replace("#",""), startingNode, foundOccur, tokenInd, tokens);
		}
		
		if (nounWords.contains(searchedToken)) {
			continueTokenIfPresent("<noun>", startingNode, foundOccur, tokenInd, tokens);
		}
		if (verbWords.contains(searchedToken)) {
			continueTokenIfPresent("<verb>", startingNode, foundOccur, tokenInd, tokens);
		}
		if (adjWords.contains(searchedToken)) {
			continueTokenIfPresent("<adj>", startingNode, foundOccur, tokenInd, tokens);
		}
		if (searchedToken.contains("<int_num>")) {
			continueTokenIfPresent("<num>", startingNode, foundOccur, tokenInd, tokens);
		}
	}
	
	
	@Override
	protected void checkIfTokenContinuationIsPresent(PrefixNode startingNode, 
			Map<String, Integer> foundOccur, int tokenInd, List<String> tokens)
	{
		if (tokenInd == tokens.size())
			return;
		
		if (savePositions && startingNode == rootPrefixNode) {
			termsFoundWithCurTokenStart = new HashMap<String, Integer>();
		}
		
		checkIfTokenContinuationIsPresentSimple(startingNode, foundOccur, tokenInd, tokens);
		
		if (startingNode != rootPrefixNode) {
			// not the first token of the pattern -> allow short skip between with up to skipAllowenceBetweenSubstringTokens;
			// but no separators can appear in between if treatSeparators is set to true.
		
			for (int skipInd = 0; skipInd < skipAllowenceBetweenSubstringTokens; ++skipInd) {
				if (tokenInd + skipInd + 1 == tokens.size()) break; // no next token
				if (treatSeparators && UtilText.isSentenceSeparator(tokens.get(tokenInd + skipInd))) {
					// the skipped token is separator
					break;
				}
				checkIfTokenContinuationIsPresentSimple(startingNode, foundOccur, tokenInd + skipInd + 1, tokens);
			}
		}
		
		if (savePositions && startingNode == rootPrefixNode) {
			// need to update the starting point for all the last occurrences of all found terms
			for (Map.Entry<String, Integer> foundTerm : termsFoundWithCurTokenStart.entrySet()) {
				List<int[]> foundTermPositions = lastPositions.get(foundTerm.getKey());
				// update several found last positions
				for (int posInd = foundTermPositions.size() - 1; posInd >= foundTermPositions.size() - foundTerm.getValue(); --posInd) {
					foundTermPositions.get(posInd)[0] = tokenInd;
				}
			}
			termsFoundWithCurTokenStart = null;
		}
	}

}
