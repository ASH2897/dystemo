/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utility.Pair;

public class JaccardSimilarity implements ITextSimilarity{

	@Override
	public double getMaxSimilarityValue() {
		return 1.0;
	}

	@Override
	public double getMinSimilarityValue() {
		return 0.0;
	}

	@Override
	public double getSimilarityScore(String text1, String text2) 
	{
		//TODO: get token set from texts!
		String[] tokens1 = text1.split(" ");
		String[] tokens2 = text2.split(" ");
		
		return getSimilarityScore(new HashSet<String>(Arrays.asList(tokens1)), new HashSet<String>(Arrays.asList(tokens2)));
	}
	
	
	@Override
	public <T extends Object> double getSimilarityScore(Set<T> set1, Set<T> set2)
	{
		if (set1 == null || set1.size() == 0 || set2 == null || set2.size() == 0)
			return 0.0;
		
		Set<T> union = new HashSet<T>(set1);
		union.addAll(set2);
		Set<T> intersect = new HashSet<T>(set1);
		intersect.retainAll(set2);
		return (1.0)*intersect.size() / union.size();
	}


	@Override
	public <T extends Object & Comparable<T>> double getSimilarityScore(List<T> list1, List<T> list2)
	{
		if (list1 == null || list1.size() == 0 || list2 == null || list2.size() == 0)
			return 0.0;
		
		int intersect = 0;
		int union;
		
		int ind1 = 0, ind2 = 0;
		while (ind1 < list1.size() && ind2 < list2.size())
		{
			int comp = list1.get(ind1).compareTo(list2.get(ind2));
			if (comp == 0)
			{
				ind1++;
				ind2++;
				intersect++;
			}
			else if (comp < 0)
				ind1++;
			else
				ind2++;
		}
		
		union = list1.size() + list2.size() - intersect;

		return (1.0)*intersect / union;
	}

	@Override
	/** For Jaccard similarity*/
	public <T extends Object & Comparable<T>> double getSimilarityScoreFromFeatures(
			List<Pair<T, Double>> list1, List<Pair<T, Double>> list2) {
		if (list1 == null || list1.size() == 0 || list2 == null || list2.size() == 0)
			return 0.0;
		
		double intersect = 0.0;
		double union = 0.0;
		
		int ind1 = 0, ind2 = 0;
		while (ind1 < list1.size() && ind2 < list2.size())
		{
			int comp = list1.get(ind1).first.compareTo(list2.get(ind2).first);
			if (comp == 0)
			{
				double score = Math.min(list1.get(ind1).second, list2.get(ind2).second);
				intersect += score;
				union += score;
				ind1++;
				ind2++;
			}
			else if (comp < 0)
			{
				union += list1.get(ind1).second;
				ind1++;
			}
			else
			{
				union += list2.get(ind2).second;
				ind2++;
			}
		}
		
		//finish the remaining of the list (only one would have ind < list.size()!)
		while (ind1 < list1.size())
		{
			union += list1.get(ind1).second;
			ind1++;
		}
		while (ind2 < list2.size())
		{
			union += list2.get(ind2).second;
			ind2++;
		}
		
		return intersect / union;
	}
}
