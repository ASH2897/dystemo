/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import functionality.UtilCollections;

public class SubstringExtraction {

	public static Map<String, Integer> extractAllSubstringsFromSentence(List<String> tokenizedSentence, int n)
	{
		Map<String, Integer> foundSubstrings = new HashMap<String, Integer>();
	
		List<List<String>> allTokenLists = new ArrayList<List<String>>();
		List<String> curTokensList = new ArrayList<String>(); // the tokens until the separator 
		
		for (String token : tokenizedSentence)
		{
			if (token.trim().length() == 0)
				continue;
			if (!NgramExtraction.separators.contains(token))
			{
				UtilCollections.incrementIntValueInMap(foundSubstrings, token);
				curTokensList.add(token);
			}
			else
			{
				allTokenLists.add(new ArrayList<String>(curTokensList));
				curTokensList = new ArrayList<String>();
			}
		}
		
		
		if (curTokensList.size() > 0)
			allTokenLists.add(new ArrayList<String>(curTokensList));
		
		for (List<String> subTokenList : allTokenLists)
		{
			List<List<Integer>> fullList = new ArrayList<List<Integer>>();
			List<List<Integer>> curList = new ArrayList<List<Integer>>();
			// initialize by adding unigrams
			for (int ind = 0; ind < subTokenList.size(); ++ind )
			{
				List<Integer> curTokenInd = new ArrayList<Integer>();
				curTokenInd.add(ind);
				curList.add(curTokenInd);
			}
			// fullList.addAll(curList); // no need as unigrams are already added
			
			for (int nlength = 2; nlength <= n; ++nlength)
			{
				curList = extractNewSubstringIndexesFromSubSentence(subTokenList.size() - 1, curList);
				fullList.addAll(curList);
			}
			// change from list<Integer> -> String ;   AND add found substrings into the output
			for (List<Integer> foundSubstringArr : fullList)
			{
				String foundSubstring = getSubstringByElemIndexes (subTokenList, foundSubstringArr);
				UtilCollections.incrementIntValueInMap(foundSubstrings, foundSubstring);
			}
		}
		
		return foundSubstrings;
	}
	
	public static List<List<Integer>> extractNewSubstringIndexesFromSubSentence(int maxInd, List<List<Integer>> currentSubstrings)
	{ 
		List<List<Integer>> newSubstrings = new ArrayList<List<Integer>>();
		for (List<Integer> curSubstr : currentSubstrings )
		{
			int lastInd = curSubstr.get(curSubstr.size() - 1);
			for (int newInd = lastInd + 1; newInd <= maxInd; ++newInd)
			{
				List<Integer> newSubstr = new ArrayList<Integer>(curSubstr);
				newSubstr.add(newInd);
				newSubstrings.add(newSubstr);
			}
		}
		return newSubstrings;
	}
	
	public static String getSubstringByElemIndexes (List<String> tokens, List<Integer> indexes)
	{
		StringBuilder sb = new StringBuilder();
		for (Integer ind : indexes)
		{
			sb.append(tokens.get(ind));
			sb.append(" ");
		}
		String finalSubstring = sb.toString();
		return finalSubstring.substring(0, finalSubstring.length() - 1);
	}
			
}
