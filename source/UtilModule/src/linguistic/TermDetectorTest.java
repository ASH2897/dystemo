/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import functionality.UtilCollections;

public class TermDetectorTest {
	public static void testAll() {
		TermDetectorTest tester = new TermDetectorTest();
		tester.testStandardCases();
		tester.testDeletion();
		tester.testCasesWithPatterns();
		System.out.println("Test done for TermDetector");
	}
	
	protected void testStandardCases() {
		String[] termList = {"happy", "end", "end final"};
		String[] textInput = {"happy that i 'm here", "could n't be happyer", "will be happy", "haha happy today", "u , end", "end final x"};
		String[][] textOutput = {{"happy"}, null, {"happy"}, {"happy"}, {"end"}, {"end", "end final"}};
		TermDetector tDetector = buildTermDetector(termList);
		testOnOneSet(tDetector, textInput, textOutput);
		
		termList = new String[]{"not_happy"};
		textInput = new String[]{"not happy today", "very happy"};
		textOutput = new String[][]{{"happy"}, {"happy"}}; //{{"not_happy"}, null}
		tDetector = buildTermDetector(termList);
		testOnOneSet(tDetector, textInput, textOutput);
	}
	
	protected TermDetector buildTermDetector(String[] termList) {
		List<String> termsToDetect = new ArrayList<String>();
		termsToDetect.addAll(UtilCollections.transformArrayToList(termList));
		
		TermDetector tDetector = new TermDetector(termsToDetect);
		return tDetector;
	}
	
	protected void testOnOneSet(TermDetector tDetector, String[] textInput, String[][] termListOutput) {
		List<Map<String,Integer>> termsOutput = new ArrayList<Map<String,Integer>>();
		for (int i = 0; i < textInput.length; ++i) {
			HashMap<String,Integer> curTermOutput = new HashMap<String,Integer>();
			if (termListOutput[i] != null)
				for (int j = 0; j < termListOutput[i].length; ++j) 
					UtilCollections.incrementIntValueInMap(curTermOutput, termListOutput[i][j]);
			termsOutput.add(curTermOutput);
		}
		
		for (int textInd = 0; textInd < textInput.length; ++textInd) {
			Map<String, Integer> foundTerms = tDetector.findTermsInText(textInput[textInd]);
			Map<String, Integer> expectedTerms = termsOutput.get(textInd);
			
			// check that found and expected maps are the same!
			boolean mapsAreSame = true;
			for (Map.Entry<String, Integer> foundEntry : foundTerms.entrySet()) {
				if (!expectedTerms.containsKey(foundEntry.getKey()) ||
				 !(expectedTerms.get(foundEntry.getKey()) == foundEntry.getValue())) {
					mapsAreSame = false;
					break;
				}
			}
			if (mapsAreSame)
			for (Map.Entry<String, Integer> foundEntry : expectedTerms.entrySet()) {
				if (!foundTerms.containsKey(foundEntry.getKey()) ||
				 !(foundTerms.get(foundEntry.getKey()) == foundEntry.getValue())) {
					mapsAreSame = false;
					break;
				}
			}
			if (!mapsAreSame) {
				System.out.println("Test not passed: \n Input - " + textInput[textInd] + 
						"\n FoundTerms - " + UtilCollections.joinMap(foundTerms, ";", " : ") + 
						"\n ExpectedTerms - " +  UtilCollections.joinMap(expectedTerms, ";", " : "));
			}
		}
	}
	
	
	protected void testDeletion() {
		String[] termList = {"happy", "end"};
		String[] textInput = {"happy that i 'm here", "could n't be happyer", "will be happy", "haha happy today", "u , end", "end final x"};
		String[][] textOutput = {{"happy"}, null, {"happy"}, {"happy"}, null, null};
		TermDetector tDetector = buildTermDetector(termList);
		tDetector.deleteTerm("end");
		testOnOneSet(tDetector, textInput, textOutput);
		
		termList = new String[]{"happy", "end", "end final"};
		textInput = new String[]{"happy that i 'm here", "could n't be happyer", "will be happy", "haha happy today", "u , end", "end final x"};
		textOutput = new String[][]{{"happy"}, null, {"happy"}, {"happy"}, null, {"end final"}};
		tDetector = buildTermDetector(termList);
		tDetector.deleteTerm("end");
		testOnOneSet(tDetector, textInput, textOutput);
	}

	protected void testCasesWithPatterns() {
		String[] termList = {"happ*", "end"};
		String[] textInput = {"happy that i 'm here", "could n't be happyer", "will be happy", "haha happy today", "u , end", "happy end"};
		String[][] textOutput = {{"happ*"}, {"happ*"}, {"happ*"}, {"happ*"}, {"end"}, {"happ*", "end"}};
		TermDetector tDetector = buildTermDetector(termList);
		tDetector.usePatterns = true;
		testOnOneSet(tDetector, textInput, textOutput);
	}
}
