/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.ArrayList;
import java.util.List;

import functionality.UtilCollections;

public class SubstringDetectorTest extends TermDetectorTest {
	public static void testAll() {
		SubstringDetectorTest tester = new SubstringDetectorTest();
		tester.testStandardCases();
		System.out.println("Test done for SubstringDetector");
	}
	
	@Override
	protected void testStandardCases() {
		String[] termList = {"happy", "happy days", "have days", "have <noun>", "have to <verb>", "<adj> days", "my <noun>"};
		String[] textInput = {
				"happy that i 'm here", "nothing", "many days ago", 
				"we will have our happy days", "great to be happy", "happy last days",
				"have . days", "so happy <emot24> days are going well", "have to go now", "my dear friend"};
		String[][] textOutput = {
				{"happy"}, null, {"<adj> days"},
				{"happy", "happy days", "have days", "have <noun>", "<adj> days"}, {"happy"}, {"happy", "happy days", "<adj> days", "<adj> days"},
				null, {"happy"}, {"have to <verb>"}, {"my <noun>"}};
		TermDetector tDetector = this.buildTermDetector(termList);
		testOnOneSet(tDetector, textInput, textOutput);
	}
	
	@Override
	protected TermDetector buildTermDetector(String[] termList) {
		List<String> termsToDetect = new ArrayList<String>();
		termsToDetect.addAll(UtilCollections.transformArrayToList(termList));
		
		SubstringDetector tDetector = new SubstringDetector(termsToDetect);
		tDetector.skipAllowenceBetweenSubstringTokens = 2;
		tDetector.treatSeparators = true;
		tDetector.initializeLists(UtilCollections.transformArrayToSet(new String[]{"day", "days", "friend"}),
								  UtilCollections.transformArrayToSet(new String[]{"go", "have", "going", "'m"}), 
								  UtilCollections.transformArrayToSet(new String[]{"happy", "many", "last", "dear"}));
		return tDetector;
	}
}
