/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import linguistic.UtilText;
import functionality.UtilCollections;

public class TermDetector implements Serializable {

	private static final long serialVersionUID = -5013063535665933153L;
	protected PrefixNode rootPrefixNode;
	public Set<String> terms;
	
	// PARAMETERS
	/**
	 * Whether to save the information on the indices of the found terms. 
	 * If true, saveTokenList should also be set true.
	 */
	public boolean savePositions = true;
	
	/**
	 * Whether to save the processed list of tokens.
	 * If true, saveTokenList should also be set true.
	 */
	public boolean saveTokenList = true;
	
	/**
	 * Whether to consider terms ending on * as patterns: if true, any continuation of such term will be considered as a match. 
	 * (In this case, the terms can be represented in stemmed form, such as happ* )
	 */
	public boolean usePatterns = false; 
	
	/**
	 * If true, the terms to find and the tokens of the texts will be stemmed. 
	 */
	public boolean useStemmedForms = false;
	
	/**
	 * if true, the terms will be mapped even if a hashtag symbol (#) is in the text before it; 
	 * The hashtagged words in the list of terms to detect will be mapped to hashtagged word occurrences in any case (whether this parameter is set to true or false).
	 */
	public boolean ignoreHahstagSymbols = false; 
	
	/**
	 * If true, the detector will stop term detection after one term is found. 
	 */
	public boolean stopOnFirst = false; 
	
	
	// CONSTRUCTORS
	public TermDetector()
	{
		terms = new HashSet<String>();
		rootPrefixNode = null;
	}
	
	public TermDetector(Collection<String> terms)
	{
		initializeOnTerms(terms);
	}
	
	public TermDetector(Collection<String> terms, boolean useStemmedForms)
	{
		this.useStemmedForms = useStemmedForms;
		initializeOnTerms(terms);
	}
	

	public TermDetector(TermDetector tdetector) 
	{
		this.rootPrefixNode = tdetector.rootPrefixNode;
		this.terms = tdetector.terms;
		this.usePatterns = tdetector.usePatterns;
		this.savePositions = tdetector.savePositions;
		this.saveTokenList = tdetector.saveTokenList;
		
		this.useStemmedForms = tdetector.useStemmedForms;
		this.ignoreHahstagSymbols = tdetector.ignoreHahstagSymbols;
		this.stopOnFirst = tdetector.stopOnFirst;
	}
	
	// METHODS
	
	protected void createRootNode()
	{
		rootPrefixNode = new PrefixNode();
		rootPrefixNode.prefix = "";
		rootPrefixNode.depth = 0;
	}
	
	protected void initializeOnTerms(Collection<String> terms)
	{
		this.terms = new HashSet<String>(terms);
		createRootNode();
		boolean potentiallyUsePatterns = false; // We have to set use patterns manually in order to avoid errors in automatic detection whether the patterns should be applied or not.
		
		for (String term : terms)
		{
			String usedTerm = term;
			// in order to be able to detect "not_term" we need to include such terms in the list without negation. However, we can detect them only if the wrapper TermDetectorWithNegations is applied!
			if (term.startsWith("not_")) {
				usedTerm = term.substring(4);			
			}
			
			String[] tokens = usedTerm.split(" +");
			addNewTermInPrefixNode(rootPrefixNode, tokens, 0);
			
			if (potentiallyUsePatterns)
			{
				//detect if need to use patterns (*)
				if (usedTerm.endsWith("*")) //to adjust to GALC lexicon, which contains patterns (words ending with * that should be mapped as any continuation)
					this.usePatterns = true;
				else if (UtilText.containsNonWordCharacters(usedTerm)) // However, in the later preprocessing we also marked elongated words with a * symbol. Thus, if other non-alphanumeric symbols are detected, we consider that pattern-based matching should not be used.
					{
						this.usePatterns = false;
						potentiallyUsePatterns = false;
					}
			}
			if (useStemmedForms)
			{
				String stemmedTerm = stemText(usedTerm);
				if (!stemmedTerm.equals(usedTerm))
				{
					tokens = stemmedTerm.split(" +");
					addNewTermInPrefixNode(rootPrefixNode, tokens, 0);
					this.terms.add(stemmedTerm);
				}
			}
		}
	}
	
	class PrefixNode implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2543395724291151970L;
		String prefix;
		HashMap<String, PrefixNode> continuations; // maps the next token to the next prefix node
		int depth;
		
		public String getPrefix()
		{
			return prefix;
		}
		
		/**
		 * Whether the current node can be the end of the ngram/pattern (it can still be a continuation for other ngrams/patterns)
		 * @return
		 */
		public boolean isEnding()
		{
			return continuations == null || continuations.containsKey(null);
		}
		
		public boolean hasNonEndingChildren()
		{
			return continuations != null && 
					(continuations.size() - (continuations.containsKey(null) ? 1 : 0)) > 0;
		}
		
		public PrefixNode addChild(String nextToken)
		{
			PrefixNode newChild;
			if (continuations != null && continuations.containsKey(nextToken))
			{
				newChild = continuations.get(nextToken);
			}
			else
			{
				newChild = new PrefixNode();
				if (nextToken != null)
				{
					newChild.prefix = prefix + (prefix.length()>0?" ":"") + nextToken;
					newChild.depth = depth + 1;
				}
				if (continuations == null)
					continuations = new HashMap<String, PrefixNode>();
				continuations.put(nextToken, newChild);
			}
			return newChild;
		}
	}
	
	protected void addNewTermInPrefixNode(PrefixNode currentNode, String[] tokens, int curTokenInd)
	{
		if (curTokenInd == tokens.length)
		{
			// add an empty child
			currentNode.addChild(null);
		}
		else
		{
			PrefixNode newChild = currentNode.addChild(tokens[curTokenInd]);
			addNewTermInPrefixNode(newChild, tokens, curTokenInd + 1);
		}
					
	}
	
	
	/**
	 * Find the PrefixNode corresponding to the given ngram (given by tokens)
	 * @param currentNode
	 * @param tokens
	 * @param curTokenInd
	 * @return
	 */
	protected PrefixNode findMatchedPrefixNode(PrefixNode currentNode, String[] tokens, int curTokenInd) {
		if (curTokenInd == tokens.length)
		{
			return currentNode;
		}
		else
		{
			currentNode = currentNode.continuations.get(tokens[curTokenInd]);
			if (currentNode == null)
				return null;
			else return findMatchedPrefixNode(currentNode, tokens, curTokenInd + 1);
		}
	}
	

	public static String findPatternForTerm(Collection<String> set, String term)
	{
		for (String word : set)
		{
			if (word == null) {
				continue;
			}
			Matcher m = Pattern.compile(word.replace("*", ".*")).matcher(term);
			if (m.matches())
			{
				return word;
			}
		}
		return null;
	}
	 
	/** finds terms in the text (should be processed in the same form as terms) and returns found terms with number of occurrences */
	public Map<String, Integer> findTermsInText(String text)
	{
		List<String> tokens = UtilText.getAllTokens(text);
		return findTermsForTokens(tokens);
	}
	
	/** finds terms in the text (should be processed in the same form as terms) and returns found terms with number of occurrences */
	public Map<String, Integer> findTermsForTokens(List<String> tokens)
	{
		Map<String, Integer> foundOccur = new HashMap<String, Integer>();
		
		if (savePositions)
			lastPositions = new HashMap<String, List<int[]>>();
			
		for (int tokenInd = 0; tokenInd < tokens.size(); ++tokenInd)
		{
			if (stopOnFirst && foundOccur.size() > 0)
				break;
				
			checkIfTokenIsPresent(foundOccur, tokenInd, tokens);
		}
		
		if (saveTokenList)
			lastTokenStream = tokens;
		
		return foundOccur;
	}
	
	private void checkIfTokenIsPresent(Map<String, Integer> foundOccur, int tokenInd, List<String> tokens)
	{
		checkIfTokenContinuationIsPresent(rootPrefixNode, foundOccur, tokenInd, tokens);
	}
	
	protected void checkIfTokenContinuationIsPresent(PrefixNode startingNode, Map<String, Integer> foundOccur, int tokenInd, List<String> tokens)
	{
		if (tokenInd == tokens.size())
			return;
		String searchedToken = tokens.get(tokenInd);
		continueTokenIfPresent(searchedToken, startingNode, foundOccur, tokenInd, tokens);
		if (ignoreHahstagSymbols && searchedToken.startsWith("#"))
		{
			continueTokenIfPresent(searchedToken.replace("#",""), startingNode, foundOccur, tokenInd, tokens);
		}
	}
	
	protected void continueTokenIfPresent(String curToken, PrefixNode startingNode, Map<String, Integer> foundOccur, int tokenInd, List<String> tokens)
	{
		// get the node child of the startingNode whose continuation label corresponds to the curToken
		PrefixNode nextNode = findNextPrefixNode(startingNode, curToken);
		if (nextNode == null)
			return; // nothing was found
		if (nextNode.isEnding())
		{
			// need to add a current found token in the found list
			//unigram found
			UtilCollections.incrementIntValueInMap(foundOccur, nextNode.prefix);
			if (savePositions)
				UtilCollections.appendNewObjectToListInMap(lastPositions, nextNode.prefix, new int[]{tokenInd - nextNode.depth + 1, tokenInd});
			if (stopOnFirst)
				return;
		}
		if (nextNode.hasNonEndingChildren())
			checkIfTokenContinuationIsPresent(nextNode, foundOccur, tokenInd + 1, tokens);
	}
	
	
	/** finds terms in the text and returns found terms with number of occurrences */
	public static Map<String, Integer> findTermsInText(String text, List<String> possibleTerms)
	{
		TermDetector tDet = new TermDetector(possibleTerms);
		return tDet.findTermsInText(text);
	}
	
	/**
	 * This will return the positions of the last found terms: for each term (key) - the list of its occurrences positions.
	 * Each position = int[]; where pos[0] - start index and pos[1] - end index in the token list.
	 * This will return not null, only if savePositions = true
	 * @return
	 */
	public Map<String, List<int[]>> getLastPositionsOfFoundTerms()
	{
		return lastPositions;
	}
	
	protected List<String> lastTokenStream;
	protected Map<String, List<int[]>> lastPositions;
	
	
	public List<String> getLastTokenStream()
	{
		return lastTokenStream;
	}
	
	public boolean containsTerm (String term)
	{
		if (usePatterns)
		{
			String foundPattern = findPatternForTerm(terms, term);
			if (foundPattern == null)
				return false;
			else
				return true;
		}
		else
			return terms.contains(term);
	}

	
	protected PrefixNode findNextPrefixNode(PrefixNode curNode, String nextSearchedToken)
	{
		if (!curNode.hasNonEndingChildren())
			return null;
		
		if (usePatterns)
			nextSearchedToken = findPatternForTerm(curNode.continuations.keySet(), nextSearchedToken);
		
		if (nextSearchedToken == null)
			return null;
		
		String foundToken = nextSearchedToken;
		boolean found = curNode.continuations.containsKey(nextSearchedToken);
		if (useStemmedForms && !found)
		{
			foundToken = stemTerm(nextSearchedToken);
			if (!foundToken.equals(nextSearchedToken))
				found = curNode.continuations.containsKey(foundToken);
		}
		if (found)
			return curNode.continuations.get(foundToken);
		else
			return null;
	}
	
	
	
	/** refers to Snowball stemmer which is probably too strict? */
	private static String stemTerm(String term)
	{
		return UtilText.stemTerm(term);
	}
	
	/** Stems all the tokens in the text 
	 * refers to Snowball stemmer which is probably too strict? */
	private static String stemText(String text)
	{
		String[] tokens = text.split(" +");
		if (tokens.length == 1)
			return stemTerm(text);
		else
		{
			StringBuilder sb = new StringBuilder();
			for (String token : tokens)
			{
				if (sb.length() > 0)
					sb.append(" ");
				sb.append(stemTerm(token));
			}
			return sb.toString();
		}
	}
	
	/** simplest stemming version with main possible endings (taken from Muse project) */
	private static String stemTerm2(String term)
	{
		String newTerm = term.toLowerCase();
		String fullTerm = newTerm;
		
		// strip most common suffixes
		if (newTerm.endsWith("s"))
			newTerm = newTerm.substring(0, newTerm.length()-"s".length());
		else if (newTerm.endsWith("ed"))
			newTerm = newTerm.substring(0, newTerm.length()-"ed".length());
		else if (newTerm.endsWith("ing"))
			newTerm = newTerm.substring(0, newTerm.length()-"ing".length());
		else if (newTerm.endsWith("ly"))
			newTerm = newTerm.substring(0, newTerm.length()-"ly".length());
		else if (newTerm.endsWith("ment"))
			newTerm = newTerm.substring(0, newTerm.length()-"ment".length());
		
	
		// could do other suffixes: -ness, -ation, -atious, etc.
		if (newTerm.length() >= 2)
		{
			return newTerm;		
		}
		else
			return fullTerm; // < 2 chars, play it safe and return the original term without modification
	}
	
	public void deleteTerm(String termToRemove) {
		if (!terms.contains(termToRemove))
			return;
		
		terms.remove(termToRemove);
		
		String[] tokens = termToRemove.trim().split(" +");
		PrefixNode nodeToNullify = findMatchedPrefixNode(rootPrefixNode, tokens, 0);
		
		if (nodeToNullify != null) {
			// mark it as non-ending, but continuations (if present) should work anyway.
			if (nodeToNullify.continuations != null) {
				nodeToNullify.continuations.remove(null);
			} else {
				nodeToNullify.continuations = new HashMap<String, PrefixNode>();
			}
		} 
		else
		{
			System.err.println("The term to delete from the list '" + termToRemove + "' is not found in the detector.");
		}
	}
	
	public void deleteTerms(Collection<String> termsToRemove)
	{
		for (String termToRemove : termsToRemove)
			this.deleteTerm(termToRemove);
	}

	/** adds only those that were not yet present */
	public void addTerms(Collection<String> termsToAdd) 
	{
		
		for (String term : termsToAdd)
		{
			this.addTerm(term);
		}
	}
	
	/** adds only if was NOT yet present */
	public void addTerm(String term) 
	{
		
		
			if (terms.contains(term))
				return;
			
			String[] tokens = term.trim().split(" +");
			addNewTermInPrefixNode(rootPrefixNode, tokens, 0);
		
		
		terms.add(term);
	}
	
	
	public Collection<String> getTerms()
	{
		return terms;
	}
}
