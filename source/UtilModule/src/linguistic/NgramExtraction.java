/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;

import functionality.UtilCollections;

public class NgramExtraction {
	
	public static Set<String> separators;
	
	static
	{
		separators = new HashSet<String>();
		String[] separatorsArray = {".", ",", ";", ":", "(", ")", "{", "}", "-lrb-", "-rrb-", "!", "?", "-", "``", "''", "...", "`", "\n", "!*", "?*", "...*", "\\n", "!?", "?!"};
		separators.addAll(Arrays.asList(separatorsArray));
	}
	
	public static Map<String, Integer> extractAllNGramsFromSentence(List<String> tokenizedSentence, int maxN, TermDetectionParameters termDetectionParameters)
	{
		return extractAllNGramsFromSentence(tokenizedSentence, 1, maxN, termDetectionParameters);
	}
	
	public static Map<String, Integer> extractAllNGramsFromSentence(List<String> tokenizedSentence, int minN, int maxN, TermDetectionParameters termDetectionParameters)
	{
		if (!termDetectionParameters.toTreatNegations())
			return extractAllSimpleNGramsFromSentence(tokenizedSentence, minN, maxN,  termDetectionParameters.ignoreHashtags);
		else
			return extractAllNGramsFromSentenceWithNegations(tokenizedSentence, minN, maxN, termDetectionParameters.treatNegationsParams, termDetectionParameters.ignoreHashtags);
	}
	
	static boolean savePositions = false;
	static protected Map<String, List<int[]>> lastPositions;
	
	private static void addFoundNgramsInMaps(Map<String, Integer> foundNgrams, String term, int startPos, int endPos)
	{
		UtilCollections.incrementIntValueInMap(foundNgrams, term);
		if (savePositions)
		{
			UtilCollections.appendNewObjectToListInMap(lastPositions, term, new int[]{startPos, endPos});
		}
	}
	
	/**
	 * 
	 * @param tokenizedSentence
	 * @param minN
	 * @param maxN
	 * @param repeatHashtags if true: the unigrams with the hashtags will be added both with the hashtags and without
	 * @return
	 */
	public static Map<String, Integer> extractAllSimpleNGramsFromSentence(List<String> tokenizedSentence, int minN, int maxN, boolean repeatHashtags)
	{
		Map<String, Integer> foundNgrams = new HashMap<String, Integer>();
		
		Map<String, Integer> prevNGrams = new HashMap<String, Integer>(); //stores all current previously found prefixes if they can be prolonged, maps to length
		
		if (savePositions)
			lastPositions = new HashMap<String, List<int[]>>();
		
		
		for (int tokenInd = 0; tokenInd < tokenizedSentence.size(); ++tokenInd)
		{
			String token = tokenizedSentence.get(tokenInd);
				
			if (token.trim().length() == 0)
				continue;
			Map<String, Integer> curNGrams = new HashMap<String, Integer>(); //stores all current found prefixes if they can be prolonged, maps to length
			if (!separators.contains(token))
			{
				if (minN  <= 1)
				{
					addFoundNgramsInMaps(foundNgrams, token, tokenInd, tokenInd);
					if (repeatHashtags && token.startsWith("#") && token.length() > 1)
						addFoundNgramsInMaps(foundNgrams, token.substring(1), tokenInd, tokenInd);
				}
				if (maxN > 1)
				{
					curNGrams.put(token, 1);
					if (repeatHashtags && token.startsWith("#") && token.length() > 1)
						curNGrams.put(token.substring(1), 1);
				}
			}
			// prolong the previous ngrams
			for (Map.Entry<String, Integer> prevNGram : prevNGrams.entrySet())
			{
				String newNGram = prevNGram.getKey() + " " + token;
				if (prevNGram.getValue() + 1 >= minN) // put only those ngrams that are long enough	
					addFoundNgramsInMaps(foundNgrams, newNGram, tokenInd - prevNGram.getValue(), tokenInd);
				if (prevNGram.getValue() + 1 < maxN)
				{
					 curNGrams.put(newNGram, prevNGram.getValue() + 1);
				}
				if (repeatHashtags && token.startsWith("#") && token.length() > 1)
				{
					newNGram = prevNGram.getKey() + " " + token.substring(1);
					if (prevNGram.getValue() + 1 >= minN) // put only those ngrams that are long enough	
						addFoundNgramsInMaps(foundNgrams, newNGram, tokenInd - prevNGram.getValue(), tokenInd);
					if (prevNGram.getValue() + 1 < maxN)
					{
						 curNGrams.put(newNGram, prevNGram.getValue() + 1);
					}
				}
				
			}
			if (!separators.contains(token))
				prevNGrams = curNGrams;
			else
				prevNGrams = new HashMap<String, Integer>();
		}
		return foundNgrams;
	}
	
	
	
	public static Map<String, Integer> extractAllNGrams(List<List<String>> tokenizedSentences, int n, TermDetectionParameters termDetectionParameters)
	{
		//TODO: are stop words deleted before or after?
		//TODO: drop all punctuations?
		Map<String, Integer> foundNgrams = new HashMap<String, Integer>();
		for (List<String> sentence : tokenizedSentences)
		{
			Map<String, Integer> sentenceNgrams = extractAllNGramsFromSentence(sentence, n, termDetectionParameters);
			
			for (String ngram : sentenceNgrams.keySet())
			{
				UtilCollections.incrementIntValueInMap(foundNgrams, ngram, sentenceNgrams.get(ngram));
			}
		}
		
		return foundNgrams;
	}
	
	
	/** 
	 * This method will return all the ngrams, while treating the negations present in the specified way
	 * @param tokenizedSentence
	 * @param n
	 * @return
	 */
	public static Map<String, Integer> extractAllNGramsFromSentenceWithNegations(List<String> tokenizedSentence, int minN,
								int maxN, NegationTreatmentParams negationTreatment, boolean repeatHashtags)
	{
		savePositions = true;
		Map<String, Integer> foundTerms = extractAllSimpleNGramsFromSentence(tokenizedSentence,minN, maxN, repeatHashtags);
		foundTerms = NegationDetector.treatNegationsInFoundTerms(negationTreatment, foundTerms, tokenizedSentence, lastPositions);
		lastPositions = null;
		return foundTerms;
	}

}
