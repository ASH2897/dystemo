/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package statistics;

import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import utility.Pair;

public class UtilData {
	
	public static NormalDistribution getNormalDistributionEval(List<Double> values)
	{
		double mean = UtilAggregation.average(values);
		double stdDev = UtilAggregation.stdDev(values);
		return new NormalDistribution(mean, stdDev);
	}
	
	public static double computeThresholdValue(List<Double> values, double probabilityTail, boolean useLowerTail)
	{
		NormalDistribution nDistr = getNormalDistributionEval(values);
		//System.out.println("mean of nDist: " + nDistr.getMean());
		//System.out.println("stdDev of nDist: " + nDistr.getStandardDeviation());
		if (!useLowerTail)
			probabilityTail = 1 - probabilityTail;
		return nDistr.inverseCumulativeProbability(probabilityTail);
	}
	
	/** Computes the parameters of the simple linear regression and outputs slope and intersection value*/
	public static Pair<Double,Double> makeRegression(List<Pair<Double,Double>> values)
	{
		SimpleRegression regression = new SimpleRegression();
		for (Pair<Double,Double> valuePair : values)
		{
			regression.addData(valuePair.first, valuePair.second);
		}
		
		return new Pair<Double,Double>(regression.getSlope(), regression.getIntercept());
	}
}
