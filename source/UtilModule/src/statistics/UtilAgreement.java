/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package statistics;

import java.util.ArrayList;
import java.util.List;

import functionality.UtilArrays;

public class UtilAgreement {
	public static double getOneEntryAgreement(List<Integer> categoryLabels)
	{
		int numberOfAgreedPairs = 0;
		int size = categoryLabels.size();
		for (int ind1 = 0; ind1 < size - 1; ++ind1)
		{
			for (int ind2 = ind1 + 1; ind2 < size; ++ind2)
			{
				if (categoryLabels.get(ind1) == categoryLabels.get(ind2))
					numberOfAgreedPairs++;
			}
		}
		return (1.0)*numberOfAgreedPairs * 2 / size / (size - 1);
	}
	
	public static double getOneEntryAgreement(int[] categoriesStat)
	{
		List<Integer> categoryLabels = new ArrayList<Integer>();
		for (int i = 0; i < categoriesStat.length; ++i)
		{
			for (int j = 0; j < categoriesStat[i]; ++j)
				categoryLabels.add(i);
		}
		return getOneEntryAgreement(categoryLabels);
	}
	
	public static double getOneEntryEntropy(int[] categoriesStat)
	{
		double result = 0;
		int sum = UtilArrays.getSum(categoriesStat);
		for (int i = 0; i < categoriesStat.length; ++i)
		{
			int x = categoriesStat[i];
			if (x > 0)
			{
				double probX = (1.0) * x / sum;
				result -= probX * Math.log(probX);
			}
		}
		return result;
	}
	
	
}
