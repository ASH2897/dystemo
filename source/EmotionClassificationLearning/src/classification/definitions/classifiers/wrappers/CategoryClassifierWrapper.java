/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers.wrappers;

import java.util.List;

import linguistic.TermDetectionParameters;

import classification.definitions.BinaryClassifier;
import classification.definitions.WeightedClassifier;
import classification.detectors.TermDetectorWithNegations;

/**
 * Non-full fledged class.
 * Will be used only to return the results of category training during semi-supervised learning
 *
 */
public class CategoryClassifierWrapper extends WeightedClassifier {

	BinaryClassifier internalBinaryClassifier;
	int categoryId;
	
	public CategoryClassifierWrapper(BinaryClassifier categoryClassifier, int categoryId)
	{
		this.internalBinaryClassifier = categoryClassifier;
		this.categoryId = categoryId;
	}
	
	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] findCategoryWeights(String text) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printToTextFile(String filename) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getFeatureNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFeatureNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public WeightedClassifier clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		internalBinaryClassifier.clear();
	}
	
	
	public BinaryClassifier getInternalBinaryClassifier()
	{
		return internalBinaryClassifier;
	}

	@Override
	public void setupTermDetectionParameters(
			TermDetectionParameters termDetectionParameters) {
		// TODO Auto-generated method stub
		internalBinaryClassifier.setupTermDetectionParameters(termDetectionParameters);
	}

	

}
