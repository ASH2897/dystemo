/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers.wrappers;

import java.util.Arrays;
import java.util.List;

import linguistic.TermDetectionParameters;

import classification.definitions.BinaryClassifier;
import classification.definitions.WeightedClassifier;
import data.documents.Tweet;


public class WeightedClassifierHierachicalWrapper extends WeightedClassifier {

	WeightedClassifier categoryClassifier;
	BinaryClassifier firstLevelClassifier;
	
	public WeightedClassifierHierachicalWrapper (WeightedClassifier categoryClassifier,
							BinaryClassifier firstLevelClassifier)
	{
		this.categoryClassifier = categoryClassifier;
		this.firstLevelClassifier = firstLevelClassifier;
	}
	
	@Override
	public double[] findCategoryWeights(String text) throws Exception
	{
		return findCategoryWeights(text, defaultPreprocessing);
	}
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText) throws Exception
	{
		int firstLevelClass = firstLevelClassifier.findClassIndex(text, toPreprocessText);
		
		double[] emWeights = null;
		
		// we assume that the emotional class is 1, neutral = -1
		if (firstLevelClass == 1)
		{
			emWeights = categoryClassifier.findCategoryWeights(text, toPreprocessText);
		}
		else
		{
			emWeights = new double[categoryClassifier.getCategoriesData().getCategoryNum()];
			Arrays.fill(emWeights, 0.0);
			emWeights[categoryClassifier.getCategoriesData().getCategoryIndexById(
					categoryClassifier.getCategoriesData().neutralCategory())] = 1.0;
		}
		return emWeights;
	}

	@Override
	public void printToTextFile(String filename) {
		// TODO Auto-generated method stub
		// not implemented!
	}

	@Override
	public List<String> getFeatureNames() {
		// returns features for the internal classifier
		return categoryClassifier.getFeatureNames();
	}

	@Override
	public int getFeatureNumber() {
		// returns number of features for the internal classifier
		return categoryClassifier.getFeatureNumber();
	}

	@Override
	public WeightedClassifier clone() {
		return new WeightedClassifierHierachicalWrapper(categoryClassifier, firstLevelClassifier);
	}

	@Override
	public double[] findCategoryWeights(Tweet tweet, boolean preprocessText)
			throws Exception 
	{
		int firstLevelClass = firstLevelClassifier.findClassIndex(tweet, preprocessText);
			
		double[] emWeights = null;
			
		// we assume that the emotional class is 1, neutral = -1
		if (firstLevelClass == 1)
		{
			emWeights = categoryClassifier.findCategoryWeights(tweet, preprocessText);
		}
		else
		{
			emWeights = new double[categoryClassifier.getCategoriesData().getCategoryNum()];
			Arrays.fill(emWeights, 0.0);
			emWeights[categoryClassifier.getCategoriesData().getCategoryIndexById(
					categoryClassifier.getCategoriesData().neutralCategory())] = 1.0;
		}
		return emWeights;
	}

	@Override
	public double[] findCategoryWeights(Tweet tweet) throws Exception {
		return findCategoryWeights(tweet.text);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setupTermDetectionParameters(
			TermDetectionParameters termDetectionParameters) {
		categoryClassifier.setupTermDetectionParameters(termDetectionParameters);
		firstLevelClassifier.setupTermDetectionParameters(termDetectionParameters);
	}

	
}
