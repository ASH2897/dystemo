/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.extraclassifiers;

import java.util.HashMap;
import java.util.Map;

import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.WeightedClassifierLexiconBasedIndependent;
import classification.definitions.lexicons.WeightedEmotionLexicon;

import functionality.UtilArrays;

public class TwoLayerIndependentWeightedClassifier extends  WeightedClassifierLexiconBasedIndependent{

	WeightedClassifierLexiconBased internalClassification;
	
	/** The terms of external lexicon have to be refer to the categories, quadrants or polarity in specific format (look example)*/
	public TwoLayerIndependentWeightedClassifier(
			WeightedClassifierLexiconBased internalClassification, WeightedEmotionLexicon externalLexicon) {
		super(externalLexicon);
		this.internalClassification = internalClassification;
	}
	
	

	@Override
	/** Returns the sum of the weights based on external terms, by summing up the weights of internal lexicon per category and mapping it to the value in external */
	public double[] getTermSumCategoryWeights(String text) throws Exception
	{
		double[] categoryWeights = new double[lexicon.getCategoriesToUseNum()];
		// here lexicon = externalLexicon
		
		// find terms of external lexicon
		Map<String, Double> foundTerms = getUpLayerAttributeValuesFromWeights(internalClassification, text);
		// sum them up
		for (String term : foundTerms.keySet())
		{
			if (lexicon.allTerms().contains(term)) // to check if all of the default external terms were included in the actual lexicon 
				categoryWeights = UtilArrays.sumTwoArrays(categoryWeights, 
													UtilArrays.multiplyArray(lexicon.getWeightForTerm(term), foundTerms.get(term)));
		}
		return categoryWeights;
	}
	
	public static Map<String, Double> getUpLayerAttributeValuesFromWeights(WeightedClassifierLexiconBased emClassification, String text) throws Exception
	{
		double[] emCatWeights = emClassification.findCategoryWeights(text);
		Map<String, Double> attributeValues = new HashMap<String, Double>();
		for (int emCatId = 0; emCatId < emCatWeights.length; ++emCatId)
		{
			attributeValues.put("emCat"+(emCatId+1), emCatWeights[emCatId]);
		}
		Map<Integer, Double> emQuadWeights = emClassification.getQuadrantWeights(emCatWeights);
		Map<Integer, Double> emPolarityWeights = emClassification.getPolarityWeights(emCatWeights);
		
		for (int quad = 1; quad < 5; ++quad)
		{
			attributeValues.put("emQuad"+quad, emQuadWeights.get(quad));
		}
		attributeValues.put("emPositive", emPolarityWeights.get(1));
		attributeValues.put("emNegative", emPolarityWeights.get(-1));
		return attributeValues;
	}
	

	
}
