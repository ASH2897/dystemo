/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.extraclassifiers;

import java.util.Date;
import java.util.List;
import java.util.Random;

import linguistic.TermDetectionParameters;
import classification.definitions.WeightedClassifier;

public class RandomClassifier extends WeightedClassifier {

	double[] prior;
	static Random rand = new Random(new Date().getTime());
	
	
	public RandomClassifier (double[] prior)
	{
		this.prior = prior;
	}
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText)
			throws Exception {
		double[] weights = new double[getCategoriesData().getCategoryNum()];
		rand.nextDouble();
		for (int catInd = 0; catInd < weights.length ; ++catInd)
		{
			if (rand.nextDouble() < prior[catInd])
				weights[catInd] = 1.0;
		}
		return weights;
	}

	@Override
	public void printToTextFile(String filename) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getFeatureNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFeatureNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public WeightedClassifier clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setupTermDetectionParameters(
			TermDetectionParameters termDetectionParameters) {
		// TODO Auto-generated method stub
		
	}

}
