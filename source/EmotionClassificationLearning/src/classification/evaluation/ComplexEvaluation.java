/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.evaluation;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classification.ClassificationUtil;
import classification.definitions.WeightedClassifierInterface;
import classification.definitions.classifiers.WeightedClassifierLexiconBasedWithIntensityModel;

import math.StepWiseCurve;

import utility.Pair;
import utility.Triple;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.CategoryUtil;
import data.categories.ICategoriesData;
import data.documents.Tweet;
import functionality.UtilArrays;
import functionality.UtilCollections;


public class ComplexEvaluation {
	
	public static class CategoryEvaluationResult implements Serializable
	{
		private static final long serialVersionUID = -8156977749801757320L;
		public int categoryId;
		public int objectsFoundNum;
		public int objectsGivenNum;
		
		public int objectsCorrectNum;
		
		public double precision;
		public double recall;
		public double f1Score;
		
		public double aucRoc;
		public double aucPR;
		
		public FoundData[] thresholdFoundNums; 
		
		void plus(CategoryEvaluationResult res2)
		{
			objectsFoundNum += res2.objectsFoundNum;
			objectsGivenNum += res2.objectsGivenNum;
			objectsCorrectNum += res2.objectsCorrectNum;
			
			if (res2.thresholdFoundNums != null)
			{
				if (thresholdFoundNums == null)
				{
					thresholdFoundNums = new FoundData[res2.thresholdFoundNums.length];
					for (int i = 0; i <  thresholdFoundNums.length; ++i)
						thresholdFoundNums[i] = new FoundData();
				}
				for (int i = 0; i < thresholdFoundNums.length; ++i)
				{
					thresholdFoundNums[i].plus(res2.thresholdFoundNums[i]);
				}
			}
		}
		

		void normalize(int evaluatedObjectsNum)
		{
			precision = 1.0 * objectsCorrectNum / objectsFoundNum;
			recall = 1.0 * objectsCorrectNum / objectsGivenNum;
			f1Score = 2 * precision * recall / (precision + recall);
			
			if (thresholdFoundNums != null)
			{
				// check if all equal to null
				boolean nullExist = false;
				for (int i = 0; i < thresholdFoundNums.length; ++i)
				{
					FoundData fd = thresholdFoundNums[i];
					if (fd == null)
						nullExist = true;
				}
				if (!nullExist)
				{
					List<Double[]> ROCpoints = new ArrayList<Double[]>();
					List<Double[]> PRpoints = new ArrayList<Double[]>();
					
					for (int i = 0; i < thresholdFoundNums.length; ++i)
					{
						FoundData fd = thresholdFoundNums[i];
						double thPrecision = 1.0 * fd.correctFoundNum / fd.foundNum;
						double thRecall = 1.0 * fd.correctFoundNum / objectsGivenNum;
						double thFallOut = 1.0 * (fd.foundNum - fd.correctFoundNum) / (evaluatedObjectsNum - objectsGivenNum);
						
						ROCpoints.add(new Double[]{thFallOut,thRecall});
						PRpoints.add(new Double[]{thRecall,thPrecision});
					}
					
					StepWiseCurve rocCurve = new StepWiseCurve(ROCpoints);
					StepWiseCurve prCurve = new StepWiseCurve(PRpoints);
					
					aucRoc = rocCurve.subROCArea();
					aucPR  = prCurve.subPRArea();
				}
			}
		}
		
		void normalizeSimplified(int evaluatedObjectsNum)
		{
			precision = 1.0 * objectsCorrectNum / objectsFoundNum;
			recall = 1.0 * objectsCorrectNum / objectsGivenNum;
			f1Score = 2 * precision * recall / (precision + recall);
		}
		
		public static String getTabFieldNames()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = CategoryEvaluationResult.class.getFields(); 
			for (Field field : fields)
			{
				if (field.getName().equals("thresholdFoundNums"))
					continue;
				sb.append(field.getName() + "\t");
			}
			return sb.toString();
		}
		
		public static List<String> getDataBaseFieldNames()
		{
			// FOR UPDATE: the functionality is linked to getTabFieldNames()
			List<String> resFieldNames = new ArrayList<String>();
			Field[] fields = CategoryEvaluationResult.class.getFields(); 
			for (Field field : fields)
			{
				String fieldName = field.getName();
				if (field.getName().equals("thresholdFoundNums"))
					continue;
				resFieldNames.add(fieldName);
			}
			return resFieldNames;
		}
		
		public static List<String> getDataBaseFieldTypes()
		{
			// FOR UPDATE: the functionality is linked to getDataBaseFieldNames()
			List<String> resFieldTypes = new ArrayList<String>();
			Field[] fields = CategoryEvaluationResult.class.getFields(); 
			for (Field field : fields)
			{
				String fieldName = field.getName();
				String fieldType = field.getType().getName();
				if (fieldName.equals("thresholdFoundNums"))
					continue;
				resFieldTypes.add(fieldType);
			}
			return resFieldTypes;
		}
		
		public String toTabString()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = this.getClass().getFields();
			for (Field field : fields)
			{
				try {
					if (field.getName().equals("thresholdFoundNums"))
						continue;
					Object thisValue = field.get(this);
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
							sb.append(thisArrayValue[i]  + "\t");
					}
					else
						sb.append(thisValue.toString() + "\t");
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			String result = sb.toString();
			return result.substring(0, result.length() - 1);
		}
		
		public String toString()
		{
			return toTabString();
		}
		
	}

	public static class EvaluationResult implements Serializable
	{
		private static final long serialVersionUID = -8406725071337296598L;
		int categorySize;
		static final int quadrantSize = 5;
		static final int polaritySize = 3; 
		
		private CategoryDataType catDataType;
		
		public EvaluationResult(CategoryDataType catDataType)
		{
			ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
			categorySize = categoriesData.getCategoryNum();
			this.catDataType = catDataType;
			
			categoryResults = new CategoryEvaluationResult[categorySize];
			for (int i = 0; i < categorySize; ++i)
			{
				categoryResults[i] = new CategoryEvaluationResult();
				categoryResults[i].categoryId = categoriesData.categoriesToUse()[i];
			}
			
			
			quadrantResults = new CategoryEvaluationResult[quadrantSize];
			for (int i = 0; i < quadrantSize; ++i)
			{
				quadrantResults[i] = new CategoryEvaluationResult();
				quadrantResults[i].categoryId = i;
			}
			
			polarityResults = new CategoryEvaluationResult[polaritySize];
			for (int i = 0; i < polaritySize; ++i)
			{
				polarityResults[i] = new CategoryEvaluationResult();
			}
			polarityResults[0].categoryId = 0;
			polarityResults[1].categoryId = 1;
			polarityResults[2].categoryId = -1;
		}
		
		// only double fields are used in multiplication and summation
		public int evaluatedObjectsNum;
		public int evaluatedAssignNum;
		
		public double foundEmotionPercentage; // use all tweets
		public double givenEmotionPercentage; // among all tweets for how many workers gave only non-neutral emotion labels
		public double averageFoundEmotionsWithoutNeutral; // use only the tweets where terms were found
	
		
		public double emotionPrecisionOnOne; // use all the tweets
		public double emotionPrecisionOnOneWithoutNeutral; // use only the tweets where terms were found
		
		public double emotionPrecisionOnAll; // use all the tweets
		public double emotionPrecisionOnAllWithoutNeutral; // use only the tweets where terms were found
		
		
		public double emotionRecallWithSet; // use all the tweets
		public double emotionRecallWithList; // use all the tweets
		
		public double emotionAccuracy; // use all the tweets
		
		public double emotionStrengthAccuracy; // use all the tweets
		
		public double emotionF1scoreOnAll; // f1-score between emotionPrecisionOnAllWithoutNeutral and emotionRecallWithSet (average per-tweet precision and recall)
		
		public double emotionMacroF1Score; // taking into account only classes with large amount of instances (no NaN values!)
		public double emotionMacroPrecision; // taking into account only classes with large amount of instances 
		public double emotionMacroRecall; // taking into account only classes with large amount of instances
		
		public double emotionMacroF1scoreWithNeutral;
		public double emotionMacroPrecisionWithNeutral;
		public double emotionMacroRecallWithNeutral;
		
		public double emotionGeomF1Score;
		public double emotionGeomPrecision;
		public double emotionGeomRecall;
		public double emotionGeomF1ScoreWithNeutral;
		public double emotionGeomPrecisionWithNeutral;
		public double emotionGeomRecallWithNeutral;
		
		public double emotionAvgAUCROC;
		public double emotionAvgAUCPR;
		
		public double polarityAccuracy; // use only tweets with definite polarity
		public double polarityRecall; // among tweets with definite polarity how many with polarity was found
		public double polarityStrictRecall; // among tweets with definite non-neutral polarity how many was found correctly?
		public double polarityPrecision; // among tweets with definite polarity and for which the polarity was found in how many cases is it correct
		public double polarityF1score; // f1-score between polarityPrecision and polarityStrictRecall
		public double nonNeutralPolarityPercentage; // how many tweets among all have non-neutral definite polarity according to workers' answers
		public double definitePolarityPercentage;// how many tweets among all have definite polarity according to workers' answers
		public double polarityMacroF1score;
		public double polarityMacroPrecision; // taking into account only classes with large amount of instances 
		public double polarityMacroRecall; // taking into account only classes with large amount of instances
		
		
		public double quadrantAccuracy; // use only tweets with definite emotion quadrant
		public double quadrantRecall;
		public double quadrantStrictRecall;
		public double quadrantPrecision;
		public double quadrantF1score;
		public double quadrantMacroF1score;
		public double quadrantMacroPrecision; // taking into account only classes with large amount of instances 
		public double quadrantMacroRecall; // taking into account only classes with large amount of instances
		public double definiteQuadrantPercentage;
		public double nonNeutralQuadrantPercentage;
		
		

		public double emotionMicroF1Score; // the micro-F1 score (f1-score between micro-Precision and micro-Recall)
		public double emotionMicroPrecision; // micro-Precision: percentage of correctly outputted labels
		public double emotionMicroRecall; // micro-Recall: percentage of correctly found labels
		
		
		
		//category statistics
		public CategoryEvaluationResult[] categoryResults;
		
		//quadrant statistics
		public CategoryEvaluationResult[] quadrantResults;
		
		//polarity statistics
		public CategoryEvaluationResult[] polarityResults;
		
		//confusion matrix (will be added only when sum of evaluation results in calculated
		public double[][] confusionMatrix;
		
		
		public void plus(EvaluationResult res2)
		{
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				try {
					Object thisValue = field.get(this);
					
					// use only double
					if (thisValue instanceof Double)
					{
						Double thisDoubleValue = (Double) thisValue;
						Double addValue = field.getDouble(res2);
						thisDoubleValue += addValue;
						field.setDouble(this, thisDoubleValue);
					}
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						double[] addValue = (double[])field.get(res2);
						for (int i = 0; i < thisArrayValue.length; ++i)
							thisArrayValue[i] += addValue[i];
						field.set(this, thisArrayValue);
					}
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			evaluatedObjectsNum += res2.evaluatedObjectsNum;
			evaluatedAssignNum += res2.evaluatedAssignNum;
			
			// sum up the categories data
			for (int i = 0; i < categoryResults.length; ++i)
			{
				categoryResults[i].plus(res2.categoryResults[i]);
			}
			
			// sum up the quadrant data
			for (int i = 0; i < quadrantResults.length; ++i)
			{
				quadrantResults[i].plus(res2.quadrantResults[i]);
			}
			
			// sum up the polarity data
			for (int i = 0; i < polarityResults.length; ++i)
			{
				polarityResults[i].plus(res2.polarityResults[i]);
			}
				
		}
		
		public void plusConfusionMatrix(EvaluationResult res2)
		{
			if (!(confusionMatrix == null && res2.confusionMatrix == null))
			{	
				if (confusionMatrix == null && res2.confusionMatrix != null)
				{
					confusionMatrix = new double[res2.confusionMatrix.length][res2.confusionMatrix.length];
				}
				
				for (int i1 = 0; i1 < confusionMatrix.length; ++i1)
					for (int i2 = 0; i2 < confusionMatrix.length; ++i2)		
				{
						confusionMatrix[i1][i2] += res2.confusionMatrix[i1][i2];
				}
			}
		}
		
		void multiply(double multiplier)
		{
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				try {
					Object thisValue = field.get(this);
					
					// use only double
					if (thisValue instanceof Double)
					{
						Double thisDoubleValue = (Double) thisValue;
						thisDoubleValue *= multiplier;
						field.setDouble(this, thisDoubleValue);
					}
					
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
							thisArrayValue[i] *= multiplier;
						field.set(this, thisArrayValue);
					}
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void normalize()
		{
			Field[] fields = EvaluationResult.class.getFields();
			foundEmotionPercentage /= evaluatedObjectsNum;
			givenEmotionPercentage /= evaluatedObjectsNum;
			
			for (Field field : fields)
			{
				try {
					String fieldName = field.getName();
					if (fieldName.equals("foundEmotionPercentage") || fieldName.equals("givenEmotionPercentage"))
						continue;
					Object thisValue = field.get(this);
					
					// use only double
					if (thisValue instanceof Double)
					{
						Double thisDoubleValue = (Double) thisValue;
						thisDoubleValue /= evaluatedObjectsNum;
						if (fieldName.endsWith("WithoutNeutral")) 
							thisDoubleValue /= foundEmotionPercentage;
						
						if(fieldName.contains("emotionRecall") || fieldName.contains("emotionSimilarityRecall"))
							thisDoubleValue /= givenEmotionPercentage;
						
						
						field.setDouble(this, thisDoubleValue);
					}
					
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
						{
							thisArrayValue[i] /= evaluatedObjectsNum;
							if (fieldName.endsWith("WithoutNeutral"))
								thisArrayValue[i] /= foundEmotionPercentage;
							
							if(fieldName.contains("emotionRecall") || fieldName.contains("emotionSimilarityRecall"))
								thisArrayValue[i] /= givenEmotionPercentage;
						}
						field.set(this, thisArrayValue);
					}
					
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			
			polarityAccuracy /= definitePolarityPercentage;
			quadrantAccuracy /= definiteQuadrantPercentage;
			
			polarityRecall /= definitePolarityPercentage;
			polarityStrictRecall /= nonNeutralPolarityPercentage;
			polarityPrecision /= definitePolarityPercentage * polarityRecall;
			polarityF1score = computeF1score(polarityPrecision, polarityStrictRecall);
			
			quadrantRecall /= definiteQuadrantPercentage;
			quadrantStrictRecall /= nonNeutralQuadrantPercentage;
			quadrantPrecision /= definiteQuadrantPercentage * quadrantRecall;
			quadrantF1score = computeF1score(quadrantPrecision, quadrantStrictRecall);
			
			// normalize categories data
			for (int i = 0; i < categoryResults.length; ++i)
			{
				categoryResults[i].normalize(evaluatedObjectsNum);
			}
			
			// normalize quadrant data
			for (int i = 0; i < quadrantResults.length; ++i)
			{
				quadrantResults[i].normalize(evaluatedObjectsNum);
			}
			// normalize polarity data
			for (int i = 0; i < polarityResults.length; ++i)
			{
				polarityResults[i].normalize(evaluatedObjectsNum);
			}
			
			emotionF1scoreOnAll = computeF1score(emotionPrecisionOnAllWithoutNeutral, emotionRecallWithSet);
			
			ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
			int[] emotionCatsIndices = UtilArrays.copyArray(categoriesData.emotionalCategoriesToUse());
			for (int i = 0; i < emotionCatsIndices.length; ++i) {
				emotionCatsIndices[i] = categoriesData.getCategoryIndexById(emotionCatsIndices[i]);
			}
			
			double maxWeight = 10.0;
			List<CategoryEvaluationResult> emCatResults = UtilArrays.subArrayFixed(categoryResults, emotionCatsIndices);
			
			emotionMacroF1Score = computeMacroF1scoreForClassArray(emCatResults, "f1Score", maxWeight);
			polarityMacroF1score = computeMacroF1scoreForClassArray(UtilArrays.subArray(polarityResults, 1, 2), "f1Score", maxWeight);
			quadrantMacroF1score = computeMacroF1scoreForClassArray(UtilArrays.subArray(quadrantResults, 1, 4), "f1Score", maxWeight);
			
			emotionMacroPrecision = computeMacroF1scoreForClassArray(emCatResults, "precision", maxWeight);
			polarityMacroPrecision = computeMacroF1scoreForClassArray(UtilArrays.subArray(polarityResults, 1, 2), "precision", maxWeight);
			quadrantMacroPrecision = computeMacroF1scoreForClassArray(UtilArrays.subArray(quadrantResults, 1, 4), "precision", maxWeight);
			
			emotionMacroRecall = computeMacroF1scoreForClassArray(emCatResults, "recall", maxWeight);
			polarityMacroRecall = computeMacroF1scoreForClassArray(UtilArrays.subArray(polarityResults, 1, 2), "recall", maxWeight);
			quadrantMacroRecall = computeMacroF1scoreForClassArray(UtilArrays.subArray(quadrantResults, 1, 4), "recall", maxWeight);
			
			emotionGeomF1Score = computeGeomScoreForClassArray(emCatResults, "f1Score", maxWeight);
			emotionGeomPrecision = computeGeomScoreForClassArray(emCatResults, "precision", maxWeight);
			emotionGeomRecall = computeGeomScoreForClassArray(emCatResults, "recall", maxWeight);
			
			
			emotionAvgAUCROC = computeAverageAUCForClassArray(emCatResults, "aucRoc",  maxWeight);
			emotionAvgAUCPR = computeAverageAUCForClassArray(emCatResults, "aucPR",  maxWeight);
			
			List<CategoryEvaluationResult> EmAndNeutCatResults = UtilArrays.subArrayFixed(categoryResults, emotionCatsIndices);
			// add neutral category results in the following computations
			EmAndNeutCatResults.add(categoryResults[categoriesData.getCategoryIndexById(categoriesData.neutralCategory())]); 
			emotionGeomF1ScoreWithNeutral = computeGeomScoreForClassArray(EmAndNeutCatResults, "f1Score", maxWeight);
			emotionGeomPrecisionWithNeutral = computeGeomScoreForClassArray(EmAndNeutCatResults, "precision", maxWeight);
			emotionGeomRecallWithNeutral = computeGeomScoreForClassArray(EmAndNeutCatResults, "recall", maxWeight);
			
			emotionMacroF1scoreWithNeutral = computeMacroF1scoreForClassArray(EmAndNeutCatResults, "f1Score", maxWeight);
			emotionMacroPrecisionWithNeutral = computeMacroF1scoreForClassArray(EmAndNeutCatResults, "precision", maxWeight);
			emotionMacroRecallWithNeutral = computeMacroF1scoreForClassArray(EmAndNeutCatResults, "recall", maxWeight);
			
			Triple<Double, Double, Double> microScores = computeMicroScoresForClassArray (emCatResults);
			
			emotionMicroF1Score =(microScores.first == null) ? Double.NaN : microScores.first;
			emotionMicroPrecision = (microScores.second == null) ? Double.NaN : microScores.second;
			emotionMicroRecall = (microScores.third == null) ? Double.NaN : microScores.third;
			
		}
		
		public void normalizeSimplified()
		{
			foundEmotionPercentage /= evaluatedObjectsNum;
			givenEmotionPercentage /= evaluatedObjectsNum;
			
			emotionAccuracy /= evaluatedObjectsNum;
			
			// normalize categories data
			for (int i = 0; i < categoryResults.length; ++i)
			{
				categoryResults[i].normalizeSimplified(evaluatedObjectsNum);
			}
			
			ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
			int[] emotionCatsIndices = UtilArrays.copyArray(categoriesData.emotionalCategoriesToUse());
			for (int i = 0; i < emotionCatsIndices.length; ++i) {
				emotionCatsIndices[i] = categoriesData.getCategoryIndexById(emotionCatsIndices[i]);
			}
			
			double maxWeight = 10.0;
			List<CategoryEvaluationResult> emCatResults = UtilArrays.subArrayFixed(categoryResults, emotionCatsIndices);
			
			emotionMacroF1Score = computeMacroF1scoreForClassArray(emCatResults, "f1Score", maxWeight);
			emotionMacroPrecision = computeMacroF1scoreForClassArray(emCatResults, "precision", maxWeight);
			emotionMacroRecall = computeMacroF1scoreForClassArray(emCatResults, "recall", maxWeight);
				
			Triple<Double, Double, Double> microScores = computeMicroScoresForClassArray (emCatResults);
			
			emotionMicroF1Score = microScores.first;
			emotionMicroPrecision = microScores.second;
			emotionMicroRecall = microScores.third;
			
		}
		
		private double[] computeF1scores (double[] precision, double[] recall)
		{
			double[] res = new double[precision.length];
			for (int i = 0; i < res.length; ++i)
				res[i] =  computeF1score(precision[i], recall[i]);
			return res;
		}
		
		private double computeF1score (double precision, double recall)
		{
			return 2 * precision * recall / (precision + recall);
		}
		
		private double computeMacroF1scoreForClassArray (List<? extends CategoryEvaluationResult> classesData, String fieldName, double maxWeight)
		{
			int allNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				allNum += classResult.objectsGivenNum;
			}
			double finalScore = 0.0;
			int foundClassNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				if (classResult.objectsGivenNum <= maxWeight)
					continue;
				foundClassNum++;
				try
				{
					double score = classResult.getClass().getField(fieldName).getDouble(classResult);
					finalScore += Double.isNaN(score)?0.0:score;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
			}
			return finalScore / (1.0 * foundClassNum);
		}
		
		
		private double computeGeomScoreForClassArray (List<? extends CategoryEvaluationResult> classesData, String fieldName, double maxWeight)
		{
			int allNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				allNum += classResult.objectsGivenNum;
			}
			double finalScore = 1.0;
			int foundClassNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				/*if ( allNum / (1.0 * classResult.objectsGivenNum) > maxWeight)
				continue;*/
				if (classResult.objectsGivenNum <= maxWeight)
					continue;
				try
				{
					double score = classResult.getClass().getField(fieldName).getDouble(classResult);
					foundClassNum++;
					finalScore *= (Double.isNaN(score) || score == 0.0) ? 0.001 : score;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
			}
			return Math.pow(finalScore, 1 / (1.0 * foundClassNum));
		}
		
		public static Triple<Double, Double, Double> computeMicroScoresForClassArray (List<? extends CategoryEvaluationResult> classesData)
		{
			if (classesData.size() == 0)
				return new Triple (null, null, null);
			
			int allCorrectNum = 0;
			int allFoundNum = 0;
			int allGivenNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				 allGivenNum += classResult.objectsGivenNum;
				 allFoundNum += classResult.objectsFoundNum;
				 allCorrectNum += classResult.objectsCorrectNum;
			}
			
			
			Double microPrecision = 1.0 * allCorrectNum / (1.0 * allFoundNum);
			Double microRecall = 1.0 * allCorrectNum / (1.0 * allGivenNum);
			Double microF1Score =  (2 * microPrecision * microRecall)/ (microPrecision + microRecall);
			
			microF1Score = Double.isNaN(microF1Score)?0.0:microF1Score;
			microPrecision = Double.isNaN(microPrecision)?null:microPrecision;
			microRecall = Double.isNaN(microRecall)?null:microRecall;
			
			return new Triple<Double, Double, Double>(microF1Score, microPrecision, microRecall);
		}
		
		private double computeAverageAUCForClassArray (List<? extends CategoryEvaluationResult> classesData, String fieldName, double maxWeight)
		{
			int allNum = 0;
			for (CategoryEvaluationResult classResult : classesData)
			{
				allNum += classResult.objectsGivenNum;
			}
			
			double[] classPrior = new double[classesData.size()];
			
			for (int i = 0; i < classesData.size(); ++i)
			{
				CategoryEvaluationResult classResult = classesData.get(i);
				/*if ( allNum / (1.0 * classResult.objectsGivenNum) > maxWeight)
				continue;*/
				if (classResult.objectsGivenNum <= maxWeight)
					continue;
				classPrior[i] = classResult.objectsGivenNum;
			}
			UtilArrays.normalizeArrayInternal(classPrior);
			
			double finalScore = 0.0;
			for (int i = 0; i < classesData.size(); ++i)
			{
				CategoryEvaluationResult classResult = classesData.get(i);
				
				try
				{
					double score = classResult.getClass().getField(fieldName).getDouble(classResult);
					finalScore += score * classPrior[i];
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
			}
			return finalScore;
		}
		
		public void clearSimplified()
		{
			foundEmotionPercentage = 0;
			givenEmotionPercentage = 0;
			

			emotionAccuracy = 0;
			
			emotionMacroF1Score = 0;
			emotionMacroPrecision = 0;
			emotionMacroRecall = 0;
				
			
			emotionMicroF1Score = 0;
			emotionMicroPrecision = 0;
			emotionMicroRecall = 0;
			
			categoryResults = new CategoryEvaluationResult[categorySize];
			ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
			categoryResults = new CategoryEvaluationResult[categorySize];
			for (int i = 0; i < categorySize; ++i)
			{
				categoryResults[i] = new CategoryEvaluationResult();
				categoryResults[i].categoryId = categoriesData.categoriesToUse()[i];
			}
		}
		
		public String toString()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				try {
					Object thisValue = field.get(this);
					
					sb.append(field.getName() + ": ");
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
							sb.append(thisArrayValue[i]  + "\t");
						sb.append("\n\r");
					}
					else if (thisValue instanceof double[][])
					{
						double[][] thisArrayValue = (double[][]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
						{
							for (int j = 0; j < thisArrayValue[i].length; ++j)
								sb.append(thisArrayValue[i][j]  + "\t");
							sb.append("\n\r");
						}
						sb.append("\n\r");
					}
					else
					{
						if (thisValue != null)
							sb.append(thisValue.toString());
						else
							sb.append("null");
						sb.append("\n\r");
					}
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			return sb.toString();
		}
		
		public String toTabString()
		{
			return toTabString(true);
		}
		
		public String toTabString(boolean toPrintCategories)
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				try {
					Object thisValue = field.get(this);
					String fieldName = field.getName();
					
					if (thisValue instanceof double[])
					{
						double[] thisArrayValue = (double[]) thisValue;
						for (int i = 0; i < thisArrayValue.length; ++i)
							sb.append(thisArrayValue[i]  + "\t");
					}
					else if (thisValue instanceof double[][])
					{
						double[][] thisArrayValue = (double[][]) thisValue;
						StringBuilder sbArray = new StringBuilder();
						for (int arrInd1 = 0; arrInd1 < thisArrayValue.length; ++arrInd1)
						{
							double[] curLine = thisArrayValue[arrInd1];
							for (int arrInd2 = 0; arrInd2 < curLine.length; ++arrInd2)
							{
								sbArray.append(thisArrayValue[arrInd1][arrInd2]);
								sbArray.append(" ");
							}
							sbArray.append(";");
						}
							sb.append(sbArray.toString()  + "\t");
					}
					else if ((fieldName.equals("polarityResults") || fieldName.equals("quadrantResults") || fieldName.equals("categoryResults")))
					{
						if (!toPrintCategories)
							continue;
						sb.append(thisValue.toString() + "\t");
						
						if (fieldName.equals("quadrantResults"))
						{
							sb.append("QR\t");
							CategoryEvaluationResult[] quadrantData = (CategoryEvaluationResult[]) thisValue;
							for (CategoryEvaluationResult oneQuadrantData : quadrantData)
							{
								sb.append(oneQuadrantData.toTabString() + "\t");
							}
						}
						else if (fieldName.equals("polarityResults"))
						{
							sb.append("PR\t");
							CategoryEvaluationResult[] polarityData = (CategoryEvaluationResult[]) thisValue;
							for (CategoryEvaluationResult onePolarityData : polarityData)
							{
								sb.append(onePolarityData.toTabString() + "\t");
							}
						}
						else if (fieldName.equals("categoryResults"))
						{
							sb.append("CR\t");
							CategoryEvaluationResult[] categoryData = (CategoryEvaluationResult[]) thisValue;
							for (CategoryEvaluationResult oneCategoryData : categoryData)
							{
								sb.append(oneCategoryData.toTabString() + "\t");
							}
						}
					}
					else
						sb.append(thisValue.toString() + "\t");
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			return sb.toString();
		}
		
		public static String getTabFieldNames()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				String fieldName = field.getName();
				if (fieldName.equals("polarityResults") || fieldName.equals("quadrantResults"))
					sb.append("res\t"+ CategoryEvaluationResult.getTabFieldNames());
				else if (fieldName.equals("categoryResults"))
					sb.append("CatRes\t" + CategoryEvaluationResult.getTabFieldNames());
				else
					sb.append(field.getName() + "\t");
			}
			return sb.toString();
		}
		
		public static String getTabFieldNamesWithoutCategoryResults()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				String fieldName = field.getName();
				if (fieldName.equals("polarityResults") || fieldName.equals("quadrantResults") || fieldName.equals("categoryResults"))
					continue;
				else
					sb.append(field.getName() + "\t");
			}
			return sb.toString();
		}
		
		public static List<String> getDataBaseFieldNamesWithoutCategories()
		{
			// FOR UPDATE: the functionality should be linked to getTabFieldNames()
			List<String> resFieldNames = new ArrayList<String>();
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				String fieldName = field.getName();
				if (fieldName.equals("polarityResults") || 
						fieldName.equals("quadrantResults") ||
						fieldName.equals("categoryResults"))
				{
					continue;
				}
				else
				{
					resFieldNames.add(field.getName());
				}
			}
			return resFieldNames;
		}
		
		public static List<String> getDataBaseFieldTypesWithoutCategories()
		{
			// FOR UPDATE: the functionality should be linked to getDataBaseFieldNamesWithoutCategories()
			List<String> resFieldTypes = new ArrayList<String>();
			Field[] fields = EvaluationResult.class.getFields();
			for (Field field : fields)
			{
				String fieldName = field.getName();
				String fieldType = field.getType().getName();
				if (fieldName.equals("polarityResults") || 
						fieldName.equals("quadrantResults") ||
						fieldName.equals("categoryResults"))
				{
					continue;
				}
				else
				{
					resFieldTypes.add(fieldType);
				}
			}
			return resFieldTypes;
		}
	}

	
	public static EvaluationResult getEvaluationSums(Map<Integer, List<Integer>> emotionLabels, Map<Integer, String> textData, WeightedClassifierInterface classifier) throws Exception
	{
		CategoryDataType catDataType = classifier.getCategoriesData().getCategoriesType();
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Integer tweetId : textData.keySet())
		{
			EvaluationResult twdResult = getEvaluation(
					textData.get(tweetId), classifier, emotionLabels.get(tweetId), 
					null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluationSumsOnTweets(Map<Integer, List<Integer>> emotionLabels, Map<Integer, Tweet> tweetData, WeightedClassifierInterface classifier) throws Exception
	{
		CategoryDataType catDataType = classifier.getCategoriesData().getCategoriesType();
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Integer tweetId : tweetData.keySet())
		{
			EvaluationResult twdResult = getEvaluation(
					tweetData.get(tweetId), classifier, emotionLabels.get(tweetId), 
					null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluationSumsOnTweets(Map<Integer, List<Integer>> emotionLabels, List<Tweet> tweetData, WeightedClassifierInterface classifier) throws Exception
	{
		CategoryDataType catDataType = classifier.getCategoriesData().getCategoriesType();
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Tweet tweet : tweetData)
		{
			int tweetId = tweet.tweetId;
			EvaluationResult twdResult = getEvaluation(
					tweet, classifier, emotionLabels.get(tweetId), 
					null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	/**
	 * The emotion labels are given in binary format: for each tweet id (first element in the pair), we specify whether the considered emotion is present (+id) or absent (-id). 
	 */
	public static EvaluationResult getEvaluationSumsInBinaryMode(List<Pair<Integer, Integer>> emotionLabels, Map<Integer, String> textData, WeightedClassifierInterface classifier) throws Exception
	{
		CategoryDataType catDataType = classifier.getCategoriesData().getCategoriesType();
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Pair<Integer,Integer> tweetLabel: emotionLabels)
		{
			int tweetId = tweetLabel.first;
			int catLabel = tweetLabel.second;
				EvaluationResult twdResult = getBinaryEvaluation(
						textData.get(tweetId), classifier, catLabel, 
						null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluationSumsInBinaryModeOnTweets(List<Pair<Integer, Integer>> emotionLabels, Map<Integer, Tweet> tweetData, WeightedClassifierInterface classifier) throws Exception
	{
		CategoryDataType catDataType = classifier.getCategoriesData().getCategoriesType();
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Pair<Integer,Integer> tweetLabel: emotionLabels)
		{
			int tweetId = tweetLabel.first;
			int catLabel = tweetLabel.second;
				EvaluationResult twdResult = getBinaryEvaluation(
						tweetData.get(tweetId), classifier, catLabel, 
						null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluation(Map<Integer, List<Integer>> truthData, Map<Integer, double[]> foundAnnotation, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = getEvaluationSums(truthData, foundAnnotation, catDataType);
		result.normalize();
		return result; 
	}
	
	/** Evaluate only as the dominant category output */
	public static EvaluationResult getEvaluationSums(Map<Integer, List<Integer>> truthData, Map<Integer, double[]> foundAnnotation, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Integer tweetId : truthData.keySet()) // we evaluate the results only for those tweets were we know the ground truth
		{
			double[] weights = foundAnnotation.get(tweetId);
			List<Integer> dominantCatsIds = ClassificationUtil.getMaximalCategoriesIds(weights, catDataType);
			EvaluationResult twdResult = getEvaluation(weights, dominantCatsIds, 2, truthData.get(tweetId), null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluationSumsOnFoundLabels (Map<Integer, List<Integer>> truthData, Map<Integer, List<Integer>> foundAnnotation, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		initializeNewConfusionMatrix(catDataType);
		
		for (Integer tweetId : truthData.keySet()) // we evaluate the results only for those tweets were we know the ground truth
		{
			List<Integer> dominantCatsIds = foundAnnotation.get(tweetId);
			double[] weights = CategoryUtil.transformCategoryListToWeightsWithCounts(dominantCatsIds, catDataType);
			EvaluationResult twdResult = getEvaluation(weights, dominantCatsIds, 2, truthData.get(tweetId), null, catDataType);
			result.plus(twdResult);
		}
		
		setConfusionMatrix(result);
		return result;
	}
	
	public static EvaluationResult getEvaluationTotalOnFoundLabelsSimplified (Map<Integer, List<Integer>> truthData, Map<Integer, List<Integer>> foundAnnotation, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		
		for (Integer tweetId : truthData.keySet()) // we evaluate the results only for those tweets were we know the ground truth
		{
			List<Integer> dominantCatsIds = foundAnnotation.get(tweetId);
			EvaluationResult twdResult = getEvaluationSimplified(dominantCatsIds, truthData.get(tweetId), catDataType);
			result.plus(twdResult);
		}
		result.normalize();
		return result;
	}
	
	/**
	 * 
	 * @param data : the triple is <tweetId, truthLabels, foundLabels> 
	 * @param catDataType
	 * @return
	 * @throws Exception
	 */
	public static EvaluationResult getEvaluationTotalOnFoundLabelsSimplified (List<Triple<Integer, List<Integer>, List<Integer>>> data, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		
		for (Triple<Integer, List<Integer>, List<Integer>> oneEntry : data) 
		{
			if (oneEntry.second != null)// we evaluate the results only for those tweets were we know the ground truth
			{
				EvaluationResult twdResult = getEvaluationSimplified(oneEntry.third, oneEntry.second, catDataType);
				result.plus(twdResult);
			}
		}
		result.normalize();
		return result;
	}
	
	
	/**
	 * 
	 * @param data : the triple is <tweetId, truthLabels, foundLabels> 
	 * @param catDataType
	 * @return
	 * @throws Exception
	 */
	public static EvaluationResult getEvaluationTotalOnFoundLabelsOverSimplified (List<Triple<Integer, List<Integer>, List<Integer>>> data, CategoryDataType catDataType) throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		
		for (Triple<Integer, List<Integer>, List<Integer>> oneEntry : data) 
		{
			if (oneEntry.second != null)// we evaluate the results only for those tweets were we know the ground truth
			{
				getEvaluationOverSimplified(oneEntry.third, oneEntry.second, catDataType, result);
			}
		}
		result.normalize();
		return result;
	}
	
	
	
	public static EvaluationResult getEvaluation(
			String text, WeightedClassifierInterface classifier,
			List<Integer> categoryLabels, List<Integer> categoryStrengthLabels,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		double[] categoryWeights = classifier.findCategoryWeights(text);
		
		List<Integer> dominantCatsIds = classifier.getDominantCategories(categoryWeights);		
		
		int classifierStrenthLabel = 2;
		if (classifier instanceof WeightedClassifierLexiconBasedWithIntensityModel)
		   classifierStrenthLabel = ((WeightedClassifierLexiconBasedWithIntensityModel)classifier).computeStrengthLabel();
	
		return getEvaluation(categoryWeights, dominantCatsIds, classifierStrenthLabel, categoryLabels, categoryStrengthLabels, catDataType);
	}
	
	public static EvaluationResult getEvaluation(
			Tweet tweet, WeightedClassifierInterface classifier,
			List<Integer> categoryLabels, List<Integer> categoryStrengthLabels,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		double[] categoryWeights = classifier.findCategoryWeights(tweet);
		
		List<Integer> dominantCatsIds = classifier.getDominantCategories(categoryWeights);		
		
		int classifierStrenthLabel = 2;
		if (classifier instanceof WeightedClassifierLexiconBasedWithIntensityModel)
		   classifierStrenthLabel = ((WeightedClassifierLexiconBasedWithIntensityModel)classifier).computeStrengthLabel();
	
		return getEvaluation(categoryWeights, dominantCatsIds, classifierStrenthLabel, categoryLabels, categoryStrengthLabels, catDataType);
	}
	
	public static EvaluationResult getBinaryEvaluation(
			String text, WeightedClassifierInterface classifier,
			Integer categoryLabel, Integer categoryStrengthLabel,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		double[] categoryWeights = classifier.findCategoryWeights(text);
		
		List<Integer> dominantCatsIds = classifier.getDominantCategories(categoryWeights);		
		
		int classifierStrenthLabel = 2;
		if (classifier instanceof WeightedClassifierLexiconBasedWithIntensityModel)
		   classifierStrenthLabel = ((WeightedClassifierLexiconBasedWithIntensityModel)classifier).computeStrengthLabel();
	
		return getBinaryEvaluation(categoryWeights, dominantCatsIds, classifierStrenthLabel, categoryLabel, categoryStrengthLabel, catDataType);
	}
	
	public static EvaluationResult getBinaryEvaluation(
			Tweet tweet, WeightedClassifierInterface classifier,
			Integer categoryLabel, Integer categoryStrengthLabel,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		double[] categoryWeights = classifier.findCategoryWeights(tweet);
		
		List<Integer> dominantCatsIds = classifier.getDominantCategories(categoryWeights);		
		
		int classifierStrenthLabel = 2;
		if (classifier instanceof WeightedClassifierLexiconBasedWithIntensityModel)
		   classifierStrenthLabel = ((WeightedClassifierLexiconBasedWithIntensityModel)classifier).computeStrengthLabel();
	
		return getBinaryEvaluation(categoryWeights, dominantCatsIds, classifierStrenthLabel, categoryLabel, categoryStrengthLabel, catDataType);
	}
	
	
	
	static double[][] experimentConfusionMatrix = null;
	
	
	
	public static void initializeNewConfusionMatrix(CategoryDataType catDataType)
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		experimentConfusionMatrix = new double[categoriesData.getCategoryNum()][categoriesData.getCategoryNum()];
	}
	
	public static void setConfusionMatrix(EvaluationResult eval)
	{
		eval.confusionMatrix = experimentConfusionMatrix;
	}
	
	
	public static EvaluationResult getEvaluation(
			double[] categoryWeightsAnswers, List<Integer> dominantCatsIds, int strengthLabelAnswer, // obtained data
			List<Integer> categoryLabels, List<Integer> categoryStrengthLabels,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		result.evaluatedObjectsNum++;
		result.evaluatedAssignNum += categoryLabels.size();
		boolean isNeutral = false;
		
		if (dominantCatsIds.size() == 0)
		{
			if (UtilArrays.getSum(categoryWeightsAnswers) == 0.0)
			{
				isNeutral = true;
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
		}
		else
		{
			if (dominantCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
			{
				isNeutral = true;
				dominantCatsIds = new ArrayList<Integer>();
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
			else
				result.foundEmotionPercentage++;
			
		}
		
		if (dominantCatsIds.size() == 0)
		{
			isNeutral = true;
			dominantCatsIds = new ArrayList<Integer>();
			dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));
			
		}
		
		for (Integer dominantCategory : dominantCatsIds)
			result.categoryResults[categoriesData.getCategoryIndexById(dominantCategory)].objectsFoundNum++;
		
			
		Set<Integer> answerCatsIds = new HashSet(categoryLabels);
		for (Integer answerCategory : answerCatsIds)
			result.categoryResults[categoriesData.getCategoryIndexById(answerCategory)].objectsGivenNum++;	
		
		List<Integer> intersect = new ArrayList<Integer>(dominantCatsIds);
		intersect.retainAll(answerCatsIds);
		
		for (Integer category : intersect)
			result.categoryResults[categoriesData.getCategoryIndexById(category)].objectsCorrectNum++;
		
		result.emotionPrecisionOnAll = 1.0 * intersect.size() / dominantCatsIds.size();
		result.emotionPrecisionOnOne = (intersect.size() > 0) ? 1.0 : 0.0;
		
		if (!isNeutral)
		{
			result.emotionPrecisionOnOneWithoutNeutral = result.emotionPrecisionOnOne;
			result.emotionPrecisionOnAllWithoutNeutral = result.emotionPrecisionOnAll;
			
			result.averageFoundEmotionsWithoutNeutral = dominantCatsIds.size();
		}
		
		if (!answerCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
		{
			// consider only tweets with non-neutral emotion
			result.givenEmotionPercentage++;
			
			result.emotionRecallWithSet = 1.0 * intersect.size() / answerCatsIds.size();
			
			int foundNum = 0;
			for (int i = 0; i < categoryLabels.size(); ++i)
			{
				if (dominantCatsIds.contains(categoryLabels.get(i)))
					foundNum++;
				
			}
			result.emotionRecallWithList = 1.0 * foundNum / categoryLabels.size();
		}
			
		int unionSize = answerCatsIds.size() + dominantCatsIds.size() - intersect.size();
		result.emotionAccuracy = 1.0 * intersect.size() / unionSize; //Jaccard measure
		
		// compute accuracy of strength
		if (categoryStrengthLabels != null)
		{
			double averageStrength = 0.0;
			for (int i = 0; i < categoryLabels.size(); ++i)
				averageStrength += categoryStrengthLabels.get(i);
			averageStrength /= categoryStrengthLabels.size();
			
			int realLabel = WeightedClassifierLexiconBasedWithIntensityModel.getClosestStrengthLabel(averageStrength);
			
			result.emotionStrengthAccuracy = (realLabel == strengthLabelAnswer)?1.0:0.0;
		}
		else
			result.emotionStrengthAccuracy = 0.0;
		
		//evaluate polarity statistics
		List<Integer> workerPolarityAnswers = UtilCollections.findKeysWithMaxValues(UtilCollections.computeFrequencies(CategoryUtil.getCategoryPolarities(categoryLabels, catDataType)));
		if (workerPolarityAnswers.size() == 1)
		{
			result.definitePolarityPercentage++;
			int polarityAnswer = workerPolarityAnswers.get(0);
			
			if (polarityAnswer != 0)
				result.nonNeutralPolarityPercentage++;
			
			List<Integer> algPolarityResults = CategoryUtil.getDominantPolarityList(categoryWeightsAnswers, catDataType.toString());
			int polarityResult = algPolarityResults.get(0);
			
			if (algPolarityResults.size() == 1 && polarityResult == 0)
			{
				result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsFoundNum++;
				if (polarityAnswer == 0)
					result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsCorrectNum++;
			}
			
			if (algPolarityResults.size() > 1 || isNeutral || polarityResult < -1)
			{
				// no strict output - don't use in recall or precision
				polarityResult = 0;
			}
			else
			{
				// strict output - use in recall and precision
				result.polarityRecall++;
				if (polarityResult == polarityAnswer) // correct answer
				{
					result.polarityPrecision++;
					if (polarityAnswer != 0)
						result.polarityStrictRecall++;
					result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsCorrectNum++;
				}
				result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsFoundNum++;
			}
			result.polarityAccuracy = (polarityResult == polarityAnswer) ? 1.0 : 0.0;
			
			result.polarityResults[categoriesData.getPolarityIndex(polarityAnswer)].objectsGivenNum++;
		}
		
		//evaluate quadrant statistics
		List<Integer> workerQuadrantAnswers = UtilCollections.findKeysWithMaxValues(
				UtilCollections.computeFrequencies(CategoryUtil.getCategoryQuadrants(categoryLabels, catDataType)));
		if (workerQuadrantAnswers.size() == 1)
		{
			result.definiteQuadrantPercentage++;
			int quadrantAnswer = workerQuadrantAnswers.get(0);
			
			if (quadrantAnswer != 0)		
				result.nonNeutralQuadrantPercentage++;
		
			
			List<Integer> algQuadrantResults = CategoryUtil.getDominantQuadrantList(categoryWeightsAnswers, catDataType.toString());
			
			int quadrantResult = algQuadrantResults.get(0);
					
			if (algQuadrantResults.size() > 1 || isNeutral || quadrantResult < 0) 
			{
				// no strict output - don't use in recall or precision; including the context-dependent (<0)
				quadrantResult = 0;
			}
			else
			{
				// strict output - use in recall and precision
				result.quadrantRecall++;
				if (quadrantResult == quadrantAnswer) // correct answer
				{
					result.quadrantPrecision++;
					if (quadrantAnswer != 0)	
						result.quadrantStrictRecall++;
					result.quadrantResults[quadrantResult].objectsCorrectNum++;
				}
				result.quadrantResults[quadrantResult].objectsFoundNum++;
			}
			result.quadrantAccuracy = (quadrantResult == quadrantAnswer) ? 1.0 : 0.0;
			
			result.quadrantResults[quadrantAnswer].objectsGivenNum++;
		}
		
		// evaluate the found objects for varying threshold (needed for ROC and PR curves)
		FoundData[][] foundPoints = getCurvePoints(categoryWeightsAnswers, categoryLabels, categoriesData);
		for (int catInd = 0; catInd < foundPoints.length; ++catInd)
		{
			result.categoryResults[catInd].thresholdFoundNums = foundPoints[catInd];
		}
		
		// add points to the confusion matrix
		for (Integer answerCategory : answerCatsIds)
			for (Integer dominantCategory : dominantCatsIds)
				experimentConfusionMatrix[categoriesData.getCategoryIndexById(answerCategory)][dominantCategory - 1]++;
		
		return result;
	}
	
	
	public static EvaluationResult getEvaluationSimplified(
			List<Integer> dominantCatsIds, // obtained data
			List<Integer> categoryLabels, // test data
			CategoryDataType catDataType)
					throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		result.evaluatedObjectsNum++;
		result.evaluatedAssignNum += categoryLabels.size();
		if (dominantCatsIds.size() == 0)
		{
			dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
		}
		else
		{
			if (dominantCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
			{
				dominantCatsIds = new ArrayList<Integer>();
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
			else
				result.foundEmotionPercentage++;
			
		}
		
		for (Integer dominantCategory : dominantCatsIds)
			result.categoryResults[categoriesData.getCategoryIndexById(dominantCategory)].objectsFoundNum++;
		
			
		Set<Integer> answerCatsIds = new HashSet(categoryLabels);
		for (Integer answerCategory : answerCatsIds)
			result.categoryResults[categoriesData.getCategoryIndexById(answerCategory)].objectsGivenNum++;	
		
		List<Integer> intersect = new ArrayList<Integer>(dominantCatsIds);
		intersect.retainAll(answerCatsIds);
		
		for (Integer category : intersect)
			result.categoryResults[categoriesData.getCategoryIndexById(category)].objectsCorrectNum++;
			
		int unionSize = answerCatsIds.size() + dominantCatsIds.size() - intersect.size();
		result.emotionAccuracy += 1.0 * intersect.size() / unionSize; //Jaccard measure

		return result;
	}
	
	public static double getJaccardAccuracy(
			List<Integer> dominantCatsIds, // obtained data
			List<Integer> categoryLabels, // test data
			CategoryDataType catDataType)
					throws Exception
	{
		if (dominantCatsIds.size() == 0)
		{
			dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
		}
		else
		{
			if (dominantCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
			{
				dominantCatsIds = new ArrayList<Integer>();
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
		}
			
		Set<Integer> answerCatsIds = new HashSet(categoryLabels);
		List<Integer> intersect = new ArrayList<Integer>(dominantCatsIds);
		intersect.retainAll(answerCatsIds);
		
			
		int unionSize = answerCatsIds.size() + dominantCatsIds.size() - intersect.size();
		return 1.0 * intersect.size() / unionSize; //Jaccard measure
	}
	
	/**
	 * 
	 * @param dominantCatsIds
	 * @param categoryLabels have to be different!
	 * @param catDataType
	 * @param result
	 * @throws Exception
	 */
	public static void getEvaluationOverSimplified(
			List<Integer> dominantCatsIds, // obtained data
			List<Integer> categoryLabels, // test data
			CategoryDataType catDataType, EvaluationResult result)
					throws Exception
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		result.evaluatedObjectsNum++;
		result.evaluatedAssignNum += categoryLabels.size();
		if (dominantCatsIds.size() == 0)
		{
			dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
		}
		else
		{
			if (dominantCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
			{
				dominantCatsIds = new ArrayList<Integer>();
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
			else
				result.foundEmotionPercentage++;
			
		}
		
		for (Integer dominantCategory : dominantCatsIds)
			result.categoryResults[categoriesData.getCategoryIndexById(dominantCategory)].objectsFoundNum++;
		
			
		for (Integer answerCategory : categoryLabels)
			result.categoryResults[categoriesData.getCategoryIndexById(answerCategory)].objectsGivenNum++;	
		
		int intersectSize = 0;
		for (Integer category : dominantCatsIds)
		{
			if (categoryLabels.contains(category))
			{
				result.categoryResults[categoriesData.getCategoryIndexById(category)].objectsCorrectNum++;
				intersectSize++;
			}
		}
			
		int unionSize = categoryLabels.size() + dominantCatsIds.size() - intersectSize;
		result.emotionAccuracy += 1.0 * intersectSize / unionSize; //Jaccard measure
	}

	
	public static class FoundData implements Serializable
	{
		private static final long serialVersionUID = 7779275281236184269L;
		public int foundNum = 0;
		public int correctFoundNum = 0;
		
		
		public void plus(FoundData res2) {
			if (res2 == null)
				return;
			foundNum += res2.foundNum;
			correctFoundNum += res2.correctFoundNum;
		}
	}
	
	static int stepNum = 10;
	static double stepDelta = 0.1;

	private static FoundData[][] getCurvePoints(
			double[] categoryWeightsAnswers, 
			Collection<Integer> categoryLabels, 
			ICategoriesData categoriesData)
	{
		 FoundData[][] categoryFoundThresholdPoints = new FoundData[categoryWeightsAnswers.length][];
		 
		 double[] thresholdWeights = null;
		 
		 if (UtilArrays.getSum(categoryWeightsAnswers) != 0.0)
			 thresholdWeights = UtilArrays.multiplyArray(categoryWeightsAnswers, 1 / UtilArrays.getMax(categoryWeightsAnswers));		 
		 else
		 {
			 thresholdWeights = new double[categoryWeightsAnswers.length];
			 Arrays.fill(thresholdWeights, 0.0);
		 }
		 
		 for ( int catInd = 0; catInd < categoryWeightsAnswers.length; ++catInd)
		 {
			 categoryFoundThresholdPoints[catInd] = new FoundData[stepNum];
			 for ( int i = 1; i <= stepNum; ++i)
			 {
				 double alpha = stepDelta  * i;
				 if (thresholdWeights[catInd] >= alpha)
				 {
					 categoryFoundThresholdPoints[catInd][i - 1] = new FoundData();
					 categoryFoundThresholdPoints[catInd][i - 1].foundNum++;
					 if (categoryLabels.contains(categoriesData.getCategoryId(catInd)))
						 categoryFoundThresholdPoints[catInd][i - 1].correctFoundNum++;
				 }
				 else
					 break;
			 }
		 }
		 
		 return categoryFoundThresholdPoints;
	}
	
	/**
	 *  in this case we assume that the tested object has the label related to only one category - either present (+id) or absent (-id)
	 * @param categoryWeightsAnswers
	 * @param dominantCatsIds
	 * @param strengthLabelAnswer
	 * @param consideredCategoryLabel
	 * @param categoryStrengthLabel
	 * @param catDataType
	 * @return
	 * @throws Exception
	 */
	public static EvaluationResult getBinaryEvaluation(
			double[] categoryWeightsAnswers, List<Integer> dominantCatsIds, int strengthLabelAnswer, // obtained data
			Integer consideredCategoryLabel, Integer categoryStrengthLabel,  // test data
			CategoryDataType catDataType)
					throws Exception
	{
		EvaluationResult result = new EvaluationResult(catDataType);
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		result.evaluatedObjectsNum++;
		result.evaluatedAssignNum++; 
		boolean isNeutral = false;
		
		if (dominantCatsIds.size() == 0)
		{
			if (UtilArrays.getSum(categoryWeightsAnswers) == 0.0)
			{
				isNeutral = true;
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
		}
		else
		{
			if (dominantCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
			{
				isNeutral = true;
				dominantCatsIds = new ArrayList<Integer>();
				dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));//add neutral category
			}
			else
				result.foundEmotionPercentage++;
		}
		
		if (dominantCatsIds.size() == 0)
		{
			isNeutral = true;
			dominantCatsIds = new ArrayList<Integer>();
			dominantCatsIds.add(CategoryProcessor.neutralCategory(catDataType));
			
		}
		
		int focusCategoryId = Math.abs(consideredCategoryLabel);
		if (dominantCatsIds.contains(focusCategoryId))
			result.categoryResults[categoriesData.getCategoryIndexById(focusCategoryId)].objectsFoundNum++;
			
		if (consideredCategoryLabel > 0)
			result.categoryResults[categoriesData.getCategoryIndexById(focusCategoryId)].objectsGivenNum++;
			
		Set<Integer> answerCatsIds = new HashSet();
		if (consideredCategoryLabel > 0)
			answerCatsIds.add(consideredCategoryLabel);
		else
			answerCatsIds.add(categoriesData.neutralCategory());	
		
		List<Integer> intersect = new ArrayList<Integer>(dominantCatsIds);
		intersect.retainAll(answerCatsIds);
		
		for (Integer category : intersect)
			result.categoryResults[categoriesData.getCategoryIndexById(category)].objectsCorrectNum++;
		
		result.emotionPrecisionOnAll = 1.0 * intersect.size() / dominantCatsIds.size();
		result.emotionPrecisionOnOne = (intersect.size() > 0) ? 1.0 : 0.0;
		
		
		if (!isNeutral)
		{
			result.emotionPrecisionOnOneWithoutNeutral = result.emotionPrecisionOnOne;
			result.emotionPrecisionOnAllWithoutNeutral = result.emotionPrecisionOnAll;
			result.averageFoundEmotionsWithoutNeutral = dominantCatsIds.size();
		}
		
		if (!answerCatsIds.contains(CategoryProcessor.neutralCategory(catDataType)))
		{
			// consider only tweets with non-neutral emotion
			result.givenEmotionPercentage++;
			
			result.emotionRecallWithSet = 1.0 * intersect.size() / answerCatsIds.size();
			result.emotionRecallWithList = result.emotionRecallWithSet;
		}
			
		int unionSize = answerCatsIds.size() + dominantCatsIds.size() - intersect.size();
		result.emotionAccuracy = 1.0 * intersect.size() / unionSize; //Jaccard measure
		
		
		// compute accuracy of strength
		if (categoryStrengthLabel != null)
		{
			int realLabel = WeightedClassifierLexiconBasedWithIntensityModel.getClosestStrengthLabel(categoryStrengthLabel);
			
			result.emotionStrengthAccuracy = (realLabel == strengthLabelAnswer)?1.0:0.0;
		}
		else
			result.emotionStrengthAccuracy = 0.0;
		
		//evaluate polarity statistics
		List<Integer> workerPolarityAnswers = UtilCollections.findKeysWithMaxValues(UtilCollections.computeFrequencies(CategoryUtil.getCategoryPolarities(answerCatsIds, catDataType)));
		if (workerPolarityAnswers.size() == 1)
		{
			result.definitePolarityPercentage++;
			int polarityAnswer = workerPolarityAnswers.get(0);
			
			if (polarityAnswer != 0)
				result.nonNeutralPolarityPercentage++;
			
			List<Integer> algPolarityResults = CategoryUtil.getDominantPolarityList(categoryWeightsAnswers, catDataType.toString());
			int polarityResult = algPolarityResults.get(0);
			
			if (algPolarityResults.size() == 1 && polarityResult == 0)
			{
				result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsFoundNum++;
				if (polarityAnswer == 0)
					result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsCorrectNum++;
			}
			
			if (algPolarityResults.size() > 1 || isNeutral || polarityResult < -1)
			{
				// no strict output - don't use in recall or precision
				polarityResult = 0;
			}
			else
			{
				// strict output - use in recall and precision
				result.polarityRecall++;
				if (polarityResult == polarityAnswer) // correct answer
				{
					result.polarityPrecision++;
					if (polarityAnswer != 0)
						result.polarityStrictRecall++;
					result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsCorrectNum++;
				}
				result.polarityResults[categoriesData.getPolarityIndex(polarityResult)].objectsFoundNum++;
			}
			result.polarityAccuracy = (polarityResult == polarityAnswer) ? 1.0 : 0.0;
			
			result.polarityResults[categoriesData.getPolarityIndex(polarityAnswer)].objectsGivenNum++;
		}
		
		//evaluate quadrant statistics
		List<Integer> workerQuadrantAnswers = UtilCollections.findKeysWithMaxValues(
				UtilCollections.computeFrequencies(CategoryUtil.getCategoryQuadrants(answerCatsIds, catDataType)));
		if (workerQuadrantAnswers.size() == 1)
		{
			result.definiteQuadrantPercentage++;
			int quadrantAnswer = workerQuadrantAnswers.get(0);
			
			if (quadrantAnswer != 0)		
				result.nonNeutralQuadrantPercentage++;
		
			
			List<Integer> algQuadrantResults = CategoryUtil.getDominantQuadrantList(categoryWeightsAnswers, catDataType.toString());
			
			int quadrantResult = algQuadrantResults.get(0);
					
			if (algQuadrantResults.size() > 1 || isNeutral || quadrantResult < 0) 
			{
				// no strict output - don't use in recall or precision; including the context-dependent (<0)
				quadrantResult = 0;
			}
			else
			{
				// strict output - use in recall and precision
				result.quadrantRecall++;
				if (quadrantResult == quadrantAnswer) // correct answer
				{
					result.quadrantPrecision++;
					if (quadrantAnswer != 0)	
						result.quadrantStrictRecall++;
					result.quadrantResults[quadrantResult].objectsCorrectNum++;
				}
				result.quadrantResults[quadrantResult].objectsFoundNum++;
			}
			result.quadrantAccuracy = (quadrantResult == quadrantAnswer) ? 1.0 : 0.0;
			
			result.quadrantResults[quadrantAnswer].objectsGivenNum++;
		}
		
		// evaluate the found objects for varying threshold (needed for ROC and PR curves)
		FoundData[][] foundPoints = getCurvePoints(categoryWeightsAnswers, answerCatsIds, categoriesData);
		for (int catInd = 0; catInd < foundPoints.length; ++catInd)
		{
			result.categoryResults[catInd].thresholdFoundNums = foundPoints[catInd];
		}
		
		// add points to the confusion matrix
		for (Integer answerCategory : answerCatsIds)
			for (Integer dominantCategory : dominantCatsIds)
				experimentConfusionMatrix
				[categoriesData.getCategoryIndexById(answerCategory)]
						[categoriesData.getCategoryIndexById(dominantCategory)]++;
		
		return result;
	}
	
	
}
