/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

/**
 * The parameters for transforming weighted emotion distribution into the label set
 *
 */
public class MultiLabelRefineParams implements IParameters {
	
	public double alphaCut;
	public int emotionLimit;
	
	public MultiLabelRefineParams(double alphaCut, int emotionLimit)
	{
		this.alphaCut = alphaCut;
		this.emotionLimit = emotionLimit;
	}

	public MultiLabelRefineParams() {};
	
	public String printParametersShort()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("-aCut-" + alphaCut);
		sb.append("-emL-" + emotionLimit);
		return sb.toString();
	}
}
