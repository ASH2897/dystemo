/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import learners.given.ClassifierLearnerFactory.LearnerName;

public class AllLearningParameters implements IParameters {
	
	public static AllLearningParameters currentSetup;

	public FeatureExtractionParameters featureExtractionParams;
	public FeatureSelectionParameters featureSelectionParams;
	public LearnerName learnerName;
	public ClassifierLearnerParams learnerParams;
	public String trainDataName;
	
	public AllLearningParameters()
	{
		featureExtractionParams = new FeatureExtractionParameters();
		featureSelectionParams = new FeatureSelectionParameters();
		learnerName = null;
		learnerParams = null;
	}
	
	public AllLearningParameters(FeatureExtractionParameters featureExtractionParams, 
			FeatureSelectionParameters featureSelectionParams, 
			LearnerName learnerName,
			ClassifierLearnerParams learnerParams,
			String trainDataName)
	{
		this.featureExtractionParams = featureExtractionParams;
		this.featureSelectionParams = featureSelectionParams;
		this.learnerName = learnerName;
		this.learnerParams = learnerParams;
		this.trainDataName = trainDataName;
	}


	@Override
	public String printParametersShort() {
		return printValues("\t", true);
	}
	
	
	public static String printNames(String separator)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("trainDataName");
		sb.append(separator);
		sb.append("featureExtractionParams");
		sb.append(separator);
		sb.append("featureSelectionParams");
		sb.append(separator);
		sb.append("LearnerName");
		sb.append(separator);
		sb.append("ClassifierLearnerParams");
		sb.append(separator);
		
		return sb.toString();
	}
	
	public String printValues(String separator, boolean putShortName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append((putShortName?"TrainData=":""));
		sb.append(trainDataName);
		sb.append(separator);
		sb.append((putShortName?"FeatExtrAlg=":""));
		sb.append(featureExtractionParams.printParametersShort());
		sb.append(separator);
		sb.append((putShortName?"FeatSelAlg=":""));
		sb.append(featureSelectionParams.printParametersShort());
		sb.append(separator);
		sb.append((putShortName?"LearnerName=":""));
		if (learnerName == null)
			sb.append("null");
		else
			sb.append(learnerName.name());
		sb.append(separator);
		sb.append((putShortName?"LearnerParams=":""));
		if (learnerParams == null)
			sb.append("null");
		else
			sb.append(learnerParams.printParametersShort());
		sb.append(separator);
		
		return sb.toString();
	}
	
	public AllLearningParameters clone()
	{
		AllLearningParameters newParams = new AllLearningParameters();
		newParams.featureExtractionParams = this.featureExtractionParams.clone();
		newParams.featureSelectionParams = this.featureSelectionParams.clone();
		newParams.learnerName = this.learnerName;
		newParams.learnerParams = this.learnerParams.clone();
		newParams.trainDataName = this.trainDataName;
		return newParams;
	}
}
