/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;

public class ClassifierApplicationParameters implements IParameters {

	public TermDetectionParameters parametersOfTermDetectionForClassifier;
	public HierarchyApplyOption hierarchyOption = HierarchyApplyOption.None; // specifies how to deal with hierarchy in weighted classifiers (that is, if specified - first the dominant low-level hierarchy is detected (by the sum of the weights), and then only emotions corresponding to that one are returned
	public MultiLabelRefineParams multiLabelOutputRefinementParams; // applied in case of general classifier
	public ProbabilisticOutputTreatment probabilisticBinaryOutputTreatmentParams; //applied in case of binary independent classifiers
	
	public ClassifierApplicationParameters(){};
	public ClassifierApplicationParameters(boolean treatNegations, boolean ignoreHashtags)
	{
		this.parametersOfTermDetectionForClassifier = new TermDetectionParameters(treatNegations,  ignoreHashtags);

	}
	
	public ClassifierApplicationParameters(TermDetectionParameters parametersOfTermDetectionForClassifier)
	{
		this.parametersOfTermDetectionForClassifier = parametersOfTermDetectionForClassifier;
	}
	
	
	
	public ClassifierApplicationParameters( ClassifierApplicationParameters initial)
	{
		this.hierarchyOption = initial.hierarchyOption;
		this.multiLabelOutputRefinementParams = initial.multiLabelOutputRefinementParams;
		if (initial.parametersOfTermDetectionForClassifier != null)
			this.parametersOfTermDetectionForClassifier = new TermDetectionParameters(initial.parametersOfTermDetectionForClassifier);
		if (initial.probabilisticBinaryOutputTreatmentParams != null)
			this.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(initial.probabilisticBinaryOutputTreatmentParams.useProbabilities, initial.probabilisticBinaryOutputTreatmentParams.probabilityThreshold);
	};
	
	
	@Override
	public String printParametersShort() {
		return printValues(";", true);
	}
	
	public String printValues(String separator, boolean putShortName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(putShortName?"termDet=":"");
		sb.append(parametersOfTermDetectionForClassifier.printParametersShort());
		sb.append(separator);
		if (hierarchyOption != HierarchyApplyOption.None)
		{
			sb.append((putShortName?"hierO=":""));
			sb.append(hierarchyOption.toString());
			sb.append(separator);
		}
		
		sb.append(putShortName?"OutRef=":"");
		if (multiLabelOutputRefinementParams != null)
		{
			sb.append(multiLabelOutputRefinementParams.printParametersShort());
		}
		else
			sb.append("-NoMLRef");
		sb.append(separator);
		if (probabilisticBinaryOutputTreatmentParams != null)
			if (probabilisticBinaryOutputTreatmentParams.useProbabilities)
				sb.append("-PrT-" + probabilisticBinaryOutputTreatmentParams.probabilityThreshold);
		sb.append(separator);
	
		return sb.toString();
	}
	
	public static class ClassifierApplicationParamsEnumerationList implements IParametersEnumerationList
	{
		public MultiLabelRefineParams[] outputMultiLabelOptions;
		public TermDetectionParameters[] termDetectionOptions;
		public HierarchyApplyOption[] hierarchyOptions;
		public ProbabilisticOutputTreatment[] probabilisticOutputTreatmentOptions;
	}
	
	public static ClassifierApplicationParameters getDefaultClassifierApplicationParams() {
		ClassifierApplicationParameters params = new ClassifierApplicationParameters();
		params.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);//new TermDetectionParameters(true,  true);
		params.multiLabelOutputRefinementParams = null;//new MultiLabelRefineParams(1.0, 30);
		return params;
	}

}
