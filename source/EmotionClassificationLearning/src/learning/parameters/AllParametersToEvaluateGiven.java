/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

public class AllParametersToEvaluateGiven extends AllLearningParameters{

	public String evaluatedClassifierName;
	public ClassifierApplicationParameters classifierApplicationParams;
	
	public AllParametersToEvaluateGiven()
	{
		featureExtractionParams = null;
		featureSelectionParams = null;
		learnerName = null;
		learnerParams = null;
		trainDataName = null;
	}
	
	
	public static String printNames(String separator)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("givenClassifierName");
		sb.append(separator);
		sb.append("givenClassifierApplicationParams");
		sb.append(separator);
		
		return sb.toString();
	}
	
	@Override
	public String printParametersShort() {
		return printValues("\t", true);
	}
	
	public String printValues(String separator, boolean putShortName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append((putShortName?"classifierName=":""));
		sb.append(evaluatedClassifierName);
		sb.append(separator);
		sb.append((putShortName?"classifierApplicationParams=":""));
		sb.append(classifierApplicationParams.printParametersShort());
		sb.append(separator);
		
		return sb.toString();
	}
	
}
