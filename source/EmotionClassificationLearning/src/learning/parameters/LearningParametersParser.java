/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import semisupervisedlearning.parameters.SSLFrameworkParameters;
import utils.RebalancePreprocess;

import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import linguistic.NegationDetector.NegationTreatmentType;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import learners.given.ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams;
import learners.given.ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams;

public class LearningParametersParser {

	public static void applyExtraStandardParams (AllLearningParameters fullLearningParams )
	{
		fullLearningParams.featureExtractionParams.useCache = true; // if true, will need to set up currentSetup parameters as well
		
		
	}
	
	public static AllLearningParameters parseAndLoadAllLearningParameters(Map<String,String> readParametersMap) throws Exception
	{
		/*
		 * Example entries: 
		 * TrainData=MixedHashtagEmojiDataLabeled_250000
		 * FeatExtrAlg=Ngrams:-maxN-2-minO-5-noSt-1-tNeg-1
		 * FeatSelAlg=PolSel-0,EmtSel-0
		 * LearnerName=IndependentWekaLogReg
		 * LearnerParams=:InRef:-No-:OutRef:-aCut-1.0-emL-30,-lT-WekaLogReg-lexP--FS:-alg-pmi-thr-0.1-minO-5-iN-0-appSm-1:-PrT-0.7-simUse-IncludeOut	

		 */
		
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		String featureExtractionParamsStr = readParametersMap.get("featureExtractionParams");
		int featureTypeNum = countNumberOfEntriesOfSubstring(featureExtractionParamsStr, ":"); 
		// for now only one feature type is parsed: Ngrams
		if (featureExtractionParamsStr.startsWith("Ngrams:"))
		{
			// if to extract ngrams (example: Ngrams:-maxN-2-minO-5-noSt-1-tNeg-1)
			fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
			fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN =  Integer.parseInt(getPatternEntry("maxN-([\\w]+?)-", featureExtractionParamsStr));
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = Integer.parseInt(getPatternEntry("minO-([\\w]+?)-", featureExtractionParamsStr));
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = Integer.parseInt(getPatternEntry("noSt-([\\w]+?)-", featureExtractionParamsStr)) > 0;
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection =  TermDetectionParameters.parseParametersStr(featureExtractionParamsStr);
			fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = featureExtractionParamsStr.contains("sevNonOv-1");
		}
		else
			throw new Exception ("Other features extraction will not be parsed. Implement this parsing first");
		
		String featureSetectionParamsStr = readParametersMap.get("featureSelectionParams");
		// some examples: PolSel-0,EmtSel-0      PolSel-1-alg-pmi-thr-0.1-minO-5-iN-0-appSm-1,EmtSel-0	PolSel-1-alg-pmi-thr-0.9-minO-5-iN-0-appSm-1,EmtSel-1-alg-pmi-thr-0.5-minO-5-iN-0-appSm-1-onlyPosV-1,OccSel-1-minOcc-5
		// for now only the PolarityBased selection is parsed!
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		
		List<SelectionType> selectionTypesToApply = new ArrayList<SelectionType>();
		//TODO: what if the order if different?
		if (featureSetectionParamsStr.startsWith("PolSel-1"))
		{
			selectionTypesToApply.add(SelectionType.OnPolarity);
			String curFeatureSelectionSubstring = featureSetectionParamsStr.substring(featureSetectionParamsStr.indexOf("PolSel-"), featureSetectionParamsStr.indexOf("EmtSel-"));
			fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
			fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionParameters.findFeatureSelectionAlgorithmByName(getPatternEntry("alg-([\\w]+?)(?=-|$|,)", curFeatureSelectionSubstring));
			fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = Double.parseDouble(getPatternEntry("thr-(\\d.[\\d]+)-", curFeatureSelectionSubstring));
			fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = Integer.parseInt(getPatternEntry("minO-([\\w]+?)-", curFeatureSelectionSubstring));
			fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = Integer.parseInt(getPatternEntry("-appSm-([\\w]+?)(?=-|$|,)", curFeatureSelectionSubstring)) > 0;
			fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = Integer.parseInt(getPatternEntry("-iN-([\\w]+?)-", curFeatureSelectionSubstring)) > 0;
			fullLearningParams.featureSelectionParams.polaritySelectionParams.useOnlyPositive = curFeatureSelectionSubstring.contains("-onlyPosV-1");
		}
		if (featureSetectionParamsStr.contains("EmtSel-1"))
		{
			selectionTypesToApply.add(SelectionType.OnEmotionality);
			int end = featureSetectionParamsStr.indexOf("OccSel-");
			if (end == -1)
				end = featureSetectionParamsStr.length();
			String curFeatureSelectionSubstring = featureSetectionParamsStr.substring(featureSetectionParamsStr.indexOf("EmtSel-"), end);
			fullLearningParams.featureSelectionParams.emotionalitySelectionParams = new BinarySelectionParams();
			fullLearningParams.featureSelectionParams.emotionalitySelectionParams.selectionAlg = FeatureSelectionParameters.findFeatureSelectionAlgorithmByName(getPatternEntry("alg-([\\w]+?)(?=-|$|,)", curFeatureSelectionSubstring));
			if (fullLearningParams.featureSelectionParams.emotionalitySelectionParams.selectionAlg != FeatureSelectionAlgOption.No)
			{
				fullLearningParams.featureSelectionParams.emotionalitySelectionParams.scoreThreshold = Double.parseDouble(getPatternEntry("thr-(\\d.\\d)-", curFeatureSelectionSubstring));
				fullLearningParams.featureSelectionParams.emotionalitySelectionParams.minOccurNum = Integer.parseInt(getPatternEntry("minO-([\\w]+?)-", curFeatureSelectionSubstring));
				fullLearningParams.featureSelectionParams.emotionalitySelectionParams.applySmoothing = Integer.parseInt(getPatternEntry("-appSm-([\\w]+?)(?=-|$|,)", curFeatureSelectionSubstring)) > 0;
				fullLearningParams.featureSelectionParams.emotionalitySelectionParams.includeNeutral = Integer.parseInt(getPatternEntry("-iN-([\\w]+?)-",curFeatureSelectionSubstring)) > 0;
				fullLearningParams.featureSelectionParams.emotionalitySelectionParams.useOnlyPositive = curFeatureSelectionSubstring.contains("-onlyPosV-1");
			}
		}
		if (featureSetectionParamsStr.contains("OccSel-1"))
		{
			selectionTypesToApply.add(SelectionType.OnOccurrence);
			String curFeatureSelectionSubstring = featureSetectionParamsStr.substring(featureSetectionParamsStr.indexOf("OccSel-"));
			fullLearningParams.featureSelectionParams.minOccurrenceThreshold = Integer.parseInt(getPatternEntry("minOcc-([\\w]+?)(?=-|$|,)", curFeatureSelectionSubstring));
		}
		
		
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[selectionTypesToApply.size()];
		for (int selTInd = 0; selTInd < selectionTypesToApply.size(); ++selTInd)
		{
			fullLearningParams.featureSelectionParams.selectionTypesToApply[selTInd] =  selectionTypesToApply.get(selTInd);
		}
		
		
		String learnerNameStr = readParametersMap.get("LearnerName");
		fullLearningParams.learnerName = LearnerName.valueOf(learnerNameStr);
	
		String classifierLearnerParamsStr = readParametersMap.get("ClassifierLearnerParams");
		// examples
		// for IndependentWekaLogReg :InRef:-No-:OutRef:-aCut-1.0-emL-30,-lT-WekaLogReg-lexP--FS:-alg-pmi-thr-0.1-minO-5-iN-0-appSm-1:-PrT-0.7-simUse-IncludeOut
		// for IndependentPMI    :InRef:-No-:OutRef:-aCut-1.0-emL-30,-lT-PMI-lexP--T0.1-S1-FS:-alg-no:-simUse-IncludeOut
		// for IndependentWekaMNB   :InRef:-No-:OutRef:-aCut-1.0-emL-30,-lT-WekaMNB-lexP--FS:-alg-no:-simUse-IncludeOut
		// for BalancedWeightedVoting: :Blc:-blcT-0:InRef:-aCut-1.0-emL-30:ApplParams:termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-aCut-1.0-emL-30;;,-LexAmb-0-NoLowProb-0,-blcT-0
		// :InRef:-aCut-0.9-emL-30:ApplParams:termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-aCut-1.0-emL-30;;,-wekaCl-LogisticRegression
		//:InRef:-aCut-0.9-emL-30:ApplParams:termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-NoMLRef;-PrT-0.9;,-lT-WekaMNB-lexP--FS:-alg-pmi-thr-0.1-minO-5-iN-0-appSm-1:-simUse-IncludeOut

		
		
		if (learnerNameStr.startsWith("Independent"))
		{
			IndependentLearnerParameters curParams = new IndependentLearnerParameters(fullLearningParams.learnerName);
			 
			curParams.closeCategoryUseType = CloseCategoryUseType.valueOf(getPatternEntry("-simUse-([\\w]+?)(?=-|$)", classifierLearnerParamsStr));
			if (curParams.closeCategoryUseType == CloseCategoryUseType.IncludeOut)
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
			else
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.valueOf(getPatternEntry("-simType-([\\w]+?)(?=-|$)", classifierLearnerParamsStr));
			if (classifierLearnerParamsStr.contains("-useDep")) {
				curParams.useDependentClassifierForOutput = true;
			}
			
			curParams.binaryLearnerParameters.binaryLexiconParams = getPatternEntry("-lexP-(.*?)-FS:", classifierLearnerParamsStr); 
			int startOfBinFS = classifierLearnerParamsStr.indexOf("-FS:");
			String binaryFeatureSelectionParamsStr =  classifierLearnerParamsStr.substring(startOfBinFS, classifierLearnerParamsStr.indexOf(":", startOfBinFS + 4 ));
			curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg = FeatureSelectionParameters.findFeatureSelectionAlgorithmByName(getPatternEntry("alg-([\\w]+?)(?=-|:|$)", binaryFeatureSelectionParamsStr));
			if (curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg != FeatureSelectionAlgOption.No)
			{
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.scoreThreshold = Double.parseDouble(getPatternEntry("thr-(\\d.\\d)-", binaryFeatureSelectionParamsStr));
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.minOccurNum = Integer.parseInt(getPatternEntry("minO-([\\w]+?)-", binaryFeatureSelectionParamsStr));
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.applySmoothing = Integer.parseInt(getPatternEntry("-appSm-([\\w]+?)(?=-|$|,)", binaryFeatureSelectionParamsStr)) > 0;
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.includeNeutral = Integer.parseInt(getPatternEntry("-iN-([\\w]+?)-", binaryFeatureSelectionParamsStr)) > 0;
			}
			
			fullLearningParams.learnerParams = curParams;
		}
		else if (learnerNameStr.contains("CategoryWeka"))
		{
			fullLearningParams.learnerParams = new WekaCategoryClassifierLearnerParams(fullLearningParams.learnerName);
		}
		else if (learnerNameStr.equals( LearnerName.WeightedBalancedVoting.toString()))
		{
			if (!classifierLearnerParamsStr.contains(":Blc:"))
				throw new Exception("Rebalancing was not set for WeightedBalancedVoting!");
			else
			{
				BalanceWeightType balanceType = BalanceWeightType.values()[Integer.parseInt(getPatternEntry("-blcT-([\\w]+?)(?=-|$|,|:)", classifierLearnerParamsStr))];
				RebalancingParameters rebalancingParams = new RebalancingParameters(balanceType);
				fullLearningParams.learnerParams = new WeightedBalancedVotingLearnerParams(rebalancingParams, false, false);
			}
			
			
		}
		else if (learnerNameStr.equals( LearnerName.WeightedVoting.toString()))
		{
			fullLearningParams.learnerParams = new WeightedVotingLearnerParams(false, false);
		}
		else
			throw new Exception("Parsing of other algorithms parameters is not yet implemented!");
		
		
		if (classifierLearnerParamsStr.contains(":InRef:-No-"))
			fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
		else
		{
			int start = classifierLearnerParamsStr.indexOf(":InRef:");
			String inRefParamsStr = classifierLearnerParamsStr.substring(start, classifierLearnerParamsStr.indexOf(":",start + 7));
			//example: -aCut-1.0-emL-30 
			double alphaML = Double.parseDouble(getPatternEntry("-aCut-(\\d.\\d)-", inRefParamsStr));
			int alphaLim = Integer.parseInt(getPatternEntry("emL-([\\w]+?)(?=-|$)",inRefParamsStr));
			fullLearningParams.learnerParams.multiLabelInitialRefinementParams =  new MultiLabelRefineParams( alphaML, alphaLim);
		}
		
		fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
		
		if (classifierLearnerParamsStr.contains("OutRef=-NoMLRef"))
			fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = null;
		else
		{
			int start = classifierLearnerParamsStr.indexOf("OutRef=");
			int end = classifierLearnerParamsStr.indexOf(",", start);
			if (end == -1)
				 end = classifierLearnerParamsStr.indexOf(";", start);
			String outRefParamsStr = classifierLearnerParamsStr.substring(start, end);
			//example: -aCut-1.0-emL-30
			double alphaML = Double.parseDouble(getPatternEntry("-aCut-(\\d.\\d)-", outRefParamsStr));
			int alphaLim = Integer.parseInt(getPatternEntry("emL-([\\w]+?)(?=-|$|;|,)",outRefParamsStr));
			fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams( alphaML, alphaLim);
		}
		
		if (classifierLearnerParamsStr.contains(":extra:"))
		{
			int extraBegin = classifierLearnerParamsStr.indexOf(":extra:");
			fullLearningParams.learnerParams.specificLearnerParams = classifierLearnerParamsStr.substring(extraBegin + 7, classifierLearnerParamsStr.indexOf("::",extraBegin));
		}
		else
			fullLearningParams.learnerParams.specificLearnerParams = null;
		
		if (classifierLearnerParamsStr.contains(":Blc:-blcT-") )
		{
			BalanceWeightType balanceType = BalanceWeightType.values()[Integer.parseInt(getPatternEntry("-blcT-([\\w]+?)(?=-|$|,|:)", classifierLearnerParamsStr))];
			fullLearningParams.learnerParams.rebalancingParameters = new RebalancingParameters(balanceType);
		}
		else			
			fullLearningParams.learnerParams.rebalancingParameters = null;

		// parse application parameters:
		if (classifierLearnerParamsStr.contains(":ApplParams:"))
		{
			int start = classifierLearnerParamsStr.indexOf(":ApplParams:");
			String applicParamsSubstr = classifierLearnerParamsStr.substring(start, classifierLearnerParamsStr.indexOf(";",start));
			// extract the term Detection params
			fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = TermDetectionParameters.parseParametersStr(applicParamsSubstr);
			// for independent
			if (classifierLearnerParamsStr.contains("-PrT-"))
			{
				double probThresholdValue = Double.parseDouble(getPatternEntry("-PrT-(\\d.\\d)(?=-|$|,|;)", classifierLearnerParamsStr));
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(true, probThresholdValue);
			}
			else
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = null;
		}
		String trainDataStr = readParametersMap.get("trainDataName");
		if (trainDataStr.equals("null"))
			trainDataStr = null;
		fullLearningParams.trainDataName = trainDataStr;
		
		
		// check if it is actually the parameters for SSL
		if (readParametersMap.containsKey("sslInitialClassifier") && readParametersMap.get("sslInitialClassifier") != null)
		{
			String initialClassifier = readParametersMap.get("sslInitialClassifier");
			String sslFullParams = readParametersMap.get("sslFullParams");
			
			
			//parse full parameters;
			// format: DT=SSL-Olympic-100000-Neutral;IC=PMI-Hash;FramWSet=mxI1;;corNeut=11;treatNeg=-negLens-2-negType-3;initClassP=termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-NoMLRef;;;
			AllSemiSupervisedLearningParameters sslFullLearningParams = new AllSemiSupervisedLearningParameters();
			sslFullLearningParams.featureExtractionParams = fullLearningParams.featureExtractionParams;
			sslFullLearningParams.featureSelectionParams = fullLearningParams.featureSelectionParams;
			sslFullLearningParams.learnerName = fullLearningParams.learnerName;
			sslFullLearningParams.learnerParams  = fullLearningParams.learnerParams;
			sslFullLearningParams.trainDataName = fullLearningParams.trainDataName;
			
			SSLFrameworkParameters sslParams = new SSLFrameworkParameters();
			
			
			sslParams.initialClassifierName = initialClassifier;
			sslParams.initialClassifierApplicationParams = new ClassifierApplicationParameters();
			int initCLStart = sslFullParams.indexOf("initClassP=");
			String initClParams = sslFullParams.substring(initCLStart, sslFullParams.indexOf(";", initCLStart));
			sslParams.initialClassifierApplicationParams.parametersOfTermDetectionForClassifier = TermDetectionParameters.parseParametersStr(initClParams);
			sslParams.initialClassifierApplicationParams.multiLabelOutputRefinementParams = null;
			
			sslParams.maxIterationNum = Integer.parseInt(getPatternEntry("mxI([\\w]+?)(?=-|$|;)",sslFullParams));
			sslParams.unlabeledDataName = getPatternEntry("DT=([\\w-_]+?)(?=$|;)",sslFullParams);
			
			sslFullLearningParams.sslParameters = sslParams;
			return sslFullLearningParams;
			
		}
		
		
		return fullLearningParams;
	}
	
	private static String getPatternEntry(String regexPattern, String text)
	{
		
		Matcher m = Pattern.compile(regexPattern).matcher(text);
		
		while(m.find())
		{
			String x = new String(m.group(1));
			return x;
		}
		
		return null;
	}
	
	private static int countNumberOfEntriesOfSubstring(String text, String substr)
	{
		int curNum = 0;
		int curI = -1;
		while ((curI = text.indexOf(substr, curI + 1)) != -1)
			curNum++;
		return curNum;
	}
}
