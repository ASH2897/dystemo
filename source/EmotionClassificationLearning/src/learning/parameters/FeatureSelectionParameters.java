/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

public class FeatureSelectionParameters  implements IParameters {

	public SelectionType[] selectionTypesToApply;
	
	
	public enum FeatureSelectionAlgOption {PMI, IG, PMISimple, No, Percentage};
	
	public static FeatureSelectionAlgOption findFeatureSelectionAlgorithmByName(String value)
	{
		for (FeatureSelectionAlgOption alg : FeatureSelectionAlgOption.values())
		{
			if (value.toLowerCase().equals(alg.toString().toLowerCase()))
				return alg;
		}
		return null;
	}
	
	/**
	 * Describes possible types of feature selection
	 * OnPolarity = select terms that are either positive or negative
	 * OnEmotionality = select terms that appear more often among emotional classes
	 * OnEmotion = select terms that are correlated with some emotion
	 * OnOccurrence = select terms that have occurred more times than the specified threshold on the labeled data (this feature selection is needed when full data given and the annotated data given are different, and thus feature extraction might not have eliminated those terms that appeared not enough times on annotated data)
	 */
	public enum SelectionType {OnPolarity, OnEmotionality, OnEmotion, OnOccurrence};
	
	public BinarySelectionParams polaritySelectionParams;
	public BinarySelectionParams emotionalitySelectionParams;
	
	public int minOccurrenceThreshold = -1;
	
	//public double emotionalitySelectionMin;
	
	public FeatureSelectionParameters clone()
	{
		FeatureSelectionParameters newParameters  = new FeatureSelectionParameters();
		newParameters.selectionTypesToApply = this.selectionTypesToApply;
		if (this.polaritySelectionParams != null)
			newParameters.polaritySelectionParams = new BinarySelectionParams (this.polaritySelectionParams);
		if (this.emotionalitySelectionParams != null)
			newParameters.emotionalitySelectionParams = new BinarySelectionParams (this.emotionalitySelectionParams);
		newParameters.minOccurrenceThreshold = this.minOccurrenceThreshold;
		return newParameters;
	}
	
	public static class BinarySelectionParams  implements IParameters 
	{
		public FeatureSelectionAlgOption selectionAlg;
		public double scoreThreshold; // used to define the ambiguous terms having score below threshold
		public int minOccurNum; // needed to avoid the skewness in scores for low occurred terms
		public boolean includeNeutral; // whether to include neutral class in the selection process (usually as negative example); not always relevant
		public boolean applySmoothing; // whether to apply Laplase smoothing in the PMI formula (needed to obtain more correct score estimation); not always relevant
		public boolean applyToAbsoluteSoAScores = true; // whether to apply threshold on absolute value or not; not always relevant, but default is true
		public boolean useOnlyPositive; // whether to remove negative scores
		public boolean nullifyLowPositiveScores; // whether to set to 0.0 PMI scores that have too low corresponding frequencies
		
		public  BinarySelectionParams () {};
		public  BinarySelectionParams ( BinarySelectionParams  initial)
		{
			this.selectionAlg = initial.selectionAlg;
			this.scoreThreshold = initial.scoreThreshold;
			this.minOccurNum = initial.minOccurNum;
			this.applySmoothing = initial.applySmoothing;
			this.includeNeutral = initial.includeNeutral;
			this.useOnlyPositive  = initial.useOnlyPositive;
			this.nullifyLowPositiveScores = initial.nullifyLowPositiveScores;
			this.applyToAbsoluteSoAScores = initial.applyToAbsoluteSoAScores ;
		}
		
		public String printParametersShort()
		{
			StringBuilder sb = new StringBuilder();
			sb.append("-alg-" + selectionAlg.toString().toLowerCase());
			if (selectionAlg != FeatureSelectionAlgOption.No)
			{
				sb.append("-thr-" + scoreThreshold);
				sb.append("-minO-" + minOccurNum); 
				sb.append("-iN-" + (includeNeutral?1:0));
				sb.append("-appSm-" + (applySmoothing?1:0));
				if (useOnlyPositive)
					sb.append("-onlyPosV-1"); //only positive values
				if (nullifyLowPositiveScores)
					sb.append("-null-1");
				if (!applyToAbsoluteSoAScores)
					sb.append("-notAbsT");
			}
			return sb.toString();
		}
		
		public static abstract class FeatureSelectionParamsEnumerationList  implements IParametersEnumerationList
		{
			
		}
		
		public static class ThresholdOnlySelectionParamsEnumerationList   extends  FeatureSelectionParamsEnumerationList 
		{
			public int[] intThresholdsOptions;
		}
		
		public static class BinarySelectionParamsEnumerationList extends  FeatureSelectionParamsEnumerationList implements IParametersEnumerationList
		{
			public FeatureSelectionAlgOption[] selectionAlgsOptions;
			public double[] thresholdsOptions;
			public ExtraParamCombinationForBinaryFeatureSelection[] extraOptions;
			
			
			public BinarySelectionParamsEnumerationList(FeatureSelectionAlgOption[] selectionAlgsOptions,
					double[] thresholdsOptions,
					ExtraParamCombinationForBinaryFeatureSelection[] extraOptions)
			{
				this.selectionAlgsOptions = selectionAlgsOptions;
				this.thresholdsOptions = thresholdsOptions;
				this.extraOptions = extraOptions;
			}
			
			public BinarySelectionParamsEnumerationList(BinarySelectionParamsEnumerationList anotherList)
			{
				this.selectionAlgsOptions = anotherList.selectionAlgsOptions;
				this.thresholdsOptions = anotherList.thresholdsOptions;
				this.extraOptions = anotherList.extraOptions;
			}
			
			/**
			 * Look for details of the parameters within BinarySelectionParams
			 * @author valentina
			 *
			 */
			public static class ExtraParamCombinationForBinaryFeatureSelection 
			{
				public int minOccurNum; 
				public boolean includeNeutral;
				public boolean applySmoothing;
				public boolean useOnlyPositive;
				public boolean nullifyLowPositiveScores;
				public boolean applyToAbsoluteSoAScores;
				
				public ExtraParamCombinationForBinaryFeatureSelection (int minOccurNum, 
				 boolean includeNeutral,
				 boolean applySmoothing,
				 boolean useOnlyPositive,
				 boolean nullifyLowPositiveScores, 
				 boolean applyToAbsoluteSoAScores)
				{
					this.minOccurNum = minOccurNum; 
					this.includeNeutral = includeNeutral;
					this.applySmoothing = applySmoothing;
					this.useOnlyPositive = useOnlyPositive;
					this.nullifyLowPositiveScores = nullifyLowPositiveScores;
					this.applyToAbsoluteSoAScores = applyToAbsoluteSoAScores;
				}
				
				public void setParameters(BinarySelectionParams binaryParams) {
					binaryParams.minOccurNum = this.minOccurNum;
					binaryParams.includeNeutral = this.includeNeutral;
					binaryParams.applySmoothing = this.applySmoothing;
					binaryParams.applyToAbsoluteSoAScores = this.applyToAbsoluteSoAScores;
					binaryParams.nullifyLowPositiveScores = this.nullifyLowPositiveScores;
					binaryParams.useOnlyPositive = this.useOnlyPositive;
				}
			}
		}
	}
	
	
	public String printParametersShort()
	{
		StringBuilder sb = new StringBuilder();
		boolean doPolarityAmbiquityRefinementOnRepresentation = false;
		boolean doEmotionalityLimitRefinementOnRepresentation = false;
		boolean doOccurrenceLimitRefinementOnRepresentation = false;
		for (SelectionType selType : selectionTypesToApply)
		{
			if (selType == SelectionType.OnPolarity)
			{
				 doPolarityAmbiquityRefinementOnRepresentation = true;
				 if (polaritySelectionParams.selectionAlg == FeatureSelectionAlgOption.No)
					 doPolarityAmbiquityRefinementOnRepresentation = false;
			}
			else if (selType == SelectionType.OnEmotionality)
			{
				doEmotionalityLimitRefinementOnRepresentation = true;
				if (emotionalitySelectionParams.selectionAlg == FeatureSelectionAlgOption.No)
					doEmotionalityLimitRefinementOnRepresentation = false;
			}
			else if (selType == SelectionType.OnOccurrence)
				doOccurrenceLimitRefinementOnRepresentation = true;
		}
		
		sb.append("PolSel-" + (doPolarityAmbiquityRefinementOnRepresentation?1:0));
		if (doPolarityAmbiquityRefinementOnRepresentation)
		{
			sb.append(
					polaritySelectionParams.printParametersShort()
					);
		}
		sb.append(",EmtSel-" + (doEmotionalityLimitRefinementOnRepresentation?1:0));
		
		if (doEmotionalityLimitRefinementOnRepresentation)
		{
			sb.append(emotionalitySelectionParams.printParametersShort());
		}
		
		
		
		if (doOccurrenceLimitRefinementOnRepresentation)
		{
			sb.append(",OccSel-1");
			sb.append("-minOcc-" + minOccurrenceThreshold);
		}
		
		return sb.toString();
	}
	
	/* previous params allocation:
	 * 
	 * PMIPolarityThreshold = 0.2
	 * minPMIPolarityOccur = 5;
	 * includeNeutral = false;
	 * applySmoothing = false;
	 * 
	 *  emotionalitySelectionMin = 0.5;
	 *   doEmotionalityLimitRefinementOnRepresentation = false;
	 *    doPolarityAmbiquityRefinementOnRepresentation = true
	 */
	
}
