/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import java.io.Serializable;

import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;

public class BinaryLearnerParameters implements IParameters,Serializable {

	private static final long serialVersionUID = -8280056204286330136L;

	public enum BinaryLearnerName {PMI, WekaLogReg, WekaSVM, WekaMNB, PMISimple}; //PMISimple will not learn a real classifier, but compute and store simple PMI scores separately for each emotion
	
	public BinaryLearnerName binaryLearnerName;
	public String binaryLexiconParams;
	
	public BinarySelectionParams binaryFeatureSelectionParams;
	
	
	/**
	 * Specifies whether to apply the probability thresholding on the output. 
	 * Will work only with weka classifiers supporting probability output (e.g LogRegression and MNB)
	 */
	public static class ProbabilisticOutputTreatment implements Serializable
	{
		private static final long serialVersionUID = 6393941907310855817L;
		public boolean useProbabilities;
		public double probabilityThreshold; // the minimum probablity of the positive class to be outputed
		public ProbabilisticOutputTreatment (boolean useProbabilities, double threshold)
		{
			this.useProbabilities = useProbabilities;
			this.probabilityThreshold = threshold;
		}
		public ProbabilisticOutputTreatment () {};
	}
	
	public BinaryLearnerParameters()
	{
		binaryFeatureSelectionParams = new BinarySelectionParams();
		binaryFeatureSelectionParams.selectionAlg = FeatureSelectionAlgOption.No;
	}
	
	public String printParametersShort()
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append("-lT-" +  binaryLearnerName);
		sb.append("-lexP-" + binaryLexiconParams);
		
		sb.append("-FS:");
		sb.append(binaryFeatureSelectionParams.printParametersShort());
		sb.append(":");
		
		return sb.toString();
	}
	
	public static class BinaryLearnerParametersEnumerationList implements  IParametersEnumerationList
	{
		public String[] differentAdditionalBinaryLearnerOptions;
		public BinarySelectionParamsEnumerationList  binarySelectionParamsEnumerationList;
		
		public BinaryLearnerParametersEnumerationList() {};
		public BinaryLearnerParametersEnumerationList (BinaryLearnerParametersEnumerationList anotherList)
		{
			this.differentAdditionalBinaryLearnerOptions = anotherList.differentAdditionalBinaryLearnerOptions;
			this.binarySelectionParamsEnumerationList = new BinarySelectionParamsEnumerationList(anotherList.binarySelectionParamsEnumerationList);
		}
	}
}
