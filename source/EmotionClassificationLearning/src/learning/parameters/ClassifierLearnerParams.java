/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import functionality.UtilArrays;

public class ClassifierLearnerParams implements IParameters {

	public MultiLabelRefineParams multiLabelInitialRefinementParams; //if not null, it will specify the parameters of initial annotation refinement (// whether to refine the initial weighted annotation (refinement eliminates the low weights from the emotion distribution))
	public ClassifierApplicationParameters outputClassifierApplicationParameters;
	
	public String specificLearnerParams = "";
	
	public RebalancingParameters rebalancingParameters; // if null, no rebalancing will be applied
	
	public boolean doRebalance()
	{
		return rebalancingParameters != null;
	}
	
	public ClassifierLearnerParams() {};
	public ClassifierLearnerParams(ClassifierLearnerParams initial)
	{
		
	}
	

	@Override
	public String printParametersShort() {
		StringBuilder sb = new StringBuilder();
		
		if (doRebalance())
			sb.append(":Blc:" + rebalancingParameters.printParametersShort());
		
		sb.append(":InRef:");
		if (multiLabelInitialRefinementParams != null)
		{
			sb.append(multiLabelInitialRefinementParams.printParametersShort());
		}
		else
		{
			sb.append("-No-");
		}
		
		sb.append(":ApplParams:");
		sb.append(outputClassifierApplicationParameters.printParametersShort());
		
		if (specificLearnerParams != null)
		{
			sb.append(":extra:");
			sb.append(specificLearnerParams);
			sb.append("::");
		}
		
		return sb.toString();
	}
	
	
	public static class ClassifierLearnerParamsEnumerationList implements IParametersEnumerationList
	{
		public MultiLabelRefineParams[] initialMultiLabelOptions;
		public String[] extraParameters;
		public RebalancingParameters[] rebalancingOptions;
		
		public  ClassifierLearnerParamsEnumerationList () {};
		public  ClassifierLearnerParamsEnumerationList (ClassifierLearnerParamsEnumerationList anotherList) {
			this.initialMultiLabelOptions = UtilArrays.copyArray(anotherList.initialMultiLabelOptions);
			this.extraParameters = UtilArrays.copyArray(anotherList.extraParameters);
			this. rebalancingOptions = UtilArrays.copyArray(anotherList. rebalancingOptions);
		};
	}
	
	public void copyDataFromAnother(ClassifierLearnerParams initial)
	{
		this.multiLabelInitialRefinementParams = initial.multiLabelInitialRefinementParams;
		this.outputClassifierApplicationParameters = new ClassifierApplicationParameters(initial.outputClassifierApplicationParameters);
		this.specificLearnerParams = initial.specificLearnerParams;
		if (initial.rebalancingParameters != null)
			this.rebalancingParameters = new RebalancingParameters(initial.rebalancingParameters.weightType);
		else
			this.rebalancingParameters = null;
		
	}
	
	@Override
	public ClassifierLearnerParams clone()
	{
		ClassifierLearnerParams newLearnerParams = new ClassifierLearnerParams ();
		newLearnerParams.copyDataFromAnother(this);
		return newLearnerParams;
	}
	
	
}
