/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning;

import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearner;
import classification.definitions.WeightedClassifier;
import data.documents.Tweet;
import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import utility.Pair;

public class SemiSupervisedLearnerFactory {	
	
	/**
	 * The logic to load correct version of Semi-Supervised Learner based on the given parameters and provided data.
	 * 
	 * If allParameters.trainDataName is set (not null): these data will be used in the training process disregarding of other parameters.
	 * If allParameters.sslParameters.unlabeledDataName is set (not null): the semi-supervised learning procedure will be applied (distant learning from pseudo-labeled data: unlabaled data annotated with the initial classifier). If this value set up to "None", the semi-supervised process is ignored.
	 * If neutralTweetData is set (not null): the semi-supervised learning procedure will use in the training process the given pseudo-neutral tweets. (This is checked only if semi-supervised process is to be performed).
	 * 
	 * @param allParameters
	 * @param initialClassifier
	 * @param classifierLearner
	 * @param unlabeledTweetData
	 * @param neutralTweetData
	 * @param givenLabeledTweetData
	 * @return
	 * @throws Exception
	 */
	public static SemiSupervisedLearner initializeSemiSupervisedLearner(
			AllSemiSupervisedLearningParameters allParameters,
			WeightedClassifier initialClassifier, 
			ClassifierLearner classifierLearner,
			List<? extends Tweet> unlabeledTweetData,
			List<? extends Tweet> neutralTweetData,
			Pair<List<Tweet>, Map<Integer, double[]>> givenLabeledTweetData			
	) throws Exception
	{
		
		SemiSupervisedLearner sslLearner = null;
		
		if (allParameters.sslParameters != null)
		{
			String unlabeledDataName = allParameters.sslParameters.unlabeledDataName;
			String labeledDataName = allParameters.trainDataName;
			
			if (labeledDataName == null)
			{
				// semi-supervised learning without the initial labeled data
				if (unlabeledDataName == null || unlabeledDataName.equals("None"))
				{
					sslLearner = new NoneSemiSupervisedLearner(allParameters.sslParameters);
					sslLearner.initialize((List<? extends Tweet>)null, initialClassifier, null);
				}
				else 
				{
					if (neutralTweetData != null)
					{
						// set semi-supervised learning with neutral tweets
						sslLearner = new SemiSupervisedLearnerWithNeutral(allParameters.sslParameters);
						((SemiSupervisedLearnerWithNeutral) sslLearner).setNeutralData(neutralTweetData);
					}
					else
						sslLearner = new SemiSupervisedLearner(allParameters.sslParameters);
					sslLearner.initialize(unlabeledTweetData, initialClassifier, classifierLearner);
				}
			}
			else
			{
				if (unlabeledDataName == null || unlabeledDataName.equals("None"))
				{
					sslLearner = new SemiSupervisedLearnerWithGiven(allParameters.sslParameters); // it will be assumed to be initialed on the null unlabeled data
					((SemiSupervisedLearnerWithGiven) sslLearner).storeAdditionalTweetEmotions(givenLabeledTweetData.first, givenLabeledTweetData.second);
					sslLearner.initialize((List<? extends Tweet>)null, null, classifierLearner);
				}
				else 
				{
					if (neutralTweetData != null)
					{
						sslLearner = new SemiSupervisedLearnerWithGivenAndNeutral(allParameters.sslParameters);
						((SemiSupervisedLearnerWithNeutral) sslLearner).setNeutralData(neutralTweetData);
						((SemiSupervisedLearnerWithGivenAndNeutral) sslLearner).storeAdditionalTweetEmotions(givenLabeledTweetData.first, givenLabeledTweetData.second);
						
					}
					else
					{
						sslLearner = new SemiSupervisedLearnerWithGiven(allParameters.sslParameters);
						((SemiSupervisedLearnerWithGiven) sslLearner).storeAdditionalTweetEmotions(givenLabeledTweetData.first, givenLabeledTweetData.second);
					}
					sslLearner.initialize(unlabeledTweetData, initialClassifier, classifierLearner);
				}
			}
		}
		else
		{
			throw new Exception("The parameters for semi-supervised learning are not set!");
		}
	
		return sslLearner;
		
	}
}
