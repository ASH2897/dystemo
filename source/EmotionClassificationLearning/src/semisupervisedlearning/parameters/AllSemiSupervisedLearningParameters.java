/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning.parameters;

import learning.parameters.AllLearningParameters;

public class AllSemiSupervisedLearningParameters extends AllLearningParameters {

	public SSLFrameworkParameters sslParameters;
	
	@Override
	public String printValues(String separator, boolean putShortName)
	{
		
		StringBuilder sb = new StringBuilder();
		sb.append(super.printValues(separator, putShortName));
		
		
		sb.append((putShortName?"FullSSLParams=":""));
		sb.append(sslParameters.printParametersShort());
		sb.append(separator);
		
		
		sb.append((putShortName?"UnlabeledDataName=":""));
		sb.append(sslParameters.getUnlabeledDataName());
		sb.append(separator);
		
		sb.append((putShortName?"InitialClassifier=":""));
		sb.append(sslParameters.initialClassifierName);
		sb.append(separator);
		return sb.toString();
	}
	
	public static String printNames(String separator)
	{

		StringBuilder sb = new StringBuilder();
		sb.append(AllLearningParameters.printNames(separator));
		sb.append("sslFullParams");
		sb.append(separator);
		sb.append("sslUnlabeledDataName");
		sb.append(separator);
		sb.append("sslInitialClassifier");
		sb.append(separator);
		
		return sb.toString();
	}
	
	@Override
	public AllLearningParameters clone()
	{
		AllSemiSupervisedLearningParameters newParams = new AllSemiSupervisedLearningParameters();
		//AllLearningParameters newParams = super.clone();
		//AllSemiSupervisedLearningParameters newSSLParams = new AllSemiSupervisedLearningParameters();
		//newSSLParams.featureExtractionParams = newParams.featureExtractionParams;
		newParams.featureExtractionParams = this.featureExtractionParams.clone();
		newParams.featureSelectionParams = this.featureSelectionParams.clone();
		newParams.learnerName = this.learnerName;
		newParams.learnerParams = this.learnerParams.clone();
		newParams.trainDataName = this.trainDataName;
		newParams.sslParameters = this.sslParameters.clone();
		return newParams;
	}
}
