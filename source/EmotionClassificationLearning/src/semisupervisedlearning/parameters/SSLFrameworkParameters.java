/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning.parameters;

import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.IParameters;

public class SSLFrameworkParameters implements IParameters {
	
		// INITIALIZATION PARAMETERS
		/**
		 * The name of the dataset used as unlabeleded data (it will influence the choice of the SemiSupervisedLearner to use).
		 * Note that we assume that provided unlabeled data can be either TweetData or CompositeData types. 
		 * In the former case, only the strict unlabeled data would be used.
		 * In the latter case, both unlabeled data and pseudo-neutral data would be used (if given). 
		 */
		public String unlabeledDataName = null; 
		
		/**
		 * The name of initial classifier to be used. 
		 * If loaded within the EmotionClassificationExperiments, it should be the name of a classifier from UsedClassifierFactory.
		 */
		public String initialClassifierName; 
		
		/**
		 * Parameters for application of the given initial classifier
		 */
		public ClassifierApplicationParameters initialClassifierApplicationParams;
		
		/// PARAMETERS related to the framework setup
		/**
		 *  how many iterations by default should be done at most
		 */
		public int maxIterationNum = 1; 
		
		/**
		 * if true - after the new classifier is computed, the features from the initial one will be added compulsory (this is possible only if ClassifierLearner outputs the same type of classifier as initial one, and this type is lexicon based
		 */
		public boolean preserveInitialClassifierIfPossible = false;
		
		/**
		 * if true - not only ngrams will be extracted in the text, but also terms from the given lexicon
		 */
		public boolean preserveTermsFromInitialClassifier = false;  
		
		/// PARAMETERS  related to the options for treating the data annotation
		/**
		 * if to remove all the tweets containing negation terms while learning
		 */
		public boolean dropTweetsWithNegationsInLearning = false; 
		
		/**
		 * if to leave only dominant emotions in the annotated data (might be useful in case of weighted annotation)
		 */
		public boolean limitToDominantEmotions = false;
		
		/**
		 * if true - ignore (remove from annotation) those tweets that were labeled as neutral by the initial classifier in the unlabeled data(by default before was false, but more correctly should be true)
		 */
		public boolean ignoreNeutralInAnnotation = true; 
		
		/**
		 * if true - will ignore the annotation of pseudo-neutral tweets by the initial classifier.
		 * if false - will remove the pseudo-neutral tweets that were found as emotional by the initial classifier.
		 */
		public boolean ignoreInitialAnnotationInNeutralTweets = true;
		
		
		/// OTHER RUNNING PARAMETERS
		public boolean useCacheForAnnotation = true;
		public boolean evaluateLexiconsInIterations = false;
		
		public SSLFrameworkParameters () {};
		
		public String getUnlabeledDataName() {
			if (unlabeledDataName == null)
					return "None";
			return unlabeledDataName;
		}
		
		public String printFrameworkSetupParametersShort()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.append("mxI" + maxIterationNum);
			if (preserveInitialClassifierIfPossible)
				sb.append("-svPr1");
			if (preserveTermsFromInitialClassifier)
				sb.append("-svTi1");
			
			return sb.toString();
		}
		
		public String printParametersShort()
		{
			return printValues(";", true);
		}
		
		public String printValues(String separator, boolean putShortName)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(((putShortName)?"DT=":"") + getUnlabeledDataName());
			sb.append(separator);
			sb.append(((putShortName)?"IC=":"") + initialClassifierName);
			sb.append(separator);
			if (!unlabeledDataName.equals("None"))
			{
				sb.append(((putShortName)?"FramWSet=":"") + printFrameworkSetupParametersShort());
				sb.append(separator);
			}
			
			if (limitToDominantEmotions)
			{
				sb.append(((putShortName)?"LimS=":"") + 1);
			}
			
			sb.append(separator);
			sb.append(((putShortName)?"corNeut=":"") + (ignoreNeutralInAnnotation?1:0) + (ignoreInitialAnnotationInNeutralTweets?1:0));
			
			sb.append(separator);
			sb.append(((putShortName)?"treatNeg=":"") + printNegationVariation()) ;
			
			sb.append(separator);
			sb.append(((putShortName)?"initClassP=":"") + initialClassifierApplicationParams.printParametersShort()) ;
			sb.append(separator);
			
			
			return sb.toString();
		}
		
		private String printNegationVariation()
		{
			String negationTreat = "" + (dropTweetsWithNegationsInLearning?'2':(initialClassifierApplicationParams.parametersOfTermDetectionForClassifier.treatNegationsParams.printParametersShort()));
					 
			return negationTreat;
		}
		
		public SSLFrameworkParameters clone()
		{
			 SSLFrameworkParameters newParams = new SSLFrameworkParameters();
			 
			 newParams.dropTweetsWithNegationsInLearning = this.dropTweetsWithNegationsInLearning;
			 newParams.evaluateLexiconsInIterations = this.evaluateLexiconsInIterations;
			 newParams.ignoreInitialAnnotationInNeutralTweets = this.ignoreInitialAnnotationInNeutralTweets;
			 newParams.ignoreNeutralInAnnotation = this.ignoreNeutralInAnnotation;
			 newParams.initialClassifierApplicationParams = new  ClassifierApplicationParameters(this.initialClassifierApplicationParams);
			 newParams.initialClassifierName = this.initialClassifierName;
			 newParams.limitToDominantEmotions = this.limitToDominantEmotions;
			 newParams.maxIterationNum = this.maxIterationNum;
			 newParams.preserveInitialClassifierIfPossible = this.preserveInitialClassifierIfPossible;
			 newParams.unlabeledDataName = this.unlabeledDataName;
			 newParams.useCacheForAnnotation = this.useCacheForAnnotation;
			 return newParams;
		}
}
