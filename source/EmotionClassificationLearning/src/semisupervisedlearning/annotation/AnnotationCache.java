/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning.annotation;

import java.util.HashMap;
import java.util.Map;

public class AnnotationCache {

	static Map<String, Map<Integer, double[]>> cachedAnnotatedData = new HashMap<String, Map<Integer, double[]>>();

	/** 
	 * Gets the stored annotated data using the dataname and the classifier name (the initial version of classifier, the annotation before refinement, but after the normalization)
	 * @param dataClassifierName
	 * @return
	 * @throws Exception 
	 */
	public static Map<Integer, double[]> getCachedAnnotation(String dataAndClassifierName) 
	{
		if (cachedAnnotatedData.containsKey(dataAndClassifierName))
			return cachedAnnotatedData.get(dataAndClassifierName);
		else 
			return null;
	}
	
	public static boolean isAnnotationSaved(String dataAndClassifierName)
	{
		return cachedAnnotatedData.containsKey(dataAndClassifierName);
	}
	
	public static void saveNewAnnotation(String dataAndClassifierName, Map<Integer, double[]> annotationData)
	{
		cachedAnnotatedData.put(dataAndClassifierName, annotationData);
	}
	
	public static void clear()
	{
		cachedAnnotatedData.clear();
	}
}
