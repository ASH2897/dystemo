/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import linguistic.UtilText;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import data.categories.CategoryUtil;
import functionality.UtilString;

public class LexiconRefinement {

	/** this method deletes terms from the lexicon that were found to be ambiguous for the corresponding category set **/ 
	public static void deleteAmbiguousTerms(WeightedEmotionLexicon lexicon, double ambLevel)
	{
		Set<String> ambTerms = CategoryUtil.detectAmbiguousTerms(lexicon.getAllTermsData(),  lexicon.categoriesData,  ambLevel);
		
		System.out.println("There were detected " + ambTerms.size() + " ambiguous terms");
		
		lexicon.removeTerms(ambTerms);
	}
	
	/**
	 * Deletes the standard stop-words, as well as ambiguous stop words and their combination
	 * @param lexicon
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void deleteStopWords(WeightedEmotionLexicon lexicon, Set<String> ambiguousTerms) throws FileNotFoundException, UnsupportedEncodingException
	{
		Set<String> termsToRemove = new HashSet<String>();
		for (String term : lexicon.allTerms())
		{
			
			if ( (UtilText.isStopWord(term)) || ambiguousTerms.contains(term))
				termsToRemove.add(term);
			else 
				{
					int wordLength = UtilString.getWordNumber(term);
					if (wordLength > 1 && wordLength < 4 && UtilText.containsOnlyStopWordsAndGiven(term, ambiguousTerms))
						termsToRemove.add(term);
				}
		}
		System.out.println("There were detected " + termsToRemove.size() + " non-lexicon terms");
		lexicon.removeTerms(termsToRemove);
	}
	
	public static void downweightEmoticons(WeightedEmotionLexicon lexicon)
	{
		String emotoPattern = "<emot\\d{1,2}>";
		lexicon.reweightTerms(emotoPattern, 0.5);
	}
	
	public static void dropLowLexiconWeights(WeightedEmotionLexicon lexicon)
	{
		double alpha = 0.1;
		Map<String, double[]> lexData = lexicon.getAllTermsData();
		
		for (Map.Entry<String, double[]> entry : lexData.entrySet())
		{
			EmotionDistributionAdoption.dropLowProbabilitiesByAlphaCut(entry.getValue(), alpha);
			lexicon.updateTermWeight(entry.getKey(), entry.getValue());
		}
	}
}
