/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import classification.loaders.WekaClassifierFactory;
import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;
import learners.given.BinaryLexiconConstruction;
import learners.models.PMIBinaryLexiconConstruction;
import learners.models.PMISimpleBinaryLexiconConstruction;
import learners.models.WekaBasedModelConstruction;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;

public class UtilBinaryLearnerLoader {

	/** This will return the class for lexicon construction if current learnerType supports it, or null otherwise
	 * 
	 * @param learnerType
	 * @param lexParams
	 * @return
	 */
	public static BinaryLexiconConstruction getInstanceOfLexiconConstruction(BinaryLearnerName learnerType, String lexParams)
	{
		if (!isLearningLexicon(learnerType))
			return null;
		
		switch (learnerType)
		{
			case PMI: return new PMIBinaryLexiconConstruction(lexParams);
			case PMISimple: return new PMISimpleBinaryLexiconConstruction(lexParams);
			default: return null;
		}
	}
	
	private static boolean isLearningLexicon (BinaryLearnerName learnerType)
	{
		return (learnerType == BinaryLearnerName.PMI || learnerType == BinaryLearnerName.PMISimple );
	}
	
	private static boolean isLearningWekaModel (BinaryLearnerName learnerType)
	{
		return (learnerType.name().startsWith("Weka"));
	}
	
	public static WekaBasedModelConstruction getInstanceOfWekaBasedBinaryLearner(BinaryLearnerName learnerType, String lexParams)
	{
		if (! isLearningWekaModel(learnerType))
			return null;
		
		AvailableWekaClassifiers wekaClassifierType = WekaClassifierFactory.getWekaClassifierByNameFromLearnerName(learnerType.name());
		
		if (wekaClassifierType == null)
			return null;
		
		return new WekaBasedModelConstruction(
				WekaClassifierFactory.getWekaClassifierByName(
						wekaClassifierType, lexParams));
	}
	
	public static boolean ifSupportsProbabilisticOutput(BinaryLearnerName learnerType)
	{
		return (learnerType == BinaryLearnerName.WekaLogReg || learnerType == BinaryLearnerName.WekaMNB);
	}
}
