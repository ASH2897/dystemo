/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import data.categories.Polarity;

import functionality.UtilCollections;

public class PMICalculation {
	
	// PARAMETERS
	enum PMISmoothingType {None, EqualFixedAlpha, OnVocabulary};
	
	// default smoothing params
	static double alphaSmoothParam = 1;
	static PMISmoothingType smoothType = PMISmoothingType.OnVocabulary;
	
	// TEMPORARY VARIABLES
	static Map<String, Double> positiveFrequencies;
	
	static Map<String, Double> negativeFrequencies;
	
	static Map<String, Double> neutralFrequencies;
	
	/**
	 * Computes the actual PMI values with the Positive class (+1) of all the terms in representation for which it is possible to compute or specify. The term P(class) is not included.
	 * @param sentimentLabels
	 * @param termData
	 * @param minOccurNum
	 * @param threshold
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPositivePMIValues(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, int minOccurNum, double threshold, boolean applySmoothing) throws Exception
	{
		
		HashMap<String, Double> resultTermValues = new HashMap<String, Double>();
		
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		positiveFrequencies = new HashMap<String, Double>();
		negativeFrequencies = new HashMap<String, Double>();
		
		
		// compute frequencies of terms
		FrequencyComputations.extractFrequencies(sentimentLabels, termData, null, termFrequencies, positiveFrequencies, negativeFrequencies);
				
		double usedAlphaSmoothParam = 0.0;
		if (applySmoothing)
		{
			System.out.println("Used smoothing in PMI computation with the parameters: alpha = " + alphaSmoothParam + ", Type = " + smoothType.toString());
			
					double classSmooth = alphaSmoothParam;
					usedAlphaSmoothParam = alphaSmoothParam;
					if (smoothType ==  PMISmoothingType.OnVocabulary)
						classSmooth *= termFrequencies.size(); // size of vocabulary
										
				    /*
				    double smoothAlpha = (1.0) * positiveClassSize / negativeClassSize;
					if (smoothAlpha > 1)
						smoothAlpha = 1 / smoothAlpha;
					smoothAlpha = 1;
					*/
		}
		
		for(String term : termFrequencies.keySet())
		{
			if(termFrequencies.get(term).intValue() < minOccurNum)
			{
				continue;
			}
			
			int positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term).intValue();
			}
			
			int termFrequency = 0;
			
			if (termFrequencies.containsKey(term))
			{
				termFrequency = termFrequencies.get(term).intValue();
			}
			
			/*
			if (positiveFrequency < 0.6*minOccurNum ||  negativeFrequency == 0)
			{
				continue;
			}*/
			

			if (applySmoothing)
			{
				positiveFrequency += usedAlphaSmoothParam;
				termFrequency += usedAlphaSmoothParam;
			}
			
			//calculate Strength Of Association based on PMI
			double SoAValue = Math.log( positiveFrequency / (double)(termFrequency));
			
			if (Math.abs(SoAValue) >= threshold)
			{
				resultTermValues.put(term, SoAValue);
			}
		}	
		
		
		return resultTermValues;
	}
	
	
	/**
	 * Computes the PMI values (Strength of association) of all the terms in representation for which it is possible to compute or specify
	 * @param sentimentLabels
	 * @param termData
	 * @param minOccurNum
	 * @param threshold
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPMIValues(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, int minOccurNum, double threshold, boolean applySmoothing) throws Exception
	{
		
		HashMap<String, Double> resultTermValues = new HashMap<String, Double>();
		
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		positiveFrequencies = new HashMap<String, Double>();
		negativeFrequencies = new HashMap<String, Double>();
		
		
		int positiveClassSize = UtilCollections.countValueOccurrenceInMap(sentimentLabels, Polarity.POSITIVE);
		int negativeClassSize = UtilCollections.countValueOccurrenceInMap(sentimentLabels, Polarity.NEGATIVE);
		
		// compute frequencies of terms
		FrequencyComputations.extractFrequencies(sentimentLabels, termData, null, termFrequencies, positiveFrequencies, negativeFrequencies);
				
		double usedAlphaSmoothParam = 0.0;
		if (applySmoothing)
		{
			System.out.println("Used smoothing in PMI computation with the parameters: alpha = " + alphaSmoothParam + ", Type = " + smoothType.toString());
			
					double classSmooth = alphaSmoothParam;
					usedAlphaSmoothParam = alphaSmoothParam;
					if (smoothType ==  PMISmoothingType.OnVocabulary)
						classSmooth *= termFrequencies.size(); // size of vocabulary
					
					positiveClassSize += classSmooth; 
				    negativeClassSize += classSmooth; 
					
				    /*
				    double smoothAlpha = (1.0) * positiveClassSize / negativeClassSize;
					if (smoothAlpha > 1)
						smoothAlpha = 1 / smoothAlpha;
					smoothAlpha = 1;
					*/
		}
		
		for(String term : termFrequencies.keySet())
		{
			if(termFrequencies.get(term).intValue() < minOccurNum)
			{
				continue;
			}
			
			int positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term).intValue();
			}
			
			int negativeFrequency = 0;
			
			if (negativeFrequencies.containsKey(term))
			{
				negativeFrequency = negativeFrequencies.get(term).intValue();
			}
			
			/*
			if (positiveFrequency < 0.6*minOccurNum ||  negativeFrequency == 0)
			{
				continue;
			}*/
			

			if (applySmoothing)
			{
				positiveFrequency += usedAlphaSmoothParam;
				negativeFrequency += usedAlphaSmoothParam;
			}
			
			//calculate Strength Of Association based on PMI
			double SoAValue = Math.log( positiveFrequency * negativeClassSize / (double)(positiveClassSize * negativeFrequency));
			
			if (Math.abs(SoAValue) >= threshold)
			{
				resultTermValues.put(term, SoAValue);
			}
		}	
		
		
		return resultTermValues;
	}
	
	/**
	 * Computes the PMI values of all the terms in representation for which it is possible to compute or specify
	 * (with this Simple method only the positive occurrences are taken into account)
	 * @param sentimentLabels
	 * @param termData
	 * @param objectWeights
	 * @param minOccurNum - pay attention that this value will be in terms of sum of weights! 
	 * @param threshold 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPMIValuesWithSimple(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights,
			int minOccurNum, double threshold, boolean applySmoothing) throws Exception
	{
		
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		
		positiveFrequencies = new HashMap<String, Double>();
		negativeFrequencies = new HashMap<String, Double>();
		
		double positiveClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.POSITIVE);
		double negativeClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.NEGATIVE);

		// compute frequencies of terms
		FrequencyComputations.extractFrequencies(sentimentLabels, termData, objectWeights, termFrequencies, positiveFrequencies, negativeFrequencies);

		return extractPMIValuesWithSimple(termFrequencies, positiveFrequencies, negativeFrequencies, positiveClassSize, negativeClassSize, minOccurNum, threshold, applySmoothing);
	}
	
	public static Map<String, Double> extractPMIValuesWithSimple(
			Map<String, Double> termFrequencies, 
			Map<String, Double> positiveFrequenciesCur, 
			Map<String, Double> negativeFrequenciesCur, 
			double positiveClassSize, double negativeClassSize,
			int minOccurNum, double threshold, boolean applySmoothing) throws Exception
	{
		HashMap<String, Double> resultTermValues = new HashMap<String, Double>();
		
		double objectNumber = positiveClassSize + negativeClassSize;
		positiveFrequencies = positiveFrequenciesCur;
		negativeFrequencies = negativeFrequenciesCur;
		
		double usedAlphaSmoothParam = 0.0;
		if (applySmoothing)
		{
			System.out.println("Used smoothing in PMI computation with the parameters: alpha = " + alphaSmoothParam + ", Type = " + smoothType.toString());
			
			double classSmooth = alphaSmoothParam;
			usedAlphaSmoothParam = alphaSmoothParam;
			if (smoothType ==  PMISmoothingType.OnVocabulary)
				classSmooth *= termFrequencies.size(); // size of vocabulary
			
			positiveClassSize += classSmooth; 
			objectNumber += classSmooth;
		}
		
		
		
		for(String term : termFrequencies.keySet())
		{
			double termFrequency = termFrequencies.get(term);
			if( termFrequency < minOccurNum)
			{
				continue;
			}
			
			double positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term);
				
			}
			
			if (applySmoothing)
			{
				positiveFrequency +=  usedAlphaSmoothParam;
				termFrequency += usedAlphaSmoothParam;
			}
			
			double pmiValue = Math.log( positiveFrequency * objectNumber / (double)(positiveClassSize * termFrequency));
			if (positiveFrequency <= usedAlphaSmoothParam)
				pmiValue = 0.0;	
					
			if (term.equals("<emoji_person27>"))
			{
				int h = 0;
				h++;
			}
			
			if (Math.abs(pmiValue) >= threshold)
			{
				resultTermValues.put(term, pmiValue);
			}
		}	
		
		return resultTermValues;
	}
	
	
	/**
	 * Computes the PMI values of all the terms in representation for which it is possible to compute or specify
	 * @param sentimentLabels
	 * @param termData
	 * @param objectWeights
	 * @param minOccurNum - pay attention that this value will be in terms of sum of weights! 
	 * @param threshold 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPMIValues(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights,
			int minOccurNum, double threshold, boolean applySmoothing) throws Exception
	{
		return extractPMIValues(sentimentLabels, termData, objectWeights, minOccurNum, threshold, applySmoothing, false, false);
	}
	
	/**
	 * Computes the PMI values of all the terms in representation for which it is possible to compute or specify
	 * @param sentimentLabels
	 * @param termData
	 * @param objectWeights
	 * @param minOccurNum - pay attention that this value will be in terms of sum of weights! 
	 * @param threshold 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPMIValues(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights,
			int minOccurNum, double threshold, boolean applySmoothing, boolean usePositiveValues, boolean nullifyLowPositiveScores) throws Exception
	{
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		
		positiveFrequencies = new HashMap<String, Double>();
		negativeFrequencies = new HashMap<String, Double>();
		
		double positiveClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.POSITIVE);
		double negativeClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.NEGATIVE);
		
		// compute frequencies of terms
		FrequencyComputations.extractFrequencies(sentimentLabels, termData, objectWeights, termFrequencies, positiveFrequencies, negativeFrequencies);
		
		return extractPMIValues(termFrequencies, positiveFrequencies, negativeFrequencies, positiveClassSize, negativeClassSize, minOccurNum, threshold, applySmoothing,
				usePositiveValues, nullifyLowPositiveScores);
	}

	public static Map<String, Double> extractPMIValues(
			Map<String, Double> termFrequencies, 
			Map<String, Double> positiveFrequenciesCur, 
			Map<String, Double> negativeFrequenciesCur, 
			double positiveClassSize, double negativeClassSize,
				int minOccurNum, double threshold, boolean applySmoothing, boolean usePositiveValues, boolean nullifyLowPositiveScores) throws Exception
		{	
		HashMap<String, Double> resultTermValues = new HashMap<String, Double>();
		positiveFrequencies = positiveFrequenciesCur;
		negativeFrequencies = negativeFrequenciesCur;
		double usedAlphaSmoothParam = 0.0;
		if (applySmoothing)
		{
			System.out.println("Used smoothing in PMI computation with the parameters: alpha = " + alphaSmoothParam + ", Type = " + smoothType.toString());
			
			double classSmooth = alphaSmoothParam;
			usedAlphaSmoothParam = alphaSmoothParam;
			if (smoothType ==  PMISmoothingType.OnVocabulary)
				classSmooth *= termFrequencies.size(); // size of vocabulary
			
			positiveClassSize += classSmooth; 
		    negativeClassSize += classSmooth; 
			
		    /*
		    double smoothAlpha = (1.0) * positiveClassSize / negativeClassSize;
			if (smoothAlpha > 1)
				smoothAlpha = 1 / smoothAlpha;
			smoothAlpha = 1;
			*/
		}
		
		
		
		for(String term : termFrequencies.keySet())
		{
			if(termFrequencies.get(term) < minOccurNum)
			{
				continue;
			}
			
			double positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term);
			}
			
			double negativeFrequency = 0;
			
			if (negativeFrequencies.containsKey(term))
			{
				negativeFrequency = negativeFrequencies.get(term);
			}
			
			/*
			if (positiveFrequency < 0.6*minOccurNum ||  negativeFrequency == 0)
			{
				continue;
			}*/
			
			if (term.equals("team gold") || term.equals("great effort"))
			{
				int k = 0;
				k++;
			}
			
			if (applySmoothing)
			{
				positiveFrequency +=  usedAlphaSmoothParam;
				negativeFrequency +=  usedAlphaSmoothParam;
			}
			
			//calculate Strength Of Association based on PMI
			double SoAValue = Math.log( positiveFrequency * negativeClassSize / (double)(positiveClassSize * negativeFrequency));
			
			if (nullifyLowPositiveScores && applySmoothing && (SoAValue > 0.0 && positiveFrequency <= usedAlphaSmoothParam || 
					SoAValue < 0.0 && negativeFrequency <= usedAlphaSmoothParam)) {
				SoAValue = 0.0;
			}
			
			if ((!usePositiveValues && Math.abs(SoAValue) >= threshold) || (usePositiveValues && (SoAValue >= threshold)))
			{
				resultTermValues.put(term, SoAValue);
				if (Double.isInfinite(SoAValue) || Double.isNaN(SoAValue) || SoAValue == 0.0)
				{
					int l = 0;
					l++;
				}
				if (Math.signum(SoAValue) * (positiveFrequency - negativeFrequency) < 0)
				{
					int l = 0;
					l++;
				}
			}
		}	
		
		
		return resultTermValues;
	}
	
	/**
	 * Computes the PMI values of all the terms in representation for which it is possible to compute or specify
	 * @param sentimentLabels
	 * @param termData
	 * @param objectWeights
	 * @param minOccurNum - pay attention that this value will be in terms of sum of weights! 
	 * @param threshold 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Double> extractPMIValues(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights,
			int minOccurNum, double threshold, boolean applySmoothing, boolean includeNeutral, boolean usePositiveValues, boolean nullifyLowPositiveScores) throws Exception
	{
		if (!includeNeutral)
			return  extractPMIValues(sentimentLabels, termData, objectWeights, minOccurNum, threshold, applySmoothing, usePositiveValues, nullifyLowPositiveScores);
		
		HashMap<String, Double> resultTermValues = new HashMap<String, Double>();
		
		HashMap<String, Double> termFrequencies = new HashMap<String, Double>();
		
		positiveFrequencies = new HashMap<String, Double>();
		negativeFrequencies = new HashMap<String, Double>();
		neutralFrequencies = new HashMap<String, Double>();
		
		
		double positiveClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.POSITIVE);
		double negativeClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.NEGATIVE);
		double neutralClassSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, Polarity.NEUTRAL);
		
		// compute frequencies
		FrequencyComputations.extractFrequenciesWithNeutral(sentimentLabels, termData, objectWeights, termFrequencies, positiveFrequencies, negativeFrequencies, neutralFrequencies);
				
		
		double usedAlphaSmoothParam = 0.0;
		if (applySmoothing)
		{
			System.out.println("Used smoothing in PMI computation with the parameters: alpha = " + alphaSmoothParam + ", Type = " + smoothType.toString());
		
			double classSmooth = 0.0;
			if (smoothType ==  PMISmoothingType.OnVocabulary)
			{
				classSmooth = termFrequencies.size()*alphaSmoothParam; // size of vocabulary
				usedAlphaSmoothParam = alphaSmoothParam;
			}
			else if (smoothType ==  PMISmoothingType.EqualFixedAlpha)
			{
				classSmooth = alphaSmoothParam;
				usedAlphaSmoothParam = alphaSmoothParam;
			}
			
			positiveClassSize += classSmooth; 
		    negativeClassSize += classSmooth; 
		    neutralClassSize += classSmooth;
		    		
		    /*
		    double smoothAlpha = (1.0) * positiveClassSize / negativeClassSize;
			if (smoothAlpha > 1)
				smoothAlpha = 1 / smoothAlpha;
			smoothAlpha = 1;
			*/
		}
			
		
		
		for(String term : termFrequencies.keySet())
		{
			if(termFrequencies.get(term) < minOccurNum)
			{
				continue;
			}
			
			double positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term);
			}
			
			double negativeFrequency = 0;
			
			if (negativeFrequencies.containsKey(term))
			{
				negativeFrequency = negativeFrequencies.get(term);
			}
			
			double neutralFrequency = 0;
			
			if (neutralFrequencies.containsKey(term))
			{
				neutralFrequency = neutralFrequencies.get(term);
			}
			
			/*
			if (positiveFrequency < 0.6*minOccurNum ||  negativeFrequency == 0)
			{
				continue;
			}*/
			
			if (term.equals("team gold") || term.equals("great effort"))
			{
				int k = 0;
				k++;
			}
			
			if (applySmoothing)
			{
				positiveFrequency += usedAlphaSmoothParam;
				negativeFrequency += usedAlphaSmoothParam;
				neutralFrequency += usedAlphaSmoothParam;
			}
			
			//calculate Strength Of Association based on PMI (as max of SoA among classes)
			double SoAValue = Math.max(Math.max(
					Math.log( positiveFrequency * (negativeClassSize + neutralClassSize) / (double)(positiveClassSize * (negativeFrequency + neutralFrequency))),
					Math.log( negativeFrequency * (positiveClassSize + neutralClassSize) / (double)(negativeClassSize * (positiveFrequency + neutralFrequency))) ),
					Math.log( neutralFrequency * (negativeClassSize + positiveClassSize) / (double)(neutralClassSize * (negativeFrequency + positiveFrequency)))
					);
			
			if (nullifyLowPositiveScores && applySmoothing && positiveFrequency <= usedAlphaSmoothParam) {
				SoAValue = 0.0;
				//TODO: change here to ensure that we renull only for the category that gets associated!
			}
			
			if ((!usePositiveValues && Math.abs(SoAValue) >= threshold) || (usePositiveValues && (SoAValue >= threshold)))
			{
				resultTermValues.put(term, SoAValue);
				if (Double.isInfinite(SoAValue) || Double.isNaN(SoAValue) || SoAValue == 0.0)
				{
					int l = 0;
					l++;
				}
				if (Math.signum(SoAValue) * (positiveFrequency - negativeFrequency) < 0)
				{
					int l = 0;
					l++;
				}
			}
		}	
		
		
		return resultTermValues;
	}
	
	/** Removes those terms whose PMI value sign is not consistent with distribution of the labels for the terms. 
	 * That is the term is deleted if e.g. SoA < 0 (meaning that the term is rather negative), while negativeFreq < positiveFreq.
	 * The recorded values of positive and negative frequencies will be used. */
	public static void dropAmbiguousPMIValuesInPrevious(Map<String, Double> termValues) throws Exception
	{
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String,Double> termEntry : termValues.entrySet())
		{
			String term = termEntry.getKey();
			double positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(term))
			{
				positiveFrequency = positiveFrequencies.get(term);
			}
			
			double negativeFrequency = 0;
			
			if (negativeFrequencies.containsKey(term))
			{
				negativeFrequency = negativeFrequencies.get(term);
			}

			if (Math.signum(termEntry.getValue()) * (positiveFrequency - negativeFrequency) < 0)
			{
				termsToRemove.add(term);
			}
		}
		UtilCollections.removeAllKeysFromMap(termValues, termsToRemove);
	}
	
	
	
}
