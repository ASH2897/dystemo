/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;



import data.categories.ICategoriesData;
import data.categories.Polarity;
import functionality.UtilCollections;

/**
 * The class for splitting the category annotation of all documents into two binary classes for each category (e.g. in category, not in category)
 * 
 */
public class CategoriesBinarySplitting {

	public static Map<Integer,Integer> getBinaryDataForCategory(ICategoriesData categoriesData, int categoryId, 
			Map<Integer, List<Integer>> emotionLabels, CloseCategoryUseType closeCategoryUseType, CloseCategoryCalculation closeCategoryCalculation)
	{
		Map<Integer,Integer> resultBinaryData = new HashMap<Integer, Integer>();
		
		List<Integer> closeCategories = getCloseCategories(categoriesData, categoryId, closeCategoryCalculation);
		for (Map.Entry<Integer,List<Integer>> tweetEntry : emotionLabels.entrySet())
		{
			List<Integer> assignedCategories = tweetEntry.getValue();
			boolean containsGiven = assignedCategories.contains(categoryId);
			boolean containsClose = UtilCollections.containsAny(assignedCategories, closeCategories);
			
			boolean binaryBoolLabel = containsGiven;
			boolean toExclude = false;
			if (!containsGiven)
			{
				switch (closeCategoryUseType)
				{
					case IncludeOut: break;
					case IncludeIn: binaryBoolLabel = containsClose; break;
					case Exclude: if (containsClose) { toExclude = true; break;}
				}
			}
			
			int binaryIntLabel = binaryBoolLabel ? Polarity.POSITIVE : Polarity.NEGATIVE;
			if (!toExclude)
				resultBinaryData.put(tweetEntry.getKey(), binaryIntLabel);
		}
		
		return resultBinaryData;
	}
	
	/** Returns the indexes of the categories which are considered to be close to the given one. 
	 * The given category is always included in the list.*/
	private static List<Integer> getCloseCategories(ICategoriesData categoriesData, int givenCategoryId, CloseCategoryCalculation closeCategoryCalculation)
	{
		List<Integer> closeCategories = new ArrayList<Integer>();
		
		for (int catId : categoriesData.categoriesToUse())
		{
			if (areCategoriesClose( categoriesData, catId, givenCategoryId, closeCategoryCalculation))
				closeCategories.add(catId);
		}
		return closeCategories;
	}
	
	/** Answers if 2 categories are close to each other. The same categories should always be close.*/
	private static boolean areCategoriesClose(ICategoriesData categoriesData, int catId1, int catId2, CloseCategoryCalculation closeCategoryCalculation)
	{
		switch (closeCategoryCalculation)
		{
			case Similarity: 
				// based on the adjacency (similarity >= level)
				return (categoriesData.getCategorySimilarity(catId1, catId2) >= 0.9); // similarity = 0.9 for the adjacent categories on the wheel
			case Quadrant:
				// based on the same quadrant of the categories
				return (categoriesData.getCategoryQuadrant(catId1) == categoriesData.getCategoryQuadrant(catId2));
			case Polarity:
				// based on the same polarity of the categories
				return (categoriesData.getCategoryPolarity(catId1) == categoriesData.getCategoryPolarity(catId2));
			default:
				return (catId1 == catId2);		
		}
	}
}
