/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import parameters.EmotionRecognitionParameters;
import classification.ClassificationUtil;
import utility.Pair;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import data.documents.Tweet;
import learners.models.PMIBinaryLexiconConstruction;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import linguistic.UtilText;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class DataRepresentationRefinement {

	
	/**
	 * Removes all the terms which appear not often enough
	 * @param tweetRepresentation
	 * @param frequencyThreshold - 
	 * if the number of the overall term occurrence is lower than this threshold, the term is deleted from all the representation entries
	 */
	public static void deleteLowFrequencyTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, int frequencyThreshold)
	{
		// 1. compute the number of occurrences for each term
		Map<String, Integer> occurTermMap = UtilLearner.computeTermOccurrence(tweetRepresentation);
		
		// 2. find terms with low occurrence
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Integer> termEntry : occurTermMap.entrySet())
		{
			if (termEntry.getValue() < frequencyThreshold)
				termsToRemove.add(termEntry.getKey());
		}
		
		// 3. remove terms with low occurrence
		for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
		{
			reprEntry.getValue().removeAll(termsToRemove);
		}
	}
	
	/**
	 * Removes all the terms which appear not often enough in the annotated data set
	 * @param tweetRepresentation
	 * @param frequencyThreshold - 
	 * if the number of the overall term occurrence is lower than this threshold, the term is deleted from all the representation entries
	 */
	public static void deleteLowFrequencyTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, Map<Integer, double[]> annotatedTweetEmotions, int frequencyThreshold)
	{
		// 1. compute the number of occurrences for each term
		Map<String, Integer> occurTermMap = UtilLearner.computeTermOccurrence(tweetRepresentation, annotatedTweetEmotions.keySet());
		
		// 2. find terms with low occurrence
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Integer> termEntry : occurTermMap.entrySet())
		{
			if (termEntry.getValue() < frequencyThreshold)
				termsToRemove.add(termEntry.getKey());
		}
		
		// 3. remove terms with low occurrence
		for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
		{
			reprEntry.getValue().removeAll(termsToRemove);
		}
	}
	

	
	/** deletes terms with low absolute PMI values in terms of polarity. */
	public static void deletePolarityAmbiguousTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, Integer> annotatedTweetPolarities, Map<Integer, Double> objectWeights, int minPolPMIOccur, double PMIthreshold, boolean applySmoothing)
	{		
		
		try {
			
			// 1. compute the PMI scores for the current terms
			Map<String, Double> termScores;
			termScores = PMICalculation.extractPMIValues(annotatedTweetPolarities, tweetRepresentation, objectWeights,
					minPolPMIOccur, PMIthreshold, applySmoothing);
		
			// 2. find terms to delete: those that didn't appear in PMIScores
			Set<String> termsToRemove = new HashSet<String>();
			for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
			{
				// we add all the terms in the beginning
				termsToRemove.addAll(reprEntry.getValue());
			}
			// and then delete all those that are not given PMI score
			termsToRemove.removeAll(termScores.keySet());
			
			
			// 3. remove terms with low occurrence
			for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
			{
				reprEntry.getValue().removeAll(termsToRemove);
			}
			
		} catch (Exception e) {
			System.out.println("Couldn't delete ambiguous terms!");
			e.printStackTrace();
		}
	}
	
	/** deletes terms with low absolute PMI values in terms of polarity. 
	 * Compared to another version - creates a new copy of representation*/
	public static Map<Integer,List<String>> detectAndDeletePolarityAmbiguousTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, Integer> annotatedTweetPolarities, Map<Integer, Double> objectWeights, int minPolPMIOccur, double PMIthreshold, boolean applySmoothing)
	{		
		Map<Integer, List<String>> finalRepresentation = null;
		
		try {
			
			// 1. compute the PMI scores for the current terms
			Map<String, Double> termScores;
			termScores = PMICalculation.extractPMIValues(annotatedTweetPolarities, tweetRepresentation, objectWeights,
					minPolPMIOccur, PMIthreshold, applySmoothing);
			//PMICalculation.dropAmbiguousPMIValuesInPrevious(termScores);
			
			// 2. create a new representation contatining only the terms with given PMI score
			finalRepresentation = detectAndDeleteFilteredTermsFromRepresentation(tweetRepresentation, termScores.keySet());
			
		} catch (Exception e) {
			System.out.println("Couldn't delete ambiguous terms!");
			e.printStackTrace();
		}
		
		return finalRepresentation;
	}
	
	/** deletes terms with low absolute PMI values in terms of polarity classes (1, -1, 0) . 
	 * Compared to another version - creates a new copy of representation*/
	public static Map<Integer,List<String>> detectAndDeletePolarityAmbiguousTermsFromRepresentationWithPMI(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, Integer> annotatedTweetPolarities, Map<Integer, Double> objectWeights,
			BinarySelectionParams polaritySelectionParams)
	{	
		Map<Integer, List<String>> finalRepresentation = null;
		
		try {
			
			// 1. compute the PMI scores for the current terms
			PMIBinaryLexiconConstruction pmiLexLearner = new PMIBinaryLexiconConstruction(polaritySelectionParams);
			Map<String, Double> termScores;
			termScores =  pmiLexLearner.extractBinaryLexiconFromRepresentation(annotatedTweetPolarities, tweetRepresentation, objectWeights);
			//PMICalculation.dropAmbiguousPMIValuesInPrevious(termScores);
			
			// 2. create a new representation contatining only the terms with given PMI score
			finalRepresentation = detectAndDeleteFilteredTermsFromRepresentation(tweetRepresentation, termScores.keySet());
			
		} catch (Exception e) {
			System.out.println("Couldn't delete ambiguous terms!");
			e.printStackTrace();
		}
		
		return finalRepresentation;
	}
	
	/** deletes terms with low absolute PMI values in terms of polarity classes (1, -1, 0) . 
	 * Compared to another version - creates a new copy of representation*/
	public static Map<Integer,List<String>> detectAndDeletePolarityAmbiguousTermsFromRepresentationWithPMISimple(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, Integer> annotatedTweetPolarities, Map<Integer, Double> objectWeights, int minPolPMIOccur, double PMIthreshold, boolean applySmoothing)
	{	
		Map<Integer, List<String>> finalRepresentation = null;
		
		try {
			
			// 1. compute the PMI scores for the current terms
			Map<String, Double> termScores;
			termScores = PMICalculation.extractPMIValuesWithSimple(annotatedTweetPolarities, tweetRepresentation, objectWeights,
					minPolPMIOccur, PMIthreshold, applySmoothing);
			//PMICalculation.dropAmbiguousPMIValuesInPrevious(termScores);
			
			// 2. create a new representation contatining only the terms with given PMI score
			finalRepresentation = detectAndDeleteFilteredTermsFromRepresentation(tweetRepresentation, termScores.keySet());
			
		} catch (Exception e) {
			System.out.println("Couldn't delete ambiguous terms!");
			e.printStackTrace();
		}
		
		return finalRepresentation;
	}
	
	/** deletes terms with low absolute PMI values in terms of polarity. */
	public static Map<Integer,List<String>> detectAndDeleteLowEmotionalTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, double[]> annotatedTweetWeights, double minEmotionalityThreshold)
	{		
		Map<Integer, List<String>> finalRepresentation = null;
		
		try {
			
			// 1. compute the PMI scores for the current terms
			Map<String, Double> termScores;
			termScores = computeEmotionalityScores(tweetRepresentation, annotatedTweetWeights, minEmotionalityThreshold);
			
			// 2. create a new representation contatining only the terms with score above threshold
			finalRepresentation = detectAndDeleteFilteredTermsFromRepresentation(tweetRepresentation, termScores.keySet());
			
		} catch (Exception e) {
			System.out.println("Couldn't delete ambiguous terms!");
			e.printStackTrace();
		}
		
		return finalRepresentation;
	}
	
	
	/** This computes the emotionality score of each term appeared in the presentation
	 * Emotionality of a term is the percentage of emotional documents (tweets) among all where the term appeared
	 * @param tweetRepresentation
	 * @param annotatedTweetWeights
	 * @param minThreshold
	 * @return
	 */
	public static Map<String, Double> computeEmotionalityScores(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, double[]> annotatedTweetWeights, double minThreshold)
	{
		Map<String, Double> confidenceScores = new HashMap<String, Double>();
		Map<String, Integer> occurrencesNumber = new HashMap<String, Integer>();
		
		try {
			
			// get original emotionality presence check
			for (Map.Entry<Integer, List<String>> tweetRepr : tweetRepresentation.entrySet())
			{
				boolean isTweetEmotional = false;
				if (annotatedTweetWeights.containsKey(tweetRepr.getKey()))
					isTweetEmotional = ClassificationUtil.checkIfEmotional(annotatedTweetWeights.get(tweetRepr.getKey()), "GEW");
				for (String term : tweetRepr.getValue())
				{
					UtilCollections.incrementIntValueInMap(occurrencesNumber, term);
					if (isTweetEmotional)
						UtilCollections.incrementIntValueInMap(confidenceScores, term, 1.0);
				}
			}
			
			for (Map.Entry<String, Double> score : confidenceScores.entrySet())
			{
				int occurrence = occurrencesNumber.get(score.getKey());
				score.setValue(score.getValue()/(1.0 * occurrence));
			}
		}
		catch (Exception e) {
			System.out.println("Problem with computing confidence scores!");
			e.printStackTrace();
		}
		
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Double> score : confidenceScores.entrySet())
		{
			if (score.getValue() < minThreshold)
				termsToRemove.add(score.getKey());
		}
		UtilCollections.removeAllKeysFromMap(confidenceScores, termsToRemove);
		return confidenceScores;
	}
	
	/**
	 * !This looks unfinished!
	 * @param tweetRepresentation
	 * @param annotatedTweetWeights
	 * @param minThreshold
	 * @return
	 */
	public static Map<String, Double> computePMIEmotionalityScores(Map<Integer,List<String>> tweetRepresentation, 
			Map<Integer, double[]> annotatedTweetWeights, double minThreshold)
	{
		Map<String, Double> confidenceScores = new HashMap<String, Double>();
		Map<String, Integer> occurrencesNumber = new HashMap<String, Integer>();
		
		try {
			
			// get original emotionality presence check
			for (Map.Entry<Integer, List<String>> tweetRepr : tweetRepresentation.entrySet())
			{
				boolean isTweetEmotional = false;
				if (annotatedTweetWeights.containsKey(tweetRepr.getKey()))
					isTweetEmotional = ClassificationUtil.checkIfEmotional(annotatedTweetWeights.get(tweetRepr.getKey()), "GEW");
				for (String term : tweetRepr.getValue())
				{
					UtilCollections.incrementIntValueInMap(occurrencesNumber, term);
					if (isTweetEmotional)
						UtilCollections.incrementIntValueInMap(confidenceScores, term, 1.0);
				}
			}
			
			for (Map.Entry<String, Double> score : confidenceScores.entrySet())
			{
				int occurrence = occurrencesNumber.get(score.getKey());
				score.setValue(score.getValue()/(1.0 * occurrence));
			}
		}
		catch (Exception e) {
			System.out.println("Problem with computing confidence scores!");
			e.printStackTrace();
		}
		
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Double> score : confidenceScores.entrySet())
		{
			if (score.getValue() < minThreshold)
				termsToRemove.add(score.getKey());
		}
		UtilCollections.removeAllKeysFromMap(confidenceScores, termsToRemove);
		return confidenceScores;
	}
	
	private static Pair<Double, Double> splitPresenceAbsenceOfWeightForIndex(double[] weights, int index) {
		Pair<Double, Double> presAbsencePair = new Pair<Double, Double>();
		presAbsencePair.first = weights[index]; // presence
		presAbsencePair.second = UtilArrays.getSum(weights) - weights[index]; // absence
		return presAbsencePair;
		
	}
	
	private static Pair<Double, Double> computePositiveNegativeSplitOfWeight(double[] weights, ICategoriesData categoriesData) {
		Pair<Double, Double> posNegPair = new Pair<Double, Double>(0.0, 0.0);
		
		for (int categoryInd = 0; categoryInd <  weights.length; ++categoryInd) {
			int polarity = categoriesData.getCategoryPolarity(categoriesData.getCategoryId(categoryInd));
			if (polarity == 1) {// positive 
				posNegPair.first += weights[categoryInd];
			} else if (polarity == -1) { // negative
				posNegPair.second +=weights[categoryInd];
			}
		}
		
		return posNegPair;
		
	}
	
	public static Map<String, double[]> computePMIScoresForAllEmotions(Map<Integer, List<String>> tweetRepresentation, 
			Map<Integer, double[]> annotatedTweetWeights)
	{
		// compute number of occurrences of terms in each emotion class.
		Map<String, double[]> termEmotionCounts = new HashMap<String, double[]>();
		UtilLearner.computeTermClassFrequency(annotatedTweetWeights, tweetRepresentation, termEmotionCounts);
		// compute total occurrences of each emotion class.
		double[] emotionCounts = null;
		try {
			emotionCounts = FrequencyComputations.computeSumWeightsForAllValues(annotatedTweetWeights);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return computePMIScoresForAllEmotions(termEmotionCounts, emotionCounts);
	}
		
	public static Map<String, double[]> computePMIScoresForAllEmotions(
			Map<String, double[]> termEmotionCounts, 
			double[] emotionCounts)
		{
		int emotionNum = emotionCounts.length;
		
		//  create map with empty pmi scores
		Map<String, double[]> pmiScores = new HashMap<String, double[]>();
		for (String term : termEmotionCounts.keySet()) {
			pmiScores.put(term, new double[emotionNum]);
		}
		
		// compute total term presence
		Map<String, Double> termFrequencies = new HashMap<String, Double>();
		
		for (Map.Entry<String, double[]> termOccur : termEmotionCounts.entrySet()) {
			termFrequencies.put(termOccur.getKey(), UtilArrays.getSum(termOccur.getValue()));
		}
		
		// compute pmi scores separately for each emotion class
		for (int categoryInd = 0; categoryInd < emotionNum; ++categoryInd) {
			// extract frequencies of terms for current emotions
			Map<String, Double> presenceFrequencies = new HashMap<String, Double>();
			Map<String, Double> absenceFrequencies = new HashMap<String, Double>();
			for (Map.Entry<String, double[]> termOccur : termEmotionCounts.entrySet()) {
				if (termOccur.getValue().length < emotionNum) {
					int r = 0;
					r++;
				}
				Pair<Double, Double> presAbsencePair = splitPresenceAbsenceOfWeightForIndex(termOccur.getValue(), categoryInd);
				presenceFrequencies.put(termOccur.getKey(), presAbsencePair.getFirst());
				absenceFrequencies.put(termOccur.getKey(), presAbsencePair.getSecond());
			}
			Pair<Double, Double> presAbsencePairTotal = splitPresenceAbsenceOfWeightForIndex(emotionCounts, categoryInd);
			double presenceClassSize = presAbsencePairTotal.first;
			double absenceClassSize = presAbsencePairTotal.second;
			
			if (presenceClassSize == 0)
				continue;
			
			// compute pmi scores for the current emotion
			Map<String, Double> curEmotionPmiScores = null;
			try {
				curEmotionPmiScores = PMICalculation.extractPMIValues(
						termFrequencies, presenceFrequencies, absenceFrequencies, 
						presenceClassSize, absenceClassSize, 0, 0.0, true, false, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// update pmi scores
			for (Map.Entry<String, Double> pmiEmScore : curEmotionPmiScores.entrySet()) {
				double[] curScores = pmiScores.get(pmiEmScore.getKey());
				curScores[categoryInd] = pmiEmScore.getValue();
				pmiScores.put(pmiEmScore.getKey(), curScores);
			}
		}
		return pmiScores;
	}
	
	public static Map<String, Double> computePolarityPMIScoresFromEmotionCounts(
			Map<String, double[]> termEmotionCounts, 
			double[] emotionCounts)
		{
		int emotionNum = emotionCounts.length;
	
		// compute total term presence
		Map<String, Double> termFrequencies = new HashMap<String, Double>();
		
		for (Map.Entry<String, double[]> termOccur : termEmotionCounts.entrySet()) {
			termFrequencies.put(termOccur.getKey(), UtilArrays.getSum(termOccur.getValue()));
		}
		
		// compute counts of presence of each term in positive and negative classes respectively
		Map<String, Double> positiveFrequencies = new HashMap<String, Double>();
		Map<String, Double> negativeFrequencies = new HashMap<String, Double>();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(EmotionRecognitionParameters.defaultEmotionCategoriesType);
		
		for (Map.Entry<String, double[]> termOccur : termEmotionCounts.entrySet()) {
			String term = termOccur.getKey();
			Pair<Double, Double> posNegWeights = computePositiveNegativeSplitOfWeight(termOccur.getValue(), categoriesData);
			UtilCollections.incrementIntValueInMap(positiveFrequencies, term, posNegWeights.first);
			UtilCollections.incrementIntValueInMap(negativeFrequencies, term, posNegWeights.second);
		}
		
		// compute overall positive/negative sizes
		Pair<Double, Double> posNegTotalWeights =  computePositiveNegativeSplitOfWeight(emotionCounts, categoriesData);
			
		// compute pmi scores for all emotions
		Map<String, Double> pmiScores = null;
		try {
			pmiScores = PMICalculation.extractPMIValues(
					termFrequencies, positiveFrequencies, negativeFrequencies, 
					posNegTotalWeights.first, posNegTotalWeights.second, 0, 0.0, true, false, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pmiScores;
	}
	
	
	
	
	/** Leaves in the representation only those terms that were remainint. Returns the new copy of representation*/
	public static Map<Integer,List<String>> detectAndDeleteFilteredTermsFromRepresentation(Map<Integer,List<String>> tweetRepresentation, 
			Set<String> remainingTerms)
	{		
		Map<Integer, List<String>> finalRepresentation = new LinkedHashMap<Integer, List<String>>();
		
		try {
			
			// create a new representation contatining only the terms with given PMI score
			for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
			{
				List<String> newEntryValue = new ArrayList<String>(reprEntry.getValue());
				newEntryValue.retainAll(remainingTerms);
				finalRepresentation.put(reprEntry.getKey(), newEntryValue);
			}
		} catch (Exception e) {
			System.out.println("Couldn't delete filtered terms from  the map!");
			e.printStackTrace();
		}
		
		return finalRepresentation;
	}
}
