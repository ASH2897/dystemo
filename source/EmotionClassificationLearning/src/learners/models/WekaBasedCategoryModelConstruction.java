/*
    Copyright 2016 Valentina Sintsova, Marina Boia
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.models;

import java.util.List;
import java.util.Map;
import java.util.Set;

import classification.featurerepresentation.WekaAttributesSet;
import utils.WekaUtil;
import weka.classifiers.Classifier;
import weka.core.Instances;

public class WekaBasedCategoryModelConstruction extends WekaBasedModelConstruction {

	public WekaBasedCategoryModelConstruction(Classifier wekaClassifier) {
		super(wekaClassifier);
	}

	protected Instances loadInstances(Map<Integer, List<Integer>> sentimentLabels,
			Map<Integer, List<String>> textData,
			Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet, 
			List<Integer> categories, Set<Integer> nonConsideredCategories)
			throws Exception
	{
		System.out.println("Loading train data");
		Instances instances = WekaUtil.loadInstancesWithSpecificCategoryData(sentimentLabels, textData, objectWeights, attributesSet, categories, nonConsideredCategories);
		
		System.out.println("Done: " + instances.numAttributes() + " attributes");
		System.out.println(instances.numInstances() + " instances");
		
		System.out.println();
		
		return instances;
	}

}
