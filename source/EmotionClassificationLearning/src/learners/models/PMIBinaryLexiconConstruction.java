/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import learners.given.BinaryLexiconConstruction;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;


import utils.PMICalculation;

import data.categories.Polarity;

import functionality.UtilCollections;
import functionality.UtilString;

public class PMIBinaryLexiconConstruction extends BinaryLexiconConstruction{

	/* parameters */
	double threshold = 0;
	int numToOccur = -1;
	int ngramLength = 1;
	boolean applySmoothing = false;
	boolean applyToAbsoluteSoAScores = true; // was always true by default
	boolean saveOnlyPositiveScores = false; // was always false by default
	boolean nullifyLowPositiveScores = false; // was always false by default
	
	@Override
	public String getParameters() {
		return  "-T" + threshold + "-N" + numToOccur + "-L" + ngramLength + "-S" + (applySmoothing?1:0) + (saveOnlyPositiveScores ? "-P1" : "") + (nullifyLowPositiveScores ? "-F1" : "") + (applyToAbsoluteSoAScores?"-A1":"-A0") ;
	}
	
	public PMIBinaryLexiconConstruction() {};
	public PMIBinaryLexiconConstruction(String params) {
		setParameters(params);
	}
	
	public PMIBinaryLexiconConstruction(BinarySelectionParams params) {
		applySmoothing = params.applySmoothing;
		threshold = params.scoreThreshold;
		numToOccur = params.minOccurNum;
		saveOnlyPositiveScores = params.useOnlyPositive;
		nullifyLowPositiveScores = params.nullifyLowPositiveScores;
		applyToAbsoluteSoAScores  = params.applyToAbsoluteSoAScores ;
	}
	
	/** Parse the string of parameters in the following format: -T<threshold>-N<numToOccur>-L<ngramLength>'
	 * If any is omitted, the default value is taken*/
	@Override
	public void setParameters(String params) {
		String[] parsedParams = params.split("-");
		for (String param : parsedParams)
		{
			if (param.trim().length() == 0)
				continue;
			if (param.charAt(0) == 'T')
				threshold = Double.parseDouble(param.substring(1));
			if (param.charAt(0) == 'N')
				numToOccur = Integer.parseInt(param.substring(1));
			if (param.charAt(0) == 'L')
				ngramLength = Integer.parseInt(param.substring(1));
			if (param.charAt(0) == 'S')
				applySmoothing = param.substring(1).equals("1");
			if (param.charAt(0) == 'P')
				saveOnlyPositiveScores = param.substring(1).equals("1");
			if (param.charAt(0) == 'F')
				nullifyLowPositiveScores = param.substring(1).equals("1");
			if (param.charAt(0) == 'A')
				applyToAbsoluteSoAScores = !param.substring(1).equals("0");
		}
	}
	
	/**
	 * DO NOT USE: have to change for consistency with others
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet text as value
	 */
	@Override
	public Map<String, Double> extractBinaryLexicon(Map<Integer, Integer> sentimentLabels, Map<Integer, String> textData) throws Exception
	{
		HashMap<String, Double> updatedSubjecitivityLexicon = new HashMap<String, Double>();
		
		HashMap<String, Integer> tokenFrequencies = new HashMap<String, Integer>();
		
		HashMap<String, Integer> positiveFrequencies = new HashMap<String, Integer>();
		
		HashMap<String, Integer> negativeFrequencies = new HashMap<String, Integer>();
		
		int positiveClassSize = UtilCollections.countValueOccurrenceInMap(sentimentLabels, Polarity.POSITIVE);
		int negativeClassSize = UtilCollections.countValueOccurrenceInMap(sentimentLabels, Polarity.NEGATIVE);
		
		
		for(Integer key : sentimentLabels.keySet())
		{
			List<String> tokens = UtilString.getAllSeparatedNgrams(textData.get(key), ngramLength);
						
			for(int i = 0; i < tokens.size(); i++)
			{
				String token = tokens.get(i);
				
				if (token.length() < 2)
					continue;
				
				if(!tokenFrequencies.containsKey(token))
				{
					tokenFrequencies.put(token, new Integer(1));
				} 
				else
				{
					tokenFrequencies.put(token, tokenFrequencies.get(token).intValue() + 1);
				}
				
				if(sentimentLabels.get(key).intValue() == Polarity.POSITIVE)
				{
					if(!positiveFrequencies.containsKey(token))
					{
						positiveFrequencies.put(token, new Integer(0));
					}
					
					if(i > 0 && tokens.get(i - 1).equalsIgnoreCase("not")) //TODO: better detect negations
					{
						positiveFrequencies.put(token, positiveFrequencies.get(token).intValue() - 1);
							
					}
					else
					{
						positiveFrequencies.put(token, positiveFrequencies.get(token).intValue() + 1);	
					}
				}
				
				if(sentimentLabels.get(key).intValue() == Polarity.NEGATIVE)
				{
					if(!negativeFrequencies.containsKey(token))
					{
						negativeFrequencies.put(token, new Integer(0));
					} 
					
					if(i > 0 && tokens.get(i - 1).equalsIgnoreCase("not")) //TODO: better detect negations
					{
						negativeFrequencies.put(token, negativeFrequencies.get(token).intValue() - 1);						
					}
					else
					{
						negativeFrequencies.put(token, negativeFrequencies.get(token).intValue() + 1);	
					}
				}
			}
		}
		
		for(String token : tokenFrequencies.keySet())
		{
			if(tokenFrequencies.get(token).intValue() < numToOccur)
			{
				continue;
			}
			
			if (token.equals("❤❤❤❤"))
			{
				int i = 0;
				i++;
			}
			
			int positiveFrequency = 0;
			
			if(positiveFrequencies.containsKey(token))
			{
				positiveFrequency = positiveFrequencies.get(token).intValue();
			}
			
			int negativeFrequency = 0;
			
			if (negativeFrequencies.containsKey(token))
			{
				negativeFrequency = negativeFrequencies.get(token).intValue();
			}
			
			if (positiveFrequency < 0.6*numToOccur ||  negativeFrequency == 0)
			{
				continue;
			}
			
			//calculate Strength Of Association based on PMI
			double SoAValue = Math.log( positiveFrequency * negativeClassSize / (double)(positiveClassSize * negativeFrequency));
			
			if (Math.abs(SoAValue) >= threshold)
			{
				updatedSubjecitivityLexicon.put(token, SoAValue);
			}
		}	
		
		return updatedSubjecitivityLexicon;
	}
	
	private void RemoveNonPositiveScores(Map<String, Double> termScores) {
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Double> entry : termScores.entrySet()) {
			if (entry.getValue() <= 0.0)
				termsToRemove.add(entry.getKey());
		}
		UtilCollections.removeAllKeysFromMap(termScores, termsToRemove);
	}
	
	/**
	 * DO NOT USE: this method is not yet developed!
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet text as value
	 * IMPORTANT: in this case the parameters of NgramLength and freqLim will be ignored!
	 * 
	 */
	@Override
	public Map<String, Double> extractBinaryLexiconFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData) throws Exception
	{
		Map<String, Double> pmiScore = PMICalculation.extractPMIValues(sentimentLabels, textData, -1, threshold, applySmoothing);
		if (saveOnlyPositiveScores) {
			RemoveNonPositiveScores(pmiScore);
		}
		
		return pmiScore;
	}
	
	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet text as value
	 * IMPORTANT: in this case the parameters of NgramLength and freqLim will be ignored!
	 */
	@Override
	public Map<String, Double> extractBinaryLexiconFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights) throws Exception
	{
		Map<String, Double> PMIScores = PMICalculation.extractPMIValues(sentimentLabels, textData, objectWeights, numToOccur, threshold, applySmoothing, !applyToAbsoluteSoAScores, nullifyLowPositiveScores);
		
		// ! limit the infinity values by the absolute maximum + 1 over all category values
		double maximumPMIValue = 0.0;
		for (Map.Entry<String, Double> termEntry : PMIScores.entrySet())
			{
				Double curValue = termEntry.getValue();
				if (!curValue.isInfinite())
					if (Math.abs(curValue) > maximumPMIValue)
						maximumPMIValue = Math.abs(curValue);
			}
		
		for (Map.Entry<String, Double> termEntry : PMIScores.entrySet())
		{
			Double curValue = termEntry.getValue();
			if (curValue.isInfinite())
			{
				if (curValue > 0)
					PMIScores.put(termEntry.getKey(), maximumPMIValue + 1.0);
				else
					PMIScores.put(termEntry.getKey(), -maximumPMIValue - 1.0);
			}
		}
		
		if (saveOnlyPositiveScores) {
			RemoveNonPositiveScores(PMIScores);
		}
		
		return PMIScores;
	}
	
}
