/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.models;

import java.util.List;
import java.util.Map;

import classification.featurerepresentation.WekaAttributesSet;


import utils.WekaUtil;
import weka.classifiers.Classifier;
import weka.core.Instances;

public class WekaBasedBinaryLexiconConstruction extends WekaBasedModelConstruction {

	public WekaBasedBinaryLexiconConstruction(Classifier wekaClassifier)
	{
		super(wekaClassifier);
	}
	
	
	protected Instances loadInstances(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		System.out.println("Loading train data");
		Instances instances = WekaUtil.loadInstancesWithSpecificBinaryData(sentimentLabels, textData, objectWeights, false, attributesSet);
		
		System.out.println("Done: " + instances.numAttributes() + " attributes");
		System.out.println(instances.numInstances() + " instances");
		
		System.out.println();
		
		return instances;
	}
	
	
}
