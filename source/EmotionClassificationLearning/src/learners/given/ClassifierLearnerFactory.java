/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.List;

import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;

import learners.definitions.ClassifierLearner;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learning.parameters.AllLearningParameters;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.DefaultAllLearningParameters;
import learning.parameters.IndependentLearnerParameters;

public class ClassifierLearnerFactory {

	public enum LearnerName {
		None, WeightedVoting, WeightedBalancedVoting, 
		IndependentPMI, IndependentWekaMNB, IndependentWekaLogReg, IndependentWekaSVM,
		CategoryWekaLogReg, CategoryWekaMNB, CategoryWekaSVM, 
		ClassifierLearnerForOneCategoryWithWekaLogReg, ClassifierLearnerForOneCategoryWithPMI};
	
	public static ClassifierLearner getLearner(AllLearningParameters fullLearningParams, List<String> indicatorCandidates)
	{
		if (fullLearningParams.learnerParams == null)
			fullLearningParams.learnerParams = DefaultAllLearningParameters.getDefaultClassifierLearnerParamsForClassifier(fullLearningParams.learnerName);
		
		switch (fullLearningParams.learnerName)
		{
			case None: return null;
			case WeightedVoting: 
				{
					return new ClassifierLearnerWeightedVoting(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
				}
			case WeightedBalancedVoting: 
				return new ClassifierLearnerWeightedBalancedVoting(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
				
			case IndependentPMI:
			{
				//((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMI;
				return new ClassifierLearnerIndependentCategories(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case IndependentWekaMNB:
			{
				((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.WekaMNB;
				return new ClassifierLearnerIndependentCategoriesOnWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case IndependentWekaLogReg:
			{
				((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.WekaLogReg;
				return new ClassifierLearnerIndependentCategoriesOnWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case IndependentWekaSVM:
			{
				((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.WekaSVM;
				return new ClassifierLearnerIndependentCategoriesOnWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case CategoryWekaLogReg:
			{
				((WekaCategoryClassifierLearnerParams)fullLearningParams.learnerParams).classifierModel = AvailableWekaClassifiers.LogisticRegression;
				return new ClassifierLearnerForWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case CategoryWekaMNB:
			{
				((WekaCategoryClassifierLearnerParams)fullLearningParams.learnerParams).classifierModel = AvailableWekaClassifiers.NaiveBayesMultinomial;
				return new ClassifierLearnerForWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case CategoryWekaSVM:
			{
				((WekaCategoryClassifierLearnerParams)fullLearningParams.learnerParams).classifierModel = AvailableWekaClassifiers.SVM;
				return new ClassifierLearnerForWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case ClassifierLearnerForOneCategoryWithWekaLogReg:
			{
				((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.WekaLogReg;
				return new ClassifierLearnerForOneCategoryWithWeka(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			case ClassifierLearnerForOneCategoryWithPMI:
			{
				((IndependentLearnerParameters)fullLearningParams.learnerParams).binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMI;
				return new ClassifierLearnerForOneCategoryOnIndependent(indicatorCandidates, fullLearningParams.learnerParams, fullLearningParams.featureExtractionParams, fullLearningParams.featureSelectionParams);
			}
			default: return null;
		}
	}
	
	
	public static ClassifierLearner getCurrentLearner(List<String> indicatorCandidates)
	{
		return getLearner(AllLearningParameters.currentSetup, indicatorCandidates);
	}

}
