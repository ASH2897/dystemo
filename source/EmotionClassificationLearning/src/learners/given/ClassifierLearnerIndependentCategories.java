/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearnerWithInternalRepresentation;
import learners.featureselection.FeatureSelectionAlgorithm;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.IndependentLearnerParameters;

import utils.CategoriesBinarySplitting;
import utils.TweetCorpusAnnotationProcessing;
import utils.UtilBinaryLearnerLoader;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.WeightedClassifierLexiconBasedIndependent;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import data.categories.CategoryProcessor;
import data.categories.ICategoriesData;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class ClassifierLearnerIndependentCategories extends ClassifierLearnerWithInternalRepresentation {


	public ClassifierLearnerIndependentCategories(
			List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
		
		if (learnerParams instanceof IndependentLearnerParameters)
		{
			curLearnerParams = (IndependentLearnerParameters)learnerParams;
		}
		else
		{
			System.out.print("Incorrect parameter setting for ClassifierLearnerIndependentCategories");
		}
	}

	protected IndependentLearnerParameters curLearnerParams;

	@Override
	public WeightedClassifier learnClassifierOnRefinedData (Map<Integer, double[]> annotatedTweetEmotions) throws Exception {
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(emotionCategoriesType); 
		
		BinaryLexiconConstruction binLexConstruction = 
				UtilBinaryLearnerLoader.
					getInstanceOfLexiconConstruction(
							curLearnerParams.binaryLearnerParameters.binaryLearnerName,
							curLearnerParams.binaryLearnerParameters.binaryLexiconParams);
		
		Map<Integer, List<Integer>> emotionLabels = 
				TweetCorpusAnnotationProcessing.
					getEmotionLabelsFromWeights(annotatedTweetEmotions, 
							curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.alphaCut : 1.0,
							curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.emotionLimit : categoriesData.getCategoryNum());
		
		Map<Integer, Map<String, Double>> mapCategoryToTerms = new HashMap<Integer, Map<String, Double>>();
		
		Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
		for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
		{
			annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
		}
		
		boolean applyAdditionalFeatureSelection = curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg != FeatureSelectionAlgOption.No;
		try {
			
			// constructing binary lexicon for each category
			for (int categoryId : categoriesData.categoriesToUse())
			{
				Map<Integer,Integer> categoryLabelData = CategoriesBinarySplitting.
						getBinaryDataForCategory(
								CategoryProcessor.getCategoriesData(emotionCategoriesType), 
								categoryId, emotionLabels, 
								curLearnerParams.closeCategoryUseType,
								curLearnerParams.closeCategoryCalculation);
				
				if (!containsEnoughExamplesToLearn(categoryLabelData))
					continue; // not enough examples to learn for the current category
				
				Map<Integer, List<String>> featureRepresentation = null; 
				if (applyAdditionalFeatureSelection)
				{
					 featureRepresentation = FeatureSelectionAlgorithm.selectFeaturesFromBinaryRepresentation(
							tweetRepresentation, 
							categoryLabelData, 
							annotatedTweetWeights,
							curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams);
				}
				else
					featureRepresentation = tweetRepresentation;
				
				
				Map<String, Double> categoryWeightedLexicon = binLexConstruction.extractBinaryLexiconFromRepresentation(categoryLabelData, featureRepresentation, annotatedTweetWeights);
				
				mapCategoryToTerms.put(categoryId, categoryWeightedLexicon);
				//System.out.println("Binary lexicon for category " + categoryId + " constructed. Found " + categoryWeightedLexicon.size() + " terms.");if (applyAdditionalFeatureSelection && ((FullyIndependentLearningParams)params).fsOption == FeatureSelectionOption.PMI)
				if (applyAdditionalFeatureSelection)
					featureRepresentation.clear();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return createClassifierFromLexicons(mapCategoryToTerms);
	}
	
	/**
	 * Counts if there are some data to learn for the category
	 * @param categoryLabelData
	 * @return
	 */
	protected boolean containsEnoughExamplesToLearn(Map<Integer,Integer> categoryLabelData)
	{
		int positiveExamplesNum = UtilCollections.countValueOccurrenceInMap(categoryLabelData, 1); // 1 for positive class
		return (positiveExamplesNum > 0);
	}
	
	/**
	 * merging all the binary lexicons into one weighted for all emotions
	 * @param mapCategoryToTerms
	 * @return
	 */
	public WeightedClassifier createClassifierFromLexicons(Map<Integer, Map<String, Double>> mapCategoryToTerms)
	{
	
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(emotionCategoriesType); 
		
		Map<String, double[]> termsData = new HashMap<String, double[]>();
		for (int categoryId : categoriesData.categoriesToUse())
		{
			Map<String, Double> categoryWeightedLexicon = mapCategoryToTerms.get(categoryId);
			if (categoryWeightedLexicon == null)
				continue;
			for (String term : categoryWeightedLexicon.keySet())
			{
				double[] termWeights = termsData.get(term);
				if (termWeights == null)
				{
					termWeights = new double[categoriesData.getCategoryNum()];
					termsData.put(term, termWeights);
				}
				termWeights[categoriesData.getCategoryIndexById(categoryId)] = categoryWeightedLexicon.get(term);
			}
		}
		WeightedEmotionLexicon constructedLexicon = new WeightedEmotionLexicon();
		constructedLexicon.createSimpleFromGivenData(termsData, categoriesData);
		if (curLearnerParams.useDependentClassifierForOutput) {
			return new WeightedClassifierLexiconBased(constructedLexicon);
		} else {
			return new WeightedClassifierLexiconBasedIndependent(constructedLexicon);
		}
	}

	@Override
	public WeightedClassifier updateOutputClassifierForLearnerSpecificApplicationParameters(
			WeightedClassifier classifier,
			ClassifierApplicationParameters applicationParameters) {
		if (curLearnerParams.useDependentClassifierForOutput) {
			WeightedClassifierWrapperWithApplicationParams resClassifier;
			if (classifier instanceof WeightedClassifierWrapperWithApplicationParams)
			{
				resClassifier = (WeightedClassifierWrapperWithApplicationParams)classifier;
			}
			else
				resClassifier = new WeightedClassifierWrapperWithApplicationParams(classifier);
			
			resClassifier.curApplicationParams.alphaCut = 
					applicationParameters.multiLabelOutputRefinementParams != null ? applicationParameters.multiLabelOutputRefinementParams.alphaCut : 1.0;
			return resClassifier;
		} else {
			return classifier;
		}
	}
	
}
