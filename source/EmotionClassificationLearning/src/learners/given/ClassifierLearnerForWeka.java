/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import learners.definitions.ClassifierLearnerWithInternalRepresentation;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.models.WekaBasedModelConstruction;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import utils.TweetCorpusAnnotationProcessing;
import utils.WekaUtil;
import weka.core.Instances;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierWekaBased;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.featurerepresentation.WekaAttributesSet;
import classification.loaders.WekaClassifierFactory;
import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;
import data.categories.CategoryProcessor;
import data.categories.ICategoriesData;
import functionality.UtilArrays;

public class ClassifierLearnerForWeka extends ClassifierLearnerWithInternalRepresentation {

	

	public ClassifierLearnerForWeka(List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
		

		if (learnerParams instanceof  WekaCategoryClassifierLearnerParams)
		{
			curLearnerParams = ( WekaCategoryClassifierLearnerParams)learnerParams;
		}
		else
		{
			System.out.print("Incorrect parameter setting for ClassifierLearnerForWeka ");
		}
	}
	
	WekaCategoryClassifierLearnerParams curLearnerParams;
	
	public static class WekaCategoryClassifierLearnerParams extends ClassifierLearnerParams
	{
		public AvailableWekaClassifiers classifierModel;
		
		public WekaCategoryClassifierLearnerParams (LearnerName curLearnerName)
		{
			String learnerName = curLearnerName.name();
			if (!learnerName.contains("Weka"))
			{
				System.out.print("Incorrect parameter setting for " + curLearnerName);
			}
			else
			{
				String wekaClassType = learnerName.substring(learnerName.indexOf("Weka"));
				classifierModel =  WekaClassifierFactory.getWekaClassifierByNameFromLearnerName(wekaClassType); 
			}
		}
		
		public WekaCategoryClassifierLearnerParams (AvailableWekaClassifiers classifierModel)
		{
			this.classifierModel = classifierModel;
		}
		
		@Override
		public String printParametersShort()
		{
			String original = super.printParametersShort();
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("-wekaCl-" + classifierModel.toString());
			
			return original + "," + sb.toString();
		}
		
		@Override
		public ClassifierLearnerParams clone()
		{
			ClassifierLearnerParams newLearnerParams = new WekaCategoryClassifierLearnerParams (this.classifierModel);
			newLearnerParams.copyDataFromAnother(this);
			return newLearnerParams;
		}
		
	}

	@Override
	public WeightedClassifier learnClassifierOnRefinedData(
			Map<Integer, double[]> annotatedTweetEmotions) {
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(emotionCategoriesType); 
		
		Map<Integer, List<Integer>> emotionLabels = 
				TweetCorpusAnnotationProcessing.
					getEmotionLabelsFromWeights(annotatedTweetEmotions, 
							curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.alphaCut : 1.0,
							curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.emotionLimit : categoriesData.getCategoryNum());
		
		Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
		for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
		{
			annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
		}
		
		int[] categoriesIdsToUse = categoriesData.categoriesToUse();
		List<Integer> emotionCategories = new ArrayList<Integer>();
		for (int catId : categoriesIdsToUse)
			emotionCategories.add(catId);
		
		Set<Integer> nonConsideredCategories = new HashSet<Integer>();
		nonConsideredCategories.add(categoriesData.contextCategory());
		
		WeightedClassifierWekaBased classifier = null;
		try {
			WekaAttributesSet attributesSet = WekaUtil.detectAttributesFromTexts(tweetRepresentation);
			Instances exampleProblem = WekaUtil.createExampleProblemCategory(attributesSet, emotionCategories);
			
			WekaBasedModelConstruction modelConstruction = new WekaBasedModelConstruction(WekaClassifierFactory.getWekaClassifierByName(curLearnerParams.classifierModel, curLearnerParams.specificLearnerParams));
			weka.classifiers.Classifier emotionModel = modelConstruction.extractModelFromRepresentation(emotionLabels, tweetRepresentation, annotatedTweetWeights, attributesSet, emotionCategories, nonConsideredCategories);
			classifier = new WeightedClassifierWekaBased(emotionModel, attributesSet, exampleProblem);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return classifier;
	}

	@Override
	public WeightedClassifier updateOutputClassifierForLearnerSpecificApplicationParameters(
			WeightedClassifier classifier,
			ClassifierApplicationParameters applicationParameters) {
		
		// if classifier supports the probabilistic output:
		if (WekaClassifierFactory.fl_supportsProbabilisticOutput(curLearnerParams.classifierModel))
		{
			WeightedClassifierWrapperWithApplicationParams resClassifier;
			if (classifier instanceof WeightedClassifierWrapperWithApplicationParams)
			{
				resClassifier = (WeightedClassifierWrapperWithApplicationParams)classifier;
			}
			else
				resClassifier = new WeightedClassifierWrapperWithApplicationParams(classifier);
			resClassifier.curApplicationParams.alphaCut = 
					applicationParameters.multiLabelOutputRefinementParams != null ? applicationParameters.multiLabelOutputRefinementParams.alphaCut : 1.0;
			return resClassifier;
		}
		else
		{
			return classifier;
		}
	}
	
}
