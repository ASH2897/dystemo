/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import learners.featureextraction.IndependentWekaRepresentationCache;
import learners.featureselection.FeatureSelectionAlgorithm;
import learners.models.WekaBasedModelConstruction;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import utils.CategoriesBinarySplitting;
import utils.TweetCorpusAnnotationProcessing;
import utils.UtilBinaryLearnerLoader;
import utils.WekaUtil;
import weka.core.Instances;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierWekaBasedIndependent;
import classification.definitions.classifiers.WeightedClassifierWekaBasedIndependentFully;
import classification.definitions.classifiers.wrappers.ClassifierWekaBasedIndependentFullyWithProbabilities;
import classification.definitions.classifiers.wrappers.ClassifierWekaBasedIndependentWithProbabilities;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.featurerepresentation.WekaAttributesSet;
import data.categories.CategoryProcessor;
import data.categories.ICategoriesData;
import functionality.UtilArrays;
import functionality.UtilFiles;

public class ClassifierLearnerIndependentCategoriesOnWeka extends ClassifierLearnerIndependentCategories{

	
	
	public ClassifierLearnerIndependentCategoriesOnWeka(
			List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
		
	}

	boolean toSaveCurrentCatModels = false;
	
	@Override
	public WeightedClassifier learnClassifierOnRefinedData(Map<Integer, double[]> annotatedTweetEmotions) throws Exception {
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(emotionCategoriesType); 
		
		Map<Integer, List<Integer>> emotionLabels = TweetCorpusAnnotationProcessing.
				getEmotionLabelsFromWeights(annotatedTweetEmotions, 
						curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.alphaCut : 1.0,
								curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.emotionLimit : categoriesData.getCategoryNum());
		
		Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
		for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
		{
			annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
		}
		
		List<weka.classifiers.Classifier> catModels = new ArrayList<weka.classifiers.Classifier>();
		WeightedClassifier classifier = null;
		boolean applyAdditionalFeatureSelection = curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg != FeatureSelectionAlgOption.No;
		
		try {
			WekaAttributesSet attributesSet = null;
			Instances exampleProblem = null;
			
			List<WekaAttributesSet> categoryAttrSets = null;
			List<Instances> categoryExampleProblems = null;
			
			if (applyAdditionalFeatureSelection)
			{
				// as features will be selected separately per category, we will need to store those separate feature representation for each category
				categoryAttrSets = new ArrayList<WekaAttributesSet>();
				categoryExampleProblems = new ArrayList<Instances>();
			}
			else
			{
				// the feature representation will be common/shared for all categories 
				attributesSet = WekaUtil.detectAttributesFromTexts(tweetRepresentation);
				exampleProblem = WekaUtil.createExampleProblemBinary(attributesSet);
			}
			
			// constructing binary model for each category
			Map<Integer, List<String>> featureRepresentation = null; 
			Map<Integer,Integer> categoryLabelData = null;
			for (int categoryId : categoriesData.categoriesToUse())
			{
				//if (categoryId < 15)
				//	continue;
				
				System.out.println();
				System.out.println("Started consructing the classifier for category " + categoryId);
				System.out.println();
				categoryLabelData = CategoriesBinarySplitting.getBinaryDataForCategory(
										CategoryProcessor.getCategoriesData(emotionCategoriesType),
										categoryId,
										emotionLabels,
										curLearnerParams.closeCategoryUseType,
										curLearnerParams.closeCategoryCalculation);
				
				if (!containsEnoughExamplesToLearn(categoryLabelData))
				{
					// not enough examples to learn for the current category
					catModels.add(null);
					if (applyAdditionalFeatureSelection)
					{
						categoryAttrSets.add(null);
						categoryExampleProblems.add(null);
					}
					continue;
				}
				
				if (applyAdditionalFeatureSelection)
				{
					 featureRepresentation = FeatureSelectionAlgorithm.selectFeaturesFromBinaryRepresentation(
							tweetRepresentation, 
							categoryLabelData, 
							annotatedTweetWeights,
							curLearnerParams.binaryLearnerParameters.binaryFeatureSelectionParams);
					 
					 attributesSet = WekaUtil.detectAttributesFromTexts(featureRepresentation);
					 exampleProblem = WekaUtil.createExampleProblemBinary(attributesSet);	
				}
				else
					featureRepresentation = tweetRepresentation;
				
				if (toSaveCurrentCatModels && applyAdditionalFeatureSelection) {
						UtilFiles.writeSerializableDataToFile(attributesSet, "output/tmp/ind-attributesSet-" +  categoryId);
				}
				
				
				WekaBasedModelConstruction binLexConstruction = UtilBinaryLearnerLoader.
						getInstanceOfWekaBasedBinaryLearner
								(curLearnerParams.binaryLearnerParameters.binaryLearnerName,
								curLearnerParams.binaryLearnerParameters.binaryLexiconParams);
				
				weka.classifiers.Classifier categoryModel;
				if (curLearnerParams.useCacheForIndependentRepresentation)
				{
					Instances curCategoryInstances = getCurrentCachedInstances(categoryId, categoryLabelData, featureRepresentation, annotatedTweetWeights, attributesSet);
					categoryModel = binLexConstruction.extractModelFromRepresentation(curCategoryInstances);
				}
				else
					categoryModel = binLexConstruction.extractModelFromRepresentation(categoryLabelData, featureRepresentation, annotatedTweetWeights, attributesSet);
				catModels.add(categoryModel);
				
				if (toSaveCurrentCatModels) {
					UtilFiles.writeSerializableDataToFile(categoryModel, "output/tmp/ind-catmodel-" +  categoryId);
				}
				
				
				if (applyAdditionalFeatureSelection)
				{
					featureRepresentation.clear();
					categoryAttrSets.add(attributesSet);
					categoryExampleProblems.add(exampleProblem);
				}
				categoryLabelData.clear();
			}
			
			
			if (applyAdditionalFeatureSelection)
			{
				classifier = new WeightedClassifierWekaBasedIndependentFully(catModels, categoryAttrSets, categoryExampleProblems);
			}
			else
			{
				classifier = new WeightedClassifierWekaBasedIndependent(catModels, attributesSet, exampleProblem);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return classifier;
	}
	
	
	private Instances getCurrentCachedInstances(int categoryId, 
			Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		Instances curInstances = IndependentWekaRepresentationCache.getCurrentCachedRepresentation(categoryId);
		if (curInstances == null) // no cached value was found
		{
			curInstances = WekaBasedModelConstruction.loadInstancesForSentiment(sentimentLabels, textData, objectWeights, attributesSet);
			IndependentWekaRepresentationCache.saveNewCurrentRepresentation(curInstances, categoryId);
		}
		return curInstances;
	}
	
	@Override
	public WeightedClassifier updateOutputClassifierForLearnerSpecificApplicationParameters(
			WeightedClassifier classifier,
			ClassifierApplicationParameters applicationParameters) {
		if (applicationParameters.probabilisticBinaryOutputTreatmentParams != null &&
				applicationParameters.probabilisticBinaryOutputTreatmentParams.useProbabilities == true 
				&& UtilBinaryLearnerLoader.ifSupportsProbabilisticOutput(curLearnerParams.binaryLearnerParameters.binaryLearnerName)
				)
		{
			WeightedClassifier resClassifier;
			if (classifier instanceof WeightedClassifierWekaBasedIndependent)
			{
				if (classifier instanceof ClassifierWekaBasedIndependentWithProbabilities)
					resClassifier = (ClassifierWekaBasedIndependentWithProbabilities) classifier;
				else					
					resClassifier = new ClassifierWekaBasedIndependentWithProbabilities((WeightedClassifierWekaBasedIndependent)classifier);
				((ClassifierWekaBasedIndependentWithProbabilities)resClassifier).setProbabilityLimit( applicationParameters.probabilisticBinaryOutputTreatmentParams.probabilityThreshold);
			}
			else if (classifier instanceof WeightedClassifierWekaBasedIndependentFully)
			{
				if (classifier instanceof ClassifierWekaBasedIndependentFullyWithProbabilities)
					resClassifier = classifier;
				else
					resClassifier = new ClassifierWekaBasedIndependentFullyWithProbabilities((WeightedClassifierWekaBasedIndependentFully)classifier);
				((ClassifierWekaBasedIndependentFullyWithProbabilities)resClassifier).setProbabilityLimit( applicationParameters.probabilisticBinaryOutputTreatmentParams.probabilityThreshold);
			}
			else
				resClassifier = classifier;
			return resClassifier;
		}
		else
			return classifier;
	}

}
