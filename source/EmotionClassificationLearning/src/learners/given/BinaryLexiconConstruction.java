/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BinaryLexiconConstruction extends TermScoresLexiconConstruction {
	
	public abstract Map<String, Double> extractBinaryLexicon(Map<Integer, Integer> sentimentLabels, Map<Integer, String> textData) throws Exception;
	
	abstract public Map<String, Double> extractBinaryLexiconFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData) throws Exception;
	
	
	abstract public Map<String, Double> extractBinaryLexiconFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights) throws Exception;
	
	/**
	 * Replaces positive weights with class 1, and negative weights with class -1
	 * @param weightedBinaryLexicon
	 * @return
	 */
	public static Map<String, Integer> tranformWeightedBinaryToCategorical(Map<String, Double> weightedBinaryLexicon)
	{
		Map<String, Integer> resultCategoricalLexicon = new HashMap<String, Integer>();
		for (Map.Entry<String, Double> termEntry : weightedBinaryLexicon.entrySet())
		{
			resultCategoricalLexicon.put(termEntry.getKey(), (int) Math.signum(termEntry.getValue()));
		}
		return resultCategoricalLexicon;
	}

	public String getParameters() {
		return super.getParameters();
	}

	public void setParameters(String params) {
		super.setParameters(params);
	}
	
}
