/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearner;
import learners.definitions.ClassifierLearnerWithInternalRepresentation;
import learners.models.WekaBasedModelConstruction;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;

import utils.WekaUtil;
import weka.core.Instances;

import classification.ClassificationUtil;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.BinaryClassifierWekaBased;
import classification.definitions.classifiers.wrappers.WeightedClassifierHierachicalWrapper;
import classification.featurerepresentation.SyntacticFeaturesSet;
import classification.featurerepresentation.WekaAttributesSet;
import classification.featurerepresentation.WekaAttributesSetWithSyntFeatures;
import classification.loaders.WekaClassifierFactory;
import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;
import data.documents.Tweet;
import data.documents.TweetFeaturesExtended;

public class ClassifierLearnerHierarchical extends ClassifierLearner {

	public ClassifierLearnerHierarchical(
			ClassifierLearnerParams learnerParams, ClassifierLearnerWithInternalRepresentation internalClassifierLearner) {
		super(learnerParams);
		this.internalClassifierLearner = internalClassifierLearner;
	}

	public ClassifierLearnerHierarchical(List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams, ClassifierLearnerWithInternalRepresentation internalClassifierLearner) {
		super(indicatorCandidates, learnerParams);
		this.internalClassifierLearner = internalClassifierLearner;
	}
	
	Map<Integer, TweetFeaturesExtended> featureData;
	
	ClassifierLearnerWithInternalRepresentation internalClassifierLearner;

	@Override
	public void initialize(List<? extends Tweet> tweetData)
	{
		super.initialize(tweetData);
		
		internalClassifierLearner.initialize(tweetData);
		getFeatureData(tweetData);
	}
	
	private void getFeatureData(List<? extends Tweet> tweetData)
	{
		featureData = new HashMap<Integer,TweetFeaturesExtended>();
		for (Tweet tweet : tweetData)
		{
			if (tweet instanceof TweetFeaturesExtended )
			{
				TweetFeaturesExtended featTweet = (TweetFeaturesExtended)tweet;
				featureData.put(tweet.tweetId, featTweet);
			}
		}
	}
	
	@Override
	public void initializeNextIteration()
	{
		internalClassifierLearner.initializeNextIteration();
	}
	
	@Override
	public WeightedClassifier learnClassifier(Map<Integer, double[]> annotatedTweetEmotions) throws Exception 
	{
		WeightedClassifier internalEmotionalClassifier = internalClassifierLearner.learnClassifier(annotatedTweetEmotions);
		
		// Learn classifier for neutral case
		
		
		// learn the set of linguistic attributes
		WekaAttributesSet attributesSetOrig = WekaUtil.detectAttributesFromTexts(internalClassifierLearner.originalTweetRepresentation);
		
		// extend the set of linguistic attributes with extra stylistic features
		WekaAttributesSetWithSyntFeatures attributesSet = new WekaAttributesSetWithSyntFeatures(attributesSetOrig);
		
		// create the original instances for future application
		Instances exampleProblem = WekaUtil.createExampleProblemBinary(attributesSet);
		
		// get the feature representation
		Map<Integer, Map<Integer, Integer>> featureRepresentation = constructFeatureRepresentation(internalClassifierLearner.originalTweetRepresentation, attributesSet);
		
		BinaryClassifierWekaBased neutralClassifier = null;
		try
		{
			// construct annotation data
			Map<Integer,Integer> neutralLabelData = new HashMap<Integer,Integer>();
			for (Map.Entry<Integer, double[]> tweetEmotion : annotatedTweetEmotions.entrySet())
			{
				boolean isNeutral =  !ClassificationUtil.checkIfEmotional(tweetEmotion.getValue(), "GEW");
				int neutClass = (isNeutral)?-1:1;
				neutralLabelData.put(tweetEmotion.getKey(), neutClass);
			}
			
			// construct the weights data (all with 1.0)
			Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
			for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
			{
				annotatedTweetWeights.put(tweetEntry.getKey(), 1.0);
			}
			
			WekaBasedModelConstruction modelConstruction = new WekaBasedModelConstruction(WekaClassifierFactory.getWekaClassifierByName(AvailableWekaClassifiers.LogisticRegression));//new NaiveBayesMultinomial());
			weka.classifiers.Classifier emotionModel = modelConstruction.extractModelFromRepresentationWithFeatures(neutralLabelData, featureRepresentation, annotatedTweetWeights, attributesSet);
			neutralClassifier = new BinaryClassifierWekaBased(emotionModel, attributesSet, exampleProblem);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new WeightedClassifierHierachicalWrapper(internalEmotionalClassifier, neutralClassifier);
		//TODO: set the refinement params?
	}
	
	
	private Map<Integer, Map<Integer, Integer>> constructFeatureRepresentation(Map<Integer, List<String>> textData, WekaAttributesSetWithSyntFeatures attributesSet)
	{
		Map<Integer, Map<Integer, Integer>> featureRepresentation = new HashMap<Integer,Map<Integer,Integer>>();
		
		for (Map.Entry<Integer, List<String>> tweetEntry : textData.entrySet())
		{
			int twId = tweetEntry.getKey();
			Map<Integer, Integer> tweetFeatures = new HashMap<Integer,Integer>();
			
			// add linguistic features
			for (String termFeature : tweetEntry.getValue())
				tweetFeatures.put(attributesSet.getAttributeIndexes().get(termFeature), 1);
			
			// add syntactic features
			Map<Integer, Integer> syntFeatures = SyntacticFeaturesSet.parseFeatureValues(featureData.get(twId));
			syntFeatures = attributesSet.updateSyntFeatureIndexes(syntFeatures);
			tweetFeatures.putAll(syntFeatures);
			
			featureRepresentation.put(twId, tweetFeatures);
		}
		return featureRepresentation;
	}

	@Override
	public void clear() {
		super.clear();
		this.featureData = null;
		this.internalClassifierLearner.clear();
	}

	@Override
	public WeightedClassifier updateOutputClassifierForApplicationParameters(
			WeightedClassifier classifier,
			ClassifierApplicationParameters applicationParameters) {
		return null;
	}

	

}
