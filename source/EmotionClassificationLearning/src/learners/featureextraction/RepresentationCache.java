/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureextraction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;

import learning.parameters.AllLearningParameters;


public class RepresentationCache {
	
	static Map<String, Map<Integer, List<String>>> cachedDataRepresentation = new HashMap<String, Map<Integer,List<String>>>();
	
	public static Map<Integer, List<String>> getCurrentCachedRepresentation()
	{
		String curCacheParamsString = getCurrentCacheString ();
		if (cachedDataRepresentation.containsKey(curCacheParamsString))
			return cachedDataRepresentation.get(curCacheParamsString);
		else
			return null;
	}
	
	public static String getCurrentCacheString ()
	{
		return AllLearningParameters.currentSetup.trainDataName + "-" + AllLearningParameters.currentSetup.featureExtractionParams.printParametersShort() + 
				((AllLearningParameters.currentSetup instanceof AllSemiSupervisedLearningParameters)? ("-"+((AllSemiSupervisedLearningParameters)AllLearningParameters.currentSetup).sslParameters.getUnlabeledDataName()): "");
	}
	
	public static boolean isRepresentationSaved()
	{
		return cachedDataRepresentation.containsKey(getCurrentCacheString ());
	}
	
	public static void saveNewCurrentRepresentation(Map<Integer, List<String>> annotationData)
	{
		cachedDataRepresentation.put(getCurrentCacheString (), annotationData);
	}
	
	public static void clear()
	{
		cachedDataRepresentation.clear();
	}
}
