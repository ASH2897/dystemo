/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureextraction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.core.Instances;

import learning.parameters.AllLearningParameters;
import learning.parameters.IndependentLearnerParameters;

public class IndependentWekaRepresentationCache {

	// this cache is saving the instances representation based on the corresponding string
	static Map<String, Instances> cachedDataRepresentation = new HashMap<String, Instances>();
	
	public static Instances getCurrentCachedRepresentation(int categoryId)
	{
		String curCacheParamsString = getCurrentCacheString (categoryId);
		if (cachedDataRepresentation.containsKey(curCacheParamsString))
			return cachedDataRepresentation.get(curCacheParamsString);
		else
			return null;
	}
	
	public static String getCurrentCacheString (int categoryId)
	{
		return AllLearningParameters.currentSetup.trainDataName + "-" + 
				AllLearningParameters.currentSetup.featureExtractionParams.printParametersShort() + "__"+
				AllLearningParameters.currentSetup.featureSelectionParams.printParametersShort() + "__"+
				((IndependentLearnerParameters)AllLearningParameters.currentSetup.learnerParams).binaryLearnerParameters.binaryFeatureSelectionParams.printParametersShort()
				+ "___" +
				categoryId
				
				;
	}
	
	public static boolean isRepresentationSaved(int categoryId)
	{
		return cachedDataRepresentation.containsKey(getCurrentCacheString (categoryId));
	}
	
	public static void saveNewCurrentRepresentation(Instances instancesData, int categoryId)
	{
		cachedDataRepresentation.put(getCurrentCacheString (categoryId), instancesData);
	}
	
	public static void clear()
	{
		cachedDataRepresentation.clear();
	}
}
