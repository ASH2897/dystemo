/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureextraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;

import data.documents.Tweet;

public class FeatureExtractionAlgorithm {

	public static Map<Integer,List<String>> extractFeaturesFromData(List<? extends Tweet> tweetData, FeatureExtractionParameters featureExtractionParams)
	{
		Map<Integer,List<String>> finalTweetRepresentation = null;
		for (FeatureType featureType : featureExtractionParams.featureTypes)
		{
			switch (featureType)
			{
				case Ngrams: {
					DataNgramExtractionParams ngramExtrParams = featureExtractionParams.dataNgramExtractionParams;
					Map<Integer,List<String>> tweetRepresentation = TweetRepresentation.getTokenNgramRepresentation(tweetData, ngramExtrParams);
					
					if (finalTweetRepresentation == null)
						finalTweetRepresentation = tweetRepresentation;
					else
					{
						//TODO: need to merge, but as we don't have more featureTypes for now it is fine
					}
					break;
				}
			}
		}
		
		return finalTweetRepresentation;
	}
	
	
	public static Map<Integer,List<String>> extractFeaturesFromData(List<? extends Tweet> tweetData, FeatureExtractionParameters featureExtractionParams, List<String>  indicatorCandidates)
	{
		if (indicatorCandidates == null)
			return extractFeaturesFromData(tweetData, featureExtractionParams);
		else
		{
			if (!featureExtractionParams.useGivenIndicatorsAlongWithDiscovered) {
				Map<Integer,List<String>> tweetRepresentation = TweetRepresentation.
					getTokenNgramRepresentationFromFixedIndicators(tweetData, indicatorCandidates, featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection, featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold);
				return tweetRepresentation;
			} else
			{
				Map<Integer,List<String>> tweetRepresentationWithIndicators = TweetRepresentation.
						getTokenNgramRepresentationFromFixedIndicators(tweetData, indicatorCandidates, featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection, featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold);
				Map<Integer,List<String>> tweetRepresentationUsual = extractFeaturesFromData(tweetData, featureExtractionParams);
				
				// join two representations (with removal of repeated)
				Map<Integer,List<String>> tweetRepresentation = new HashMap<Integer,List<String>>();
				for (Integer tweetID : tweetRepresentationWithIndicators.keySet()) {
					Set<String> newRepr = new HashSet<String>(tweetRepresentationWithIndicators.get(tweetID));
					newRepr.addAll(tweetRepresentationUsual.get(tweetID));
					tweetRepresentation.put(tweetID, new ArrayList<String>(newRepr));
				}
				
				return tweetRepresentation;
			}
		}
	}
}
