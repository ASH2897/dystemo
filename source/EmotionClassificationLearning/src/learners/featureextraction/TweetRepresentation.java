/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureextraction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import linguistic.TermDetectionParameters;
import linguistic.UtilText;
import processing.TweetPreprocessing;
import utils.DataRepresentationRefinement;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithProcessing;
import data.documents.Tweet;
import functionality.UtilCollections;

public class TweetRepresentation {

	
	public static Map<Integer, List<String>> getTokenNgramRepresentation(List<? extends Tweet> tweetData, DataNgramExtractionParams extractionParams, List<String> indicatorCandidates )
	{
		Map<Integer, List<String>> tweetRepresentation = new HashMap<Integer,List<String>>();
		
		// we suppose that tweets were already preprocessed, thus we only need to find correspondning terms
		if (indicatorCandidates != null)
		{
			tweetRepresentation = getTokenNgramRepresentationFromFixedIndicators(tweetData, indicatorCandidates, extractionParams.parametersOnNgramsDetection, extractionParams.minTermOccurThreshold);
		}
		else
		{
			tweetRepresentation = getTokenNgramRepresentation(tweetData, extractionParams);
		}
		return tweetRepresentation;
	}
	
	public static Map<Integer, List<String>> getTokenNgramRepresentation(List<? extends Tweet> tweetData, DataNgramExtractionParams extractionParams)
	{
		Map<Integer, List<String>> ngramRepresentation = getTokenNgramRepresentation(tweetData, 1, extractionParams.maxN, extractionParams.minTermOccurThreshold, extractionParams.removeStopWords, extractionParams.parametersOnNgramsDetection);
		return ngramRepresentation;
	}
	
	public static Map<Integer, List<String>> getTokenNgramRepresentation(List<? extends Tweet> tweetData, int maxN, int minTermOccurThreshold, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		return getTokenNgramRepresentation(tweetData, 1, maxN, minTermOccurThreshold, toRemoveNonLexicalWords, termDetectorParameters);
	}
	
	
	public static Map<Integer, List<String>> getTokenNgramRepresentation(List<? extends Tweet> tweetData, int minN, int maxN, int minTermOccurThreshold, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		Map<Integer, List<String>> tweetRepresentation = new HashMap<Integer,List<String>>();
		for (Tweet tweet : tweetData)
		{
			List<String> tweetRepr = getTokenNgramsOfText(tweet.text, minN, maxN, toRemoveNonLexicalWords, termDetectorParameters);
			tweetRepresentation.put(tweet.tweetId, tweetRepr);
		}
		
		DataRepresentationRefinement.deleteLowFrequencyTermsFromRepresentation(tweetRepresentation, minTermOccurThreshold);
		return tweetRepresentation;
	}
	
	
	
	/**
	 * This will return the list of different indicators found in the tweet (meaning that if some indicator is found twice in the same text, it will be included only once)
	 * @param tweetData
	 * @param indicatorCandidates
	 * @param toTreatNegations
	 * @param minTermOccurThreshold
	 * @return
	 */
	public static Map<Integer, List<String>> getTokenNgramRepresentationFromFixedIndicators (List<? extends Tweet> tweetData, List<String> indicatorCandidates, TermDetectionParameters termDetectorParameters, int minTermOccurThreshold)
	{
		Map<Integer, List<String>> tweetRepresentation = getTokenNgramRepresentationFromFixedIndicators(tweetData, indicatorCandidates, termDetectorParameters);
		DataRepresentationRefinement.deleteLowFrequencyTermsFromRepresentation(tweetRepresentation, minTermOccurThreshold);
		return tweetRepresentation;
	}
	
	
	public static Map<Integer, List<String>> getTokenNgramRepresentationFromFixedIndicators(List<? extends Tweet> tweetData,  List<String> indicatorCandidates, TermDetectionParameters termDetectorParameters)
	{
		Map<Integer, List<String>> tweetRepresentation = new HashMap<Integer,List<String>>();
		
		TermDetectorWithProcessing tDetector = new TermDetectorWithProcessing(indicatorCandidates, false);
		tDetector =  TermDetectorUtil.updateTermDetectorForParameters(termDetectorParameters, tDetector);
		for (Tweet tweet : tweetData)
		{
			List<String> tweetRepr = new ArrayList<String>(
					tDetector.findEmotionalTermsInText(tweet.text, false).keySet()
					);
			tweetRepresentation.put(tweet.tweetId, tweetRepr);
		}
		return tweetRepresentation;
	}
	
	
	public static List<String> getTokenNgramsOfText(String text, int minN, int maxN, boolean considerAsSet, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		List<String> tweetRepr = new ArrayList<String>(TweetPreprocessing.tokenizeWithoutPreprocessing(text));
		if (considerAsSet)
			tweetRepr = new ArrayList<String>(UtilText.getNGramsFromTokenizedSentence(tweetRepr, minN, maxN, termDetectorParameters).keySet());
		else
		{
			Map<String, Integer> foundTermMap = UtilText.getNGramsFromTokenizedSentence(tweetRepr, minN, maxN, termDetectorParameters);
			tweetRepr = new ArrayList<String>();
			for (Map.Entry<String, Integer> termEntry : foundTermMap.entrySet())
			{
				for (int i = 0; i < termEntry.getValue(); ++i)
					tweetRepr.add(termEntry.getKey());
			}
		}
		
		if (toRemoveNonLexicalWords)
		{
			// delete stop words!
			LinguisticLexiconRefinement.dropNotLexiconTerms(tweetRepr);
		}
		return tweetRepr;
	}


	public static Map<String,Integer> computePhraseFrequency( Map<Integer, List<String>> tweetRepresentation)
	{
		Map<String, Integer> occurrenceMap = new HashMap<String, Integer>();
		// compute the number of occurrences for each term
				for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
				{
					for (String term : reprEntry.getValue())
					{
						UtilCollections.incrementIntValueInMap(occurrenceMap, term);
					}
				}
				
		return occurrenceMap;
	}
	
	
	public static Map<String,Integer> ExtractAllNgramsFromDataset(List<? extends Tweet> tweetData, DataNgramExtractionParams extractionParams)
	{
		Map<Integer, List<String>> tweetRepresentation  = getTokenNgramRepresentation(tweetData, extractionParams);
		return computePhraseFrequency(tweetRepresentation);
	}
	
	
	
	/*potentially not used */

	
	public static Map<Integer, List<String>> getTokenNgramRepresentation(Map<Integer, String> tweetData, int minN, int maxN, int minTermOccurThreshold, boolean useAsSet, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		Map<Integer, List<String>> tweetRepresentation = new HashMap<Integer,List<String>>();
		
		for (Map.Entry<Integer, String> tweet : tweetData.entrySet())
		{
			List<String> tweetRepr = getTokenNgramsOfText(tweet.getValue(), minN, maxN, useAsSet, toRemoveNonLexicalWords, termDetectorParameters);
			
			tweetRepresentation.put(tweet.getKey(), tweetRepr);
		}
		DataRepresentationRefinement.deleteLowFrequencyTermsFromRepresentation(tweetRepresentation, minTermOccurThreshold);
	
		return tweetRepresentation;
	}
	
	
	public static List<String> getTokenNgramsOfText(String text, int maxN, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		return getTokenNgramsOfText(text, 1, maxN,  toRemoveNonLexicalWords, termDetectorParameters);
	}
	
	/**
	 * Returns the list of tokens . The treatment as set or as not is internal (if maxN = 1 - no, if > 1 - yes)
	 * @param text
	 * @param minN
	 * @param maxN
	 * @return
	 */
	public static List<String> getTokenNgramsOfText(String text, int minN, int maxN, boolean toRemoveNonLexicalWords, TermDetectionParameters termDetectorParameters)
	{
		return getTokenNgramsOfText(text, minN, maxN, true, toRemoveNonLexicalWords, termDetectorParameters); // we take just the presence of the ngram
	}
	
	public static List<String> getTokenSubstringsOfText(String text, int maxN, boolean considerAsSet, boolean toRemoveNonLexicalWords)
	{
		List<String> tweetRepr = new ArrayList<String>(TweetPreprocessing.tokenizeWithoutPreprocessing(text));
		if (maxN > 1)
		{
			Map<String, Integer> foundTermMap = UtilText.getSubstringsFromTokenizedSentence(tweetRepr, maxN);
			if (considerAsSet)
				tweetRepr = new ArrayList<String>(foundTermMap.keySet());
			else
			{
				tweetRepr = new ArrayList<String>();
				for (Map.Entry<String, Integer> termEntry : foundTermMap.entrySet())
				{
					for (int i = 0; i < termEntry.getValue(); ++i)
						tweetRepr.add(termEntry.getKey());
				}
			}
		}
		else
		{
			if (considerAsSet)
				tweetRepr =  new ArrayList<String>(new LinkedHashSet<String>(tweetRepr));
		}
		
		if (tweetRepr.contains(" the") || tweetRepr.contains(" <username>"))
		{
			int k = 0;
			k++;
		}
		if (toRemoveNonLexicalWords)
		{
			// delete stop words!
			LinguisticLexiconRefinement.dropNotLexiconTerms(tweetRepr);
		}
		return tweetRepr;
	}
	
	public static Set<Integer> getTweetIds (Collection<? extends Tweet> tweets)
	{
		Set<Integer> tweetIds = new HashSet<Integer>();
		for (Tweet tw : tweets)
			tweetIds.add(tw.tweetId);
		return tweetIds;
	}

}
