/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package datapreparation.helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import linguistic.StringDistance;
import utility.Pair;

public class DuplicatesDetectionHelper {

static String allSymbols;
	
	static
	{
		StringBuilder result = new StringBuilder();
		for(char c=0; c<Character.MAX_VALUE; c++)
	    {
	        result.append(c);
	    }
		allSymbols = result.toString();
	}
	
	public static Pair<Integer,Integer> computeWordDistanceAndCommonSubstringBetweenTexts(List<String> tokens1, List<String> tokens2, int maxAllowedDist)
	{
		Map<String, Character> mapOfWordsToLetters = new HashMap<String,Character>();
		int nextSymbInd = 0;
		StringBuilder token1Str = new StringBuilder();
		for (String word: tokens1)
		{
			Character curChar = null;
			if (!mapOfWordsToLetters.containsKey(word))
			{
				curChar = allSymbols.charAt(nextSymbInd);
				mapOfWordsToLetters.put(word, curChar);
				nextSymbInd++;
			}
			else
				curChar = mapOfWordsToLetters.get(word);
			token1Str.append(curChar);
		}
		
		StringBuilder token2Str = new StringBuilder();
		for (String word: tokens2)
		{
			Character curChar = null;
			if (!mapOfWordsToLetters.containsKey(word))
			{
				curChar = allSymbols.charAt(nextSymbInd);
				mapOfWordsToLetters.put(word, curChar);
				nextSymbInd++;
			}
			else
				curChar = mapOfWordsToLetters.get(word);
			token2Str.append(curChar);
		}
		
		int dist = StringDistance.computeLevenshteinDistance(token1Str.toString(), token2Str.toString());
		
		int maxCommonSubstr = StringDistance.getLengthOfLongestSubstring(token1Str.toString(), token2Str.toString());
		return new Pair(dist, maxCommonSubstr);
	}
}
