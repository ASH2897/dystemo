/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package datapreparation.helpers;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import linguistic.TermDetector;
import linguistic.UtilText;
import processing.TweetPreprocessing;
import emotionclassifiers.given.EmotionClassifierGivenFactory;
import functionality.UtilFiles;

public class CleaningNeutralDataHelper {
	
	static Set<String> diffWordsLeadingToEmotions;
	static TermDetector interjectionDetector;
	static TermDetector strongPolarityDetector;
	static Map<String, String> wrongSubjectiveWordsDetectionContext;
	
	static
	{
		diffWordsLeadingToEmotions = new HashSet<String>();
		String[] diffWords = {"i", "my", "me", "we", "our", "us", "mine", "<ext>"};
		for (String word : diffWords)
			diffWordsLeadingToEmotions.add(word);
		try {
			diffWordsLeadingToEmotions.addAll(UtilText.getAllPlusWords());
			diffWordsLeadingToEmotions.addAll(UtilText.getAllMinusWords());
			diffWordsLeadingToEmotions.remove("more");
			diffWordsLeadingToEmotions.addAll(EmotionClassifierGivenFactory.getGALCRevisedInstancesClassifier().getFeatureNames());
			interjectionDetector = new TermDetector(UtilText.getAllInterjectionsWords());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Add the preprocessed strong words from OpinionFinder (they were selected in advance as words with strong subjectivity)
		String strongOpinionFinderFile;
		try {
			strongOpinionFinderFile = CleaningNeutralDataHelper.class.getClassLoader().getResource("opinionFinder-strong-prep.wordlist").toURI().getPath();
			List<String> polarityTerms = UtilFiles.getContentLines(strongOpinionFinderFile); 
			strongPolarityDetector = new TermDetector(polarityTerms);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		wrongSubjectiveWordsDetectionContext = new HashMap<String, String>();
		wrongSubjectiveWordsDetectionContext.put("great", "great britain");
		wrongSubjectiveWordsDetectionContext.put("kind", "kind of");
	}
	
	/** This code implies that tweet text is preprocessed for correct emoji and emoticons processing!!! **/
	public static boolean isEmotionalCuePresent(String text)
	{
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing(text);
		
		for (String token : tokens)
		{
			if (token.startsWith("<emot") || token.startsWith("<emoji_"))
				return true;
			if (token.contains("!") || token.contains("*"))
				return true;
			if (token.startsWith("@") || token.equals("<username>"))
				return true;
			if (diffWordsLeadingToEmotions.contains(token))
				return true;
		}
		// check presence of interjections
		Map<String,Integer> foundStrongSubjectiveTerms = interjectionDetector.findTermsForTokens(tokens);
		foundStrongSubjectiveTerms.putAll(strongPolarityDetector.findTermsForTokens(tokens));
		if (!foundStrongSubjectiveTerms.isEmpty()) {
			int size = foundStrongSubjectiveTerms.size();
			for (String potentiallyWrongTerm : wrongSubjectiveWordsDetectionContext.keySet()) {
				if (foundStrongSubjectiveTerms.containsKey(potentiallyWrongTerm) && text.contains(wrongSubjectiveWordsDetectionContext.get(potentiallyWrongTerm)))
					size--;
			}
			
			if (size > 0)
				return true;
		}
			
		return false;
	}
	
	public static void testRun()
	{
		System.out.println("Test of CleaningNeutralData");
		String[] texts = { "reuters - uk - tennis : update <int_num> - olympics - tennis - games final could see wimbledon repeat . more #tennis news : <link>",
				"We are not here tonight",
				"today !*",
				" yay , we are coming to see you .",
				"great britain",
				"great weather today", 
				" wow , this is amazing . great britain is winning",
				"We are inside <emot10>",
				"best film ever",
				"ignore this statement",
				"this is kind of annoying",
				"kind of today",
				"stay fit"
				
		};
		boolean[] containsEmotions = {false, false, true, true, false, true, true, true, true, false, true, false, false};
		for (int i = 0; i < texts.length; ++i ) {
			boolean withCue =  isEmotionalCuePresent(texts[i]);
			if (withCue != containsEmotions[i]) {
				System.out.println("Error in text: " + texts[i]);
			}
		}
		
	
	}
}
