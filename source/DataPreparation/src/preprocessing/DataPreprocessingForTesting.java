/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package preprocessing;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cleaning.TextCleaning;
import processing.PreprocessingProcessor;
import data.DatabaseWrapper;
import data.LabeledData;
import data.documents.Tweet;
import data.loading.DataFilesFormatReader;
import data.loading.DataFilesFormatSaver;
import data.loading.DataSetLoader;

public class DataPreprocessingForTesting {

	/** This process will remove the given emotional hashtags and save the new text in the new column.
	 * Generally the result is saved in the `refined_text` column.
	 * 
	 * @throws Exception **/
	public static void preprocessTableDataByRemovingHashtags(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{
		int maxOneBatchSize = 50000;
		int offset = 0;
		int lastReturnedSize = 1;
		
		System.out.println("Remove hashtags in table " + tablename);
		while (lastReturnedSize > 0)
		{
			Date start = new Date();
			
			//List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tablename, " refined_text is null ", idColumn, initialTextColumn,  offset,  maxOneBatchSize);

			List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, offset,  maxOneBatchSize, idColumn, initialTextColumn);
			lastReturnedSize = curBench.size();
			
			if (lastReturnedSize == 0)
				continue;
			
			Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
			for (Tweet tweet : curBench)
			{
				preprocessRes.put(tweet.tweetId, tweet.text);
			}
			
			TextCleaning.toRemoveGivenEmotionHashTags(preprocessRes, true);
			
			DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
			Date finish = new Date();
			
			System.out.println("Done with offset " + offset + " ; It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; ");
			//offset += maxOneBatchSize;
		}
		System.out.println("Done");
	}
	
	/**
	 * Note: With this method only the first 1000000 tweets will be processed! (otherwise, rewrite is needed)
	 * @throws Exception **/
	public static void preprocessTableDataByRemovingEmojis(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{

		int maxOneBatchSize = 50000;
		int offset = 0;
		int lastReturnedSize = 1;
		System.out.println("Remove emojis in table " + tablename);
		
		while (lastReturnedSize > 0)
		{
			Date start = new Date();
			
			//List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tablename, " refined_text is null ", idColumn, initialTextColumn,  offset,  maxOneBatchSize);

			List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, offset, maxOneBatchSize, idColumn, initialTextColumn);
			
			lastReturnedSize = curBench.size();
			if (lastReturnedSize == 0)
				continue;
			
			Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
			for (Tweet tweet : curBench)
			{
				preprocessRes.put(tweet.tweetId, tweet.text);
			}
			
			TextCleaning.toRemoveGivenEmojis(preprocessRes, true);
			
			DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
			Date finish = new Date();
			
			System.out.println("Done with offset " + offset + " ; It took " + (finish.getTime() - start.getTime())/60000.0 + " minutes; ");
		
			//offset += maxOneBatchSize; (because some will be removed)
		}
	}
	
	public static void removeTweetsWithNoEndingHashtagAndRemoveHashtags(String testDataFileOrig, String testDataFileFinal) throws Exception
	{
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testDataFileOrig, emotionLabels, textData);
		TextCleaning.toRemoveTweetsWithNoEndingGivenHashtagAndRemoveGivenEmotionHashTags(textData, true);
		DataFilesFormatSaver.printLabeledDataIntoFile(testDataFileFinal, emotionLabels, textData);
	}
	
	public static void removeGivenHashtagsFromTestData(String testDataFileOrig, String testDataFileFinal) throws Exception
	{
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testDataFileOrig, emotionLabels, textData);
		
		TextCleaning.toRemoveGivenEmotionHashTags(textData, true);
		
		DataFilesFormatSaver.printLabeledDataIntoFile(testDataFileFinal, emotionLabels, textData);
	}
	
	/** 
	 * Reads the tweets data with labels from the file, preprocesses every tweet text, and saves the output to another file.
	 * @param initialFile
	 * @param outputFile
	 * @throws Exception
	 */
	public static void preprocessTextualTweetData(String initialFile, String outputFile) throws Exception {
		
		LabeledData labelData = DataSetLoader.getLabeledDataFromTextFile(initialFile);
		
		PreprocessingProcessor.preprocessAllTweets(labelData.textData);
		
		DataFilesFormatSaver.printLabeledDataIntoFile(outputFile, labelData.emotionLabels, labelData.textData);
	}
			
}
