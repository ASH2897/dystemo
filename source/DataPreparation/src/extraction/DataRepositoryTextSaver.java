/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package extraction;

import cleaning.TextCleaning;
import data.DataDescription;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.TweetData;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.documents.Tweet;
import data.loading.DataFilesFormatSaver;

/**
 * Saves the tweet data into the text files. Both for LabeledDataRepository and TweetDataRepository (storing unlabeled data)
 *
 */
public class DataRepositoryTextSaver {
	
	public static void SaveLabeledDataInTextFile (String dataName, String fileNameToSave) throws Exception {
		LabeledData labeledData = DataRepositoryIndex.getLabeledDataByName(dataName);
		DataFilesFormatSaver.printLabeledDataIntoFile(fileNameToSave, labeledData.emotionLabels, labeledData.textData);
	}
	
	public static void SaveLabeledDataInTextFile (DataDescription dataDescription, String fileNameToSave) throws Exception {
		LabeledData labeledData = DataRepositoryIndex.getLabeledDataFromDescription(dataDescription);
		DataFilesFormatSaver.printLabeledDataIntoFile(fileNameToSave, labeledData.emotionLabels, labeledData.textData);
	}
	
	public static void SaveTweetDataInTextFile(String dataName, String fileNameToSave) throws Exception {
		TweetData tweetData = DataRepositoryIndex.getTweetDataByName(dataName);
		DataFilesFormatSaver.printTweetDataIntoFileAsGiven(fileNameToSave, tweetData.tweets);
	}
	
	public static void SaveTweetDataInTextFile(DataDescription dataDescription, String fileNameToSave) throws Exception {
		TweetData tweetData = DataRepositoryIndex.getTweetDataFromDescription(dataDescription);
		DataFilesFormatSaver.printTweetDataIntoFileAsGiven(fileNameToSave, tweetData.tweets);
	}
	
	/** 
	 * Note that this function will also update the texts of the tweets for processing pseudo-neutral tweets (by removal of <link> references). 
	 * The tweets are expected to be preprocessed, with detected and replaced present URLs.
	 * @param tweetData
	 * @param fileNameToSave
	 * @param catDataType
	 * @throws Exception
	 */
	private static void saveNeutralTweetData_private(TweetData tweetData, String fileNameToSave, CategoryDataType catDataType) throws Exception {
		// remove links references
		for (Tweet tweet : tweetData.tweets) {
			tweet.text = tweet.text.replace("<link>", "");
			tweet.text = tweet.text.replaceAll("\\s+", " ").trim();
		}
		
		DataFilesFormatSaver.printTweetsLabeledWithFixedCategory(fileNameToSave, tweetData.tweets, CategoryProcessor.neutralCategory(catDataType));		
	}
	
	/** 
	 * Note that this function will also update the texts of the tweets for processing pseudo-neutral tweets (by removal of <link> references). 
	 * The tweets are expected to be preprocessed, with detected and replaced present URLs.
	 * @param tweetData
	 * @param fileNameToSave
	 * @param catDataType
	 * @throws Exception
	 */
	public static void SaveNeutralTextData(String dataNameInIndex, String fileNameToSave, CategoryDataType catDataType) throws Exception {
		TweetData tweetData = DataRepositoryIndex.getTweetDataByName(dataNameInIndex);
		saveNeutralTweetData_private(tweetData, fileNameToSave, catDataType);
	}
	
	/** 
	 * Note that this function will also update the texts of the tweets for processing pseudo-neutral tweets (by removal of <link> references). 
	 * The tweets are expected to be preprocessed, with detected and replaced present URLs.
	 * @param tweetData
	 * @param fileNameToSave
	 * @param catDataType
	 * @throws Exception
	 */
	public static void SaveNeutralTextData(DataDescription dataDescription, String fileNameToSave, CategoryDataType catDataType) throws Exception {
		TweetData tweetData = DataRepositoryIndex.getTweetDataFromDescription(dataDescription);
		saveNeutralTweetData_private(tweetData, fileNameToSave, catDataType);
	}
	
	private static void SaveHashtaggedLabeledDataInTextFile_private(
			LabeledData hashtaggedData,  String fileNameToSave,
			boolean toRemoveHashtags, boolean toRemoveEmojisGiven)  throws Exception
	{
		if (toRemoveHashtags)
			TextCleaning.toRemoveGivenEmotionHashTags(hashtaggedData.textData);
		
		if (toRemoveEmojisGiven)
			TextCleaning.toRemoveGivenEmojis(hashtaggedData.textData, true);
		
		DataFilesFormatSaver.printLabeledDataIntoFile(fileNameToSave, hashtaggedData.emotionLabels, hashtaggedData.textData);		
	}
	
	/** 
	 * Note that this function will also update the texts of the tweets for processing hashtagges tweets (by removal hashtags or emojis as specified). 
	 * The tweets are expected to be preprocessed.
	 * @param tweetData
	 * @param fileNameToSave
	 * @param catDataType
	 * @throws Exception
	 */
	public static void SaveHashtaggedLabeledDataInTextFile(
			String dataNameInIndex, String fileNameToSave,
			boolean toRemoveHashtags, boolean toRemoveEmojisGiven) throws Exception
	{
		LabeledData hashtaggedData = DataRepositoryIndex.getLabeledDataByName(dataNameInIndex);
		SaveHashtaggedLabeledDataInTextFile_private(hashtaggedData, fileNameToSave, toRemoveHashtags, toRemoveEmojisGiven);
	}
	
	/** 
	 * Note that this function will also update the texts of the tweets for processing hashtagges tweets (by removal hashtags or emojis as specified). 
	 * The tweets are expected to be preprocessed.
	 * @param tweetData
	 * @param fileNameToSave
	 * @param catDataType
	 * @throws Exception
	 */
	public static void SaveHashtaggedLabeledDataInTextFile(
			DataDescription dataDescription, String fileNameToSave,
			boolean toRemoveHashtags, boolean toRemoveEmojisGiven) throws Exception
	{
		LabeledData hashtaggedData = DataRepositoryIndex.getLabeledDataFromDescription(dataDescription);
		SaveHashtaggedLabeledDataInTextFile_private(hashtaggedData, fileNameToSave, toRemoveHashtags, toRemoveEmojisGiven);
	}
}
