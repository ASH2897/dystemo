/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package cleaning;

import java.util.ArrayList;
import java.util.List;

import linguistic.TermDetector;
import data.DatabaseWrapper;
import data.documents.Tweet;
import functionality.UtilCollections;

public class DatasetCleaningFromRetweets {

	public static void detectRetweetTweetsInPreprocessedDatatable(String tablename, String idColumn, String cleanTextColumn, String outFileWithDetectedIds) throws Exception {
		List<String> retweetPatterns = new ArrayList<String>();
		retweetPatterns.add("rt <username>");
		retweetPatterns.add("rt :");
		TermDetector retweetDetector = new TermDetector(retweetPatterns);
		
		List<Tweet> tweets = DatabaseWrapper.getAllTweetsFromDatabase(tablename, idColumn, cleanTextColumn);
	
		int numRemoved = 0;
		List<Integer> tweetsToRemove = new ArrayList<Integer>();
		for (int twInd = tweets.size() - 1; twInd >= 0; --twInd) {
			Tweet tweet = tweets.get(twInd);
			if (!retweetDetector.findTermsInText(tweet.text).isEmpty()) {
				// retweet was found -> need to remove it
				tweetsToRemove.add(tweet.tweetId);
				++numRemoved;
			}
		}
		
		UtilCollections.printListToFile(tweetsToRemove, outFileWithDetectedIds);
		
		System.out.println("There was " + numRemoved + " tweets with retweets in " + tweets.size() + " total tweets.");
	}
}
