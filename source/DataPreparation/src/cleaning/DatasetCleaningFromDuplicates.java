/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package cleaning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import linguistic.TermDetectionParameters;
import linguistic.TermDetector;
import linguistic.UtilText;
import processing.TweetPreprocessing;
import utility.Pair;
import data.DatabaseWrapper;
import data.documents.Tweet;
import datapreparation.helpers.DuplicatesDetectionHelper;
import functionality.UtilCollections;
import functionality.UtilCounts;

/**
 * 
 * This class allows to find duplicated tweets in the database. 
 * Note that all detected duplicates will be removed (without keeping one example per duplicate class): 
 * Potentially another implementation that keeps such examples would be more beneficial.
 * 
 * We normally run the dublicate detection several times, until no further duplicates are found, 
 * and then once more with  removeFirstWord set to true.
 */
public class DatasetCleaningFromDuplicates {

	/**
	 * This code only generates the files with ids of tweets to be removed. Then they should be removed manually via sql queries.
	 */
	public static void cleanDuplicatesInEmotionTweetsTable(String tableName, String textColumn, String idColumn)
	{
		cleanDuplicatesInEmotionTweetsTable(tableName, textColumn, idColumn, null, false);
	}
	
	/** PARAMETERS ON CLEANING **/
	public static int minDuplicateNgramLength = 8;
	public static int alternativeMinCommonSubstrLen = 10;
	public static int maxWordDistanceForDuplicates = 3;
	
	public static String outputCleaningFolder = "output/curCleaningProcess/";
	/**
	 * Whether to split the data in the database based on some column into non-overlapping sets.
	 * This specifies the name of the column.
	 */
	public static String splitColumnName = null; //"tSize";//"categoryId";
	
	
	/**
	 * This code only generates the files with ids of tweets to be removed. Then they should be removed manually via mysql code
	 * First launch with removeFirstWord - false, then - true;
	 */
	public static void cleanDuplicatesInEmotionTweetsTable(String tableName, 
			String textColumn, String idColumn, 
			String whereCondition, boolean removeFirstWord)
	{
		System.out.println("Started detecting duplicates from the table " + tableName  + " using column `" + textColumn +"`.");
		System.out.println("The ids of detected tweets will be outputted in files " + outputCleaningFolder + " repeatedTweetsIdsFor...");
		
		int maxOneBatchSize = 1000000;
		int initOffset = 0;
		boolean doOnce = false;
		int lastReturnedSize = 1;
		int startValueInd = 0;
		
		boolean mergeTheValues = false;
		int occLimitForMerge = 500;
		
		
		/** This is to simplify the data if possible to non-overlap.**/
		
		List<String> diffSplitValues = new ArrayList<String>();
		if (splitColumnName != null)
		{
			try {
				diffSplitValues = DatabaseWrapper.getDistinctValuesOfField(tableName, splitColumnName);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		else
			diffSplitValues.add(null);
		
		System.out.println("Have found " + diffSplitValues.size() + " different values in " + splitColumnName + " column. Start from " + startValueInd);
		if (mergeTheValues)
		{
			
			try {
				
				StringBuilder mergedSplitValue = new StringBuilder();
				Map<String, Integer> splitValuesOccs = DatabaseWrapper.getDistinctValuesOfFieldWithCounts(tableName, splitColumnName);
				for (int spValueInd = diffSplitValues.size() - 1; spValueInd >= 0  ; --spValueInd)
				{
					// remove the already seen values 
					if (spValueInd < startValueInd)
					{
						String splitV = diffSplitValues.get(spValueInd);
						//int occ = splitValuesOccs.get(splitV);
						diffSplitValues.remove(spValueInd);
					}
					else
					{
						String splitV = diffSplitValues.get(spValueInd);
						int occ = splitValuesOccs.get(splitV);
						if (occ < occLimitForMerge && splitV != null)
						{
							// merge low-occurred values together
							//String conditionV = (splitV == null)? (splitColumnName + " is null") : (splitColumnName + " = '" + splitV.replace("\\", "\\\\")) +"'";
							//mergedSplitValue.append( conditionV + " OR ");
							mergedSplitValue.append("'" + splitV.replace("\\", "\\\\") +"',");
							diffSplitValues.remove(spValueInd);
						}
					}
				}
				if (mergedSplitValue.length() > 0)
				{
					String mergedSearch = splitColumnName + " in (" +mergedSplitValue.substring(0, mergedSplitValue.length() - 1) + ")";
					diffSplitValues.add(mergedSearch);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		int valueInd = -1;
		for (String splitValue : diffSplitValues)
		{
			valueInd++;
			
			if (!mergeTheValues && valueInd < startValueInd)
				continue;
			
			int offset = initOffset;
			lastReturnedSize = 1;
			while (lastReturnedSize > 0)
			{
				
				try
				{
				
					HashMap<String, Integer> repeatedTexts = new HashMap<String, Integer>();
					LinkedHashSet<Integer> tweetsToRemove = new LinkedHashSet<Integer>();
					List<Tweet> tweetsRemainingAfterRemove = new ArrayList<Tweet>();
					Map<String, Integer> repeatableLongNgrams = new LinkedHashMap<String,Integer>();
					
					Date start = new Date();
					String condition;
					if (splitColumnName == null)
					{
						condition = null;
						if (whereCondition != null)
							condition =   whereCondition; 
					}
					else
					{
						if ((splitValue != null) && (splitValue.contains(" OR ") || splitValue.contains(" in (")))
							condition = splitValue;
						else
							condition = (splitValue == null)? (splitColumnName + " is null") : (splitColumnName + " = '" + splitValue.replace("\\", "\\\\")) +"'";
						if (whereCondition != null && condition != null)
								condition = "(" + whereCondition + ") and (" + condition + ")"; 
							
					}
					List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tableName, condition, offset,  maxOneBatchSize, idColumn, textColumn);
					lastReturnedSize = curBench.size();
					if (lastReturnedSize != 0)
					{
						if (lastReturnedSize == 1)
						{
							System.out.println("\n" + valueInd + ": Too few tweets");
							lastReturnedSize = 0;
							continue;
						}
						// remove those tweets with the same ids
						int removed = 0;
						Set<Integer> tweetsFound = new HashSet<Integer>();
						for (int twInd = curBench.size() - 1; twInd >= 0; --twInd)
						{
							Tweet tweet = curBench.get(twInd);
							if (tweetsFound.contains(tweet.tweetId))
							{
								curBench.remove(twInd);
								removed++;
							}
							else
								tweetsFound.add(tweet.tweetId);
						}
						tweetsFound.clear();
						tweetsFound = null;
						
						System.out.println("\n" + valueInd + ": Obtained " + (lastReturnedSize - removed) + " tweets for " + splitValue + "; Removed " +  removed + " repeated ones.");
						// normalize texts (remove punctuation signs)
						for (Tweet tweet : curBench)
						{
							tweet.text = TweetPreprocessing.removeAllNonAlphaNumericLetters(tweet.text, false, false);
							// to use in the second run: to remove the first word in the tweet
							if (removeFirstWord)
								tweet.text = tweet.text.substring(tweet.text.indexOf(' ') + 1);
						}
						
						// sort the returned tweets by text
						Collections.sort(curBench, new Comparator<Tweet>()
								{

									@Override
									public int compare(Tweet o1, Tweet o2) {
										return o1.text.compareTo(o2.text);
									}
								
								});
						
						int errorNum = 0;
						Tweet prevTweet =  curBench.get(0);
						List<String> prevTokens = TweetPreprocessing.tokenizeWithoutPreprocessing(prevTweet.text);
						boolean lastWasRemoved = false;
						for (int twId = 1; twId < curBench.size(); ++twId)
							
						{
							Tweet tweet = curBench.get(twId);
							
							List<String> curTokens = TweetPreprocessing.tokenizeWithoutPreprocessing(tweet.text);
							Pair<Integer,Integer> distComPair = DuplicatesDetectionHelper.computeWordDistanceAndCommonSubstringBetweenTexts(prevTokens, curTokens, maxWordDistanceForDuplicates);
							int dist = distComPair.first;
							int commonSubstrLen = distComPair.second;
							
							if (dist < maxWordDistanceForDuplicates && prevTokens.size() >= minDuplicateNgramLength && curTokens.size() >= minDuplicateNgramLength || 
									(dist == 0 && (!removeFirstWord || prevTokens.size() >= minDuplicateNgramLength && curTokens.size() >= minDuplicateNgramLength)) || 
									(commonSubstrLen >= alternativeMinCommonSubstrLen ) || (prevTokens.size() - dist >= minDuplicateNgramLength && curTokens.size() - dist >= minDuplicateNgramLength))
							{
								// need to remove the previous tweet and put it in the database of repeated 
								List<String> NgramsToRemove = null;
								
								if (tweet.tweetId > prevTweet.tweetId)
								{
									tweetsToRemove.add(prevTweet.tweetId);
									UtilCollections.incrementIntValueInMap(repeatedTexts, prevTweet.text);
									prevTokens = curTokens;
									prevTweet = tweet;
									NgramsToRemove = new ArrayList<String>(
											UtilText.getNGramsFromTokenizedSentence(
													TweetPreprocessing.tokenizeWithoutPreprocessing(prevTweet.text), minDuplicateNgramLength, minDuplicateNgramLength, 
													TermDetectionParameters.defaultEmptyTreatmentParams).keySet());
								}
								else
								{
									tweetsToRemove.add(tweet.tweetId);
									UtilCollections.incrementIntValueInMap(repeatedTexts, tweet.text);
									
									NgramsToRemove = new ArrayList<String>(
											UtilText.getNGramsFromTokenizedSentence(
													TweetPreprocessing.tokenizeWithoutPreprocessing(tweet.text), minDuplicateNgramLength, minDuplicateNgramLength, 
													TermDetectionParameters.defaultEmptyTreatmentParams).keySet());
									// no change in previous tokens, as they were preserved
								}
								for (String term : NgramsToRemove)
								{
									UtilCollections.incrementIntValueInMap(repeatableLongNgrams, term);
								}
								lastWasRemoved = true;
							}
							else
							{
								if (lastWasRemoved)
									tweetsRemainingAfterRemove.add(prevTweet);
								lastWasRemoved = false;
								prevTokens = curTokens;
								prevTweet = tweet;
							}
							
						}
						if (lastWasRemoved)
							tweetsRemainingAfterRemove.add(prevTweet);
						
						Set<String> termsToRemove = UtilCounts.findLowOccurrencesInMap(repeatableLongNgrams, 2);
						UtilCollections.removeAllKeysFromMap(repeatableLongNgrams, termsToRemove);
						
						TermDetector td = new TermDetector(repeatableLongNgrams.keySet());
						td.stopOnFirst = true;
						
						for (int twId = 0; twId < curBench.size(); ++twId)
							
						{
							Tweet tweet = curBench.get(twId);
							if (tweetsToRemove.contains(tweet.tweetId))
								continue;
							
							Map<String,Integer> foundTerms = td.findTermsInText(tweet.text);
							if (foundTerms.size() > 0)
							{
								tweetsToRemove.add(tweet.tweetId); // this will remove all the tweets with potentially repeated ngrams, without leaving even one
							}
						}	
						String splitValueRef;
						if ((splitValue != null) && (splitValue.contains(" OR ") || splitValue.contains(" in (")))
							splitValueRef = "low_merged";
						else
							splitValueRef = (splitValue == null)?"null":splitValue.replace("\\", "") ;
						UtilCollections.printToFileMap(repeatedTexts, outputCleaningFolder + "repeatedTweetsTextsFor_" + tableName.substring(0,Math.max(15, tableName.length())) + "_" + splitValueRef + (offset>=0 ? ("_off" + offset) :"") + ".txt");
						UtilCollections.printListToFile(tweetsToRemove, outputCleaningFolder + "repeatedTweetsIdsFor_" + tableName.substring(0,Math.max(15, tableName.length())) + "_" + splitValueRef + (offset>=0 ? ("_off" + offset) :"") + ".txt");
						UtilCollections.printToFileMap(repeatableLongNgrams, outputCleaningFolder + "repeatedNgramsFor_" + tableName.substring(0,Math.max(15, tableName.length())) + "_" + splitValueRef + (offset>=0 ? ("_off" + offset) :"") + ".txt");
						
						
						Date finish = new Date();
						System.out.println(finish.toString() + " : Processed offset is " + offset + ", found " + lastReturnedSize + 
								" entries, last EntryID = " + curBench.get(curBench.size() - 1).tweetId + 
								" Number of errors: " + errorNum +  
								". It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; \n" +
								"   Free memory left in bytes: " +  Runtime.getRuntime().freeMemory() +
								"; Total memory used in bytes: " + Runtime.getRuntime().totalMemory() );
						curBench.clear();
						curBench = null;
						start = finish;
						
					}
				}
				catch(Exception e)
				{
					System.out.println("Error during data processing from offset " + offset + ": " + e.getMessage() + "\n\r Stack:" );
					e.printStackTrace();
					lastReturnedSize = 1;
				}
				offset += maxOneBatchSize;
				if (doOnce)
					lastReturnedSize = 0;
			
			}
			
		}
	}
	
	
	
	
	
}
