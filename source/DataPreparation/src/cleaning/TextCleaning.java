/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package cleaning;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import processing.TweetPreprocessing;
import classification.definitions.lexicons.CategoryLexiconUnrestricted;
import classification.detectors.TermDetectorWithProcessing;
import data.documents.Tweet;
import datapreparation.helpers.HashtagsEmojisDetailsHelper;
import emotionclassifiers.given.EmotionLexiconsGivenFactory;
import functionality.UtilCollections;
import functionality.UtilString;

public class TextCleaning {

	public static void toRemoveGivenEmotionHashTags(Map<Integer, String> textData)
	{
		toRemoveGivenEmotionHashTags(textData, false);
	}
	
	public static void toRemoveGivenEmotionHashTags(Map<Integer, String> textData, boolean ignoreCase)
	{
		CategoryLexiconUnrestricted lexicon = null;
		try {
			lexicon = EmotionLexiconsGivenFactory.getLexiconOfGivenGEWHashtags(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		TermDetectorWithProcessing tDetector = new TermDetectorWithProcessing(lexicon.getAllLexiconTerms(), false);
		
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			Set<String> foundTerms = tDetector.findEmotionalTermsInText(ignoreCase?text.toLowerCase():text, false).keySet();
			for (String foundTerm : foundTerms)
	    	{
	    		text = text.replaceAll((ignoreCase?"(?i)":"") + "(?<=(\\s|\u00A0|^))" + foundTerm+"(?=(\\b|\\s|\u00A0|$))", "");
	    		text = text.replaceAll("\\s+", " "); // delete possibly occurred double white spaces
	    	}
	    	text = text.trim();
	    	textData.put(textEntry.getKey(), text);
	    	
		}
	}
	
	public static void toRemoveTweetsWithNoEndingGivenHashtagAndRemoveGivenEmotionHashTags(Map<Integer, String> textData, boolean ignoreCase)
	{
		CategoryLexiconUnrestricted lexicon = null;
		try {
			lexicon = EmotionLexiconsGivenFactory.getLexiconOfGivenGEWHashtags(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		TermDetectorWithProcessing tDetector = new TermDetectorWithProcessing(lexicon.getAllLexiconTerms(), false);
		
		Set<Integer> tweetsToRemove = new HashSet<Integer>();
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			Set<String> foundTerms = tDetector.findEmotionalTermsInText(ignoreCase?text.toLowerCase():text, false).keySet();
			
			boolean isCurHashtagEnding = false;
			
			for (String foundTerm : foundTerms)
	    	{
				isCurHashtagEnding |= HashtagsEmojisDetailsHelper.defineIfHashtagIsEndingInTweet(text, foundTerm.substring(1));
				
	    		text = text.replaceAll((ignoreCase?"(?i)":"") + "(?<=(\\s|\u00A0|^))" + foundTerm+"(?=(\\b|\\s|\u00A0|$))", "");
	    		text = text.replaceAll("\\s+", " "); // delete possibly occurred double white spaces
	    	}
			if (isCurHashtagEnding)
			{
				text = text.trim();
				textData.put(textEntry.getKey(), text);
			}
			else
			{
				tweetsToRemove.add(textEntry.getKey());
			}
		}
		
		UtilCollections.removeAllKeysFromMap(textData, tweetsToRemove);
	}
	
	
	public static void toRemoveGivenEmojis(Map<Integer, String> textData, boolean ignoreCase)
	{
		CategoryLexiconUnrestricted lexicon = null;
		try {
			lexicon = EmotionLexiconsGivenFactory.getLexiconOfGivenGEWEmojis();
		} catch (Exception e) {
			e.printStackTrace();
		}
		TermDetectorWithProcessing tDetector = new TermDetectorWithProcessing(lexicon.getAllLexiconTerms(), false);
		
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			Set<String> foundTerms = tDetector.findEmotionalTermsInText(ignoreCase?text.toLowerCase():text, false).keySet();
			for (String foundTerm : foundTerms)
	    	{
	    		text = text.replaceAll((ignoreCase?"(?i)":"") + "(?<=(\\s|\u00A0|^))" + foundTerm+"(?=(\\b|\\s|\u00A0|$))", "");
	    		text = text.replaceAll("\\s+", " "); // delete possibly occurred double white spaces
	    	}
	    	text = text.trim();
	    	textData.put(textEntry.getKey(), text);
	    	
		}
	}
	
	public static void toRemoveAllEmojis(Map<Integer, String> textData)
	{
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			text = TweetPreprocessing.removeEmojis(text, true);
	    	text = text.trim();
	    	textData.put(textEntry.getKey(), text);
		}
	}
	
	public static void decodeSpecialSymbols(Map<Integer, String> textData)
	{
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = UtilString.unescapeSpecialSymbols(textEntry.getValue());
			textData.put(textEntry.getKey(), text);
		}
	}
	
	
	public static void toRemoveShortTweets(Map<Integer, String> textData, int minRealWordNum)
	{
		Set<Integer> tweetsToRemove = new HashSet<Integer>();
		
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			int realWNum = TweetPreprocessing.getNumberOfRealWords(text, false, false); 
			if (realWNum < minRealWordNum)
				tweetsToRemove.add(textEntry.getKey());
		}
		UtilCollections.removeAllKeysFromMap(textData, tweetsToRemove);
	}
	
	/**
	 * The texts should be preprocessed with the <quote> present
	 * @param textData
	 * @param minRealWordNum
	 */
	public static void toRemoveTweetsWithQuotes(Map<Integer, String> textData)
	{
		Set<Integer> tweetsToRemove = new HashSet<Integer>();
		
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			if (text.contains("<quote>"))
				tweetsToRemove.add(textEntry.getKey());
		}
		UtilCollections.removeAllKeysFromMap(textData, tweetsToRemove);
	}
	
	/**
	 * Removes the entries of <link> (representing links) from the text of the tweets. 
	 * This assumes that the text is fully preprocessed, with URLs entries replaces by <link>
	 * @param tweets
	 */
	public static void toRemoveLinksFromTextsPreprocessed(List<Tweet> tweets)
	{
		for (Tweet tweet : tweets)
		{
			tweet.text = tweet.text.replace("<link>", "");
			tweet.text = tweet.text.replaceAll("\\s+", " ");
		}
	}
	
	/**
	 * Removes URL entries from the text of the tweets. 
	 * This assumes that the text of the tweets is only partially preprocessed, while the URL entries are present as is in the text.
	 * @param textData
	 */
	public static void toRemoveLinksFromTextsUnpreprocessed(Map<Integer, String> textData)
	{
		for (Map.Entry<Integer,String> textEntry : textData.entrySet())
		{
			String text = textEntry.getValue();
			text = TweetPreprocessing.removeAllLinks(text);
	    	text = text.trim();
	    	textData.put(textEntry.getKey(), text);
		}
	}
	
	
}
