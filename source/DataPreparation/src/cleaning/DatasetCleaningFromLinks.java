/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package cleaning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import processing.TweetPreprocessing;
import data.DatabaseWrapper;
import data.documents.Tweet;
import functionality.UtilCollections;

public class DatasetCleaningFromLinks {

	public static String outputCleaningFolder = "output/curCleaningProcess/";
	
	public static void findTweetsWithLinks(String tableName, String textColumnName, String idColumn) {
		
		System.out.println("Started detecting tweets with URLS from the table " + tableName  + " using column `" + textColumnName +"`. ");
		System.out.println("The ids of detected tweets will be outputted in files " + outputCleaningFolder + "tweetsWithURLsFor...");
		
		int maxOneBatchSize = 1000000;
		int offset = 0;
		boolean doOnce = false;
		int lastReturnedSize = 1;
		
		while (lastReturnedSize > 0)
		{
			try
			{
				Date start = new Date();
				List<Tweet> curBench = 
						DatabaseWrapper.getTweetsFromDatabaseUsingIdLimits(
								tableName, offset, offset + maxOneBatchSize, idColumn, textColumnName);
				
				lastReturnedSize = curBench.size();
				List<Integer> tweetIdsWithURLs = new ArrayList<Integer>();
				if (lastReturnedSize != 0)
				{
					for (Tweet tweet : curBench)
					{
						if (TweetPreprocessing.containsLinkInPreprocessed(tweet.text))
							tweetIdsWithURLs.add(tweet.tweetId);
					}
					
					UtilCollections.printListToFile(tweetIdsWithURLs, 
							outputCleaningFolder + "tweetsWithURLsFor_" + tableName.substring(0,Math.max(15, tableName.length())) + "_" +  (offset>=0 ? ("_off" + offset) :"") + ".txt");
					tweetIdsWithURLs.clear();
					
					Date finish = new Date();
					System.out.println(finish.toString() + " : Processed offset is " + offset + ", found " + lastReturnedSize + 
							" entries, last EntryID = " + curBench.get(curBench.size() - 1).tweetId + 
							" It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; ");
					curBench.clear();
					curBench = null;
					start = finish;
				}
			}
			catch(Exception e)
			{
				System.out.println("Error during data processing from offset " + offset + ": " + e.getMessage() + "\n\r Stack:" );
				e.printStackTrace();
				lastReturnedSize = 1;
			}
			offset += maxOneBatchSize;
			if (doOnce)
				lastReturnedSize = 0;
		}
		
	}
}
