/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import data.documents.TimedTweet;
import data.documents.Tweet;
import data.documents.TweetFeaturesExtended;

public class DatabaseWrapper {
	
	private static String usedDatabaseConnectionName = null;
	
	public static Connection getCurrentConnection() throws Exception
	{
		return DatabaseConnectionsIndex.getConnection(usedDatabaseConnectionName);
	}

	public static void setCurrentConnection(String databaseConnectionName) throws Exception
	{
		usedDatabaseConnectionName = databaseConnectionName;
	}
	
	public static String getCurrentConnectionName() {
		return usedDatabaseConnectionName;
	}
	
	static final String defaultIdColumn = "id";
	static final String defaultTextColumn = "text";
	
	public static List<Tweet> getTweetsFromDatabase(String tablename, int offset, int size) throws Exception
	{
		return  getTweetsFromDatabase(tablename, offset, size, defaultIdColumn, defaultTextColumn);
	}
	
	/**
	 * Returns all the tweets in the given database with standard column names. 
	 * If the table is too large, there might be problems with the memory. 
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public static List<Tweet> getAllTweetsFromDatabase(String tablename) throws Exception
	{
		return  getTweetsFromDatabase(tablename, null, defaultIdColumn, defaultTextColumn);
	}
	
	public static List<Tweet> getAllTweetsFromDatabase(String tablename, String idColumn, String textColumn) throws Exception
	{
		return getTweetsFromDatabase(tablename, null, idColumn, textColumn);
	}
	
	
	public static List<Tweet> getTweetsFromDatabase(String tablename, String extraCondition, String idColumn, String textColumn) throws Exception
	{
		Connection conn = getCurrentConnection();
		List<Tweet> allTweets = new ArrayList<Tweet>();
		if (extraCondition == null)
			extraCondition = "";
	    try
	    {
		    Statement stat = conn.createStatement();
		    ResultSet rs = stat.executeQuery("select * from " + tablename + " " + extraCondition + ";"); 
		    while (rs.next()) 
		    {
		    	Tweet tw = new Tweet();
		    	tw.tweetId = rs.getInt(idColumn);
		    	tw.text = rs.getString(textColumn);
		    	
		    	allTweets.add(tw);
		    }
		    rs.close();
		    stat.close();
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	    finally{
		    conn.close();
		 } 
	   
	    return allTweets;
	}
	
	public static List<Tweet> getTweetsFromDatabase(String tablename, int offset, int size, String idColumn, String textColumn) throws Exception
	{
		String extraLimitCondition =  " limit " + offset + ", "+ size +";";
		return getTweetsFromDatabase(tablename, extraLimitCondition, idColumn, textColumn);
	}
	

	/**
	 * maxId will not be inclused (exclusive)
	 * @param tablename
	 * @param minId
	 * @param maxInd
	 * @return
	 * @throws Exception 
	 */
	public static List<TimedTweet> getTimedTweetsFromDatabaseUsingIdLimits(String tablename, int minId, int maxId, String idColumn, String textColumn) throws Exception
	{
		String whereCondition = idColumn + ">=" + minId + " AND " + idColumn + " < " + maxId;
		return getTimedTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, textColumn);
	}
	
	/**
	 * maxId will not be included (exclusive)
	 * @param tablename
	 * @param minId
	 * @param maxInd
	 * @return
	 * @throws Exception 
	 */
	public static List<Tweet> getTweetsFromDatabaseUsingIdLimits(String tablename, int minId, int maxId, String idColumn, String textColumn) throws Exception
	{
		String whereCondition = idColumn + ">=" + minId + " AND " + idColumn + " < " + maxId;
		return getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, textColumn);
	}
	
	/**
	 * maxId will not be included (exclusive)
	 * @param tablename
	 * @param minId
	 * @param maxInd
	 * @return
	 * @throws Exception 
	 */
	public static List<Tweet> getTweetsFromDatabaseWithConditionUsingIdLimits(String tablename, int minId, int maxId, String idColumn, String textColumn, String whereCondition) throws Exception
	{
		whereCondition += " AND " + idColumn + ">=" + minId + " AND " + idColumn + " < " + maxId;
		return getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, textColumn);
	}
	
	public static List<TimedTweet> getTimedTweetsFromDatabaseWithCondition(String tablename, String whereCondition, String idColumn, String textColumn) throws Exception
	{
		Connection conn = getCurrentConnection();
		 List<TimedTweet> allTweets = new ArrayList<TimedTweet>();
		
	    try
	    {
	    	 Calendar calendar = Calendar.getInstance(); 
				calendar.setTimeZone(TimeZone.getTimeZone("BST"));// set London time zone
				
		    Statement stat = conn.createStatement();
		    ResultSet rs = stat.executeQuery("select * from "+tablename+" where " + whereCondition+ ";");
		    while (rs.next()) 
		    {
		    	TimedTweet tw = new TimedTweet();
		    	tw.tweetId = rs.getInt(idColumn);
		    	tw.text = rs.getString( textColumn);
		    	tw.datetime = rs.getTimestamp("date_creation",calendar);
		    	
		    	allTweets.add(tw);
		    }
		    rs.close();
		    stat.close();
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	    finally{
		    conn.close();
		 } 
	   
	    return allTweets;
	}
	

	public static List<Tweet> getTweetsFromDatabaseWithCondition(String tablename, String whereCondition, int offset, int size, String idColumn, String textColumn) throws Exception
	{
		return  getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, textColumn, offset, size);
	}
	
	public static List<Tweet> getTweetsFromDatabaseWithCondition(String tablename, String whereCondition, String idColumn, String textColumn, int offset, int size) throws Exception
	{
		if (whereCondition == null)
			return getTweetsFromDatabase(tablename, offset, size, idColumn, textColumn);
		String extraLimitCondition = " where " + whereCondition + " limit " + offset + ", "+ size +";";
		return getTweetsFromDatabase(tablename, extraLimitCondition, idColumn, textColumn);
	}
	
	
	public static List<Tweet> getTweetsFromDatabaseWithCondition(String tablename, String whereCondition, String idColumn, String textColumn) throws Exception
	{
		Connection conn = getCurrentConnection();
		 List<Tweet> allTweets = new ArrayList<Tweet>();
		
	    try
	    {
		    Statement stat = conn.createStatement();
		    ResultSet rs = stat.executeQuery("select * from "+ tablename + 
		    		(whereCondition != null ? (" where " + whereCondition) : "") + ";");
		    while (rs.next()) 
		    {
		    	Tweet tw = new Tweet();
		    	tw.tweetId = rs.getInt(idColumn);
		    	tw.text = rs.getString( textColumn);
		    	
		    	allTweets.add(tw);
		    }
		    rs.close();
		    stat.close();
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	    finally{
		    conn.close();
		 } 
	   
	    return allTweets;
	}
	
	public static List<Tweet> getTweetsFromDatabaseWithCondition(String tablename, String whereCondition, int offset, int size) throws Exception
	{
		Connection conn = getCurrentConnection();
		 List<Tweet> allTweets = new ArrayList<Tweet>();
		
	    try
	    {
		    Statement stat = conn.createStatement();
		    ResultSet rs = stat.executeQuery("select * from "+tablename+" where " + whereCondition+ " limit " + offset + ", "+ size +";");
		    while (rs.next()) 
		    {
		    	Tweet tw = new Tweet();
		    	tw.tweetId = rs.getInt(defaultIdColumn);
		    	tw.text = rs.getString(defaultTextColumn);
		    	
		    	allTweets.add(tw);
		    }
		    rs.close();
		    stat.close();
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	    finally{
		    conn.close();
		 } 
	   
	    return allTweets;
	}
	
	/**
	 * Return the tweet having the specified id. Returns an empty tweet if nothing was found.
	 * @param tablename
	 * @param tweetId
	 * @return
	 * @throws Exception
	 */
	public static Tweet getTweetById(String tablename, int tweetId) throws Exception
	{
		return  getTweetById(tablename, tweetId, defaultIdColumn, defaultTextColumn);
	}
	
	/**
	 * Return the tweet having the specified id. Returns an empty tweet if nothing was found.
	 * @param tablename
	 * @param tweetId
	 * @param idColumn
	 * @param textColumn
	 * @return
	 * @throws Exception
	 */
	public static Tweet getTweetById(String tablename, int tweetId, String idColumn, String textColumn) throws Exception
	{
		Connection conn = getCurrentConnection();
		
	    Tweet tweet = new Tweet();
	    try
	    { 
		    PreparedStatement prep = conn.prepareStatement(
		    		"select * from " + tablename + " where `" + idColumn + "` = ?;");
		    prep.setInt(1, tweetId);
		    ResultSet rs = prep.executeQuery();
		    while (rs.next()) 
		    {
		    	tweet.tweetId = rs.getInt(idColumn);
		    	tweet.text = rs.getString(textColumn);
		    }
		    rs.close();
		    prep.close();
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	    finally{
		    conn.close();
		 } 
	   
	    return tweet;
	}
	
	public static List<Tweet> getTweetsDataFromTableWithSize (
			String tablename, String idColumn, String textColumn, String whereCondition, 
			int dataSize, String sizeColumn) throws Exception {
		String extraCondition = sizeColumn + " <= " + dataSize + " and "+ sizeColumn + " is not null";
		if (whereCondition != null)
			extraCondition = "(" + whereCondition  + ") and " + extraCondition;
		return getTweetsFromDatabaseWithCondition(tablename, extraCondition, idColumn, textColumn);
	}
	
	
	public static void saveTweetPreprocessingResults(
			Map<Integer,String> tweetData, String tablename, String idColumn, String cleanTextColumn) throws Exception {
		
		Connection conn = getCurrentConnection();
		 
		 try
		 {			 
		    PreparedStatement prep = conn.prepareStatement(
				      "UPDATE "+tablename+" SET `" + cleanTextColumn+"`=? WHERE `" + idColumn+ "`=?");

		    for (Map.Entry<Integer,String> curTweet : tweetData.entrySet())
			{
		    	prep.setString(1, curTweet.getValue());
		    	prep.setInt(2, curTweet.getKey());
		    	prep.addBatch();
			 }
			  
		    conn.setAutoCommit(false);
			  prep.executeBatch();
			  conn.setAutoCommit(true);
			  prep.close();
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
		    
	}
	
	/**
	 * This assumes that there is a categoryColumn in the tweet database and `tSize` column representing which data limits the tweet belongs. 
	 * @param tablename
	 * @param idColumn
	 * @param textColumn
	 * @param categoryColumn
	 * @param whereCondition
	 * @param dataSize
	 * @param sizeColumn
	 * @param emotionLabels
	 * @param textData
	 * @throws Exception
	 */
	public static void readTweetCategoryDataFromTableWithSize (
			String tablename, String idColumn, String textColumn, String categoryColumn, String whereCondition, 
			int dataSize, String sizeColumn,
			Map<Integer, List<Integer>> emotionLabels, Map<Integer, String> textData) throws Exception
	{
		String extraCondition = sizeColumn + " <= " + dataSize + " and "+ sizeColumn + " is not null";
		if (whereCondition != null)
			extraCondition = "(" + whereCondition  + ") and " + extraCondition;
		readTweetCategoryData (tablename, idColumn, textColumn, categoryColumn, extraCondition, emotionLabels, textData);
	}
	
	public static void readTweetCategoryData (String tablename, String idColumn, String textColumn, String categoryColumn, String whereCondition,
			Map<Integer, List<Integer>> emotionLabels, Map<Integer, String> textData) throws Exception
	{
		Connection conn = getCurrentConnection();
		 
		 try
		 {
			 
			 PreparedStatement prep = conn.prepareStatement(
			    		"select * from "+tablename+ ((whereCondition != null)?" where " + whereCondition:"") + ";"
			   );
			 ResultSet rs = prep.executeQuery();
			  while (rs.next()) 
			  {
				int tweetId = rs.getInt(idColumn);
			    String text =  rs.getString(textColumn);
			    String categories = rs.getString(categoryColumn);
			    List<Integer> newCategories = new ArrayList<Integer>();
			    if (categories.contains(","))
			    {
			    	String[] parsedCategories = categories.split(",");
			    	for (String parsedCat : parsedCategories)
			    		newCategories.add(Integer.parseInt(parsedCat));
			    }
			    else
			    	newCategories.add(Integer.parseInt(categories));
			    if (!textData.containsKey(tweetId))
			    {
			    	textData.put(tweetId, text);
			    	emotionLabels.put(tweetId, newCategories);
			    }
			    else
			    {
			    	emotionLabels.get(tweetId).addAll(newCategories);
			    }
			  }
			
		      rs.close();
			  prep.close();
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
		    
	}
	
	
	/**
	 * Extracts the distinct values in the specified column.
	 * @param tablename
	 * @param columnName
	 * @return
	 * @throws Exception
	 */
	public static List<String> getDistinctValuesOfField(String tablename, String columnName) throws Exception
	{
		Connection conn = getCurrentConnection();
		List<String> distinctValues = new ArrayList<String>();
		
		  try
		    {
		    	Statement stat = conn.createStatement();
			    ResultSet rs = stat.executeQuery("select distinct " + columnName + " from " + tablename + ";");
			    while (rs.next()) 
			    {
			    	String value = rs.getString(1);
			    	distinctValues.add(value);
			    }
			    stat.close();
			    rs.close();
		    }
		    catch(Exception e)
		    {
		    	throw e;
		    }
		    finally{
			    conn.close();
			 } 
		
		return distinctValues;
	}
	
	/**
	 * Extracts distinct values for the given column with the number of times such value appears 
	 * (it uses sql queries for computation, so will be slow on large tables!!!)
	 * @param tablename
	 * @param columnName
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Integer> getDistinctValuesOfFieldWithCounts(String tablename, String columnName) throws Exception
	{
		Connection conn = getCurrentConnection();
		 Map<String, Integer> valuesMap = new HashMap<String, Integer>();
		
		  try
		    {
		    	Statement stat = conn.createStatement();
			    ResultSet rs = stat.executeQuery("select " + columnName + ", count(*) from "+tablename+" group by " + columnName + ";");
			    while (rs.next()) 
			    {
			    	String value = rs.getString(1);
			    	int count = rs.getInt(2);
			    	valuesMap.put(value, count);
			    }
			    stat.close();
			    rs.close();
		    }
		    catch(Exception e)
		    {
		    	throw e;
		    }
		    finally{
			    conn.close();
			 } 
		
		return valuesMap;
	}
	
	/** Get the extended tweets (with additional features column) from the given table. */
	public static List<TweetFeaturesExtended> getUnlabeledTweetsForSSLWithFeatures(
			String tablename, String idColumn, String textColumn, String featureColumn) throws Exception
	{
		Connection conn = DatabaseWrapper.getCurrentConnection();
		List<TweetFeaturesExtended> tweetsRes = new ArrayList<TweetFeaturesExtended>();
		 try
		 {
			
			  PreparedStatement prep = conn.prepareStatement(
			    		"SELECT `" + idColumn +"` as ID, `"+ textColumn + "` as `text`, `" + featureColumn +"` as `features` " +
			    		" FROM " + tablename + ";");
			    		 
			  ResultSet rs = prep.executeQuery();
			  while (rs.next()) 
			  {
				  Tweet tweet1 = new Tweet();
			  	   
			    	tweet1.tweetId = rs.getInt("ID");
			    	tweet1.text = rs.getString("text");
			    	String featuresStr = rs.getString("features");
			    	TweetFeaturesExtended tweet = new TweetFeaturesExtended(tweet1,featuresStr);
			    	tweetsRes.add(tweet);
			  }
			  rs.close();
			  
			 
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 }
		 return tweetsRes;
	}
}
