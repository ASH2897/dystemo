/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.loading;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import processing.TweetPreprocessing;
import utility.Pair;
import data.LabeledData;
import data.documents.Tweet;
import functionality.UtilCollections;

public class DataFilesFormatSaver {

	/**
	 * Will print each tweet, one per line, where the text of the tweets will be tokenized and lemmatized with Stanford Lemmatizer.  
	 * @param filename
	 * @param allTweets
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void printTweetDataIntoFileInWordForm(String filename, List<Tweet> allTweets) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		TweetPreprocessing tproc = new TweetPreprocessing();
		
		for (Tweet tw : allTweets)
		{
			List<String> twTokens = tproc.tokenize(tw.text);
			pw.println(tw.tweetId + "\t" + UtilCollections.join(twTokens, " "));
		}
		
		pw.close();
	}
	
	/**
	 * Saves tweets in a datafile
	 * format: tweetId<tab>tweetText
	 * @param filename
	 * @param unlabeledData
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void printTweetDataIntoFileAsGiven(String filename, List<? extends Tweet> unlabeledData) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		
		for (Tweet tw : unlabeledData)
		{
			pw.println(tw.tweetId + "\t" + tw.text.replace("\n", " \\n ").replace("\r", " \\n ").replace("\t", " "));
		}
		
		pw.close();
	}
	
	
	/**
	 * Print data in the labeled format:
	 * tweetId<tab>tweetText<tab>categories ids separated by comma
	 * P.S. This implementation assumes that all the tweets from textData have assigned emotion labels in the map.
	 * @param filenameToPrint
	 * @param emotionLabels
	 * @param textData 
	 * @throws Exception
	 */
	public static void printLabeledDataIntoFile(String filenameToPrint, Map<Integer, List<Integer>> emotionLabels, Map<Integer, String> textData ) throws Exception
	{
		File tempFile = new File(filenameToPrint);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		
		for (Integer twId : textData.keySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(twId);
			sb.append("\t");
			sb.append(textData.get(twId).replace("\n", " \\n ").replace("\r", " \\n ").replace("\t", " "));
			sb.append("\t");
			sb.append(UtilCollections.join(new HashSet<Integer>(emotionLabels.get(twId)), ","));
			pw.println(sb.toString());
		}
		
		pw.close();
	}
	

	/**
	 * Print data in the labeled format:
	 * tweetId<tab>tweetText<tab>categories ids separated by comma
	 * P.S. This implementation assumes that all the tweets from textData have assigned emotion labels in the map.
	 * @param filenameToPrint
	 * @param emotionLabels
	 * @param textData 
	 * @throws Exception
	 */
	public static void printLabeledDataIntoFile(String filenameToPrint, LabeledData data) throws Exception
	{
		printLabeledDataIntoFile(filenameToPrint, data.emotionLabels, data.textData);
	}
	
	/**
	 * TODO: Describe better what is binary format
	 * Print data in the binary labeled format:
	 * tweetId<tab>tweetText<tab>current category id: either positive - which means that the category is present, or negative - which means that the category is absent. 
	 * P.S. This implementation assumes that all the tweets from textData have assigned emotion labels in the map.
	 * @param filenameToPrint
	 * @param emotionLabels
	 * @param textData 
	 * @throws Exception
	 */
	public static void printBinaryDataIntoFile(String filenameToPrint, List<Pair<Integer, Integer>> emotionLabels, Map<Integer, String> textData ) throws Exception
	{
		File tempFile = new File(filenameToPrint);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		
		for (Pair<Integer, Integer> twLabelPair : emotionLabels)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(twLabelPair.first);
			sb.append("\t");
			sb.append(textData.get(twLabelPair.first).replace("\n", " \\n ").replace("\r", " \\n ").replace("\t", " "));
			sb.append("\t");
			sb.append(twLabelPair.second);
			pw.println(sb.toString());
		}
		
		pw.close();
	}
	
	/**
	 * Prints out the tweets in the labeled format while assigning each tweet to the specified category
	 * @param filename
	 * @param tweets
	 * @param categoryId
	 * @throws Exception
	 */
	public static void printTweetsLabeledWithFixedCategory(String filename, List<? extends Tweet> tweets, int categoryId) throws Exception {
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		
		for (Tweet tw : tweets)
		{
			pw.println(tw.tweetId + "\t" + tw.text + "\t" + categoryId);
		}
		
		pw.close();
	}
}
