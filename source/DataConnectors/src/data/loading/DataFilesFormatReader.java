/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.loading;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utility.Pair;
import data.documents.Tweet;
import data.documents.TweetFeaturesExtended;
import functionality.UtilFiles;
import functionality.UtilString;


public class DataFilesFormatReader {
	
	
	/** 
	 * Reads the labeled tweet data from the file.
	 * @param dataFile
	 * @param emotionLabels
	 * @param textData
	 * @throws IOException
	 */
	public static void readLabeledDataFromFile(String dataFile, Map<Integer, List<Integer>> emotionLabels, Map<Integer, String> textData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			String categoriesStr = parsedLine[2];
			List<Integer> categories = UtilString.getIntegerListFromString(categoriesStr, ",");
			
			textData.put(tweetId, tweetText);
			emotionLabels.put(tweetId,categories);
		}
	}
	
	/** 
	 * Reads the raw (without labels) tweet data from the file.
	 * This assumes that file has format: tweetId<tab>tweetText
	 * @param dataFile
	 * @param textData
	 * @throws IOException
	 */
	public static void readTextDataFromFile(String dataFile, Map<Integer, String> textData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			
			textData.put(tweetId, tweetText);
		}
	}
	
	/** 
	 * Reads the raw (without labels) tweet data from the file.
	 * @param dataFile
	 * @param textData
	 * @throws IOException
	 */
	public static void readTextDataFromFile(String dataFile, List<Tweet> textData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			Tweet tw = new Tweet();
			tw.tweetId = tweetId;
			tw.text = tweetText;
			textData.add(tw);
		}
	}
	
	/** 
	 * Reads the labeled tweet data from the file in binary format.
	 * @param dataFile
	 * @param emotionLabels
	 * @param textData
	 * @throws IOException
	 */
	public static void readBinaryDataFromFile(String dataFile, List<Pair<Integer, Integer>> emotionLabels, Map<Integer, String> textData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			String categoriesStr = parsedLine[2];
			int categoryId = Integer.parseInt(categoriesStr);
			
			textData.put(tweetId, tweetText);
			emotionLabels.add(new Pair(tweetId,categoryId));
		}
	}
	
	/** 
	 * Reads the labeled tweet data from the file with extra features.
	 * Each line has the following format: 
	 * <id>	<tweetText(usualy normalized)>	<categoryLabels in comma>	<standrardFeatureLine in semicomma>
	 * @param dataFile
	 * @param emotionLabels
	 * @param textData
	 * @throws IOException
	 */
	public static void readDataFromFileWithFeatures(String dataFile, Map<Integer, List<Integer>> emotionLabels, Map<Integer, Tweet> tweetData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			String categoriesStr = parsedLine[2];
			List<Integer> categories = UtilString.getIntegerListFromString(categoriesStr, ",");
			
			Tweet tweet = new Tweet();
			tweet.tweetId  = tweetId;
			tweet.text = tweetText;
			if (parsedLine.length > 3)
			{
				String featureLine = parsedLine[3];
				tweet = new TweetFeaturesExtended(tweet, featureLine);
			}
			
			tweetData.put(tweetId, tweet);
			emotionLabels.put(tweetId,categories);
		}
	}
	
	/** 
	 * Reads the labeled tweet data from the file with extra features.
	 * Each line has the following format: 
	 * <id>	<tweetText(usualy normalized)>	<categoryLabel>	<standrardFeatureLine in semicomma>
	 * @param dataFile
	 * @param emotionLabels
	 * @param textData
	 * @throws IOException
	 */
	public static void readBinaryDataFromFileWithFeatures(String dataFile, List<Pair<Integer, Integer>> emotionLabels, Map<Integer, Tweet> tweetData) throws IOException
	{
		List<String> dataLines = UtilFiles.getContentLines(dataFile);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			int tweetId = Integer.parseInt(parsedLine[0]);
			String tweetText = parsedLine[1];
			String categoriesStr = parsedLine[2];
			int categoryId = Integer.parseInt(categoriesStr);
			
			Tweet tweet = new Tweet();
			tweet.tweetId  = tweetId;
			tweet.text = tweetText;
			if (parsedLine.length > 3)
			{
				String featureLine = parsedLine[3];
				tweet = new TweetFeaturesExtended(tweet, featureLine);
			}
			
			tweetData.put(tweetId, tweet);
			emotionLabels.add(new Pair(tweetId,categoryId));
		}
	}
	
	/**
	 * Read the labels assigned to tweets from the classifier's detailed output file, as generated by ExperimentsResultsSaver.saveAnnotationResults
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public static Map<Integer, List<Integer>> getLabelDataFromFileOutput(String filename) throws IOException
	{
		List<String[]> lines = UtilFiles.getParsedLines(filename, "\t", 0);
		Map<Integer, List<Integer>> labels = new HashMap<Integer, List<Integer>>();
		
		for (String[] line : lines)
		{
			if (line.length > 0)
			{
				int tweetId = Integer.parseInt(line[0]);
				List<Integer> categories;
				if (line[2].equals( ""))
				{
					categories = new ArrayList<Integer>();
				}
				else
					categories = UtilString.getIntegerListFromString(line[2], ",");
				labels.put(tweetId, categories);
			}
		}
		return labels;
	}
	
}
