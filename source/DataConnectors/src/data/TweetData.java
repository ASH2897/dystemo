/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.util.ArrayList;
import java.util.List;

import data.documents.Tweet;

/**
 * TweetData presents a collection of tweets, without any additional annotation.
 * 
 */
public class TweetData extends Dataset {

	public List<? extends Tweet> tweets;
	
	public TweetData (List<? extends Tweet> tweets)
	{
		this.tweets = tweets;
	}

	/**
	 * This function will assign new tweet ids to the tweets from the second dataset to avoid overlap in ids with the first dataset.
	 * @param data1
	 * @param data2
	 * @return
	 */
	public static TweetData mergeTwoTweetData(TweetData data1, TweetData data2) {
		List<Tweet> resTweets = new ArrayList<Tweet>(data1.tweets);
		
		// find max tweet id
		int maxInt = -1;
		for (Tweet tweet : data1.tweets) {
			if (tweet.tweetId > maxInt)
				maxInt = tweet.tweetId;
		}
		
		for (Tweet tweet : data2.tweets)
		{
			Tweet newTweet = new Tweet();
			newTweet.tweetId = tweet.tweetId + maxInt;
			newTweet.text = tweet.text;
			resTweets.add(newTweet);
		}
		return new TweetData(resTweets);
	}
}
