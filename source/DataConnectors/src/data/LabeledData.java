/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * LabeledData stores both the texts of documents and their associated labels.
 * 
 */
public class LabeledData extends Dataset
{
	public Map<Integer, List<Integer>> emotionLabels;
	public Map<Integer, String> textData;
	
	public LabeledData (
			Map<Integer, List<Integer>> emotionLabels,
			Map<Integer, String> textData)
	{
		this.emotionLabels = emotionLabels;
		this.textData = textData;
	}
	
	/**
	 * This function will assign new tweet ids to the tweets from the second dataset to avoid overlap in ids with the first dataset.
	 * @param data1
	 * @param data2
	 * @return
	 */
	public static LabeledData mergeTwoLabeledData(LabeledData data1, LabeledData data2)
	{
		Map<Integer, String> textData = new HashMap<Integer,String>(data1.textData);
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>(data1.emotionLabels);
		
		int maxInt = Collections.max(textData.keySet());
		
		for (Integer twId2 : data2.textData.keySet())
		{
			textData.put(twId2 + maxInt, data2.textData.get(twId2));
			emotionLabels.put(twId2 + maxInt, data2.emotionLabels.get(twId2));
		}
		return new LabeledData(emotionLabels, textData);
	}
}
