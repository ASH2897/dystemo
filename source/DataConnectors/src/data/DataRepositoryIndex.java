/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import data.DataDescription.DataSourceType;
import data.DataDescription.DataType;
import data.DataDescription.MergedDataDetails;
import data.loading.DataSetLoader;

@XmlRootElement( name = "DataRepositoryIndex" )
public class DataRepositoryIndex {

	@XmlElement(name = "DataDescription")
	public void setAvailableDataList( List<DataDescription> availableDataList )
	{
		this.availableDataList = availableDataList;
	}
	
	public List<DataDescription> getAvailableDataList()
	{
		return availableDataList;
	}
	
	private void updateDataDescriptionMap() {
		this.availableDataMap = new HashMap<String, DataDescription>();
		for (DataDescription dataInfo : availableDataList) {
			this.availableDataMap.put(dataInfo.dataName, dataInfo);
		}
	}
	
	private void updateFilePathPrefix(String pathPrefix, String pathPrefixPlaceholder) {
		for (DataDescription dataInfo : availableDataList) {
			if (dataInfo.sourceType == DataSourceType.TextFile) {
				dataInfo.textFileName = dataInfo.textFileName.replace(pathPrefixPlaceholder, pathPrefix);
			}
		}
	}

	private List<DataDescription> availableDataList;
	
	private Map<String, DataDescription> availableDataMap;
	
	/**
	 * Initialize an empty index
	 */
	public DataRepositoryIndex() {
		this.availableDataList = new ArrayList<DataDescription>();
		this.availableDataMap = new HashMap<String, DataDescription>();
	}
	
	public DataRepositoryIndex(List<DataDescription> availableDataList) {
		this. availableDataList = availableDataList;
		updateDataDescriptionMap();
	}
	
	/**
	 * 
	 * @param filePath Should ideally have .xml extension
	 */
	public void saveDataIndexToFile(String filePath) {
		JAXB.marshal(this, new File(filePath));
	}
	
	/**
	 * Load available connections from the XML file.
	 * An example XML file can be found in DataConnectors/examples/data-repository-index-example.xml
	 * The paths of the text files should be absolute if this function is called. Otherwise, call loadFromXMLFile(String dataIndexFile, String pathPrefix)
	 * @param connectionsIndexFile
	 */
	static public DataRepositoryIndex loadFromXMLFile(String dataIndexFile) {
		DataRepositoryIndex res = JAXB.unmarshal(new File(dataIndexFile), 
				DataRepositoryIndex.class);
		res.updateDataDescriptionMap();
		return res;
	}
	
	/**
	 * Load available connections from the XML file.
	 * An example XML file can be found in DataConnectors/examples/data-repository-index-example.xml
	 * The paths of the text files can be relative, with __PREFIX__ placeholder specified in the filename. 
	 * That placeholder will be automatically replaced by the specified string in pathPrefix.
	 * @param connectionsIndexFile
	 */
	static public DataRepositoryIndex loadFromXMLFile(String dataIndexFile, String pathPrefix) {
		DataRepositoryIndex res = JAXB.unmarshal(new File(dataIndexFile), 
				DataRepositoryIndex.class);
		res.updateFilePathPrefix(pathPrefix, "__PREFIX__");
		res.updateDataDescriptionMap();
		return res;
	}
	
	/**
	 * Save the given dataIndex into the XML file.
	 * @param connectionsIndex
	 * @param filePath
	 */
	static public void saveConnectionParametersToFile(DataRepositoryIndex dataIndex, String filePath) {
		dataIndex.saveDataIndexToFile(filePath);
	}
	
	private static DataRepositoryIndex usedDataRepositoryIndex = null;
	
	static public void setDefaultDataRepositoryIndex(DataRepositoryIndex dataIndex) {
		usedDataRepositoryIndex = dataIndex;
	}
	
	
	// MAIN FUNCTIONALITY
	public DataDescription getDataDescription(String dataName) throws Exception {
		if (!availableDataMap.containsKey(dataName))
			throw new Exception("Data '" + dataName + "' is not described in the repository index");
		return availableDataMap.get(dataName);
	}
	
	
	
	
	public static LabeledData getLabeledDataByName (String dataName) throws Exception 
	{
		if (storeDataCache) {
			Dataset cached = getCachedData(dataName);
			if (cached != null && cached instanceof LabeledData)
				return (LabeledData)cached;
		}
		
		if (usedDataRepositoryIndex == null)
			throw new Exception("Default data repository is not set!");
		DataDescription dataInfo = usedDataRepositoryIndex.getDataDescription(dataName);

		return getLabeledDataFromDescription(dataInfo);
	}
		
	public static LabeledData getLabeledDataFromDescription(DataDescription dataInfo) throws Exception 
	{
		if (dataInfo == null) {
			throw new Exception("The description of the requested data is not provided.");
		}
		
		String dataName = dataInfo.dataName;
		
		if (dataInfo.dataType != DataType.Labeled)
			throw new Exception("The data '" + dataName + "' is not assigned to labeled data.");
		
		LabeledData res = null;
		if (dataInfo.sourceType == DataSourceType.Database)
		{
			if (dataInfo.databaseDataDetails == null)
				throw new Exception("The database data details are not specified for data '" + dataName + "'.");
			// get data from the database
			res =  DataSetLoader.getLabeledDataFromDatabase(dataInfo.databaseDataDetails);
		}
		else if (dataInfo.sourceType == DataSourceType.TextFile)
		{
			// get data from the text file
			if (dataInfo.textFileName == null)
				throw new Exception("The text file is not specified for data '" + dataName + "'.");
			else {
				res = DataSetLoader.getLabeledDataFromTextFile(dataInfo.textFileName);
			}
		}
		else if (dataInfo.sourceType == DataSourceType.Merged) {
			// Note that the tweet ids in the merged dataset will not correspond to the ones in the original files/database.
			if (dataInfo.mergedDataDetails == null) {
				throw new Exception("The merged data details are not specified for data '" + dataName + "'.");
			}
			res = getLabeledDataByName(dataInfo.mergedDataDetails.dataToMerge.get(0).name);
			if (dataInfo.mergedDataDetails.dataToMerge.size() > 1)
				for (int dataInd = 1; dataInd < dataInfo.mergedDataDetails.dataToMerge.size(); ++dataInd) {
					LabeledData tmp = getLabeledDataByName(dataInfo.mergedDataDetails.dataToMerge.get(dataInd).name);
					res = LabeledData.mergeTwoLabeledData(res, tmp);
				}
		}
		
		if (res != null) {
			if (storeDataCache)
				putDataInCache(dataName, res);
			res.dataName = dataName;
		}
		
		return res;
	}
	
	/**
	 * This returns tweet data (with no labels, containing only information about the tweet text and ids)
	 * @param dataName
	 * @return
	 * @throws Exception
	 */
	public static TweetData getTweetDataByName (String dataName) throws Exception 
	{
		if (storeDataCache) {
			Dataset cached = getCachedData(dataName);
			if (cached != null && cached instanceof TweetData)
				return (TweetData)cached;
		}
		
		if (usedDataRepositoryIndex == null)
			throw new Exception("Default data repository is not set!");
		DataDescription dataInfo = usedDataRepositoryIndex.getDataDescription(dataName);
		return getTweetDataFromDescription(dataInfo);
	}
		
	public static TweetData getTweetDataFromDescription(DataDescription dataInfo) throws Exception 
	{
		if (dataInfo == null) {
			throw new Exception("The description of the requested data is not provided.");
		}
		String dataName = dataInfo.dataName;
		TweetData res = null;
		if (dataInfo.sourceType == DataSourceType.Database)
		{
			if (dataInfo.databaseDataDetails == null)
				throw new Exception("The database data details are not specified for data '" + dataName + "'.");
			// get data from the database
			res =  DataSetLoader.getTweetDataFromDatabase(dataInfo.databaseDataDetails);
		}
		else if (dataInfo.sourceType == DataSourceType.TextFile)
		{
			// get data from the text file
			if (dataInfo.textFileName == null)
				throw new Exception("The text file is not specified for data '" + dataName + "'.");
			else {
				res = DataSetLoader.getTweetDataFromTextFile(dataInfo.textFileName);
			}
		}
		else if (dataInfo.sourceType == DataSourceType.Merged) {
			// Note that the tweet ids in the merged dataset will not correspond to the ones in the original files/database.
			if (dataInfo.mergedDataDetails == null) {
				throw new Exception("The merged data details are not specified for data '" + dataName + "'.");
			}
			res = getTweetDataByName(dataInfo.mergedDataDetails.dataToMerge.get(0).name);
			if (dataInfo.mergedDataDetails.dataToMerge.size() > 1)
				for (int dataInd = 1; dataInd < dataInfo.mergedDataDetails.dataToMerge.size(); ++dataInd) {
					TweetData tmp = getTweetDataByName(dataInfo.mergedDataDetails.dataToMerge.get(dataInd).name);
					res = TweetData.mergeTwoTweetData(res, tmp);
				}
		}
		
		if (res != null) {
			if (storeDataCache)
				putDataInCache(dataName, res);
			res.dataName = dataName;
		}
		
		return res;
	}
	
	/**
	 * This returns the composite tweet data, e.g. where one part is unlabeled (text-only) and another one are detected pseudo-neutral tweets (text-only). 
	 * If the actual stored data is TweetData, then only unlabeled part of composite data is set, while other remain null.
	 * For more details, look at the description of CompositeData
	 * @param dataName
	 * @return
	 * @throws Exception
	 */
	public static CompositeData getCompositeDataByName(String dataName) throws Exception 
	{
		if (usedDataRepositoryIndex == null)
			throw new Exception("Default data repository is not set!");
		DataDescription dataInfo = usedDataRepositoryIndex.getDataDescription(dataName);
		
		if (storeDataCache && dataInfo.dataType == DataType.Composite) { // we store the composite data in cache only if the data are of composite type
			Dataset cached = getCachedData(dataName);
			if (cached != null && cached instanceof CompositeData)
				return (CompositeData)cached;
		}
		
		return getCompositeDataFromDescription(dataInfo);
	}
	
	public static CompositeData getCompositeDataFromDescription(DataDescription dataInfo) throws Exception 
	{
		if (dataInfo == null) {
			throw new Exception("The description of the requested data is not provided.");
		}
		String dataName = dataInfo.dataName;
		
		CompositeData res = new CompositeData();
		
		if (dataInfo.dataType == DataType.Composite) {
			
			if (dataInfo.sourceType != DataSourceType.Merged) {
				throw new Exception("The source type for composite data '" + dataName + "' has to be set to 'Merged'.");
			}
			
			// Note that the tweet ids in the merged dataset will not correspond to the ones in the original files/database.
			if (dataInfo.mergedDataDetails == null) {
				throw new Exception("The merged data details are not specified for data '" + dataName + "'.");
			}
			
			for (MergedDataDetails.CompositeDataDetails compositeDataDetails : dataInfo.mergedDataDetails.dataToMerge) {
				if (compositeDataDetails.compositeType.equals("unlabeled")) {
					res.unlabeledData = getTweetDataByName(compositeDataDetails.name);
				} else if (compositeDataDetails.compositeType.equals("pseudo-neutral"))
				{
					res.pseudoNeutralData = getTweetDataByName(compositeDataDetails.name);
				}
			}
		} else if (dataInfo.dataType == DataType.TextOnly) {
			// we set only unlabeled part then
			res.unlabeledData = getTweetDataByName(dataInfo.dataName);
			res.pseudoNeutralData = null;
		}
		
		if (res != null && res.unlabeledData != null) {
			if (storeDataCache && dataInfo.dataType == DataType.Composite) // we will store the composite data in cache only if it is of composite type
				putDataInCache(dataName, res);
			res.dataName = dataName;
			return res;
		}
		else 
			return null;
		
	}
	
	
	// Functions and fields for cached data processing:
	public static boolean storeDataCache = false; 
	
	private static Dataset getCachedData(String dataName) throws Exception {
		if (cachedData.containsKey(dataName))
			return cachedData.get(dataName);
		else
			return null;
	}
	
	private static void putDataInCache(String dataName, Dataset data) throws Exception {
		cachedData.put(dataName, data);
	}
	
	static Map<String, Dataset> cachedData;
	static {
		cachedData = new HashMap<String, Dataset>();
	}
	
	
	
}
