/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learning.experiments.OneLearningProcessHelper;
import learning.parameters.AllLearningParameters;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.IndependentLearnerParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.RebalancingParameters;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import learners.given.ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams;

public class PredefinedExperimentsCatalog {

	public static AllLearningParameters  getParametersForTrainingBasicMNBHashClassifier(
			String trainingDataName) throws Exception
	{
		// set up all the parameter for learning run
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		fullLearningParams.featureExtractionParams.useCache = true; // if true, will need to set up currentSetup parameters as well
		
		// if to extract ngrams
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 10;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true; // tried with false too - it looks like precision is higher
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToReplace;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
		
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1];
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.No;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		
		fullLearningParams.learnerName = LearnerName.CategoryWekaMNB;
		fullLearningParams.learnerParams = new WekaCategoryClassifierLearnerParams(fullLearningParams.learnerName);
		fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null; 
		fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(1.0, 30); 
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
		fullLearningParams.learnerParams.specificLearnerParams = null;
		fullLearningParams.learnerParams.rebalancingParameters = null;
		
		// Set data for training
		fullLearningParams.trainDataName = trainingDataName;
		
		return fullLearningParams;
	
	}
	
	public static AllLearningParameters getParametersForTrainingBasicPMIHashClassifier(
			String trainingDataName) throws Exception
	{
		// set up all the parameter for learning run
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		fullLearningParams.featureExtractionParams.useCache = false; // if true, will need to set up currentSetup parameters as well
		
		// if to extract ngrams
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 10;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true; // tried with false too - it looks like precision is higher
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToReplace;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
		
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1];
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.No;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		
		fullLearningParams.learnerName = LearnerName.IndependentPMI;
		IndependentLearnerParameters curParams = new IndependentLearnerParameters();
		curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
		curParams.closeCategoryCalculation = CloseCategoryCalculation.Own;
		curParams.binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMI;
		curParams.binaryLearnerParameters.binaryLexiconParams = "-S1-P1-F1"; // "-T0.1 - this should be an equivalent of feature selection option with PMI with the same threshold
		curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
		curParams.useDependentClassifierForOutput = true;
		fullLearningParams.learnerParams = curParams;
		fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null; 
		fullLearningParams.learnerParams.specificLearnerParams = null;
		fullLearningParams.learnerParams.rebalancingParameters = null;
		fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(1.0, 30); 
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
		
		// Set data for training
		fullLearningParams.trainDataName = trainingDataName;
				
		return fullLearningParams;
	
	}
	
	
	public static AllLearningParameters  getParametersForTrainingBasicBWV(
			String trainingDataName) throws Exception
	{
		// set up all the parameter for learning run
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		fullLearningParams.featureExtractionParams.useCache = true; // if true, will need to set up currentSetup parameters as well
		
		// if to extract ngrams
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 10;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true; // tried with false too - it looks like precision is higher
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToReplace;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
		
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1];
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.No;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		fullLearningParams.learnerName = LearnerName.WeightedBalancedVoting;
		
		RebalancingParameters rebalancingParams = new RebalancingParameters(BalanceWeightType.LogPrior);
		
		fullLearningParams.learnerParams = new WeightedBalancedVotingLearnerParams(rebalancingParams, false, false);
		fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
		fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(0.9, 30); 
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
		
		// Set data for training
		fullLearningParams.trainDataName = trainingDataName;
				
		return fullLearningParams;
	}
	
	
	public static void crossValidatingPMIHashClassifier() throws Exception
	{
		AllLearningParameters fullLearningParams = getParametersForTrainingBasicPMIHashClassifier(
				"EmotionHashtagDataLabeled_500000");
		OneLearningProcessHelper.runOverParametersWithCrossValidation(fullLearningParams);
	}

	
	public static void trainPMIHashClassifier() throws Exception
	{
		AllLearningParameters fullLearningParams = getParametersForTrainingBasicPMIHashClassifier(
				"EmotionHashtagDataLabeled_500000");
		
		String testDataName = "Test_EmotionHashtagDataLabeled_General";
		
		OneLearningProcessHelper.toPrintResultClassifier = true;
		OneLearningProcessHelper.toPrintWithClassifiersRunNum = true;
		OneLearningProcessHelper.runOverParametersWithGivenTrainTest(fullLearningParams, testDataName);
		// the trained classifier will be saved in 'output/learnedClassifiers/trained-classifier-<runID>.txt' 
	}
	
}

