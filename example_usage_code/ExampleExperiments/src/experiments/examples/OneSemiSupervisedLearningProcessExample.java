/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import learners.given.ClassifierLearnerWeightedBalancedVoting;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learners.given.ClassifierLearnerWeightedVoting;
import learning.experiments.OneSemiSupervisedLearningProcessHelper;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.DefaultAllLearningParameters;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.IndependentLearnerParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.RebalancingParameters;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import semisupervisedlearning.parameters.SSLFrameworkParameters;
import experimentsavers.ExperimentsResultsSaver;

public class OneSemiSupervisedLearningProcessExample {

	public static void runExample() throws Exception
	{
		ExperimentsResultsSaver.currentTestName = "OneSemiSupervisedLearningProcessExample";
		
		AllSemiSupervisedLearningParameters fullLearningParams = getExampleParameters(
				"ExampleCompositeU+NData", LearnerName.WeightedBalancedVoting, "GALC-R-Inst");
		
		String testDataName = "ExampleTestData";
		OneSemiSupervisedLearningProcessHelper.runOverGivenTest(fullLearningParams, testDataName);
	}
	
	/**
	 * 
	 * @param unlabeledDataName
	 * @param learnerName
	 * @param initialClassifierName Should be the name of classifier from UsedClassifierFactory
	 * @return
	 * @throws Exception
	 */
	public static AllSemiSupervisedLearningParameters getExampleParameters(
			String unlabeledDataName, LearnerName learnerName, String initialClassifierName) throws Exception
	{
		// set up all the parameter for learning run
		AllSemiSupervisedLearningParameters fullLearningParams = new AllSemiSupervisedLearningParameters();
		
		// feature extraction process
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		fullLearningParams.featureExtractionParams.useCache = true; // if true, will need to set up currentSetup parameters as well
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 5;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true;
		
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToReplace;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
	

		// if to use the given list
		/*
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.GivenList;
		fullLearningParams.featureExtractionParams.givenFeatureListsNames = "some name, e.g. 'emojis'"; // The name of the given ngrams list, should be present in GivenFeaturesIndex. To add the file with the feature list, run GivenFeaturesIndex.addNewFeatureList(String featureListName, String fileWithFeatureEnumeration)
		*/
		
		// feature selection process
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1]; // the number in brackets depends on how many feature selection algorithms is to be applied
			
		/*
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.PMI;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.3;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		fullLearningParams.featureSelectionParams.selectionTypesToApply[1] = SelectionType.OnEmotionality;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.selectionAlg = FeatureSelectionAlgOption.PMI;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.includeNeutral = false;
		fullLearningParams.featureSelectionParams.emotionalitySelectionParams.useOnlyPositive = true;
		 */
		
		
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnOccurrence;
		fullLearningParams.featureSelectionParams.minOccurrenceThreshold = 5;
		
		// learner type and parameters
		
		fullLearningParams.learnerName = learnerName;
		switch (learnerName) {
			case WeightedBalancedVoting: {
				RebalancingParameters rebalancingParams = new RebalancingParameters(BalanceWeightType.LogPrior);
				
				fullLearningParams.learnerParams = new ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams(rebalancingParams, false, false);
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				
				break;
			}
			
			case WeightedVoting: {
				fullLearningParams.learnerParams = new ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams(false, false);
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
			}
			
			case CategoryWekaMNB: case CategoryWekaLogReg: {
				fullLearningParams.learnerParams = new WekaCategoryClassifierLearnerParams(fullLearningParams.learnerName);
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = new MultiLabelRefineParams(0.7, 30); 
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(1.0, 30); 
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				
				break;
			}
			
			case IndependentPMI: {
				IndependentLearnerParameters curParams = new IndependentLearnerParameters();
				curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
				curParams.binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMISimple;//PMI
				curParams.binaryLearnerParameters.binaryLexiconParams = "-S1"; // "-T0.1 - this should be an equivalent of feature selection option with PMI with the same threshold
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
				fullLearningParams.learnerParams = curParams;
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			case IndependentWekaLogReg: {
				IndependentLearnerParameters curParams = new IndependentLearnerParameters(fullLearningParams.learnerName);
			    curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
				curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
				fullLearningParams.learnerParams = curParams;
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(true, 0.7);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			case IndependentWekaMNB: {
				 IndependentLearnerParameters curParams = new IndependentLearnerParameters(fullLearningParams.learnerName);
				    curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
					curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.PMI;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.applySmoothing = true;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.minOccurNum = 5;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.scoreThreshold = 0.1;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.includeNeutral = false;
					fullLearningParams.learnerParams = curParams;
					fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
					fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(1.0, 30);
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(true, 0.5);
					fullLearningParams.learnerParams.specificLearnerParams = null;
					fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			default:
				fullLearningParams.learnerParams = DefaultAllLearningParameters.getDefaultClassifierLearnerParamsForClassifier(fullLearningParams.learnerName);
				if (fullLearningParams.learnerParams == null)
					throw new Exception("The default parameters for learner " + learnerName + " are not set!");
				break;
		}

		
		fullLearningParams.sslParameters = new SSLFrameworkParameters();
		fullLearningParams.sslParameters.initialClassifierName = initialClassifierName;
		fullLearningParams.sslParameters.initialClassifierApplicationParams = new ClassifierApplicationParameters(new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true));
		
		fullLearningParams.sslParameters.unlabeledDataName = unlabeledDataName;
		
		fullLearningParams.trainDataName = null; // the additional training data can be added. Then the new classifier will be also learning over those in addition to the pseudo-labeled data created by applying initial classifier over the unlabeled data.
		
		return fullLearningParams;
	}
}
