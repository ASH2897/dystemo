/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import learning.experiments.TestSavedClassifiersProcessHelper;
import experimentsavers.ExperimentsResultsSaver;

public class TestSavedClassifiersProcessExample {

	public static void runExample() throws Exception
	{
		ExperimentsResultsSaver.currentTestName = "RetestSavedClassifiersExample";
		ExperimentsResultsSaver.toSaveDetailedClassifierResults = true; // in order to save the detailed output of the classifiers.  
		
		String[] testDataNames = new String[] {
				"ExampleTestData",
				"ExampleTestData2"
		};
		
		for (String testDataName : testDataNames)
		{
			runAllLexiconBasedClassifiers(testDataName);
		}
	}
	
	
	public static void runAllLexiconBasedClassifiers(String testDataName) throws Exception {
		// previously saved classifier runs
		int[] lexiconBasedClassifiers = {
				62839,62834,62844,62840,62869,62845,
				62850,62851,62852,62864,62865,62867,
				62869
				}; 
		
		String filenamePattern = "output/trainedclassifiers/trained-classifier-ssl<int>.txt";
		
		TestSavedClassifiersProcessHelper.runAllSavedClassifiersOverTest(
				testDataName, lexiconBasedClassifiers, filenamePattern, 
				"Lexicon", false);
	}
}
