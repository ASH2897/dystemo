/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import classifiers.initialization.UsedClassifierFactory;
import learning.experiments.OneGivenClassifierProcessHelper;
import experimentsavers.ExperimentsResultsSaver;

public class OneGivenClassifierProcessExample {

	public static void runExample1() throws Exception
	{
		
		ExperimentsResultsSaver.currentTestName =  "TestGivenClassifiers1";
		String[] testDataNames = new String[] {
				"ExampleTestData",
				"ExampleTestData2"
		};
		
		for (String testDataName : testDataNames)
		{
			runStandardClassifiersOverTest(testDataName);
		} 
	}
	
	public static void runExample2() throws Exception
	{
		
		ExperimentsResultsSaver.currentTestName =  "TestGivenClassifiers2";
		String[] testDataNames = new String[] {
				"ExampleTestData",
				"ExampleTestData2"
		};
		String classifierName = "GALC-R-Inst";
		
		for (String testDataName : testDataNames)
		{
			OneGivenClassifierProcessHelper.runOverTest(testDataName, classifierName);
		} 
	}
	
	/**
	 * The classifiers will be tested with the default classification application parameters, 
	  as specified by ClassifierApplicationParameters getDefaultClassifierApplicationParams()
				
	 * @param testDataNameInIndex
	 * @throws Exception
	 */
	public static void runStandardClassifiersOverTest(String testDataNameInIndex) throws Exception {
		String[] standardClassifiers = UsedClassifierFactory.standardUsedClassifiers;
		for (String classifierName : standardClassifiers)
			OneGivenClassifierProcessHelper.runOverTest(testDataNameInIndex, classifierName);
	}
}
