/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import data.categories.CategoryProcessor.CategoryDataType;
import learning.experiments.SignificanceTestsHelper;

public class SignificanceTestsExample {

	public static void runExample() throws Exception
	{
		String testDataName = "ExampleTestData";
		
		String filenamePattern = "output/evaluatedLexiconsOutput/lexoutput-<int>-testdataname.txt";
		
		int[][] runComparisons = new int[][]{
				{30107,30115},{30108,30115},{30109,30115}, // for GALC
				{30110,30116},{30112,30116},{30113,30116},  // for OlympLex
				{30103,30114},{30104,30114},{30105,30114}, // for PMI-Hash
		};
		
		SignificanceTestsHelper.doSignificanceTests(testDataName, filenamePattern, runComparisons, CategoryDataType.GEW);
	}
	
	public static void runExample2() throws Exception
	{
		String testDataName = "ExampleTestData";
		
		String filenamePattern = "output/evaluatedLexiconsOutput/lexoutput-<int>-testdataname.txt";
		
		SignificanceTestsHelper.doSignificanceTests(testDataName, filenamePattern, 30115, new int[]{30107, 30108, 30109}, CategoryDataType.GEW);
	}
}
