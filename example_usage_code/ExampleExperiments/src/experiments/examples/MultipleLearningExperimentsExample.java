/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utility.Pair;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.experiments.MultipleLearningExperimentsHelper;
import learning.experiments.MultipleLearningExperimentsHelper.AllLearningParametersEnumerationList;
import learning.parameters.AllLearningParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.RebalancingParameters;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerParametersEnumerationList;
import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import learning.parameters.ClassifierApplicationParameters.ClassifierApplicationParamsEnumerationList;
import learning.parameters.ClassifierLearnerParams.ClassifierLearnerParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.DataNgramExtractionParamsEnumerationList;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams.DataNgramExtractionParamsEnumerationList.ExtraParamCombinationForNgramExtraction;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.FeatureSelectionParamsEnumerationList;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams.BinarySelectionParamsEnumerationList.ExtraParamCombinationForBinaryFeatureSelection;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.IndependentLearnerParameters.IndependentLearnerParamsEnumerationList;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import linguistic.TermDetectionParameters;

public class MultipleLearningExperimentsExample {

	/**
	 * Here is an example of how to launch multiple learning experiments. 
	 * It will not be run properly unless the mentioned data are provided in the default index.
	 * @throws Exception
	 */
	public static void runExample() throws Exception
	{
		AllLearningParametersEnumerationList allParamsEnumList =  generateDifferentLearningParametersExample(
				"ExampleMultipleLearningExperiment", "ExampleTrainingData");
		List<AllLearningParameters> diffLearningParams = MultipleLearningExperimentsHelper.getAllLearningParametersEnumerationFromGivenInput(allParamsEnumList);

		String testDataName = "ExampleTestData";
		MultipleLearningExperimentsHelper.runOverMultipleParametersLearnAndTestWithMultipleApplicationParameters(
				diffLearningParams, testDataName);
	}
	
	public static AllLearningParametersEnumerationList generateDifferentLearningParametersExample(
			String experimentName, String trainDataName)
	{
		AllLearningParametersEnumerationList allLearningParametersEnumerationList = new AllLearningParametersEnumerationList();
	
		allLearningParametersEnumerationList.experimentsName = experimentName;
		
		allLearningParametersEnumerationList.trainDataName = trainDataName;
		
		allLearningParametersEnumerationList.useCache = false; // because not impelented well for cross-valdation
		
		// different feature extraction parameters
		allLearningParametersEnumerationList.usedFeatureType = FeatureType.Ngrams;
		int[] diffTermOccNumList = {5};//, 10, 15
		int[] diffMaxNgramLengthList = {1, 2};//1, 2, 3, 4, 5
		ExtraParamCombinationForNgramExtraction[] extraParamsForNgramExtraction = {
				new ExtraParamCombinationForNgramExtraction(true,  new TermDetectionParameters(true,true), false)
		};
		
		DataNgramExtractionParamsEnumerationList dataNgramExtractionParamsList = new DataNgramExtractionParamsEnumerationList(
				diffTermOccNumList, diffMaxNgramLengthList, extraParamsForNgramExtraction);
		allLearningParametersEnumerationList.featureExtractionParamsEnumerationList =  dataNgramExtractionParamsList;
		

		// different feature selection parameters
		Map<SelectionType, FeatureSelectionParamsEnumerationList> featureSelectionParamsEnumerationListMap = new HashMap<SelectionType, FeatureSelectionParamsEnumerationList> ();
		FeatureSelectionAlgOption[] selectionAlgsOptions = {FeatureSelectionAlgOption.No, FeatureSelectionAlgOption.PMI};
		double[] thresholdsOptions = {0.1, 0.3, 0.5, 0.7, 0.9};
		ExtraParamCombinationForBinaryFeatureSelection[] extraOptions = {
				new ExtraParamCombinationForBinaryFeatureSelection(5, false, true, false, false, true)
		};
	
		BinarySelectionParamsEnumerationList polarityFeatureSelectionParamsOptions = new  BinarySelectionParamsEnumerationList(selectionAlgsOptions, thresholdsOptions, extraOptions);
		featureSelectionParamsEnumerationListMap.put(SelectionType.OnPolarity, polarityFeatureSelectionParamsOptions);
		
		allLearningParametersEnumerationList.featureSelectionParamsEnumerationListMap = featureSelectionParamsEnumerationListMap;
		allLearningParametersEnumerationList.usedFeatureSelectionTypes = new ArrayList<SelectionType[]>();
		allLearningParametersEnumerationList.usedFeatureSelectionTypes.add(new SelectionType[]{SelectionType.OnPolarity});
		
		
		
		// different learners
		allLearningParametersEnumerationList.learnerNames = new LearnerName[] {
				
				LearnerName.WeightedBalancedVoting, 
				LearnerName.CategoryWekaMNB,
				LearnerName.CategoryWekaLogReg,
				
				LearnerName.IndependentPMI,
				LearnerName.IndependentWekaLogReg, 
				LearnerName.IndependentWekaMNB
				};
		
		// different parameters enumeration for different classifier learners
		Map<LearnerName, ClassifierLearnerParamsEnumerationList> classifierLearnersParametersEnumeration = new HashMap<LearnerName, ClassifierLearnerParamsEnumerationList>();
		Map<LearnerName, ClassifierApplicationParamsEnumerationList> learnersApplicationParametersEnumeration = new HashMap<LearnerName, ClassifierApplicationParamsEnumerationList> ();
		
		ClassifierApplicationParamsEnumerationList classifierApplicationParamsEnumerationGeneral = new ClassifierApplicationParamsEnumerationList();
		classifierApplicationParamsEnumerationGeneral.outputMultiLabelOptions = new MultiLabelRefineParams[]{ 
				new MultiLabelRefineParams(1.0, 30),
				new MultiLabelRefineParams(0.9, 30),
				new MultiLabelRefineParams(0.7, 30)
				};
		classifierApplicationParamsEnumerationGeneral.hierarchyOptions = new HierarchyApplyOption[]{HierarchyApplyOption.None};
		classifierApplicationParamsEnumerationGeneral.termDetectionOptions =  new TermDetectionParameters[] {null}; // will be set based on the parameters for ngram extraction
		classifierApplicationParamsEnumerationGeneral.probabilisticOutputTreatmentOptions = new ProbabilisticOutputTreatment[]{null}; 
		
		
		ClassifierLearnerParamsEnumerationList learnerParamsEnumerationGeneral = new ClassifierLearnerParamsEnumerationList();
		learnerParamsEnumerationGeneral.initialMultiLabelOptions = new MultiLabelRefineParams[]{
				null, new MultiLabelRefineParams(0.9, 30) 
				};
		learnerParamsEnumerationGeneral.rebalancingOptions = new RebalancingParameters[]{ null };
		learnerParamsEnumerationGeneral.extraParameters = new String[] { null };
		
		for (LearnerName catLearnerName : new LearnerName[]{LearnerName.CategoryWekaMNB, LearnerName.CategoryWekaLogReg, LearnerName.CategoryWekaSVM})
		{
			learnersApplicationParametersEnumeration.put(catLearnerName, classifierApplicationParamsEnumerationGeneral);
			classifierLearnersParametersEnumeration.put(catLearnerName, learnerParamsEnumerationGeneral);
			
		}
		
		ClassifierApplicationParamsEnumerationList classifierApplicationParamsEnumerationIndependent = new ClassifierApplicationParamsEnumerationList();
		classifierApplicationParamsEnumerationIndependent.outputMultiLabelOptions = new MultiLabelRefineParams[]{ 
				null
				};
		classifierApplicationParamsEnumerationIndependent.hierarchyOptions = new HierarchyApplyOption[]{HierarchyApplyOption.None};
		classifierApplicationParamsEnumerationIndependent.termDetectionOptions =  new TermDetectionParameters[] {null}; // will be set based on the parameters for ngram extraction
		classifierApplicationParamsEnumerationIndependent.probabilisticOutputTreatmentOptions = new ProbabilisticOutputTreatment[]{null}; 
		
		
		ClassifierApplicationParamsEnumerationList classifierApplicationParamsEnumerationIndependent2 = new ClassifierApplicationParamsEnumerationList();
		classifierApplicationParamsEnumerationIndependent2.outputMultiLabelOptions = new MultiLabelRefineParams[]{ 
				null
				};
		classifierApplicationParamsEnumerationIndependent2.hierarchyOptions = new HierarchyApplyOption[]{HierarchyApplyOption.None};
		classifierApplicationParamsEnumerationIndependent2.termDetectionOptions =  new TermDetectionParameters[] {null}; // will be set based on the parameters for ngram extraction
		classifierApplicationParamsEnumerationIndependent2.probabilisticOutputTreatmentOptions = new ProbabilisticOutputTreatment[]{
				null,
				new ProbabilisticOutputTreatment(true, 0.9), new ProbabilisticOutputTreatment(true, 0.7)
				}; 
		
		
		// set parameters for independent classifiers
		IndependentLearnerParamsEnumerationList learnerParamsEnumerationIndependent = new IndependentLearnerParamsEnumerationList(learnerParamsEnumerationGeneral);
		learnerParamsEnumerationIndependent.useCacheForIndependentRepresentation = false;
		learnerParamsEnumerationIndependent.closeCategoryTreatmentOptions = new Pair[]{
				new Pair(CloseCategoryUseType.IncludeOut, CloseCategoryCalculation.Own)	
			};
		
		learnerParamsEnumerationIndependent.binaryLearnerParametersEnumerationList = new BinaryLearnerParametersEnumerationList();
		FeatureSelectionAlgOption[] selectionAlgsOptionsForBinary = {
				FeatureSelectionAlgOption.No
				};
		double[] indSelThresholdsOptions = { 0.1, 0.3, 0.5, 0.7, 0.9};
		BinarySelectionParamsEnumerationList binaryFeatureSelectionParamsOptionsGeneral = new  BinarySelectionParamsEnumerationList(selectionAlgsOptionsForBinary, indSelThresholdsOptions, extraOptions);
		learnerParamsEnumerationIndependent.binaryLearnerParametersEnumerationList.binarySelectionParamsEnumerationList = binaryFeatureSelectionParamsOptionsGeneral;
		learnerParamsEnumerationIndependent.binaryLearnerParametersEnumerationList.differentAdditionalBinaryLearnerOptions = new String[]{""};
		
		
		classifierLearnersParametersEnumeration.put(LearnerName.IndependentWekaSVM, learnerParamsEnumerationIndependent);
		learnersApplicationParametersEnumeration.put(LearnerName.IndependentWekaSVM, classifierApplicationParamsEnumerationIndependent);
		
		
		// add different variants of the probabilistic output for logistic regression
		ProbabilisticOutputTreatment[] probOutputTreatmentOptions = new ProbabilisticOutputTreatment[]{
				null,
				new ProbabilisticOutputTreatment(true, 0.9), new ProbabilisticOutputTreatment(true, 0.7)
				};
		IndependentLearnerParamsEnumerationList learnerParamsEnumerationIndependentWithMoreProbOptions = new IndependentLearnerParamsEnumerationList( learnerParamsEnumerationIndependent);
		//learnerParamsEnumerationIndependentWithMoreProbOptions.binaryLearnerParametersEnumerationList.probabilisticOutputTreatmentOptions = probOutputTreatmentOptions;
		
		classifierLearnersParametersEnumeration.put(LearnerName.IndependentWekaLogReg, learnerParamsEnumerationIndependentWithMoreProbOptions);
		learnersApplicationParametersEnumeration.put(LearnerName.IndependentWekaLogReg, classifierApplicationParamsEnumerationIndependent2);
		
		ProbabilisticOutputTreatment[] probOutputTreatmentOptions2 = new ProbabilisticOutputTreatment[]{
				null 
				//new ProbabilisticOutputTreatment(true, 0.9), new ProbabilisticOutputTreatment(true, 0.7)
				 };
		IndependentLearnerParamsEnumerationList learnerParamsEnumerationIndependentWithMoreProbOptions2 = new IndependentLearnerParamsEnumerationList( learnerParamsEnumerationIndependent);
		//learnerParamsEnumerationIndependentWithMoreProbOptions2.binaryLearnerParametersEnumerationList.probabilisticOutputTreatmentOptions = probOutputTreatmentOptions2;
		
		classifierLearnersParametersEnumeration.put(LearnerName.IndependentWekaMNB, learnerParamsEnumerationIndependentWithMoreProbOptions2);
		learnersApplicationParametersEnumeration.put(LearnerName.IndependentWekaMNB, classifierApplicationParamsEnumerationIndependent2);
		
		
		IndependentLearnerParamsEnumerationList learnerParamsEnumerationIndependentForPMI = new IndependentLearnerParamsEnumerationList( learnerParamsEnumerationIndependent);
		String[] independentPMIExtraParams = {"-S1", "-T0.1-S1", "-T0.3-S1", "-T0.5-S1", "-T0.7-S1", "-T0.9-S1"};
		learnerParamsEnumerationIndependentForPMI.binaryLearnerParametersEnumerationList.differentAdditionalBinaryLearnerOptions = independentPMIExtraParams;
		BinarySelectionParamsEnumerationList emptyFeatureSelectionOptions = new  BinarySelectionParamsEnumerationList(new FeatureSelectionAlgOption[]{FeatureSelectionAlgOption.No}, null, null);
		learnerParamsEnumerationIndependentForPMI.binaryLearnerParametersEnumerationList.binarySelectionParamsEnumerationList = emptyFeatureSelectionOptions;
		
		classifierLearnersParametersEnumeration.put(LearnerName.IndependentPMI, learnerParamsEnumerationIndependentForPMI);
		learnersApplicationParametersEnumeration.put(LearnerName.IndependentPMI, classifierApplicationParamsEnumerationIndependent);
		
		
		RebalancingParameters[] rebalancingOptionsForBalancedWeighedVoting = new RebalancingParameters[]{ new RebalancingParameters(BalanceWeightType.LogPrior)};
		ClassifierLearnerParamsEnumerationList learnerParamsEnumerationForBWV = new ClassifierLearnerParamsEnumerationList(learnerParamsEnumerationGeneral);
		learnerParamsEnumerationForBWV.rebalancingOptions =  rebalancingOptionsForBalancedWeighedVoting;
		
		classifierLearnersParametersEnumeration.put(LearnerName.WeightedBalancedVoting, learnerParamsEnumerationForBWV);
		learnersApplicationParametersEnumeration.put(LearnerName.WeightedBalancedVoting, classifierApplicationParamsEnumerationGeneral);
		
		
		allLearningParametersEnumerationList.classifierLearnersParametersEnumeration = classifierLearnersParametersEnumeration;
		
		allLearningParametersEnumerationList.learnersApplicationParametersEnumeration = learnersApplicationParametersEnumeration;
		
		return allLearningParametersEnumerationList;
	}
}
