package main;

import java.util.HashSet;
import java.util.List;

import cleaning.DatasetCleaningFromDuplicates;
import cleaning.DatasetCleaningFromLinks;
import learners.featureextraction.LinguisticLexiconRefinement;
import learning.experiments.RandomExperimentsHelper;
import parameters.EmotionRecognitionParameters;
import preprocessing.DatasetPreprocessing;
import data.DataRepositoryIndex;
import data.DatabaseConnectionsIndex;
import data.DatabaseWrapper;
import data.categories.CategoryProcessor.CategoryDataType;
import emotionclassifiers.given.EmotionLexiconsGivenFactory;
import experiments.examples.MultipleSemiSupervisedExperimentsExample;
import experiments.examples.OneGivenClassifierProcessExample;
import experiments.examples.SignificanceTestsExample;
import experiments.examples.TestClassifiersWithBestParametersExample;
import experiments.examples.TryInitialClassifiers;
import experimentsavers.EnvSettings;
import experimentsavers.EvaluationSaving;
import experimentsavers.ExperimentsResultsSaver;
import extraction.DataRepositoryTextSaver;
import extraction.ExtractingPseudoNeutralTweets;
import extraction.ExtractingTweetsWithGivenHashtags;
import functionality.UtilFiles;

public class Loader {
	
	static DatabaseConnectionsIndex givenDatabaseIndex;
	static DataRepositoryIndex givenDataRepositoryIndex;
	
	/**
	 * 
	 * This procedure enumerates the main functionality and the called entry functions to perform them. 
	 * Note that we don't run all those functions at once, as some manual manipulation of data is required in between.
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		TryInitialClassifiers.TryGALCInstantiated();
		
		// Specify the data and emotions
		setupConfiguration();
		 
		// Prepare the data in the database (preprocessing, cleaning, subset extraction, saving to text files, etc.)
		PrepareData();
		
		// Perform the full cycle of distant learning experiments
		RunDistantLearningFullEvaluationCycle();
		
	}
	
	public static void setupConfiguration() throws Exception {
		// set the configuation for database connection (if to be used)
		givenDatabaseIndex = DatabaseConnectionsIndex.loadFromXMLFile(
				"given/database-connections-index-example.xml"); 
		DatabaseConnectionsIndex.setDefaultDatabaseConnectionsIndex(givenDatabaseIndex);
		
		// set the emotion categories used as default.
		EmotionRecognitionParameters.defaultEmotionCategoriesType = CategoryDataType.GEW; 
		
		// set the list of data to be used in the experiments or preparation
		String filePathPrefix = Loader.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		filePathPrefix = filePathPrefix.substring(0, filePathPrefix.lastIndexOf("/bin")); // Alternatively, an absolute path prefix towards this code can be given directly.
		givenDataRepositoryIndex = DataRepositoryIndex.loadFromXMLFile(
				"given/data-repository-index-example.xml", filePathPrefix);
		
		DataRepositoryIndex.setDefaultDataRepositoryIndex(givenDataRepositoryIndex);
		
		DatabaseWrapper.setCurrentConnection("LocalConnection");
		EvaluationSaving.databaseConnectionForSaving = "LocalConnection";
		
		EnvSettings.computerName = "CurrentComputer";
		
		// Set up the domain-specific stop words
		List<String> domainStopWordsForEmotion = UtilFiles.getContentLines("given/domain-spec-stop-words.txt");
		LinguisticLexiconRefinement.addNewEmoStopWords(new HashSet<String>(domainStopWordsForEmotion));
				
	}
	
	/**
	 * Those are only examples of how to call the corresponding functions. 
	 * Some manual data arrangement is done as well in our experiments, e.g. extraction of random subsets
	 * 
	 * GIVEN: Tables with tweets (labeled and unlabeled) in the database. 
	   The connection is given in the provided Data Repository Index. 
	   For the format description, read details/DocsDataFormat.txt 
	 * @throws Exception
	 */
	public static void PrepareData() throws Exception {
		// Prepare the data: 
		// 1. Preprocess the tweets for the future experiments (separate tokens, etc.)
		DatabaseWrapper.setCurrentConnection("Dystemo-example");
		DatasetPreprocessing.preprocessTextInDatabaseTable("tweets_domain_all", "id", "text", "preprocessed_text");
		
		// 2. Detect the tweets with emotional hashtags and pseudo-neutral tweets
		ExtractingTweetsWithGivenHashtags.ExtractHashtagsFromDatabaseTable(
				"tweets_domain_all", "tweets_domain_hashtagged", "id", "preprocessed_text", 
				EmotionLexiconsGivenFactory.getLexiconOfGivenGEWHashtags(), true);
		ExtractingPseudoNeutralTweets.ExtractPseudoNeutralTweetsFromDatabaseTable(
				"tweets_domain_all", "tweets_domain_neutral", "id", "preprocessed_text", true);
		
		// 3. Manual: From all the tweets we can select a random subset for using as unlabeled data. 
		// It is done via SQL scripts. The selected tweets in will example will be stored in tweets_domain_random. 
		// More tweets are placed there than required in order to have the desired amount after data cleaning.
		// More details are given in the file details/DocsRandomSubsetAssignment.txt
		
		// 4. Clean up the data: remove duplicates, tweets with URL, etc.
		// Note that this functions will only detect the corresponding tweets in the databaset table. 
		// Manual: The removal or marking of the tweets as such is done manually by running the corresponding SQL queries to update/remove the detected tweets.
		DatasetCleaningFromDuplicates.cleanDuplicatesInEmotionTweetsTable("tweets_domain_neutral", "preprocessed_text", "id");
		DatasetCleaningFromDuplicates.cleanDuplicatesInEmotionTweetsTable("tweets_domain_hashtagged", "preprocessed_text", "id");
		DatasetCleaningFromDuplicates.cleanDuplicatesInEmotionTweetsTable("tweets_domain_random", "preprocessed_text", "id");
		
		DatasetCleaningFromLinks.findTweetsWithLinks("tweets_domain_random", "preprocessed_text", "id"); // update withLink field manually
		
		
		// 5. Manual: Mark the specific data size for use by assigning tSize values to the random subsets of tweets. 
		// More details are given in the file details/DocsRandomSubsetAssignment.txt
		// Also, mark randomly the development/test tweets.
		
		// 6. Manual: Add specific data into index file with database connection. We added to the previous ones _fromDatabase suffix
		
		// 7. Extract all data into text files for the easier access
		DataRepositoryTextSaver.SaveNeutralTextData("<neutral_data_in_index>", "given/unlabeledrepository/<neutral-data-name>.txt", EmotionRecognitionParameters.defaultEmotionCategoriesType);
		DataRepositoryTextSaver.SaveHashtaggedLabeledDataInTextFile("<hashtagged_data_in_index>", "given/labeledrepository/<hashtagged-data-name>.txt", true, true);
		DataRepositoryTextSaver.SaveTweetDataInTextFile("<unlabeled_data_of_size_X_in_index>", "given/unlabeledrepository/unlabeledData_<domain_size>");
		//examples:
		//DataRepositoryTextSaver.SaveTweetDataInTextFile("Domain-Unlabeled-Normal_fromDatabase", "given/unlabeledrepository/unlabeled-domain-normal_4.txt");
		//DataRepositoryTextSaver.SaveHashtaggedLabeledDataInTextFile("Domain-Labeled-Hashtagged_fromDatabase", "given/labeledrepository/example-labeledData_domain_hashtagged_Dev.txt", true, true);
		//DataRepositoryTextSaver.SaveNeutralTextData("Domain-Unlabeled-Neutral_fromDatabase", "given/unlabeledrepository/unlabeled-domain-neutral.txt", EmotionRecognitionParameters.defaultEmotionCategoriesType);
				
		
		// 8. Manual: Update index file to replace source of working data from database to text files.
		
		// 9. Create tables for saving evaluation results
		EvaluationSaving.createEmptyGeneralEvaluationResultsDatabaseTable();
		EvaluationSaving.createEmptyCategoryEvaluationResultsDatabaseTable();
	}

	
	/**
	 * The steps for running distant supervision experiments similar to ours. 
	 * The called data are assumed to be prepared, stored in text files, and enumerated in the given data repository index.
	 * 
	 * GIVEN: Tweets in the test files (or in database, as specified in the data index). 
	 * We assume to have available: 
	 * - Labeled data for testing: Development and final test sets of tweets
	 * - Unlabeled data for learning: One size for development and one size (larger) for final testing
	 * - Pseudo-neutral data for learning: One size for development and one size (larger) for final testing
	 * 
	 * Note that the enumerated functions are only pointers to the related examples and should be updated with data pointers, desired parameters, etc.
	 * @throws Exception 
	 */
	public static void RunDistantLearningFullEvaluationCycle() throws Exception {
		
		// 1. Run the training and evaluation using development datasets, for different initial classifiers, different learning algorithms, and different parameters.
		// Save the performance results in the database.
		EvaluationSaving.databaseConnectionForSaving = "Dystemo-example";
		MultipleSemiSupervisedExperimentsExample.runExample(); // this is only the pointer for an example of how to run multiple experiments
		
		// 2. Detect parameters that are optimal based on some metric. Save them into file.
		TestClassifiersWithBestParametersExample.runExample_saveBestParametersIntoFile();
		
		// 3. Evaluate the performance of the classifiers learned using the found best parameters on the test data, 
		// while training with larger dataset. Also, save in parallel the obtained classifiers learned on larger data.
		ExperimentsResultsSaver.toSaveDetailedClassifierResults = true; // in order to save the detailed output of the classifiers. Alternatively, you can get those results later by loading the saved classifiers (however, the classifier application parameters should be set individually).
		TestClassifiersWithBestParametersExample.runExample_testClassifiersWithBestParametersOnBiggerSizeFromFile(true);
		
		// 4. Evaluate the performance of the random classifier on the same test data
		String testDataNameInIndex = "ExampleTestData";
		RandomExperimentsHelper.run(1000, testDataNameInIndex, "RandomEvaluation", EmotionRecognitionParameters.defaultEmotionCategoriesType);

		// 5. Evaluate the performance of the given initial classifiers
		ExperimentsResultsSaver.toSaveDetailedClassifierResults = true; // in order to save the output from the given classifiers
		OneGivenClassifierProcessExample.runExample1();
		
		// 6. Run statistical significance tests for comparing the perfomance
		// 6.1. Save the detailed output results from the compared classifiers, if not saved before by setting ExperimentsResultsSaver.toSaveDetailedClassifierResults = true
		// If the detailed output results were not saved before, you can run something similar to 
		//   TestSavedClassifiersProcessExample.runExample()
		// 6.2. Run randomization tests
		SignificanceTestsExample.runExample();
	}
}
