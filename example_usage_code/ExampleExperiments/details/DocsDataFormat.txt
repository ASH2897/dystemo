The input tweet data can be stored either in text files or in database. 
They can have either only the information about the tweet ids and text (TweetData format), 
or additionally the emotion categories (as ids of the categories) assigned to the tweets (LabeledData format). 

They can come from three sources:

-- Source 1: Database.

+For TweetData format, the tweets' table in the database is expected to have the following columns: 
id of a tweet and its text. Additional columns can be present and it is possible to set conditions on them 
at the selection stage.

Example create statement for TweetData format (this table was used to store the randomly selected, cleaned tweets about the Olympic games):
CREATE TABLE `olympic_tweets_random` (
  `id` int(11) NOT NULL DEFAULT '0',
  `text` text,
  `preprocessed_text` varchar(1000) DEFAULT NULL,
  `tSize` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


+For LabeledData format, the tweets' table should additionally contain a column with assigned emotion category ids to a tweet. 
If several emotions are assigned, they are enumerated within one entry as separated by comma (without spaces). 
The same tweet can appear several times (e.g. to represent different appeared hashtags): 
in this case the information on the emotion labels will be joined during loading of LabeledData. 

Example create statement for LabeledData format (this table was used to store the tweets with emotional hashtags):
CREATE TABLE `tweets_general_hashtagged` (
  `id` bigint(20) NOT NULL,
  `text` text NOT NULL,
  `refined_text` varchar(1000) DEFAULT NULL,
  `hashtag` varchar(100) DEFAULT NULL,
  `categoryId` varchar(100) DEFAULT NULL,
  `endPosition` bit(1) DEFAULT NULL,
  `severalGivenHashtags` bit(1) DEFAULT NULL,
  `forTest` bit(1) DEFAULT b'0',
  `tSize` int(11) DEFAULT NULL,
  `realWordNum` int(11) DEFAULT NULL,
  UNIQUE KEY `index1` (`id`,`hashtag`),
  KEY `index2` (`forTest`),
  KEY `index3` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


We also use additional column, usually named `tSize` to record the pre-saved random subsets of tweets of a specific size. 
For example, we set tSize for 100000 random tweets in the database to 100000, and for 400000 other random tweets to 500000. 
This allows us to run queries specifying tSize <= 500000 to extract the subset of 500000 tweets. 
Read DocsRandomSubsetAssignment.txt for more details.


-- Source 2: A text file.
We generally store in the text files the dataset of a fixed size and with the preprocessed tweet text 
to avoid re-querying of the database for repeated data. 
Storing the data in text file implies that more fixed format of the data. 
In all the formats below, each line of the file contains information on one tweet with the fields/columns separated by a tab.

+TweetData format:
<tweet_id>	<tweet_text>

Example line:
45606221	aww ! #katietaylor is crying . #brayhomecoming #london<year>


+LabeledData format:
<tweet_id>	<tweet_text>	<emotion_categories_ids_separated_by_comma>

Example line:
12488348	the tension of the #gymnastics appeal by japan is immense . either way <username> did us proud #ourgreatestteam - but i 'm holding my breathe	3,7


-- Source 3: Merged (or several sources)
This type of source allows to load several datasets as one joined. 
Each of the component datasets should come either from a database or text file.

