# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.9)
# Database: dystemo_example_data
# Generation Time: 2016-10-18 15:40:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table evaluation_results_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `evaluation_results_category`;

CREATE TABLE `evaluation_results_category` (
  `TestName` text,
  `RunComp` text,
  `RunId` int(11) DEFAULT NULL,
  `TestFileName` text,
  `CategoryType` text,
  `IterNum` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `objectsFoundNum` int(11) DEFAULT NULL,
  `objectsGivenNum` int(11) DEFAULT NULL,
  `objectsCorrectNum` int(11) DEFAULT NULL,
  `precision` double DEFAULT NULL,
  `recall` double DEFAULT NULL,
  `f1Score` double DEFAULT NULL,
  `aucRoc` double DEFAULT NULL,
  `aucPR` double DEFAULT NULL,
  KEY `order` (`RunId`,`RunComp`(100),`TestFileName`(100),`CategoryType`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table evaluation_results_full
# ------------------------------------------------------------

DROP TABLE IF EXISTS `evaluation_results_full`;

CREATE TABLE `evaluation_results_full` (
  `TestName` text,
  `RunComp` text,
  `RunId` int(11) DEFAULT NULL,
  `TestFileName` text,
  `trainDataName` text,
  `featureExtractionParams` text,
  `featureSelectionParams` text,
  `LearnerName` text,
  `ClassifierLearnerParams` text,
  `sslFullParams` text,
  `sslUnlabeledDataName` text,
  `sslInitialClassifier` text,
  `givenClassifierName` text,
  `givenClassifierApplicationParams` text,
  `IterNum` int(11) DEFAULT NULL,
  `LexSize` int(11) DEFAULT NULL,
  `evaluatedObjectsNum` int(11) DEFAULT NULL,
  `evaluatedAssignNum` int(11) DEFAULT NULL,
  `foundEmotionPercentage` double DEFAULT NULL,
  `givenEmotionPercentage` double DEFAULT NULL,
  `averageFoundEmotionsWithoutNeutral` double DEFAULT NULL,
  `emotionPrecisionOnOne` double DEFAULT NULL,
  `emotionPrecisionOnOneWithoutNeutral` double DEFAULT NULL,
  `emotionPrecisionOnAll` double DEFAULT NULL,
  `emotionPrecisionOnAllWithoutNeutral` double DEFAULT NULL,
  `emotionRecallWithSet` double DEFAULT NULL,
  `emotionRecallWithList` double DEFAULT NULL,
  `emotionAccuracy` double DEFAULT NULL,
  `emotionStrengthAccuracy` double DEFAULT NULL,
  `emotionF1scoreOnAll` double DEFAULT NULL,
  `emotionMacroF1Score` double DEFAULT NULL,
  `emotionMacroPrecision` double DEFAULT NULL,
  `emotionMacroRecall` double DEFAULT NULL,
  `emotionMacroF1scoreWithNeutral` double DEFAULT NULL,
  `emotionMacroPrecisionWithNeutral` double DEFAULT NULL,
  `emotionMacroRecallWithNeutral` double DEFAULT NULL,
  `emotionGeomF1Score` double DEFAULT NULL,
  `emotionGeomPrecision` double DEFAULT NULL,
  `emotionGeomRecall` double DEFAULT NULL,
  `emotionGeomF1ScoreWithNeutral` double DEFAULT NULL,
  `emotionGeomPrecisionWithNeutral` double DEFAULT NULL,
  `emotionGeomRecallWithNeutral` double DEFAULT NULL,
  `emotionAvgAUCROC` double DEFAULT NULL,
  `emotionAvgAUCPR` double DEFAULT NULL,
  `polarityAccuracy` double DEFAULT NULL,
  `polarityRecall` double DEFAULT NULL,
  `polarityStrictRecall` double DEFAULT NULL,
  `polarityPrecision` double DEFAULT NULL,
  `polarityF1score` double DEFAULT NULL,
  `nonNeutralPolarityPercentage` double DEFAULT NULL,
  `definitePolarityPercentage` double DEFAULT NULL,
  `polarityMacroF1score` double DEFAULT NULL,
  `polarityMacroPrecision` double DEFAULT NULL,
  `polarityMacroRecall` double DEFAULT NULL,
  `quadrantAccuracy` double DEFAULT NULL,
  `quadrantRecall` double DEFAULT NULL,
  `quadrantStrictRecall` double DEFAULT NULL,
  `quadrantPrecision` double DEFAULT NULL,
  `quadrantF1score` double DEFAULT NULL,
  `quadrantMacroF1score` double DEFAULT NULL,
  `quadrantMacroPrecision` double DEFAULT NULL,
  `quadrantMacroRecall` double DEFAULT NULL,
  `definiteQuadrantPercentage` double DEFAULT NULL,
  `nonNeutralQuadrantPercentage` double DEFAULT NULL,
  `emotionMicroF1Score` double DEFAULT NULL,
  `emotionMicroPrecision` double DEFAULT NULL,
  `emotionMicroRecall` double DEFAULT NULL,
  `confusionMatrix` text,
  `geomGenProd` double DEFAULT NULL,
  `emotionf05Score` double DEFAULT NULL,
  `computeDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `order` (`RunId`,`RunComp`(100),`TestFileName`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tweets_domain_all
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tweets_domain_all`;

CREATE TABLE `tweets_domain_all` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `preprocessed_text` varchar(1000) DEFAULT NULL,
  `tSize` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tweets_domain_all` WRITE;
/*!40000 ALTER TABLE `tweets_domain_all` DISABLE KEYS */;

INSERT INTO `tweets_domain_all` (`id`, `text`, `preprocessed_text`, `tSize`)
VALUES
	(1,'funny text for test #amusement #awe','funny text for test #amusement #awe',NULL),
	(2,'test TEST test!!!:)','test test test !* <emot51>',NULL),
	(3,'Today is good','today is good',NULL),
	(4,'I am not angry at you','i am not angry at you',NULL),
	(5,'text of this tweet is just a test','text of this tweet is just a test',NULL),
	(6,'yay, #great sunny days again!!! #happy','yay , #great sunny days again !* #happy',NULL),
	(7,'#love how it\'s done','#love how it \'s done',NULL),
	(8,'good luck today! you are the best','good luck today ! you are the best',NULL),
	(9,'simple news tweet http://bbc.com','simple news tweet <link>',NULL),
	(10,'another news tweet http://cnn.com','another news tweet <link>',NULL),
	(11,'these posts are amazing http://smth.com','these posts are amazing <link>',NULL);

/*!40000 ALTER TABLE `tweets_domain_all` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tweets_domain_hashtagged
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tweets_domain_hashtagged`;

CREATE TABLE `tweets_domain_hashtagged` (
  `id` bigint(20) NOT NULL,
  `preprocessed_text` varchar(1000) NOT NULL DEFAULT '',
  `refined_text` varchar(1000) DEFAULT NULL,
  `hashtag` varchar(100) DEFAULT NULL,
  `categoryId` varchar(100) DEFAULT NULL,
  `endPosition` bit(1) DEFAULT NULL,
  `severalGivenHashtags` bit(1) DEFAULT NULL,
  `forTest` bit(1) DEFAULT b'0',
  `tSize` int(11) DEFAULT NULL,
  `realWordNum` int(11) DEFAULT NULL,
  UNIQUE KEY `index1` (`id`,`hashtag`),
  KEY `index2` (`forTest`),
  KEY `index3` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tweets_domain_hashtagged` WRITE;
/*!40000 ALTER TABLE `tweets_domain_hashtagged` DISABLE KEYS */;

INSERT INTO `tweets_domain_hashtagged` (`id`, `preprocessed_text`, `refined_text`, `hashtag`, `categoryId`, `endPosition`, `severalGivenHashtags`, `forTest`, `tSize`, `realWordNum`)
VALUES
	(1,'funny text for test #amusement #awe','funny text for test','awe','7',00000001,00000001,00000000,NULL,3),
	(1,'funny text for test #amusement #awe','funny text for test','amusement','2',00000001,00000001,00000000,NULL,3),
	(6,'yay , #great sunny days again !* #happy','yay , #great sunny days again !*','happy','4',00000001,00000000,00000000,NULL,3),
	(7,'#love how it \'s done','how it \'s done','love','6',00000000,00000000,00000000,NULL,1);

/*!40000 ALTER TABLE `tweets_domain_hashtagged` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tweets_domain_neutral
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tweets_domain_neutral`;

CREATE TABLE `tweets_domain_neutral` (
  `id` bigint(20) NOT NULL,
  `preprocessed_text` varchar(1000) NOT NULL DEFAULT '',
  `refined_text` varchar(1000) DEFAULT NULL,
  `hasEmoCue` bit(1) DEFAULT NULL,
  `forTest` bit(1) DEFAULT b'0',
  `tSize` int(11) DEFAULT NULL,
  `realWordNum` int(11) DEFAULT NULL,
  `isDuplicate` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`forTest`),
  KEY `index3` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tweets_domain_neutral` WRITE;
/*!40000 ALTER TABLE `tweets_domain_neutral` DISABLE KEYS */;

INSERT INTO `tweets_domain_neutral` (`id`, `preprocessed_text`, `refined_text`, `hasEmoCue`, `forTest`, `tSize`, `realWordNum`, `isDuplicate`)
VALUES
	(9,'simple news tweet <link>','simple news tweet',00000000,00000000,NULL,3,NULL),
	(10,'another news tweet <link>','another news tweet',00000000,00000000,NULL,2,NULL),
	(11,'these posts are amazing <link>','these posts are amazing',00000001,00000000,NULL,2,NULL),
	(12,'these posts are amazing <link>','these posts are amazing',00000001,00000000,NULL,2,00000001);

/*!40000 ALTER TABLE `tweets_domain_neutral` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tweets_domain_random
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tweets_domain_random`;

CREATE TABLE `tweets_domain_random` (
  `id` bigint(20) NOT NULL,
  `text` text NOT NULL,
  `preprocessed_text` varchar(1000) DEFAULT NULL,
  `tSize` int(5) DEFAULT NULL,
  `realWordNum` int(5) DEFAULT NULL,
  `withLink` bit(1) DEFAULT NULL,
  `withEmHash` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`tSize`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tweets_domain_random` WRITE;
/*!40000 ALTER TABLE `tweets_domain_random` DISABLE KEYS */;

INSERT INTO `tweets_domain_random` (`id`, `text`, `preprocessed_text`, `tSize`, `realWordNum`, `withLink`, `withEmHash`)
VALUES
	(1,'funny text for test #amusement #awe','funny text for test #amusement #awe',NULL,NULL,NULL,00000001),
	(11,'these posts are amazing http://smth.com','these posts are amazing <link>',4,NULL,00000001,NULL),
	(7,'#love how it\'s done','#love how it \'s done',4,NULL,NULL,NULL),
	(10,'another news tweet http://cnn.com','another news tweet <link>',2,NULL,00000001,NULL),
	(8,'good luck today! you are the best','good luck today ! you are the best',2,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tweets_domain_random` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping routines (PROCEDURE) for database 'dystemo_example_data'
--
DELIMITER ;;

# Dump of PROCEDURE get_rands
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `get_rands` */;;
/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_rands`(IN cnt INT)
BEGIN
  # delta variable specifies how often the found random tweets will get imported
  DECLARE delta int unsigned default 10000;  
  DECLARE full_cnt int unsigned;
  SET full_cnt = cnt;
	
  # create a temporary table for storing the intermediately collected random tweets
  DROP TEMPORARY TABLE IF EXISTS rands_tmp;
  CREATE TEMPORARY TABLE rands_tmp ( rand_id INT );

  SELECT max(id) FROM tweets_domain_all INTO @maxId;      

loop_me: LOOP
    IF cnt < 1 THEN
      SELECT count(*) FROM tweets_domain_random INTO @curNum;
	  IF @curNum < full_cnt THEN
	     SET cnt = full_cnt - @curNum;
	  ELSE
	     LEAVE loop_me;
	  END IF;
    END IF;

	# find next random tweet id
    INSERT INTO rands_tmp
       SELECT r1.id
         FROM tweets_domain_all AS r1 JOIN
              (SELECT (RAND() * @maxId) AS id)
               AS r2
        WHERE r1.id >= r2.id
        ORDER BY r1.id ASC
        LIMIT 1;

	# decrease counters
    SET cnt = cnt - 1;
	SET delta = delta - 1;

	# if we found required number or delta tweets, put them in the resultant table
    IF delta < 1 OR cnt < 1 THEN
	  REPLACE INTO tweets_domain_random (`id`) select * from rands_tmp;
      TRUNCATE TABLE rands_tmp;
	  SET delta = 10000;
	  
	  
	  
    END IF;

  END LOOP loop_me;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
