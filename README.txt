Dystemo Framework, Version 2.0
Release prepared on 05.11.2016
------------------------------

This code base allows to train and evaluate emotion classifiers or lexicons, 
as well as to apply the existing or built emotion lexicons to text data, 
with focus on Twitter data.
 
The Dystemo framework was introduced and described in the following journal publication:

Valentina Sintsova and Pearl Pu. “Dystemo: Distant Supervision Method for Multi-Category 
Emotion Recognition in Tweets.” ACM Transactions on Intelligent Systems and Technology 
(TIST), 8(1):Article No.13, 2016

If you use this code for research purposes, we ask you to cite the above publication. 

This code distribution is provided to you on the terms of Gnu General Public license 
(GPL v3.0). You can read a copy of the GNU General Public License in file DYSTEMO_LICENSE.txt or
read it online at <http://www.gnu.org/licenses/>.

The further updates to this code can be found at http://hci.epfl.ch/research-projects/dystemo/

Note that this distribution also includes several emotion lexicons (particularly OlympLex, 
GALC-R, and PMI-Hash) to be used for testing this framework and for research purposes only. 
You can get more information on their terms of use in LEXICONS_LICENSING.txt file.

Below are the descriptions of each component project of the code. 
The example of framework usage and launching, as well as further details on how to use the 
code are aggregated into ExampleExperiments project.

-------

EmotionClassificationApplication:

This project provides definitions for emotion classification, lexicons, and models, 
as well as for some tweet-related text preprocessing. It also provides some pre-built 
emotion lexicons. 
This project should be enough for performing emotion classification, i.e. applying 
a given emotion classifier/lexicon to text.

-------

DataConnectors:

This project provides classes for loading and saving text datasets (with a focus on 
Twitter data). It also specifies the readers of text data from database and files.

-------

DataPreparation:

This project codes different data processing procedures, such as cleaning from duplicates 
or retweets, detection of emotional hashtags, application of text preprocessing, etc.

-------

EmotionClassificationLearning:

This project codes the process of learning emotion classifiers (either in supervised or 
semi-supervised manner). Feature extraction, feature selection, extraction of 
pseudo-labeled data, and training modules (called ClassifierLearner), and performance 
evaluation are provided, along with multiple parameters setup for the learning procedure.

-------

EmotionClassificationExperiments:

This project gives multiple helper methods for experiment framework, such as launching 
learning experiments with specific parameters, loading the given classifiers by name, 
performing learning (supervised or semi-supervised) with different parameters from a list,
perform evaluation on the given data and save the evaluation results, perform significance
testing, etc.

-------

ExampleExperiments: 

This project provides examples of how to work with our code framework, e.g. how to load 
and organize data, and how to run the distant supervision experiments as described in 
the paper.

-------

UtilModule:

This is a util project with multiple helper functions to simplify handling of files, 
collections, strings, text, and others.

________________________

NOTES: 

- We hope that you would enjoy using our software. This is a research code and as such it 
might contain occasional errors. Please forgive us and let us know if you experience 
any error by sending an e-mail at valentinasintsova@gmail.com.

- We use an old version of Weka library (3.7.10), which is included in this distribution for 
simplicity. Using a new version might require further update of the code.

- Multiple other open-source libraries are incorporated for simplicity. Their licenses are 
enumerated in 3RD_PARTY_LICENSING.txt file.

- We did not include the trained models for Starford CoreNLP. Please download version 3.4 
from http://stanfordnlp.github.io/CoreNLP/history.html and place into UtilModule/resources 
(as well as add them into the build path) if you plan to use the associated functionality 
(particularly, text lemmatization).




